/**   
*    
* 项目名称：MedicalTeam   
* 类名称：Browser   
* 类描述：   
* 创建人：zhangl  
* 创建时间：2016年6月15日 下午3:49:07   
* 修改人：zhangl   
* 修改时间：2016年6月15日 下午3:49:07   
* 修改备注：   
* @version    
*    
*/
package com.lst.util.test;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @ClassName: Browser
 * @Description: 定义客户端
 * @author zhangl
 * @date 2016年6月15日 下午3:49:07
 *
 */
public class Browser {
	
	public static CloseableHttpClient httpclient = HttpClients.createDefault();
	
	public static HttpPost httppost = new HttpPost();
	
	public static ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
}
