/**   
 *    
 * 项目名称：MedicalTeam   
 * 类名称：CommCode   
 * 类描述：   
 * 创建人：zhangl  
 * 创建时间：2016年6月8日 下午8:47:18   
 * 修改人：zhangl   
 * 修改时间：2016年6月8日 下午8:47:18   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.util.test;

/**
 * @ClassName: CommCode
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangl
 * @date 2016年6月8日 下午8:47:18
 *
 */
public class CommCode {
	//tomcat
	public static final String LOCALHOST_URL_TOMCAT = "http://127.0.0.1:8080/SmartKungFu/";
	
	//weblogic
	public static final String LOCALHOST_URL_WEBLOGIC = "http://127.0.0.1:7001/SmartKungFu/";
}
