/**   
 *    
 * 项目名称：MedicalTeam   
 * 类名称：DoLogin   
 * 类描述：   
 * 创建人：zhangl  
 * 创建时间：2016年6月8日 下午8:11:50   
 * 修改人：zhangl   
 * 修改时间：2016年6月8日 下午8:11:50   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.test.sys;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import com.lst.util.test.Browser;
import com.lst.util.test.CommCode;

/**
 * @ClassName: DoLogin
 * @Description:登录测试
 * @author zhangl
 * @date 2016年6月8日 下午8:11:50
 *
 */
public class DoLogin {
	@Test
	public void doLogin() throws URISyntaxException {
		// 创建默认的httpClient实例
		//HttpClient httpclient = new DefaultHttpClient();
		//创建httppost   
		//HttpPost httppost = new HttpPost(CommCode.LOCALHOST_URL + "mstuserController/doLogin"); 
		URI uri = new URI(CommCode.LOCALHOST_URL_TOMCAT + "mstUserController/doLogin");
		Browser.httppost.setURI(uri);
		// 创建参数队列    
		List<NameValuePair> formparams = new ArrayList<NameValuePair>(); 
		formparams.add(new BasicNameValuePair("account","admin"));
		formparams.add(new BasicNameValuePair("password", "admin"));  
		
		UrlEncodedFormEntity uefEntity = null;  
		
		try {  
			uefEntity = new UrlEncodedFormEntity(formparams, "UTF-8");  
			Browser.httppost.setEntity(uefEntity);  
			
			System.out.println("executing request " + Browser.httppost.getURI()); 
			
			CloseableHttpResponse response = Browser.httpclient.execute(Browser.httppost);  
			try {  
				HttpEntity entity = response.getEntity();  
				if (entity != null) {  
					System.out.println("--------------------------------------");  
					System.out.println("Response content: " + EntityUtils.toString(entity, "UTF-8"));  
					System.out.println("--------------------------------------");  
				}  
			} finally {  
				response.close();  
			}  
		} catch (ClientProtocolException e) {  
			e.printStackTrace();  
		} catch (UnsupportedEncodingException e1) {  
			e1.printStackTrace();  
		} catch (IOException e) {  
			e.printStackTrace();  
		} finally {  
			// 关闭连接,释放资源    
			/*try {  
				Browser.httpclient.close();  
			} catch (IOException e) {  
				e.printStackTrace();  
			}  */
		}  
	}

}
