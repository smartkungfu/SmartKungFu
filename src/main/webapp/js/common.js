/**
 * @Title: common.js
 * @Package js
 * @Description: TODO(用一句话描述该文件做什么)
 * @author wangxin
 * @date 2015年3月10日 上午9:26:17
 * @version V1.0
 */
var capMap = {
	"网红节日双十一，在11月份的哪天":"11",
	"电视剧《天龙八部》段誉共几个妹妹？":"0",
	"至2016年LOL总决赛SKT共获取几次冠军？":"3",
	"场馆《达摩堂》有几套课程在“功夫平台”上架？":"3",
	"“有点功夫平台”功夫等级修炼满级是多少级？":"10",
	"功夫课程至尊MVP优惠价是几折？":"1",
	/*"距俄罗斯举办世界杯是哪一年？": "2018",
    "电视剧《爱情公寓》全剧共几季？": "4",
    "2016是LOL拳头公司多少周年生日？": "10",
    "2016是苹果公司发布了第几款iPhone？": "7",*/
   };
/*$.ajaxSetup({
	complete : function(XMLHttpRequest, textStatus) {
		var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
		if (sessionstatus != null && sessionstatus.indexOf("timeout") != -1) {
			var top = getTopWinow();
			alert("登录超时, 请重新登录。");

			var result = sessionstatus.split(",");
			top.location.href = window.location.protocol + "//" + window.location.host + result[1];
		}
	}
});*/

/**
 * 在页面中任何嵌套层次的窗口中获取顶层窗口
 * 
 * @return 当前页面的顶层窗口对象
 */
function getTopWinow() {
	var p = window;
	while (p != p.parent) {
		p = p.parent;
	}
	return p;
}

/**
 * 创建新选项卡
 * 
 * @param tabId
 *            选项卡id
 * @param title
 *            选项卡标题
 * @param url
 *            选项卡远程调用路径
 */
function addTab(tabId, title, url) {
	if ($("#" + tabId).html() == null) {
		var name = "iframe_" + tabId;
		$("#centerTab").tabs("add", {
			title : title,
			closable : true,
			cache : false,
			content : '<iframe name="' + name + '"id="' + tabId + '"src="' + url + '" width="100%" height="98%" frameborder="0" scrolling="auto"></iframe>'
		});
	} else {
		$("#centerTab").tabs("select", title);
	}
}

/**
 * json格式日期转换
 * 
 * @param time
 *            json格式日期
 */
function timeStamp2String(time) {
	var datetime = new Date();
	datetime.setTime(time);
	var year = datetime.getFullYear();
	var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
	var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
	var hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime.getHours();
	var minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
	var second = datetime.getSeconds() < 10 ? "0" + datetime.getSeconds() : datetime.getSeconds();
	return year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
}

function time2String(time) {
	var datetime = new Date();
	datetime.setTime(time);
	var year = datetime.getFullYear();
	var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
	var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
	return year + "-" + month + "-" + date;
}

QueryString = {
		data : {},
		Initial : function() {
			var aPairs, aTmp;
			var queryString = new String(window.location.search);
			queryString = queryString.substr(1, queryString.length); // remove "?"
			aPairs = queryString.split("&");
			for (var i = 0; i < aPairs.length; i++) {
				aTmp = aPairs[i].split("=");
				this.data[aTmp[0]] = aTmp[1];
			}
		},
		GetValue : function(key) {
			return this.data[key];
		}
	};

function dateStamp2String(time) {
	if (time != null) {
		var datetime = new Date();
		datetime.setTime(time);
		var year = datetime.getFullYear();
		var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
		var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
		return year + "-" + month + "-" + date;
	}
}
//空字符串显示样式
function dealStr(str){
	if(str == null || str == ""){
		return "<hr style='width:30px'/>";
	}else{
		return str;
	}
}

//删除功能模板
function delListAsUtils(paras,url,ele){
	var ids = paras.ids;
	if(ids != null && ids != ''){
		bootbox.confirm({
	  		size: "small",
	  	    title: "提示",
	  	    message: "确认要删除选择信息吗？",
	  	    buttons: {
	  	        cancel: {
	  	            label: '<i class="fa fa-times"></i> 取消'
	  	        },
	  	        confirm: {
	  	            label: '<i class="fa fa-check"></i> 确认'
	  	        }
	  	    },
	  	    callback: function (result) {
	  	        //console.log('This was logged in the callback: ' + result);
	  	        if(result){
	  	        	 $.ajax({
		  	               type: "post",
		  	               data: paras,
		  	               url: url,
		  	               success: function (data) {
		  	            		ele.bootstrapTable('refresh'); 
		  	               }
		  	           	 });
	  	        }
	  	    }
	  	});
	} else {
		bootbox.alert({ 
			  size: "small",
			  title: "提示",
			  message: "请选择删除行！", 
			  callback: function(){ 
				  return ;
			  }
		});
	}
}
//查看内容详情
function OpenInfo(){ };
OpenInfo.prototype.content = "";
OpenInfo.prototype.title = "";
OpenInfo.prototype.showContent = function(){
		bootbox.alert({ 
			  size: "large",
			  title: "《"+this.title + "》",
			  message:this.content, 
			  callback: function(){ /* your callback code */ }
			});
}
