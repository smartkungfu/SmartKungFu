<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html lang="CN">
	<head>
		<title>有点功夫-404</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	  	
	  	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/404/css/404.css" />
		<!--[if IE 6]>
		<script src="js/png.js"></script>
		<script>DD_belatedPNG.fix('*')</script>
		<![endif]-->
    </head>
  <body>
    <div id="wrap">
	<div>
		<img src="${pageContext.request.contextPath}/404/img/404.png" alt="404" />
	</div>
	<div id="text">
		<strong>
			<span></span>
			<%-- <a href="${pageContext.request.contextPath}">返回登录页</a> --%>
			<a href="javascript:history.back()">返回上一页</a>
		</strong>
	</div>
</div>

<div class="animate below"></div>
<div class="animate above"></div>
  </body>
</html>
