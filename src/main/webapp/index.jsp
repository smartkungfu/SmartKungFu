<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-首页</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="pages/fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="pages/fragment/frag_top_js.jsp"/>

   </head>
<body>
	<div class="site-holder">
		<!-- nav -->
		<jsp:include page="pages/fragment/frag_nav.jsp" />
		<!-- .box-holder -->
		<div class="box-holder">
			<!--left-sidebar  -->
			<jsp:include page="pages/fragment/frag_left.jsp" />
			<!-- .content start -->
			  <div class="content">
				<div class="row">
					<div class="col-mod-12">
						<ul class="breadcrumb">
							<li><span>首页</span></li>
						</ul>
						<h3 class="page-header"> 欢迎来到有点功夫后台管理平台... <i class="animated bounceInDown show-info"></i> </h3>
					</div>
				</div>
			</div>
			<!-- .content end-->
		</div>
	</div>
	<!-- /.right-sidebar -->
		
   <jsp:include page="pages/fragment/frag_bottom_js.jsp"/>
</body>
</html>