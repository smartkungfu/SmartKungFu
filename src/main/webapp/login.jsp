<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="cn">
<head>
<meta charset="utf-8">
<title>有点功夫-登录</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="renderer" content="webkit">

<jsp:include page="pages/fragment/frag_top_css.jsp"/>

<!-- Loading login Stylesheets -->
<link href="${pageContext.request.contextPath}/css/login.css" rel="stylesheet">
	  
<jsp:include page="pages/fragment/frag_top_js.jsp"/>

<script type="text/javascript">
	function randomNumber(min, max) {
	    return Math.floor(Math.random() * (max - min + 1) + min);
	};
	var opType = [1,2];
	function decCalc(op){
		var $captcha = $("#captchaOperation");
		
		switch (op) {
		
			case 1:
					$captcha.html([randomNumber(1, 50), '+', randomNumber(1, 50), '=','？'].join(' '));
					break;
		  	case 2:
		  			var tempArr = new Array();
		  			$.each(capMap,function(key,values){
		  				tempArr.push(key);
					});
		  			var index = randomNumber(0, tempArr.length);
		  			$captcha.html(tempArr[index * 1]);
		  			break;
			default:
					break;
		}
	}
	$(function() {
		decCalc(1);
		
		$("#changecap").click(function(){
			decCalc(1);
		});
		
		$('form').bootstrapValidator(
						{
							message : '数据错误',
							feedbackIcons : {
								 valid: 'fa fa-check',
				            	 invalid: 'fa fa-times',
				            	 validating: 'fa fa-refresh'
							},
							fields : {
								account : {
									message : '用户名验证失败',
									validators : {
										notEmpty : {
											message : '用户名不能为空！'
										}
									}
								},
								password : {
									validators : {
										notEmpty : {
											message : '密码不能为空！'
										}
									}
								},
								captcha : {
									validators : {
										callback : {
											message : '答案错误！(▰˘︹˘▰)',
											callback : function(value,
													validator) {
												// Determine the numbers which are generated in captchaOperation
												var items = $('#captchaOperation').html().split(' ');
												if(items.length > 1){
													var sum = parseInt(items[0]) + parseInt(items[2]);
													return value == sum;
												} else {
													var flag = false;
													$.each(capMap,function(key,values){
													 //   console.log(key + "---" + values);
													    if(key == $('#captchaOperation').html()){
													    	if(value == values) flag = true;
													    }
													});
													
													return  flag;
												}
												
											}
										}
									}
								}
							},
							submitHandler : function(validator, form,submitButton) {
									$.ajax({
											url : "${pageContext.request.contextPath}/mstUserController/doLogin",
											data : form.serialize(),
											type : "POST",
											async : true,
											dataType : "json",
											beforeSend : function() {
												//请求前的处理
											},
											success : function(data) {
												if (!data) {
													bootbox.alert({
														size : "small",
														title : "错误",
														message : "账号或密码不对！",
														callback : function() { /* your callback code */
														}
													});
													// Enable the submit buttons
													$(form).bootstrapValidator('disableSubmitButtons',false);
													return;
												} else {
													validator.defaultSubmit();
												}
											},
											complete : function() {
											},
											error : function(data) {
												bootbox.alert({
													size : "small",
													title : "错误",
													message : "请求超时，请重试！",
													callback : function() { /* your callback code */
													}
												});
												$(form).bootstrapValidator('disableSubmitButtons',false);
												return;
											}
										});
							}
						});
	});
</script>
</head>
<body>
	<section id="login">
		<div class="row animated fadeILeftBig">
			<div class="login-holder col-md-6 col-md-offset-3">
				<h2 class="page-header text-center text-primary"><img src="${pageContext.request.contextPath}/images/logo.png"/></h2>
				<form role="form" action="${pageContext.request.contextPath}/mstUserController/index" method="post">
					<div class="form-group">
						<input type="text" class="form-control" name="account"
							placeholder="请输入账号">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" name="password" 
							placeholder="请输入密码">
					</div>
					<label  id="captchaOperation"></label><a href="javascript:void(0)"  id="changecap" style="color: #54b5df;line-height: 20px;float: right">算不清，换一换</a>
   					 <div class="form-group">
	            		<input type="number" class="form-control" name="captcha" placeholder="请输入算法结果"/>
   					 </div>
					<div class="form-footer">
						<button type="submit" class="btn btn-info pull-right btn-submit">登录</button>
					</div>
					<div style='float:right'>
		<!-- 			<label id="growth" style="display:block">
						<image  style='vertical-align:middle;' src='http://192.168.1.143:8080//UploadImages/KUNGFU/IMG/APP/growth.png'
							
						 onclick="$(function(){$('#growth').css('display','none');$('#growthed').css('display','block');alert(0)})">
						 <span style='vertical-align:middle;'>功力值</span></label>
				   <label id="growthed" style="display:none">
						 <image style='vertical-align:middle;' src='http://192.168.1.143:8080//UploadImages/KUNGFU/IMG/APP/growthed.png'
							
						 onclick='document.getElementById("growth").style.display="block";document.getElementById("growthed").style.display="none";'>
						<span style='vertical-align:middle;'>功力值</span></label> -->
						
				</form>
			</div>
		</div>
	</section>
<!--  <br/><br/><br/><div style='float:right'>
 <label id='growth'>
 <image  style='vertical-align:middle;' src='http://192.168.1.143:8080//UploadImages/KUNGFU/IMG/APP/growth.png' onclick='document.getElementById("growth").style.display="none";document.getElementById("growthed").style.display="inline-block";window.location.href="http://192.168.1.143:8080/SmartKungFuService/ClientGrowthAddServlet?userid=1&growthtype=9&growthvalue=1&growthid=1"'>
 <span style='vertical-align:middle;'>功力值</span></label>&nbsp;&nbsp;&nbsp;<label id='growthed' style='display:none'>
 <image style='vertical-align:middle;' src='http://192.168.1.143:8080//UploadImages/KUNGFU/IMG/APP/growthed.png' onclick='javascript:void(0)'><span style='vertical-align:middle;'>功力值</span></label>&nbsp;&nbsp;&nbsp;
 
 <label id='share'><image style='vertical-align:middle;' src='http://192.168.1.143:8080//UploadImages/KUNGFU/IMG/APP/share.png' onclick='window.location.href="http://192.168.1.143:8080/SmartKungFuService/ClientGrowthAddServlet?userid=1&growthtype=9&growthvalue=1&growthid=1"'><span style='vertical-align:middle;'>12</span></label>&nbsp;&nbsp;&nbsp;
 <label id='favorite'>
	 <image style='vertical-align:middle;' src='http://192.168.1.143:8080//UploadImages/KUNGFU/IMG/APP/favarite.png' onclick='document.getElementById("favorite").style.display="none";document.getElementById("favorited").style.display="inline-block";window.location.href="http://192.168.1.143:8080/SmartKungFuService/ClientGrowthAddServlet?userid=1&growthtype=9&growthvalue=1&growthid=1"'>
	 <span style='vertical-align:middle;'>14</span></label>&nbsp;&nbsp;&nbsp;
 <label id='favorited' style='display:none'><image style='vertical-align:middle;' src='http://192.168.1.143:8080//UploadImages/KUNGFU/IMG/APP/favarited.png' onclick='document.getElementById("favorited").style.display="none";document.getElementById("favorite").style.display="inline-block";window.location.href="http://192.168.1.143:8080/SmartKungFuService/ClientGrowthAddServlet?userid=1&growthtype=9&growthvalue=1&growthid=1"'><span style='vertical-align:middle;'>14</span></label>&nbsp;&nbsp;&nbsp;
 </div><br/><br/><br/><br/><br/><div style='margin:0 auto;line-height:30px;text-align: center;color:#cecece'><span>--投稿或商业合作--</span><br/><span>请发邮件至ARTICLE@SMARTKUNGFU.COM</span></div> -->   
	<jsp:include page="pages/fragment/frag_bottom_js.jsp"/>
	<script src="${pageContext.request.contextPath}/js/scroll.js"></script>
	<script src="${pageContext.request.contextPath}/js/jquery.panelSnap.js"></script>
	<script src="${pageContext.request.contextPath}/js/login.js"></script>
</body>
</html>