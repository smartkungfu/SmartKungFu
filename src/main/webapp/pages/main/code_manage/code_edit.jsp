<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-系统管理-编辑编码</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var parentId = QueryString.GetValue("parentId");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var codeName = QueryString.GetValue("codeName");
		  		var type = QueryString.GetValue("type");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		$("input[name='parentId']").val(parentId);
		  		$("input[name='codeName']").val(codeName);
		  		$("input[name='type']").val(type);
		  		
		  		var parentName =  QueryString.GetValue("parentName");
		  		
		  		$("#parentName").html(decodeURIComponent(parentName,true) + "：<sup>*</sup>");
		  		
		  		$(".parentName").html(decodeURIComponent(parentName,true));
		  		
		  		$("#btn_cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/mstCodeController/list" + reqPara; 
		  		});
			  	  $('form').bootstrapValidator({
			            // Exclude only disabled fields
			            // The invisible fields set by Bootstrap Multiselect must be validated
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            fields: {
			            	enabled: {
			                    validators: {
			                        notEmpty: {
			                            message: '请选择是否可用'
			                        }
			                    }
			                },
			                name: {
			                    validators: {
			                        notEmpty: {
			                            message: '编码名称为空'
			                        },
			                        stringLength: {
			                            min: 0,
			                            max: 10,
			                            message: '编码名称长度为0-10'
			                        },
			                    }
			                },
			                code: {
			                    validators: {
			                    	numeric: {message: '只能输入数字'}
			                    }
			                }
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	validator.defaultSubmit();
			            }
			        });
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >编码管理</span></li>
		                   <li><a href="#" >编码管理</a></li>
		                   <li><a class="active">编辑<span class="parentName">编辑编码</span> </a></li>
		                 </ul>
		                 <h3 class="page-header"> <span class="parentName">编辑编码</span> <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	<span class="parentName">编码</span>>资料信息编辑
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/mstCodeController/save">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												<input type="hidden" name="parentId" value="" /> 
												<input type="hidden" name="type" value="" /> 
												<input type="hidden" name="codeName" value="" />
											</div>
											
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${mstCode.id }" />
											<div class="form-group">
												<label id="parentName" class="col-lg-3 control-label"></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="name"  value="${mstCode.name }" placeholder="请输入编码名称" />
												</div>
											</div>
											
											<c:if test="${mstCode.parentid == 287 }">
												<div class="form-group">
													<label class="col-lg-3 control-label">赠送功力值</label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="code"  value="${mstCode.code }" placeholder="请输入赠送功力值" />
													</div>
												</div>
											</c:if>

											<div class="form-group">
												<label class="col-lg-3 control-label">是否可用： <sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<c:if test="${mstCode.enabled}">
															<label>
																 <input type="radio" name="enabled" value="true" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="enabled" value="false" /> 否
															</label>
														</c:if>
														<c:if test="${!mstCode.enabled}">
															<label>
																 <input type="radio" name="enabled" value="true" /> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="enabled" value="false" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div>

											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>