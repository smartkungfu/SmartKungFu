<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-课程动作管理</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
	  	var paras = "";
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             					cache: false, // 不缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				                pagination: true, // 开启分页功能
				               // singleSelect: false,
				                pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500],
				                search: false, // 关闭搜索功能
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                //smartDisplay: true, // 智能显示 pagination 和 cardview 等
				                queryParams: function (params) {
									var page = params.offset;
				                	
				                	if((page * 1) != 0 ){
				            			page = page/params.limit + 1;
				            		}
				                	
				                  	//排序，并处理多表中的属性vitypeName
				                	var sort = params.sort;
				                	if(sort == "cUName"){
				                		sort == "u." + sort;
				                	} else if(sort == "actionTitle"){
				                		sort == "c2." + sort;
				                	} 
				                	var obj = {
					                        rows: params.limit,
					                        page: page,
					                        sort:sort,
							                order:params.order, 
					                        actionTitle:$("#titleFol").val(),
					                    };
				                	paras =  "rows=" + obj.rows + "&page=" + obj.page ;
				                	return obj;
				                },
				                url: "${pageContext.request.contextPath}/actionVideoController/queryList",
				                detailView:true,
				                detailFormatter:function detailFormatter(index, row) {
				                	 var html = [];
				                	 html.push('<p><b>视屏名称:</b> ' + row.videoName + '</p>');
				                     html.push('<p><b>图片地址:</b> ' + row.coverUrl + '</p>'); 
				                     return html.join('') ;
				                },
				                columns: [{
				                    field: 'state',
				                    checkbox: true
				                },{
				                    field: 'id',
				                    title: '序号',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                },{
				                    field: 'actionTitle',
				                    title: '动作标题',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	   
				                    	return "<a style='color:#428bca' href='javascript:void(0)' onclick='openVideo(\""+row.actionTitle+"\",\"http://skf.smartkungfu.com/"+row.coverUrl2+"\")'>"+value+"</a>";
				                    }
				 
				                },{
				                    field: 'videoName',
				                    title: '视频名称',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                },
				                {
				                    field: 'totalTime',
				                    title: '动作总时间(s)',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                },
				                {
				                    field: 'videoType',
				                    title: '动作播放类型',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                },
				                {
				                    field: 'groupNumber',
				                    title: '动作包含组数',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                },
				                {
				                	field: 'coverUrl',
				                    title: 'gif图',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){ 
				                    	if(value != null){
				                    		return "<img height='35px' width='50px' src='"+value.split(";")[0]+"' alt='' class='img'/> ";
				                    	} else {
				                    		return "<img height='35px' width='50px' src='' alt='' class='img'/> ";
				                    	}
				                    }
				                },],
				                onLoadSuccess:function(){
				                },
				                onLoadError: function () {
				             	   //    mif.showErrorMessageBox("数据加载失败！");
				                }
            });
        }
	  	
	  	function addObj(url){
	  		paras = paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
			paras = decodeURIComponent(paras,true);
			window.location.href = url + "?"+ paras;
	  	}
	  	
	  	function delList(url){
	  		var rows = $table.bootstrapTable('getSelections');
			var ids = "";
			$(rows).each(function(index,element){
				ids += element.id + ",";
			});
			ids = ids.substring(0,ids.length - 1);
			delListAsUtils({ids:ids},url,$table);
	  	}
	  	
	  	function editObj(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择编辑行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条编辑数据", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "id=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	  	function queryMusic(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条数据", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "actionId=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
   		$(function(){
   			
   			initTable();
   			
   			$("#btn_search").click(function(){
   				$table.bootstrapTable('refresh');
   			});
   		 
   		});
   		function openVideo(title,url){
   			bootbox.alert({ 
   			  size: "large",
   			  title: "《"+title + "》",
   			  message: "<video src='"+url+"' width='100%' controls='controls'></video>", 
   			  callback: function(){ /* your callback code */ }
   			});
   		}
	    </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     		
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>课程动作管理</span></li>
                   <li><a href="javascript:void(0)" class="active">课程动作管理</a></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
                  		<c:forEach var="g" items="${btns }">
                  			 <button type="button" class="btn btn-primary" onclick="${g.action }">
						 		<i class="${g.btnicon }"></i> ${g.btnname }
							 </button>
                  		</c:forEach>
 					 </div>
                </div>

                <h3 class="page-header">课程动作管理<i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

                <blockquote class="page-information hidden">
                 <p>
                	  更新、维护课程动作信息！
                </p>
              </blockquote>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
					<form class="form-inline" role="form" style="margin-top: 10px;margin-bottom: 10px;">
						  <input type="hidden" name="menuId" value="${folPara.menuId }">
						  <!--  
						  <div class="form-group">
						    <label>动作类型：</label>
						    <select class="multiselect" name="typeFol"></select>
						  </div>
						  -->
						  <div class="form-group">
						    <label>动作标题：</label>
						    <input type="text" class="form-control" id="titleFol" name="titleFol"  value="${folPara.titleFol }">
						  </div>
						  <button id="btn_search" type="button" class="btn btn-primary" >
						 	<i class="fa fa-search fa-lg"></i> 搜索
						  </button>
					</form>
				    <table id="table">
				    </table>
		    </div>
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>