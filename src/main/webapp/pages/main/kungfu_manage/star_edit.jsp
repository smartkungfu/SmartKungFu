<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-编辑达人</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style type="text/css">
			.modal {
				margin-top: -500px;
			}
		</style>
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		<!--include fileinput.css  -->
	  	<link href="${pageContext.request.contextPath}/css/fileinput.min.css" rel="stylesheet">
		<!-- 样式冲突 -->
	    <!-- <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
		<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
	  	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<!--include wysiwyg.js  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-wysiwyg.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.hotkeys.min.js"></script>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var minDate = QueryString.GetValue("minDate");
		  		var maxDate = QueryString.GetValue("maxDate");
		  		var authorFol = QueryString.GetValue("authorFol");
		  		var titleFol = QueryString.GetValue("titleFol");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		$("input[name='minDate']").val(minDate);
		  		$("input[name='maxDate']").val(maxDate);
		  		$("input[name='authorFol']").val(authorFol);
		  		$("input[name='titleFol']").val(titleFol);
		  		
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/starInfoController/list" + reqPara; 
		  		});
		  
			  	$('form').bootstrapValidator({
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            fields: {
			                istop: {
			                    validators: {
			                        notEmpty: {
			                            message: '请选择是否精选'
			                        }
			                    }
			                },
			                title: {
			                    validators: {
			                        notEmpty: {
			                            message: '达人标题为空'
			                        },
			                        stringLength: {
			                            min: 3,
			                            max: 30,
			                            message: '长度在为3-30位'
			                        },
			                    }
			                },
			                aothor: {
			                    validators: {
			                        notEmpty: {
			                            message: '作者为空'
			                        },
			                    }
			                }, 
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	var $editor = $("#editor");
			            	var $content = $("#content");
			            	var content = $editor.html();
			            	$content.val(content);
			            	
			            	validator.defaultSubmit();
			            }
			        });
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >有点功夫管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel" >达人管理</a></li>
		                   <li><a href="javascript:void(0)" class="active">编辑达人</a></li>
		                 </ul>
		                 <h3 class="page-header"> 编辑达人 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	达人信息编辑！
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/starInfoController/save">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												
												<input type="hidden" name="minDate" value="" /> 
												<input type="hidden" name="maxDate" value="" /> 
												<input type="hidden" name="authorFol" value="" />
											    <input type="hidden" name="titleFol" value="" /> 
											</div>
											
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${starInfo.id }" />
											<textarea name="content" id="content" rows="" cols="" hidden="hidden"></textarea>
											<div class="form-group">
												<label class="col-lg-3 control-label">图片： <sup>*</sup></label>
												<div class="col-lg-9">
													<input type="file" class="form-control file-loading" name="myfiles" multiple />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">标题： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="title"  value="${starInfo.title }" placeholder="请输入标题" />
												</div>
											</div>

											<div class="form-group">
												<label class="col-lg-3 control-label"> 作者： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="author" value="${starInfo.author }" placeholder="请输入作者" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">是否精选： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<c:if test="${starInfo.istop}">
															<label>
																 <input type="radio" name="istop" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="istop" value="0" /> 否
															</label>
														</c:if>
														<c:if test="${!starInfo.istop}">
															<label>
																 <input type="radio" name="istop" value="1" /> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="istop" value="0" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">内容：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-9">
													<div id="alerts"></div>
													<div class="btn-toolbar" data-role="editor-toolbar"
														data-target="#editor">
														<div class="btn-group">
															<a class="btn dropdown-toggle" data-toggle="dropdown"
																title="Font"><i class="icon-font"></i><b
																class="caret"></b></a>
															<ul class="dropdown-menu">
															</ul>
														</div>
														<div class="btn-group">
															<a class="btn dropdown-toggle" data-toggle="dropdown"
																title="Font Size"><i class="icon-text-height"></i>&nbsp;<b
																class="caret"></b></a>
															<ul class="dropdown-menu">
																<li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
																<li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
																<li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
															</ul>
														</div>
														<div class="btn-group">
															<a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i
																class="icon-bold"></i></a> <a class="btn" data-edit="italic"
																title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
															<a class="btn" data-edit="strikethrough"
																title="Strikethrough"><i class="icon-strikethrough"></i></a>
															<a class="btn" data-edit="underline"
																title="Underline (Ctrl/Cmd+U)"><i
																class="icon-underline"></i></a>
														</div>
														<div class="btn-group">
															<a class="btn" data-edit="insertunorderedlist"
																title="Bullet list"><i class="icon-list-ul"></i></a> <a
																class="btn" data-edit="insertorderedlist"
																title="Number list"><i class="icon-list-ol"></i></a> <a
																class="btn" data-edit="outdent"
																title="Reduce indent (Shift+Tab)"><i
																class="icon-indent-left"></i></a> <a class="btn"
																data-edit="indent" title="Indent (Tab)"><i
																class="icon-indent-right"></i></a>
														</div>
														<div class="btn-group">
															<a class="btn" data-edit="justifyleft"
																title="Align Left (Ctrl/Cmd+L)"><i
																class="icon-align-left"></i></a> <a class="btn"
																data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i
																class="icon-align-center"></i></a> <a class="btn"
																data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i
																class="icon-align-right"></i></a> <a class="btn"
																data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i
																class="icon-align-justify"></i></a>
														</div>
														<div class="btn-group">
															<a class="btn dropdown-toggle" data-toggle="dropdown"
																title="Hyperlink"><i class="icon-link"></i></a>
															<div class="dropdown-menu input-append">
																<input class="span2" placeholder="URL" type="text"
																	data-edit="createLink" />
																<button class="btn" type="button">Add</button>
															</div>
															<a class="btn" data-edit="unlink" title="Remove Hyperlink"><i
																class="icon-cut"></i></a>
	
														</div>
	
														<div class="btn-group">
															<a class="btn"
																title="Insert picture (or just drag & drop)"
																id="pictureBtn"><i class="icon-picture"></i></a> <input
																type="file" data-role="magic-overlay"
																data-target="#pictureBtn" data-edit="insertImage" />
														</div>
														<div class="btn-group">
															<a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i
																class="icon-undo"></i></a> <a class="btn" data-edit="redo"
																title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
														</div>
														<input type="text" data-edit="inserttext" id="voiceBtn"
															x-webkit-speech="">
													</div>
	
													<div id="editor">
														<c:if test="${starInfo.content eq null || starInfo.content eq '' }">请输入内容</c:if>
														<c:if test="${starInfo.content ne null && starInfo.content ne '' }">${starInfo.content }</c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
			<!--include fileinput.js  -->
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput.js"></script>
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
</body>
<script>
  $(function(){
	 var imsg = [];
	 var res = "${starInfo.url}".split(";");
	 for(var i = 0; i < res.length; i ++){
		 if(res[i] != null && res[i] != ""){
			 imsg.push("<img src='"+res[i]+"' class='file-preview-image' style='width:180px;height:120px;' />");
		 }
	 }
	 $("input[name='myfiles']").fileinput({
	        language: 'zh', //设置语言
	      	uploadUrl: "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
	        uploadAsync: true, //异步加载
	      	allowedFileExtensions : ['jpg', 'png','gif'],//接收的文件后缀
	        showUpload: true, //是否显示上传按钮
	        showCaption: false,//是否显示标题
	        overwriteInitial: true,
	        maxFileSize: 1000,
	        maxFilesNum: 10,
	        slugCallback: function(filename) {
	            return filename.replace('(', '_').replace(']', '_');
	        },
	      
	        showPreview:true,
	        initialPreview: imsg,
	        allowedPreviewTypes : [ 'image' ],
	        browseClass: "btn btn-primary", //按钮样式             
	 });
	 $("input[name='myfiles']").on("fileuploaded", function (event, data, previewId, index) {  
		// JSON.stringify(data),分析data数据格式
		// alert(data.response.resName + "==" + previewId + "==" + index)
		var $input = " <input type='hidden' name='imagename' value='"+data.response.resName+"'/>";
		var $form = $("form");
		$form.append($input);
     });  
	$("input[name='myfiles']").on('filedeleted', function(event, key) {
		 	//alert(key)
		    console.log('Key = ' + key);
	});
	$("input[name='myfiles'").on('filepredelete', function(event, key) {
		 	//alert(key)
		    console.log('Key = ' + key);
	});
	$("input[name='myfiles'").on('fileclear', function(event) {
			//alert("fileclear")
		    console.log("fileclear");
	});
    function initToolbarBootstrapBindings() {
      var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier', 
            'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
            'Times New Roman', 'Verdana'],
            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
      $.each(fonts, function (idx, fontName) {
          fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
      });
      $('a[title]').tooltip({container:'body'});
    	$('.dropdown-menu input').click(function() {return false;})
		    .change(function () {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
        .keydown('esc', function () {this.value='';$(this).change();});

      $('[data-role=magic-overlay]').each(function () { 
        var overlay = $(this), target = $(overlay.data('target')); 
        overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
      });
      if ("onwebkitspeechchange"  in document.createElement("input")) {
        var editorOffset = $('#editor').offset();
        $('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
      } else {
        $('#voiceBtn').hide();
      }
	};
	function showErrorAlert (reason, detail) {
		var msg='';
		if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
		else {
			console.log("error uploading file", reason, detail);
		}
		$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
		 '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
	};
    initToolbarBootstrapBindings();  
	$('#editor').wysiwyg({ fileUploadError: showErrorAlert} );
    window.prettyPrint && prettyPrint();
  });
</script>
<div id="fb-root"></div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-37452180-6', 'github.io');
  ga('send', 'pageview');
</script>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="http://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</html>