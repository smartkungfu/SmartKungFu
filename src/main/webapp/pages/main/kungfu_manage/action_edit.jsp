<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-编辑课程动作</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		<!-- 样式冲突 -->
	  	<!--include fileinput.css  -->
	  	<link href="${pageContext.request.contextPath}/css/fileinput.min.css" rel="stylesheet">
	  	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  		
	  	<!--include fileinput.js  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput.min.js"></script>
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/actionVideoController/Main" + reqPara; 
		  		});
		  		
			  	$('form').bootstrapValidator({
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            fields: {
				            cover: {
			            		  validators: {
			                          callback: {
				                            message: '请上传文件',
				                            callback: function(value, validator) {
				                                
				                                var $videoname = $("input[name='videoname']");
				                                var imgs = $(".defined1");
				                                return ($videoname.val() != undefined || imgs.length != 0);
				                            }
				                     }
			                      }
				            },
			                actionTitle: {
			                    validators: {
			                        notEmpty: {
			                            message: '动作名字为空'
			                        },
			                    }
			                },
			                videoName: {
			                    validators: {
			                        notEmpty: {
			                            message: '视屏名称为空'
			                        },
			                    }
			                }, 
			                totalTime: {
			                    validators: {
			                        notEmpty: {
			                            message: '动作总时间为空'
			                        },
			                    }
			                },
			                videoType: {
			                    validators: {
			                        notEmpty: {
			                            message: '播放类型为空'
			                        },
			                    }
			                },
			                groupNumber: {
			                    validators: {
			                        notEmpty: {
			                            message: '动作播放组数为空'
			                        },
			                    }
			                },
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	
			            	validator.defaultSubmit();
			            }
			        });
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >有点功夫管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel">动作管理</a></li>
		                   <li><a href="javascript:void(0)" class="active">编辑动作</a></li>
		                 </ul>
		                 <h3 class="page-header"> 编辑动作 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	课程动作信息编辑！
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/actionVideoController/save">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
											</div>
											
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${actionVideo.id }" />
											<div class="form-group">
												<label class="col-lg-3 control-label">动作视屏： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="file"  class="form-control file-loading"  name="cover"/>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 动作名字： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="actionTitle"  value="${actionVideo.actionTitle }" placeholder="请输入动作名称" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 视屏名字： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="videoName" value="${actionVideo.videoName }" placeholder="请输入视屏名字,如：A1.mp4" />
												</div>
											</div>
								
											<div class="form-group">
												<label class="col-lg-3 control-label">播放类型： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<c:if test="${actionVideo.videoType==1}">
															<label>
																 <input type="radio" name="videoType" value="1" />按组播放
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="videoType" value="2" checked="checked"/> 按时间播放
															</label>
														</c:if>
														<c:if test="${actionVideo.videoType==2}">
															<label>
																 <input type="radio" name="videoType" value="1" /> 按组播放
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="videoType" value="2" checked="checked" /> 按时间播放
															</label>
														</c:if>
														<c:if test="${actionVideo.videoType==null}">
															<label>
																 <input type="radio" name="videoType" value="1" checked="checked"/>按组播放
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="videoType" value="2" /> 按时间播放
															</label>
														</c:if>
													</div>
												</div>
											</div>
										    <div class="form-group">
												<label class="col-lg-3 control-label">动作总时间：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
												<input type="text" class="form-control" name="totalTime" value="${actionVideo.totalTime}" placeholder="请输入视频播放总时间，单位秒" >
												</div>
											</div>
										    <div class="form-group">
												<label class="col-lg-3 control-label">按组播放组数或者按时间播放时间：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
												<input type="text" class="form-control" name="groupNumber" value="${actionVideo.groupNumber}" placeholder="请输入动作包含组数" >
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">动作要点：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
												<textarea class="form-control" rows="7" name="actionPoints" placeholder="请输入动作要点,系统自动以•符号标识条目">${actionVideo.actionPoints}</textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
<script>
$(function(){
	 var cover = [];
	 var covers = "${actionVideo.coverUrl2}".split(";");
	 for(var i = 0; i < covers.length; i ++){
		 if(covers[i] != null && covers[i] != ""){
			 cover.push("<video src='"+covers[i]+"' class='defined1' width='100%' controls></video>");
		 }
		 
	 }
	 $("input[name='cover']").fileinput({
		  language: 'zh', //设置语言
	      	uploadUrl: "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
	        uploadAsync: true, //异步加载
	      	allowedFileExtensions : ['mp4', 'mov'],//接收的文件后缀
	        showUpload: false, //是否显示上传按钮
	        showCaption: false,//是否显示标题
	        overwriteInitial: true,
	        maxFileSize: 1024 * 1024,
	        maxFileCount: 1,
	        slugCallback: function(filename) {
	            return filename.replace('(', '_').replace(']', '_');
	        },
	      
	        showPreview:true,
	        initialPreview: cover,
	        allowedPreviewTypes : [ 'video' ],
	        browseClass: "btn btn-primary", //按钮样式 
	 });
	            
	 $("input[name='cover']").on("fileuploaded", function (event, data, previewId, index) {  
		var $input = " <input type='hidden' id='"+previewId+"' name='videoname' value='"+data.response.resName+"'/>";
		var $form = $("form");
		$form.append($input);
		
		$('form')
        .bootstrapValidator('updateStatus', 'cover', 'NOT_VALIDATED')
        .bootstrapValidator('validateField', 'cover');
     });  
	 
	 $("input[name='cover']").on("fileclear", function(event) {

		 var $videonames = $("input[name='videoname']");
				   
		 for(var i = 0 ; i < $videonames.length ; i ++){
					   $($videonames[i]).remove();
		 }
		 
		 var $imgs = $(".defined1");
		 for(var i = 0 ; i < $imgs.length ; i ++){
		    $($imgs[i]).remove();
		 }
				   
		 $('form')
	       .bootstrapValidator('updateStatus', 'cover', 'NOT_VALIDATED')
	       .bootstrapValidator('validateField', 'cover');
	 });
	 
	 $("input[name='cover").on('filereset', function(event) {
		  $('form')
	        .bootstrapValidator('updateStatus', 'cover', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'cover');
	});
	 
	$("input[name='cover'").on('filesuccessremove', function(event, id) {
			$("input[id='"+id+"']").remove();
			$('form')
		     .bootstrapValidator('updateStatus', 'cover', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'cover');
	});
	
	
   UE.getEditor('content');
 });
</script>
</html>