<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-在线课程</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
	  	var paras = "";
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             					//cache: false, // 不缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				                pagination: true, // 开启分页功能
				               // singleSelect: false,
				                pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500],
				                search: false, // 关闭搜索功能
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                //smartDisplay: true, // 智能显示 pagination 和 cardview 等
				                queryParams: function (params) {
									var page = params.offset;
				                	
				                	if((page * 1) != 0 ){
				            			page = page/params.limit + 1;
				            		}
				                	
				                	var obj = {
				                			  rows: params.limit,
						                      page: page,
						                      title:$("#titleFol").val(),
					                    };
				                	paras =  "rows=" + obj.rows + "&page=" + obj.page ;
				                	return obj;
				                },
				                url: "${pageContext.request.contextPath}/onlineCourseController/queryList",
				                detailView:true,
				                detailFormatter:function detailFormatter(index, row) {
				                	 var html = [];
				                	 html.push('<p><b>在线课程简介:</b> <br/>' + row.introduction + '</p>');
				                     html.push('<p><b>适应人群简述:</b> <br/>' + row.reserved10 + '</p>'); 
				                     html.push('<p><b>适应人群:</b> <br/>' + row.people + '</p>'); 
				                     html.push('<p><b>课程说明简述:</b> <br/>' + row.reserved9 + '</p>');
				                     html.push('<p><b>课程说明:</b> <br/>' + row.descript + '</p>');
				                     html.push('<p><b>动作名称:</b> <br/>' + row.actionname + '</p>'); 
				                     html.push('<p><b>训练周期:</b> <br/>' + row.cycle + '</p>'); 
				                     html.push('<p><b>注意事项:</b> <br/>' + row.notice + '</p>'); 
				                     
				                     return html.join('') ;
				                },
				                columns: [{
				                    field: 'state',
				                    checkbox: true
				                }, {
				                    field: 'id',
				                    title: '序号',
				                    align: 'center',
				                    valign: 'middle',
				 
				                },{
				                    field: 'title',
				                    title: '在线课程标题',
				                    align: 'center',
				                    valign: 'middle',
				                }, {
				                    field: 'viewTime',
				                    title: '课程时间',
				                    align: 'center',
				                    valign: 'middle',
				 
				                },{
				                    field: 'articleNum',
				                    title: '关联文章序号',
				                    align: 'center',
				                    valign: 'middle',
				 
				                },{
				                    field: 'courseno',
				                    title: '在线课程等级',
				                    align: 'center',
				                    valign: 'middle',
				 
				                },{
				                    field: 'coverurl',
				                    title: '在线课程封面',
				                    align: 'center',
				                    valign: 'middle',
				                    formatter:function(value,row,index){  
				                    	if(value != null){
				                    		return "<img height='35px' width='50px' src='"+value.split(";")[0]+"' alt='' class='img'/> ";
				                    	} else {
				                    		return "<img height='35px' width='50px' src='' alt='' class='img'/> ";
				                    	}
				                    }
				 
				                }, {
				                    field: 'calorie',
				                    title: '消耗卡路里',
				                    align: 'center',
				                    valign: 'middle',
				                    formatter:function(value,row,index){  
				                    	return (value * 1).toFixed(0) + "/千卡";
				                    }
				 
				                }, 
				             /*    {
				                    field: 'introduction',
				                    title: '简介',
				                    align: 'center',
				                    valign: 'middle',
				 
				                },{
				                    field: 'people',
				                    title: '适应人群',
				                    align: 'center',
				                    valign: 'middle',
				 
				                }, {
				                    field: 'actionname',
				                    title: '动作名称',
				                    align: 'center',
				                    valign: 'middle',
				 
				                },{
				                    field: 'cycle',
				                    title: '锻炼周期',
				                    align: 'center',
				                    valign: 'middle',
				 
				                },{
				                    field: 'notice',
				                    title: '注意事项',
				                    align: 'center',
				                    valign: 'middle',
				 
				                }, */
				                {
				                    field: 'createdate',
				                    title: '发布时间',
				                    align: 'center',
				                    valign: 'top',
				                    formatter:function(value,row,index){  
				                    	return timeStamp2String(value);
				                    }
				                },],
				                onLoadSuccess:function(){
				                },
				                onLoadError: function () {
				             	 
				                }
            });
        }
	  	
	  	function addObj(url){
	  		paras = paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
			paras = decodeURIComponent(paras,true);
			window.location.href = url + "?"+ paras;
	  	}
	  	
	  	function delList(url){
	  		var rows = $table.bootstrapTable('getSelections');
			var ids = "";
			$(rows).each(function(index,element){
				ids += element.id + ",";
			});
			ids = ids.substring(0,ids.length - 1);
			delListAsUtils({ids:ids},url,$table);
	  	}
	  	
	  	function editObj(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择编辑行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条编辑数据", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "id=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	  	
	  	function queryTips(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条数据", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "courseId=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	  	
	  	function queryNotes(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条数据", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "courseId=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	  	
	  	function queryAction(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条数据", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "courseId=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	  	
   		$(function(){
   			initTable();
   			
   			$("#btn_search").click(function(){
   				$table.bootstrapTable('refresh');
   			});
   		 
   		});
   		
   		function openVideo(title,url){
   			bootbox.alert({ 
   			  size: "large",
   			  title: "《"+title + "》",
   			  message: "<video src='"+url+"' width='100%' controls='controls'></video>", 
   			  callback: function(){ /* your callback code */ }
   			});
   		}
	    </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     		
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>在线课程</span></li>
                   <li><a href="javascript:void(0)" class="active">在线课程</a></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
                  		<c:forEach var="g" items="${btns }">
                  			 <button type="button" class="btn btn-primary" onclick="${g.action }">
						 		<i class="${g.btnicon }"></i> ${g.btnname }
							 </button>
                  		</c:forEach>
 					 </div>
                </div>

                <h3 class="page-header"> 在线课程<i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

                <blockquote class="page-information hidden">
                 <p>
                	  更新、维护在线课程信息！
                </p>
              </blockquote>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
					<form class="form-inline" role="form"
						style="margin-top: 10px;margin-bottom: 10px;">
						<input type="hidden" name="menuId" value="${folPara.menuId }">

						<div class="form-group">
							<label for="namecn">在线课程标题：</label> <input type="text"
								class="form-control" id="titleFol" name="titleFol"
								value="${folPara.titleFol }">
						</div>
						<button id="btn_search" type="button" class="btn btn-primary">
							  <i class="fa fa-search fa-lg"></i> 搜索
						</button>
					</form>
					<table id="table">
				    </table>
		    </div>
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>