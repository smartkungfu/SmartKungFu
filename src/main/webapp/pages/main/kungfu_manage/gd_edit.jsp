<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-编辑课程</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style type="text/css">
			.modal {
				margin-top: -500px;
			}
		</style>
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		<!--include fileinput.css  -->
	  	<link href="${pageContext.request.contextPath}/css/fileinput.min.css" rel="stylesheet">
		<!-- 样式冲突 -->
	    <!-- <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
	  	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var pageSize = QueryString.GetValue("pageSize");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var minDate = QueryString.GetValue("minDate");
		  		var maxDate = QueryString.GetValue("maxDate");
		  		var gymName = QueryString.GetValue("gymName");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='pageSize']").val(pageSize);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		$("input[name='minDate']").val(minDate);
		  		$("input[name='maxDate']").val(maxDate);
		  		$("input[name='gymName']").val(gymName);
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/gymnasiumDetailController/list" + reqPara; 
		  		});
		    	
		  		$.ajax({
						url : "${pageContext.request.contextPath}/mstGymnasiumController/initData",
						data : {},
						type : "GET",
						async : true,
						dataType : "json",
						success : function(data) {
							 var $gymid = $("select[name='gymid']");
							 var gymid = "${gd.gymid}";
							 
							 $gymid.append("<option value='0'>请选择</option>");
							 $.each(data, function (idx, ele) {
								 if(gymid == ele.id){
									 $gymid.append("<option selected='selected' value='"+ele.id+"'>"+ele.gymname+"</option>");
								 }else{
									 $gymid.append("<option value='"+ele.id+"'>"+ele.gymname+"</option>");
								 }
								 
						     });
							 
							 $gymid.multiselect();
						},
						error : function(data) {
							bootbox.alert({ 
								  size: "small",
								  title: "错误",
								  message: "请求超时，请重试！", 
								  callback: function(){return; }
							});
							return;
						}
				});	
		  		
		  		$.ajax({
					url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
					data : {parentId:"18"},
					type : "GET",
					async : true,
					dataType : "json",
					success : function(data) {
						 var $coursetype = $("select[name='coursetype']");
						 
						 var coursetype = "${gd.coursetype}";
						 $coursetype.append("<option value='0'>请选择</option>");
						 $.each(data, function (idx, ele) {
							 if(coursetype == ele.id){
								 $coursetype.append("<option selected='selected' value='"+ele.id+"'>"+ele.name+"</option>");	 
							 } else {
								 $coursetype.append("<option value='"+ele.id+"'>"+ele.name+"</option>");
							 }
							
					     });
						 
						 $coursetype.multiselect();
					},
					error : function(data) {
						bootbox.alert({ 
							  size: "small",
							  title: "错误",
							  message: "请求超时，请重试！", 
							  callback: function(){return; }
						});
						return;
					}
			   });	
			  	$('form').bootstrapValidator({
			  			message : '数据错误',
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            fields: {
			            	myfiles: {
			            		  validators: {
			                          file: {
			                              extension: 'jpeg,png,jpg',
			                              type: 'image/jpeg,image/png,image/jpg',
			                              maxSize: 2048 * 1024,   // 2 MB
			                              message: '文件格式非法，请重新上传'
			                          },
			                          callback: {
				                            message: '请上传文件',
				                            callback: function(value, validator) {
				                                // Get the selected options
				                                
				                                var $imagename = $("input[name='imagename']");
				                                var imgs = $(".defined");
				                                return ($imagename.val() != undefined || imgs.length != 0);
				                            }
				                     }
			                      }
				            },
			            	gymid: {
			                    validators: {
			                        callback: {
			                            message: '请选择场馆',
			                            callback: function(value, validator) {
			                                // Get the selected options
			                                var options = validator.getFieldElements('gymid').val();
			                                return (options != null && options != 0);
			                            }
			                        }
			                    }
			                }, 
			                coursename: {
			                    validators: {
			                        notEmpty: {
			                            message: '课程名称为空'
			                        },
			                        stringLength: {
			                            min: 3,
			                            max: 20,
			                            message: '长度在为3-20位'
			                        },
			                    }
			                },
			                coursetype: {
			                    validators: {
			                        callback: {
			                            message: '请选择课程类型',
			                            callback: function(value, validator) {
			                                var options = validator.getFieldElements('coursetype').val();
			                                return (options != null && options != 0);
			                            }
			                        }
			                    }
			                }, 
			                pricerang: {
			                    validators: {
			                        notEmpty: {
			                            message: '价格范围为空'
			                        },
			                    }
			                }, 
			                crowd: {
			                    validators: {
			                        notEmpty: {
			                            message: '针对人群为空'
			                        },
			                    }
			                }, 
			                courseresult: {
			                    validators: {
			                        notEmpty: {
			                            message: '课程效果为空'
			                        },
			                    }
			                }, 
			                coach: {
			                    validators: {
			                        notEmpty: {
			                            message: '教练为空'
			                        },
			                    }
			                }, 
			                coachcontent: {
			                    validators: {
			                        notEmpty: {
			                            message: '教练简介为空'
			                        },
			                    }
			                }, 
			               /*  workage: {
			                    validators: {
			                        notEmpty: {
			                            message: '工龄为空'
			                        },
			                        regexp: {
			                            regexp: "^[1-9]\\d*|0$",
			                            message: '工龄必须是正数'
			                        },
			                        callback: {
			                            message: '工龄不能大于50岁',
			                            callback: function(value, validator) {
			                                var options = validator.getFieldElements('workage').val();
			                                return (options * 1 <= 50 * 1);
			                            }
			                        }
			                    },
			                },  */
			                learningtime: {
			                    validators: {
			                        notEmpty: {
			                            message: '课程时间为空'
			                        },
			                    }
			                }, 
			                limitnum: {
			                    validators: {
			                        notEmpty: {
			                            message: '人数为空'
			                        },
			                        regexp: {
			                            regexp: "^[1-9]\\d*$",
			                            message: '人数必须是正整数'
			                        },
			                        callback: {
			                            message: '人数不能超过100人',
			                            callback: function(value, validator) {
			                                var options = validator.getFieldElements('limitnum').val();
			                                return (options * 1 <= 100 * 1);
			                            }
			                        }
			                    },
			                    
			                }, 
			                phoneno: {
			                    validators: {
			                        notEmpty: {
			                            message: '联系方式为空'
			                        },
			                        regexp: {
			                            regexp: "^(13|15|18|17)[0-9]{9}$",
			                            message: '手机号格式不正确'
			                        }
			                    }
			                },
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	
			            	validator.defaultSubmit();
			            }
			        });
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >有点功夫管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel" >课程管理</a></li>
		                   <li><a href="javascript:void(0)" class="active">编辑课程</a></li>
		                 </ul>
		                 <h3 class="page-header"> 编辑课程 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	课程信息编辑！
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/gymnasiumDetailController/save">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="pageSize" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												<input type="hidden" name="minDate" value="" /> 
												<input type="hidden" name="maxDate" value="" /> 
											    <input type="hidden" name="gymName" value="" /> 
											</div>
											
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${gd.id }" />
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">课程海报： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="file" class="form-control file-loading" name="myfiles" multiple placeholder="请上传资源" />
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">隶属场馆： <sup>*</sup></label>
													<div class="col-lg-5">
														<select class="multiselect" name="gymid"></select>
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">课程名称： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="coursename"  value="${gd.coursename }" placeholder="请输入课程名称" />
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">课程类型： <sup>*</sup></label>
												<div class="col-lg-5">
													<select class="multiselect" name="coursetype"></select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">是否推荐： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<c:if test="${gd.reserved8}">
															<label>
																 <input type="radio" name="reserved8" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved8" value="0" /> 否
															</label>
														</c:if>
														<c:if test="${!gd.reserved8}">
															<label>
																 <input type="radio" name="reserved8" value="1" /> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved8" value="0" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">价格范围： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="datetime" class="form-control" name="pricerang"  value="${gd.pricerang }" placeholder="请输入价格范围" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 针对人群： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="crowd" value="${gd.crowd }" placeholder="请输入针对人群" />
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">课程效果： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="courseresult" value="${gd.courseresult }" placeholder="请输入课程效果" />
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">教练： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="coach" value="${gd.coach }" placeholder="请输入教练" />
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">教练描述： <sup>*</sup></label>
													<div class="col-lg-5">
														<textarea class="form-control" rows="3" name="coachcontent" >${gd.coachcontent }</textarea>
													</div>
												</div>
											</div>
										<%-- 	<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">工龄： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="workage" value="${gd.workage }" placeholder="请输入工龄" />
													</div>
												</div>
											</div> --%>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">最大人数： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="number" class="form-control" name="limitnum" value="${gd.limitnum }" placeholder="请输入最大人数" />
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">课程时间： <sup>*</sup></label>
													<div class="col-lg-5">
														<textarea class="form-control" rows="7" name="learningtime" >${gd.learningtime }</textarea>
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">联系电话： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="phoneno" value="${gd.phoneno }" placeholder="请输入联系电话" />
													</div>
												</div>
											</div>
											
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
			<!--include fileinput.js  -->
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput.js"></script>
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
</body>
<script>
  $(function(){
	 var imsg = [];
	 var res = "${gd.coursebill}".split(";");
	 for(var i = 0; i < res.length; i ++){
		 imsg.push("<img src='"+res[i]+"' class='file-preview-image defined' style='width:180px;height:120px;'/>");
	 }
	 $("input[name='myfiles']").fileinput({
	        language: 'zh', //设置语言
	      	uploadUrl: "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
	        uploadAsync: true, //异步加载
	      	allowedFileExtensions : ['jpg', 'png','gif'],//接收的文件后缀
	        showUpload: false, //是否显示上传按钮
	        showCaption: false,//是否显示标题
	        overwriteInitial: true,
	        maxFileSize: 1024 * 10,
	        maxFileCount: 1,
	        slugCallback: function(filename) {
	            return filename.replace('(', '_').replace(']', '_');
	        },
	      
	        showPreview:true,
	        initialPreview: imsg,
	        allowedPreviewTypes : [ 'image' ],
	        browseClass: "btn btn-primary", //按钮样式             
	 });
	 $("input[name='myfiles']").on("fileuploaded", function (event, data, previewId, index) {  
		// JSON.stringify(data),分析data数据格式
		// alert(data.response.resName + "==" + previewId + "==" + index)
		var $input = " <input type='hidden' id='"+previewId+"' name='imagename' value='"+data.response.resName+"'/>";
		var $form = $("form");
		$form.append($input);
		
		$('form')
        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
        .bootstrapValidator('validateField', 'myfiles');
     });  
	
	$("input[name='myfiles']").on("fileclear", function(event) {
		   var $imagenames = $("input[name='imagename']");
		   
		   for(var i = 0 ; i < $imagenames.length ; i ++){
			   $($imagenames[i]).remove();
		   }
		   
		   var $imgs = $(".defined");
		   for(var i = 0 ; i < $imgs.length ; i ++){
			    $($imgs[i]).remove();
		   }
		   
		   $('form')
	        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'myfiles');
	 });
	
	$("input[name='myfiles").on('filereset', function(event) {
		  $('form')
	        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'myfiles');
	});
	
	$("input[name='myfiles'").on('filesuccessremove', function(event, id) {
		$("input[id='"+id+"']").remove();
		$('form')
	     .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	     .bootstrapValidator('validateField', 'myfiles');
	});

  });
</script>

</html>