<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-活动管理</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
	  	var paras = "";
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             					//cache: false, // 不缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				                pagination: true, // 开启分页功能
				               // singleSelect: false,
				                pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500],
				                search: false, // 关闭搜索功能
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                //smartDisplay: true, // 智能显示 pagination 和 cardview 等
				                queryParams: function (params) {
									var page = params.offset;
				                	
				                	if((page * 1) != 0 ){
				            			page = page/params.limit + 1;
				            		}
				                	
				                	//排序，并处理多表中的属性
				                	var sort = params.sort;
				                	if(sort == "cUName"){
				                		sort == "u." + sort;
				                	} else if(sort == "proTypeName"){
				                		sort == "c2." + sort;
				                	} else if(sort == "provinceName"){
				                		sort == "n1." + sort;
				                	} else if(sort == "cityName"){
				                		sort == "n2." + sort;
				                	} else if(sort == "areaName"){
				                		sort == "n3." + sort;
				                	} else{
				                		sort == "pi." + sort;
				                	}
				                	
				                	var obj = {
					                        rows: params.limit,
					                        page:page,
					                    	sort:sort,
									        order:params.order,
					                        minDate:$("#minDate").val(),
					                        maxDate:$("#maxDate").val(),
					                        title:$("#titleFol").val(),
					                        author:$("#authorFol").val(),
					                    };
				                	paras =  "rows=" + obj.rows + "&page=" + obj.page ;
				                	return obj;
				                },
				                url: "${pageContext.request.contextPath}/projectInfoController/queryList",
				                detailView:true,
				                detailFormatter:function detailFormatter(index, row) {
				                	 var html = [];
				                	 var tableHtml = "";
				                	 
				                     html.push('<p><b>报名费:</b> ' + (row.profee * 1).toFixed(2) + '/'+row.unitName+'</p>'); 
				                     html.push('<p><b>活动人数:</b> ' + row.reserved1 +'</p>'); 
				                     html.push('<p><b>隶属省:</b> ' + row.provinceName + '</p>');
				                     html.push('<p><b>隶属市:</b> ' + row.cityName + '</p>');
				                     html.push('<p><b>隶属区:</b> ' + row.areaName + '</p>');
				                     html.push('<p><b>联系地址:</b> ' + row.address + '</p>');
				                     html.push('<p><b>创始人:</b> ' + row.cUName + '</p>');
				                     html.push('<p><b>简讯:</b> ' + row.remark + '</p>');
				                     
				                     if(row.lightspot != null && row.lightspot != ""){
				                    	 html.push('<p><b>活动亮点:</b> <br/>' + row.lightspot + '</p>');
				                     }else{
				                    	 html.push('<p><b>活动亮点:</b>-</p>');
				                     }
				                     
				                     if(row.warning != null && row.warning != ""){
				                    	 html.push('<p><b>温馨提示:</b> <br/>' + row.warning + '</p>');
				                     }else{
				                    	 html.push('<p><b>温馨提示:</b>-</p>');
				                     }
				                     
				                     len = row.rolls.length;
				                      if(len != 0){
				                    	  tableHtml += '<table  class="table table-bordered table-hover table-striped display" id="example" >';
					                      tableHtml += '<thead>';
					                      tableHtml += '<tr>';
					                      tableHtml += '<th>优惠券标题</th>';
					                      tableHtml += '<th>优惠券内容</th>';
					                      tableHtml += '<th>开始时间</th>';
					                      tableHtml += '<th>结束时间</th>';
					                      tableHtml += '<th>创建者</th>';
					                      tableHtml += '<th>操作</th>';
					                      tableHtml += '</tr>';
					                      tableHtml += '</thead>';
					                      tableHtml += '<tbody>';
					                      for(var i = 0 ; i < len ; i ++){
					                    	  tableHtml += '<tr>';
					                    	  tableHtml += '<td class="center">'+row.rolls[i].title+'</td>';
					                    	  tableHtml += '<td class="center"><span class="label bg-info">'+row.rolls[i].content+'</span></td>';
					                    	  tableHtml += '<td class="center">'+ timeStamp2String(row.rolls[i].startdate)+'</td>';
						                      tableHtml += '<td class="center">'+ timeStamp2String(row.rolls[i].enddate)+'</td>';
						                      tableHtml += '<td class="center"><span class="badge bg-warning">'+row.rolls[i].cUName+'</span></td>';
						                      tableHtml += '<td class="center"><button type="button" class="btn btn-primary btn-xs" onclick="editRoll('+row.rolls[i].id+')">修改</button>  <button type="button" class="btn btn-primary btn-xs" onclick="delRoll('+row.rolls[i].id+')">删除</button></td>';
						                      tableHtml += '</tr>';
					                      }
					                      tableHtml += '</tbody>';
					                      tableHtml += '</table>';
				                      }else{
				                    	  tableHtml = '<p><b>无优惠券信息</b></p>';
				                      }
				                     
				                     return html.join('') +  tableHtml;
				                },
				                columns: [{
				                    field: 'state',
				                    checkbox: true
				                },  {
				                    field: 'probill',
				                    title: '图片',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){ 
				                    	if(value != null){
				                    		return "<img height='35px' width='50px' src='"+value.split(";")[0]+"' alt='' class='img'/> ";
				                    	} else {
				                    		return "<img height='35px' width='50px' src='' alt='' class='img'/> ";
				                    	}
				                    }
				                }, {
				                    field: 'title',
				                    title: '活动标题',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                },{
				                    field: 'proTypeName',
				                    title: '活动类型',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                }, /* {
				                    field: 'profee',
				                    title: '报名费',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                }, {
				                    field: 'unitName',
				                    title: '单位',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                },  */{
				                    field: 'procontact',
				                    title: '联系人',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                }, {
				                    field: 'prophoneno',
				                    title: '联系方式',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                }, /* {
				                    field: 'address',
				                    title: '联系地址',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                }, */ {
				                    field: 'startdate',
				                    title: '起始时间',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return dateStamp2String(value);
				                    }
				                },{
				                    field: 'enddate',
				                    title: '结束时间',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return dateStamp2String(value);
				                    }
				                }, {
				                    field: 'istop',
				                    title: '是否置顶',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	if(value){
				                    		return "是";
				                    	} else {
				                    		return "否";
				                    	}
				                    }
				                },{
				                    field: 'reserved7',
				                    title: '是否独家',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	if(value){
				                    		return "是";
				                    	} else {
				                    		return "否";
				                    	}
				                    }
				                },{
				                    field: 'createdate',
				                    title: '注册时间',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return timeStamp2String(value);
				                    }
				                }, ],
				                onLoadSuccess:function(){
				                },
				                onLoadError: function () {
				             	   //    mif.showErrorMessageBox("数据加载失败！");
				                }
            });
        }
	  	
	  	function addObj(url){
	  		paras = paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
			paras = decodeURIComponent(paras,true);
			window.location.href = url + "?"+ paras;
	  	}
	  	
	  	function delList(url){
	  		var rows = $table.bootstrapTable('getSelections');
			var ids = "";
			$(rows).each(function(index,element){
				ids += element.id + ",";
			});
			ids = ids.substring(0,ids.length - 1);
			delListAsUtils({ids:ids},url,$table);
	  	}
	  	
	  	function editObj(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择编辑行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条编辑数据", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "id=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	  	
   		$(function(){
   			initTable();
   			
   			$("#btn_search").click(function(){
   				$table.bootstrapTable('refresh');
   			});
   		 
   		});
   		
   		//添加优惠券
   		function addRoll(url){
		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择添加行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条添加数据", 
					  callback: function(){return; }
				});
	  		} else {
		  		paras = paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}&type=125&resourceid="+rows[0].id;
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
   		
   		//修改优惠券
	  	function editRoll(id){
  			paras = "index_exp=${MENU_STYLE_INDEX_EXP}"+"&index_che=${MENU_STYLE_INDEX_CHE}"+"&id=" + id + "&" +paras + "&"+  $("form").find("input").serialize()+"&type=125";
			paras = decodeURIComponent(paras,true);
			window.location.href = "${pageContext.request.contextPath}/mstRollController/edit" + "?"+ paras;
	  	}
   		
	  	//删除优惠券
	  	function delRoll(id){
			url = "${pageContext.request.contextPath}/mstRollController/delList" + "?"+ paras;
			delListAsUtils({ids:id},url,$table);
	  	}
	    </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>活动管理</span></li>
                   <li><a href="javascript:void(0)" class="active">活动管理</a></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
                  		<c:forEach var="g" items="${btns }">
                  			 <button type="button" class="btn btn-primary" onclick="${g.action }">
						 		<i class="${g.btnicon }"></i> ${g.btnname }
							 </button>
                  		</c:forEach>
 					 </div>
                </div>

                <h3 class="page-header"> 活动管理<i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

                <blockquote class="page-information hidden">
                 <p>
                	  更新、维护活动信息！
                </p>
              </blockquote>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
					<form class="form-inline" role="form" style="margin-top: 10px;margin-bottom: 10px;">
						  <input type="hidden" name="menuId" value="${folPara.menuId }">
						  
						  <div class="form-group">
						    <label for="minDate">发布时间：</label>
						    <input type="date" class="form-control" id="minDate" name="minDate" value="${folPara.minDate }"> -
						    <input type="date" class="form-control" id="maxDate" name="maxDate"  value="${folPara.maxDate }">
						  </div>
						  <br/><br/>
						  <div class="form-group">
						    <label  for="namecn">活动标题：</label>
						    <input type="text" class="form-control" id="titleFol" name="titleFol"  value="${folPara.titleFol }">
						  </div>
						  <button id="btn_search" type="button" class="btn btn-primary" >
						 	<i class="fa fa-search fa-lg"></i> 搜索
						  </button>
					</form>
				    <table id="table">
				    </table>
		    </div>
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>