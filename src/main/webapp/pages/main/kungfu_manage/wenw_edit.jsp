<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-编辑文章</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style type="text/css">
			.modal {
				margin-top: -500px;
			}
		</style>
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		<!--include fileinput.css  -->
	  	<link href="${pageContext.request.contextPath}/css/fileinput.min.css" rel="stylesheet">
		<!-- 样式冲突 -->
	    <!-- <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
		<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
	  	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<!-- ueditor插件 -->
		<script src="${pageContext.request.contextPath}/ueditor1.4.3.3/ueditor.config.js"  type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath}/ueditor1.4.3.3/ueditor.all.min.js" type="text/javascript" charset="utf-8"> </script>
		<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
		<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
		<script src="${pageContext.request.contextPath}/ueditor1.4.3.3/lang/zh-cn/zh-cn.js" type="text/javascript" charset="utf-8" ></script>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		
		  		var minDate = QueryString.GetValue("minDate");
		  		var maxDate = QueryString.GetValue("maxDate");
		  		var authorFol = QueryString.GetValue("authorFol");
		  		var titleFol = QueryString.GetValue("titleFol");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		$("input[name='minDate']").val(minDate);
		  		$("input[name='maxDate']").val(maxDate);
		  		$("input[name='authorFol']").val(authorFol);
		  		$("input[name='titleFol']").val(titleFol);
		  		
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/wenwInfoController/list" + reqPara; 
		  		});
		  		
		  		$.ajax({
					url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
					data : {parentId:"100"},
					type : "GET",
					async : true,
					dataType : "json",
					success : function(data) {
						var $wentype = $("select[name='wentype']");
						
						$.each(data, function(k, p) {
				  			 var wentype = "${wenwInfo.wentype}";
				  			 if(wentype == p.id){
				  				 option = "<option  value='"+p.id+"' selected='selected'>"+p.name+"</option>";
				  			 } else {
				  				 option = "<option  value='"+p.id+"'>"+p.name+"</option>";
				  			 }
				  			 
				  			$wentype.append(option);
				  		});
						
						$wentype.multiselect({maxHeight: 200});
					},
					error : function(data) {
						bootbox.alert({ 
							  size: "small",
							  title: "错误",
							  message: "请求超时，请重试！", 
							  callback: function(){return; }
						});
						return;
					}
			   });	
		  
			  	$('form').bootstrapValidator({
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            fields: {
			            	myfiles: {
			            		  validators: {
			                          file: {
			                              extension: 'jpeg,png,jpg,gif',
			                              type: 'image/jpeg,image/png,image/jpg',
			                              maxSize: 2048 * 1024,   // 2 MB
			                              message: '文件格式非法，请重新上传'
			                          },
			                          callback: {
				                            message: '请上传文件',
				                            callback: function(value, validator) {
				                                // Get the selected options
				                                
				                                var $imagename = $("input[name='imagename']");
				                                var imgs = $(".defined");
				                                return ($imagename.val() != undefined || imgs.length != 0);
				                            }
				                     }
			                      }
				            },
			            	reserved8: {
			                    validators: {
			                        notEmpty: {
			                            message: '请选择是否推荐'
			                        }
			                    }
			                },
			                wentype: {
			                    validators: {
			                        callback: {
			                            message: '请选择文章类型',
			                            callback: function(value, validator) {
			                                var options = validator.getFieldElements('wentype').val();
			                                return (options != null);
			                            }
			                        }
			                    }
			                },
			                title: {
			                    validators: {
			                        notEmpty: {
			                            message: '文章标题为空'
			                        },
			                        stringLength: {
			                            min: 1,
			                            max: 100,
			                            message: '长度在为1-100位'
			                        },
			                    }
			                },
			                author: {
			                    validators: {
			                        notEmpty: {
			                            message: '作者为空'
			                        },
			                    }
			                }, 
			                copyright: {
			                    validators: {
			                        notEmpty: {
			                            message: '版权为空'
			                        },
			                    }
			                },
			                remark: {
			                    validators: {
			                        notEmpty: {
			                            message: '文章简讯为空'
			                        },
			                        stringLength: {
			                            min: 1,
			                            max: 50,
			                            message: '简讯长度为1-50位'
			                        },
			                    }
			                },
			                editor: {
			                    validators: {
			                    	notEmpty: {
			                            message: '文章内容为空'
			                        },
			                    }
			                },
			               /*  cooperation: {
			                    validators: {
			                        notEmpty: {
			                            message: '合作信息为空'
			                        },
			                    }
			                }, */
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	validator.defaultSubmit();
			            }
			        });
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >有点功夫管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel" >文章管理</a></li>
		                   <li><a href="javascript:void(0)" class="active">编辑文章</a></li>
		                 </ul>
		                 <h3 class="page-header"> 编辑文章 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	文章信息编辑！
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row" style="height:100%	">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/wenwInfoController/save">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												<input type="hidden" name="minDate" value="" /> 
												<input type="hidden" name="maxDate" value="" /> 
												<input type="hidden" name="authorFol" value="" />
											    <input type="hidden" name="titleFol" value="" /> 
											</div>
											
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${wenwInfo.id }" />
											<div class="form-group">
												<label class="col-lg-3 control-label">图片： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="file" class="form-control file-loading" name="myfiles" multiple />
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">文章类型： <sup>*</sup></label>
													<div class="col-lg-5">
														<select class="multiselect" name="wentype" ></select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">标题： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="title"  value="${wenwInfo.title }" placeholder="请输入标题" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 作者： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="author" value="${wenwInfo.author }" placeholder="请输入作者" />
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">版权： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="copyright" value="${wenwInfo.copyright eq null ? 'All Rights Reserved' : wenwInfo.copyright}" placeholder="请输入版权" />
													</div>
												</div>
											</div>
											<%-- <div>
												<div class="form-group">
													<label class="col-lg-3 control-label">合作信息： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="cooperation" value="${wenwInfo.cooperation  eq null ? '加盟我们' ? }" placeholder="请输入合作信息" />
													</div>
												</div>
											</div> --%>
											<div class="form-group">
												<label class="col-lg-3 control-label">是否推荐： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<c:if test="${wenwInfo.reserved8}">
															<label>
																 <input type="radio" name="reserved8" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved8" value="0" /> 否
															</label>
														</c:if>
														<c:if test="${!wenwInfo.reserved8}">
															<label>
																 <input type="radio" name="reserved8" value="1" /> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved8" value="0" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">文章简讯：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<textarea class="form-control" rows="3" name="remark" >${wenwInfo.remark }</textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">内容：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-9">
													<textarea  name="content" id="content" id="contentCn" name="contentCn" >${wenwInfo.content }</textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
			<!--include fileinput.js  -->
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput.js"></script>
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
</body>
<script>
  $(function(){
	 var imsg = [];
	 var res = "${wenwInfo.cover}".split(";");
	 for(var i = 0; i < res.length; i ++){
		 if(res[i] != null && res[i] != ""){
			 imsg.push("<img src='"+res[i]+"' class='file-preview-image defined' style='width:180px;height:120px;' />");
		 }
	 }
	 $("input[name='myfiles']").fileinput({
	        language: 'zh', //设置语言
	      	uploadUrl: "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
	        uploadAsync: true, //异步加载
	      	allowedFileExtensions : ['jpg', 'png', 'gif'],//接收的文件后缀
	        showUpload: false, //是否显示上传按钮
	        showCaption: false,//是否显示标题
	        overwriteInitial: true,
	        maxFileSize: 1024 * 10,
	        maxFileCount: 1,
	        slugCallback: function(filename) {
	            return filename.replace('(', '_').replace(']', '_');
	        },
	      
	        showPreview:true,
	        initialPreview: imsg,
	        allowedPreviewTypes : [ 'image' ],
	        browseClass: "btn btn-primary", //按钮样式             
	 });
	 $("input[name='myfiles']").on("fileuploaded", function (event, data, previewId, index) {  
		// JSON.stringify(data),分析data数据格式
		// alert(data.response.resName + "==" + previewId + "==" + index)
		var $input = " <input type='hidden' id='"+previewId+"' name='imagename' value='"+data.response.resName+"'/>";
		var $form = $("form");
		$form.append($input);
		
		$('form')
        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
        .bootstrapValidator('validateField', 'myfiles');
     });  
	$("input[name='myfiles']").on("fileclear", function(event) {
		 var $imagenames = $("input[name='imagename']");
		 for(var i = 0 ; i < $imagenames.length ; i ++){
			  $($imagenames[i]).remove();
		 }
		 
		 var $imgs = $(".defined");
		 for(var i = 0 ; i < $imgs.length ; i ++){
			  $($imgs[i]).remove();
		 }
		 $('form')
	        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'myfiles');
	 });
	
	$("input[name='myfiles").on('filereset', function(event) {
		  $('form')
	        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'myfiles');
	});
	
	$("input[name='myfiles'").on('filesuccessremove', function(event, id) {
		$("input[id='"+id+"']").remove();
		$('form')
	     .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	     .bootstrapValidator('validateField', 'myfiles');
	});
	
    UE.getEditor('content');
  });
</script>
</html>