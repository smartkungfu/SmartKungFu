<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-编辑优惠券</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		<!--include fileinput.css  -->
	  	<link href="${pageContext.request.contextPath}/css/fileinput.min.css" rel="stylesheet">
	  	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	  	<!-- include datetimepicker.css  -->
	  	<link href="${pageContext.request.contextPath}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<!--include wysiwyg.js  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-wysiwyg.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.hotkeys.min.js"></script>
	  	<!--导入省市区JSON数据  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/ProJson.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/DistrictJson.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/CityJson.js"></script>
		<!-- include datetimepicker.js  -->
	  	<script src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.min.js"></script>
	    <script src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.zh-CN.js"></script>
		
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var pageSize = QueryString.GetValue("pageSize");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var minDate = QueryString.GetValue("minDate");
		  		var maxDate = QueryString.GetValue("maxDate");
		  		var gymName = QueryString.GetValue("gymName");
		  		
		  		var type = QueryString.GetValue("type");
		  		var resourceid = QueryString.GetValue("resourceid");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='pageSize']").val(pageSize);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		$("input[name='minDate']").val(minDate);
		  		$("input[name='maxDate']").val(maxDate);
		  		$("input[name='gymName']").val(gymName);
		  		
		  		$("input[name='type']").val(type);
		  		$("input[name='resourceid']").val(resourceid);
		  		
		  		// 加载省信息
			  	var $province = $("select[name='province']");
			  	$province.append("<option value='0'>请选择</option>");
			  	$.each(province, function(k, p) {
			  			 var proId = "${mstClient.province}";
			  			 if(proId == p.ProID){
			  				 option = "<option  value='"+p.ProID+"' selected='selected'>"+p.ProName+"</option>";
			  			 } else {
			  				 option = "<option  value='"+p.ProID+"'>"+p.ProName+"</option>";
			  			 }
			  			$province.append(option);
			  	});
			  	
			  	var $city = $("select[name='city']");
			  	$city.append("<option value='0'>请选择</option>");
		    	$.each(city, function(k, p) {
		    		var provinceId = "2";
		    		provinceId = "${mstClient.province}";
		    		var cityId = "${mstClient.city}";
					var option = "";
					if(provinceId == p.ProID){
						if(p.CityID == cityId){
							 option = "<option value='"+p.CityID+"' selected='selected'>"+p.CityName+"</option>";
						 } else {
							 option = "<option value='"+p.CityID+"'>"+p.CityName+"</option>";
						 }
					}
					
					$city.append(option);
				});
		    	
		    	var $area = $("select[name='area']");
		    	$.each(District, function(k, p) {
		    		var cityId = "3";
		    		cityId = "${mstClient.city}";
		    		var areaId = "${mstClient.area}";
		    		$area.append("<option value='0'>请选择</option>");
		    		
		    		var option = "";
		    		if(p.CityID == cityId){
		    			option = "<option value='"+p.Id+"'>"+p.DisName+"</option>";
						if(p.Id == areaId){
							 option = "<option value='"+p.Id+"' selected='selected'>"+p.DisName+"</option>"; 
						}
		    		}
					
					$area.append(option);
				}); 
		    	
		  		$.ajax({
						url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
						data : {parentId:"14"},
						type : "GET",
						async : true,
						dataType : "json",
					    beforeSend: function() {
					        //请求前的处理
					    },
						success : function(data) {
							
							$province.multiselect({
							      onChange: function(element, checked) {
							    	if(checked){
							    		var proId = $province.val();
							    		 //市数据加载
							    		var cityHtml = "<option value='0'>请选择</option>";
						                var areaHtml = "<option value='0'>请选择</option>";
						    		    $.each(city, function(k, p) {
						    				if (p.ProID == proId) {
						    					cityHtml += "<option value='"+p.CityID+"'>"+p.CityName+"</option>";
						    				}
						    			});
						    		    $city.html(cityHtml);
						    		    $area.html(areaHtml);
						    		    //刷新市、区下拉列表
						    		    $city.multiselect('rebuild');
						    		    $area.multiselect('rebuild');
					                }
							      },
							      maxHeight: 200,
							      filterPlaceholder: 'Search'
							});
							
							$city.multiselect({
							      onChange: function(element, checked) {
							    	if(checked){
							    		var cityId = $city.val();
							    		 //市数据加载
						                var areaHtml = "<option value='0'>请选择</option>";
						                $.each(District, function(k, p) {
											if (p.CityID == cityId) {
												areaHtml += "<option value='"+p.Id+"'>"+p.DisName+"</option>";
											}
										});
						    		    $area.html(areaHtml);
						    		    //刷新区下拉列表
						    		    $area.multiselect('rebuild');
					                }
							      },
							      maxHeight: 200
							});
							
							$area.multiselect({
							      onChange: function(element, checked) {
							      },
							      maxHeight: 200
							});
							
						},
						error : function(data) {
							bootbox.alert({ 
								  size: "small",
								  title: "错误",
								  message: "请求超时，请重试！", 
								  callback: function(){return; }
							});
							return;
						}
				});
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/"+ '${menuUrl }' + reqPara; 
		  		});
		  		
			  	$('form').bootstrapValidator({
			            // Exclude only disabled fields
			            // The invisible fields set by Bootstrap Multiselect must be validated
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            fields: {
			                title: {
				                    validators: {
				                        notEmpty: {
				                            message: '优惠标题为空'
				                        }
				                    }
				            },
				            aritype: {
			                    validators: {
			                        notEmpty: {
			                            message: '请选择优惠类型'
			                        }
			                    }
			                },
				            startdate: {
			                    validators: {
			                        notEmpty: {
			                            message: '开始时间为空'
			                        },
			                    }
			                },
			                enddate: {
			                    validators: {
			                        notEmpty: {
			                            message: '结束时间为空'
			                        },
			                    }
			                },
			                man: {
			                    validators: {
			                    	numeric: {message: '金额只能输入数字'}
			                    }
			                },
			                jian: {
			                    validators: {
			                    	numeric: {message: '金额只能输入数字'}
			                    }
			                },
			                dazhe: {
			                    validators: {
			                    	numeric: {message: '金额只能输入数字'},
			                    	 regexp: {
					                        regexp: /^(0\.\d{1,2}|[1-9](\.\d{1,2})?)$/,
					                        message: '只能输入0~10之间的数(两位小数)'
					                    }
			                    }
			                },
			                zhijian: {
			                    validators: {
			                    	numeric: {message: '金额只能输入数字'}
			                    }
			                }
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	validator.defaultSubmit();
			            }
			        });
			  	
			  	var radio =  document.getElementsByName("aritype");
			    for(var i=0;i<radio.length;i++){
			    	if(radio[i].checked == true){
			    		init(radio[i]);
			    	}
			    }
			    
				$('.form_date').datetimepicker({
			        language:  'zh-CN',
			        weekStart: 1,
			        todayBtn:  1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0,
					startDate: "1970-01-01"
			    }).on('hide', function(ev){
			        $('form')
			        .bootstrapValidator('updateStatus', 'startdate', 'NOT_VALIDATED')
			        .bootstrapValidator('validateField', 'startdate');
			        
			        $('form')
			        .bootstrapValidator('updateStatus', 'enddate', 'NOT_VALIDATED')
			        .bootstrapValidator('validateField', 'enddate');
			    });
		  	  });
		  	 
		  	 function init(obj){
		  		 var aritype = $(obj).val();
		  		 if(aritype == 127){
		  			 $("#man").show();
		  			 $("#jian").show();
		  			 $("#dazhe").hide();
		  			 $("#zhijian").hide();
		  		 }else if(aritype ==  128){
		  			 $("#man").hide();
		  			 $("#jian").hide();
		  			 $("#dazhe").show();
		  			 $("#zhijian").hide();
		  		 }else if(aritype == 129){
		  			 $("#man").hide();
		  			 $("#jian").hide();
		  			 $("#dazhe").hide();
		  			 $("#zhijian").show();
		  		 }
		  	 }
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >有点功夫管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel">${menuName }</a></li>
		                   <li><a href="javascript:void(0)" class="active">编辑优惠券</a></li>
		                 </ul>
		                 <h3 class="page-header">编辑优惠券 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	优惠券信息编辑
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/mstRollController/save">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="pageSize" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												<input type="hidden" name="minDate" value="" /> 
												<input type="hidden" name="maxDate" value="" /> 
												<input type="hidden" name="gymName" value="" />
												
											    <input type="hidden" name="type" value="" /> 
											    <input type="hidden" name="resourceid" value="" /> 
											</div>
											
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${mstRoll.id }" />
											
											<div class="form-group">
												<label class="col-lg-3 control-label">优惠标题： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<input type="text" class="form-control" name="title"  value="${mstRoll.title }" placeholder="请输入标题" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">优惠类型： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<div class="radio-group">
														<%-- <label>
															 <input type="radio" name="aritype" value="127" <c:if test="${mstRoll.aritype == 127}"> checked="checked" </c:if> onchange="init(this)"/> 满减
														</label>&nbsp;&nbsp;&nbsp;  --%>
														<label> 
															<input type="radio" name="aritype" value="128" <c:if test="${mstRoll.aritype == 128}"> checked="checked" </c:if> onchange="init(this)"/> 打折
														</label>&nbsp;&nbsp;&nbsp; 
														<label> 
															<input type="radio" name="aritype" value="129" <c:if test="${mstRoll.aritype == 129}"> checked="checked" </c:if> onchange="init(this)"/> 直减
														</label>
													</div>
												</div>
											</div>
											<div id="man" style="display: none;">
												<div class="form-group">
													<label class="col-lg-3 control-label">满： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="man" value="${mstRoll.man }" placeholder="请输入满金额" />
													</div>
												</div>
											</div>
											<div id="jian" style="display: none;">
												<div class="form-group">
													<label class="col-lg-3 control-label">减： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="jian" value="${mstRoll.jian }" placeholder="请输入减金额" />
													</div>
												</div>
											</div>
											<div id="dazhe" style="display: none;">
												<div class="form-group">
													<label class="col-lg-3 control-label">打折： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="dazhe" value="${mstRoll.dazhe }" placeholder="请输入折扣（小数点格式）" />
													</div>
												</div>
											</div>
											<div id="zhijian" style="display: none;">
												<div class="form-group">
													<label class="col-lg-3 control-label">直减： <sup>*</sup></label>
													<div class="input-group col-lg-5">
														<input type="text" class="form-control" name="zhijian" value="${mstRoll.zhijian }" placeholder="请输入直减金额" />
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">开始时间： <sup>*</sup></label>
												<div class="input-group date form_date col-md-5"
													data-date="" data-date-format="yyyy-mm-dd"
													data-link-field="startdate" data-link-format="yyyy-mm-dd">
													<input class="form-control" type="text" value="<fmt:formatDate value="${mstRoll.startdate }"/>"
														readonly> <span class="input-group-addon"><span
														class="glyphicon glyphicon-remove"></span></span> <span
														class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span></span>
												</div>
												<label class="col-lg-3 control-label"></label>
												<div class="input-group col-lg-5">
													<input type="hidden" class="form-control" name="startdate" id="startdate" value="<fmt:formatDate value="${mstRoll.startdate }" pattern="yyyy-MM-dd"/>" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">结束时间： <sup>*</sup></label>
												<div class="input-group date form_date col-md-5"
													data-date="" data-date-format="yyyy-mm-dd"
													data-link-field="enddate" data-link-format="yyyy-mm-dd">
													<input class="form-control" type="text" value="<fmt:formatDate value="${mstRoll.enddate }"/>"
														readonly> <span class="input-group-addon"><span
														class="glyphicon glyphicon-remove"></span></span> <span
														class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span></span>
												</div>
												<label class="col-lg-3 control-label"></label>
												<div class="input-group col-lg-5">
													<input type="hidden" class="form-control" name="enddate" id="enddate" value="<fmt:formatDate value="${mstRoll.enddate }" pattern="yyyy-MM-dd"/>" />
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
			<!--include fileinput.js  -->
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput.js"></script>
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
</body>
</html>