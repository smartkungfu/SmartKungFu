<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-编辑活动</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style type="text/css">
			.modal {
				margin-top: -500px;
			}
		</style>
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		<!--include fileinput.css  -->
	  	<link href="${pageContext.request.contextPath}/css/fileinput.min.css" rel="stylesheet">
		<!-- 样式冲突 -->
	    <!-- <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
	  	<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
	  	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	  	<!-- include datetimepicker.css  -->
	  	<link href="${pageContext.request.contextPath}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	 <!-- include datetimepicker.js  -->
	  	<script src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.min.js"></script>
	    <script src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.zh-CN.js"></script>
	  	<!--include wysiwyg.js  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-wysiwyg.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.hotkeys.min.js"></script>
	  	<!--导入省市区JSON数据  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/ProJson.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/DistrictJson.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/CityJson.js"></script>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var minDate = QueryString.GetValue("minDate");
		  		var maxDate = QueryString.GetValue("maxDate");
		  		var titleFol = QueryString.GetValue("titleFol");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		$("input[name='minDate']").val(minDate);
		  		$("input[name='maxDate']").val(maxDate);
		  		$("input[name='titleFol']").val(titleFol);
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/projectInfoController/list" + reqPara; 
		  		});
		  		
		  		// 加载省信息
			  	var $province = $("select[name='province']");
			  	$province.append("<option value='0'>请选择</option>");
			  	$.each(province, function(k, p) {
			  			 var proId = "${projectInfo.province}";
			  			 if(proId == p.ProID){
			  				 option = "<option  value='"+p.ProID+"' selected='selected'>"+p.ProName+"</option>";
			  			 } else {
			  				 option = "<option  value='"+p.ProID+"'>"+p.ProName+"</option>";
			  			 }
			  			$province.append(option);
			  	});
			  	
			  	var $city = $("select[name='city']");
			  	$city.append("<option value='0'>请选择</option>");
		    	$.each(city, function(k, p) {
		    		var provinceId = "2";
		    		provinceId = "${projectInfo.province}";
		    		var cityId = "${projectInfo.city}";
					var option = "";
					if(provinceId == p.ProID){
						if(p.CityID == cityId){
							 option = "<option value='"+p.CityID+"' selected='selected'>"+p.CityName+"</option>";
						 } else {
							 option = "<option value='"+p.CityID+"'>"+p.CityName+"</option>";
						 }
					}
					
					$city.append(option);
				});
		    	
		    	var $area = $("select[name='area']");
		    	$.each(District, function(k, p) {
		    		var cityId = "3";
		    		cityId = "${projectInfo.city}";
		    		var areaId = "${projectInfo.area}";
		    		$area.append("<option value='0'>请选择</option>");
		    		
		    		var option = "";
		    		if(p.CityID == cityId){
		    			option = "<option value='"+p.Id+"'>"+p.DisName+"</option>";
						if(p.Id == areaId){
							 option = "<option value='"+p.Id+"' selected='selected'>"+p.DisName+"</option>"; 
						}
		    		}
					
					$area.append(option);
				}); 
		    	
		  		$.ajax({
						url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
						data : {parentId:"14"},
						type : "GET",
						async : true,
						dataType : "json",
					    beforeSend: function() {
					        //请求前的处理
					    },
						success : function(data) {
							var $unit = $("select[name='unit']");
							
							var unit = "${projectInfo.unit}";
							var tempArr = new Array();
							$.each(data, function(index, ele) {
								  if(unit == ele.id){
						              var tempObj = {label: ele.name,value:ele.id,selected:"selected" };
						              tempArr.push(tempObj);
								  } else {
									  var tempObj = {label: ele.name,value:ele.id };
						              tempArr.push(tempObj);
								  }
					              
					        });
							
							$unit.multiselect('dataprovider',tempArr,{
				                onChange: function(element, checked) {
				                   //当选项改变重新校验select元素
				                   $('form').data('bootstrapValidator')                 // Get plugin instance
				                        	.updateStatus('unit', 'NOT_VALIDATED')  // Update field status
				                            .validateField('unit');                 // and re-validate it
				                }
				            });
							
							$province.multiselect({
							      onChange: function(element, checked) {
							    	if(checked){
							    		var proId = $province.val();
							    		 //市数据加载
							    		var cityHtml = "<option value='0'>请选择</option>";
						                var areaHtml = "<option value='0'>请选择</option>";
						    		    $.each(city, function(k, p) {
						    				if (p.ProID == proId) {
						    					cityHtml += "<option value='"+p.CityID+"'>"+p.CityName+"</option>";
						    				}
						    			});
						    		    $city.html(cityHtml);
						    		    $area.html(areaHtml);
						    		    //刷新市、区下拉列表
						    		    $city.multiselect('rebuild');
						    		    $area.multiselect('rebuild');
					                }
							      },
							      maxHeight: 200,
							      filterPlaceholder: 'Search'
							});
							
							$city.multiselect({
							      onChange: function(element, checked) {
							    	if(checked){
							    		var cityId = $city.val();
							    		 //市数据加载
						                var areaHtml = "<option value='0'>请选择</option>";
						                $.each(District, function(k, p) {
											if (p.CityID == cityId) {
												areaHtml += "<option value='"+p.Id+"'>"+p.DisName+"</option>";
											}
										});
						    		    $area.html(areaHtml);
						    		    //刷新区下拉列表
						    		    $area.multiselect('rebuild');
					                }
							      },
							      maxHeight: 200
							});
							
							$area.multiselect({
							      onChange: function(element, checked) {
							      },
							      maxHeight: 200
							});
							
						},
						error : function(data) {
							bootbox.alert({ 
								  size: "small",
								  title: "错误",
								  message: "请求超时，请重试！", 
								  callback: function(){return; }
							});
							return;
						}
				   });	
		  		
		  		$.ajax({
					url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
					data : {parentId:"222"},
					type : "GET",
					async : true,
					dataType : "json",
					success : function(data) {
						var $protype = $("select[name='protype']");
						
						$.each(data, function(k, p) {
				  			 var protype = "${projectInfo.protype}";
				  			 if(protype == p.id){
				  				 option = "<option  value='"+p.id+"' selected='selected'>"+p.name+"</option>";
				  			 } else {
				  				 option = "<option  value='"+p.id+"'>"+p.name+"</option>";
				  			 }
				  			 
				  			$protype.append(option);
				  		});
						
						$protype.multiselect({maxHeight: 200});
					},
					error : function(data) {
						bootbox.alert({ 
							  size: "small",
							  title: "错误",
							  message: "请求超时，请重试！", 
							  callback: function(){return; }
						});
						return;
					}
			   });	
		  
			  	$('form').bootstrapValidator({
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            fields: {
			            	myfiles: {
			            		  validators: {
			                          file: {
			                              extension: 'jpeg,png,jpg',
			                              type: 'image/jpeg,image/png,image/jpg',
			                              maxSize: 2048 * 1024,   // 2 MB
			                              message: '文件格式非法，请重新上传'
			                          },
			                          callback: {
				                            message: '请上传文件',
				                            callback: function(value, validator) {
				                                // Get the selected options
				                                
				                                var $imagename = $("input[name='imagename']");
				                                var imgs = $(".defined");
				                                return ($imagename.val() != undefined || imgs.length != 0);
				                            }
				                     }
			                      }
				            },
			                title: {
			                    validators: {
			                        notEmpty: {
			                            message: '活动标题为空'
			                        },
			                        stringLength: {
			                            min: 3,
			                            max: 20,
			                            message: '长度在为3-20位'
			                        },
			                    }
			                },
			                procontact: {
			                    validators: {
			                        notEmpty: {
			                            message: '联系人为空'
			                        },
			                    }
			                }, 
			                reserved1: {
			                    validators: {
			                        notEmpty: {
			                            message: '人数为空'
			                        },
			                        regexp: {
			                            regexp: "^[1-9]\\d*$",
			                            message: '人数必须是正整数'
			                        },
			                        callback: {
			                            message: '人数不能超过100人',
			                            callback: function(value, validator) {
			                                var options = validator.getFieldElements('reserved1').val();
			                                return (options * 1 <= 100 * 1);
			                            }
			                        }
			                    },
			                    
			                }, 
			                prophoneno: {
			                    validators: {
			                        notEmpty: {
			                            message: '联系方式为空'
			                        },
			                        regexp: {
			                            regexp: "^(13|15|18|17)[0-9]{9}$",
			                            message: '手机号格式不正确'
			                        }
			                    }
			                },
			                profee: {
			                    validators: {
			                        notEmpty: {
			                            message: '报名费为空'
			                        },
			                        regexp: {
			                            regexp: "^([+-]?)\\d*\\.?\\d+$",
			                            message: '请输入数字'
			                        }
			                    }
			                },
			                startdate: {
			                    validators: {
			                        notEmpty: {
			                            message: '开始日期为空'
			                        },
			                    }
		                	}, 
			                enddate: {
			                    validators: {
			                        notEmpty: {
			                            message: '结束日期为空'
			                        },
			                    }
			                }, 
			                perdate: {
			                    validators: {
			                        notEmpty: {
			                            message: '开展时间为空'
			                        },
			                        stringLength: {
			                            min: 3,
			                            max: 15,
			                            message: '长度在为3-15位'
			                        },
			                    }
			                }, 
			                reserved9: {
			                    validators: {
			                        notEmpty: {
			                            message: '结束时间为空'
			                        },
			                        stringLength: {
			                            min: 3,
			                            max: 15,
			                            message: '长度在为3-15位'
			                        },
			                    }
			                }, 
			                unit: {
			                    validators: {
			                        callback: {
			                            message: '请选择单位',
			                            callback: function(value, validator) {
			                                // Get the selected options
			                                var options = validator.getFieldElements('unit').val();
			                                return (options != null );
			                            }
			                        }
			                    }
			                }, 
			                protype: {
			                    validators: {
			                        callback: {
			                            message: '选择活动类型',
			                            callback: function(value, validator) {
			                                var options = validator.getFieldElements('protype').val();
			                                return (options != null );
			                            }
			                        }
			                    }
			                }, 
			                istop: {
			                    validators: {
			                        notEmpty: {
			                            message: '请选择是否精选',
			                        }
			                    }
			                },
			                remark: {
			                    validators: {
			                        notEmpty: {
			                            message: '活动简讯为空',
			                        },
			                        stringLength: {
			                            min: 3,
			                            max: 22,
			                            message: '简讯长度为3-22位',
			                        },
			                    }
			                },
			                warning: {
			                    validators: {
			                        notEmpty: {
			                            message: '温馨提示为空',
			                        },
			                        stringLength: {
			                            min: 3,
			                            max:200,
			                            message: '温馨提示长度为3-200位',
			                        },
			                    }
			                },
			                lightspot: {
			                    validators: {
			                        notEmpty: {
			                            message: '活动亮点为空',
			                        },
			                    }
			                },
			                address: {
			                    validators: {
			                        notEmpty: {
			                            message: '详细地址为空'
			                        },
			                    }
			                },
			                contents: {
			                    validators: {
			                        notEmpty: {
			                            message: '活动简讯为空',
			                        },
			                      
			                    }
			                },
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	
			            	var $contents = $("textarea[name='contents']");
			            	
			            	var len = $contents.length;
			            	if(len == 0 || len > 5){
			            		bootbox.alert({ 
									  size: "small",
									  title: "错误",
									  message: "请上传1-5份图文活动详情信息", 
									  callback: function(){return; }
								});
			            		validator.resetForm();
								return;
			            	}
			            	
			            	for(var i = 0 ; i < len;i++){
			            		var $obj = $($contents[i]);
			            		if($obj.val() == null || $obj.val() == ""){
				            		bootbox.alert({ 
										  size: "small",
										  title: "错误",
										  message: "请完善图文信息", 
										  callback: function(){return; }
									});
				            		
				            		validator.resetForm();
									return;
				            	}
			            	}
			            	
			            	validator.defaultSubmit();
			            }
			        });
			  	
			  	 var imsg = [];
				 var res = "${projectInfo.probill}".split(";");
				 for(var i = 0; i < res.length; i ++){
					 if(res[i] != null && res[i] != ""){
						 imsg.push("<img src='"+res[i]+"' class='file-preview-image defined' style='width:180px;height:120px;' />");
					 }
				 }
				 $("input[name='myfiles']").fileinput({
				        language: 'zh', //设置语言
				      	uploadUrl: "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
				        uploadAsync: true, //异步加载
				      	allowedFileExtensions : ['jpg', 'png'],//接收的文件后缀
				        showUpload: false, //是否显示上传按钮
				        showCaption: false,//是否显示标题
				        overwriteInitial: true,
				        maxFileSize: 1024 * 10,
				      //  maxFileCount: 1,
				        slugCallback: function(filename) {
				            return filename.replace('(', '_').replace(']', '_');
				        },
				       /*  minImageHeight:450,
				        maxImageHeight:450,
				        maxImageWidth:750, */
				        showPreview:true,
				        initialPreview: imsg,
				        allowedPreviewTypes : [ 'image' ],
				        browseClass: "btn btn-primary", //按钮样式             
				 });
				            
				$("input[name='myfiles']").on("fileuploaded", function (event, data, previewId, index) {  
					// JSON.stringify(data),分析data数据格式
					// alert(data.response.resName + "==" + previewId + "==" + index)
					var $input = " <input type='hidden' id='"+previewId+"' name='imagename' value='"+data.response.resName+"'/>";
					var $form = $("form");
					$form.append($input);
					
					$('form')
			        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
			        .bootstrapValidator('validateField', 'myfiles');
			    });  
				
				$("input[name='myfiles']").on("fileclear", function(event) {
					 var $imagenames = $("input[name='imagename']");
					   
					 for(var i = 0 ; i < $imagenames.length ; i ++){
						   $($imagenames[i]).remove();
					 }
					 
					 var $imgs = $(".defined");
					 for(var i = 0 ; i < $imgs.length ; i ++){
					    $($imgs[i]).remove();
					 }
					   
					$('form')
				        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
				        .bootstrapValidator('validateField', 'myfiles');
				 });
				
				$("input[name='myfiles").on('filereset', function(event) {
					  $('form')
				        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
				        .bootstrapValidator('validateField', 'myfiles');
				});
				
				$("input[name='myfiles'").on('filesuccessremove', function(event, id) {
					$("input[id='"+id+"']").remove();
					$('form')
				     .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
				     .bootstrapValidator('validateField', 'myfiles');
				});
				
			  	
			  	$('.form_date').datetimepicker({
			        language:  'zh-CN',
			        weekStart: 1,
			        todayBtn:  1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0,
					startDate:new Date(),
					
			 }).on('hide', function(ev){
			        $('form')
			        .bootstrapValidator('updateStatus', 'startdate', 'NOT_VALIDATED')
			        .bootstrapValidator('validateField', 'startdate');
			        
			        $('form')
			        .bootstrapValidator('updateStatus', 'enddate', 'NOT_VALIDATED')
			        .bootstrapValidator('validateField', 'enddate');
			    });
			 $('.form_time').datetimepicker({
			        language:  'zh-CN',
			        weekStart: 1,
			        todayBtn:  1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 1,
					minView: 0,
					maxView: 1,
					forceParse: 0,
					startDate:new Date(),
			 }).on('hide', function(ev){
			        $('form')
			        .bootstrapValidator('updateStatus', 'perdate', 'NOT_VALIDATED')
			        .bootstrapValidator('validateField', 'perdate');
			        
			        $('form')
			        .bootstrapValidator('updateStatus', 'reserved9', 'NOT_VALIDATED')
			        .bootstrapValidator('validateField', 'reserved9');
			        
			    });
		  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >有点功夫管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel" >活动管理</a></li>
		                   <li><a href="javascript:void(0)" class="active">编辑活动</a></li>
		                 </ul>
		                 <h3 class="page-header"> 编辑活动 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	活动信息编辑！
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/projectInfoController/save" enctype="multipart/form-data">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												<input type="hidden" name="minDate" value="" /> 
												<input type="hidden" name="maxDate" value="" /> 
											    <input type="hidden" name="titleFol" value="" /> 
											</div>
											
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${projectInfo.id }" />
											<div class="form-group">
												<label class="col-lg-3 control-label">活动海报<span style="color:gray">(750 x 400)</span>： <sup>*</sup></label>
												<div class="input-group col-lg-9">
													<input type="file" class="form-control file-loading" name="myfiles" multiple placeholder="请上传资源" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">活动标题： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<input type="text" class="form-control" name="title"  value="${projectInfo.title }" placeholder="请输入标题" />
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">活动类型： <sup>*</sup></label>
													<div class="input-group col-lg-5">
														<select class="multiselect" name="protype">
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label for="dtp_input2" class="col-md-3 control-label">起始日期： <sup>*</sup></label>
												<div class="input-group date form_date col-md-5"
													data-date="" data-date-format="yyyy-mm-dd"
													data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
													<input class="form-control" type="text" value="<fmt:formatDate value='${projectInfo.startdate }' pattern='yyyy-MM-dd'/>"
														readonly> <span class="input-group-addon"><span
														class="glyphicon glyphicon-remove"></span></span> <span
														class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span></span>
												</div>
												<label class="col-lg-3 control-label"></label>
												<div class="input-group col-lg-5">
													<input type="hidden" class="form-control" name="startdate" id="dtp_input2" value="<fmt:formatDate value='${projectInfo.startdate }' pattern='yyyy-MM-dd'/>" />
												</div>
											</div>
											<div class="form-group">
												<label for="dtp_input3" class="col-md-3 control-label">结束日期： <sup>*</sup></label>
												<div class="input-group date form_date col-md-5"
													data-date="" data-date-format="yyyy-mm-dd"
													data-link-field="dtp_input3" data-link-format="yyyy-mm-dd">
													<input class="form-control" type="text" value="<fmt:formatDate value="${projectInfo.enddate }"  pattern="yyyy-MM-dd"/>"
														readonly> <span class="input-group-addon"><span
														class="glyphicon glyphicon-remove"></span></span> <span
														class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span></span>
												</div>
												<label class="col-lg-3 control-label"></label>
												<div class="input-group col-lg-5">
													<input type="hidden" class="form-control" name="enddate" id="dtp_input3" value="<fmt:formatDate value="${projectInfo.enddate }" pattern="yyyy-MM-dd"/>" />
												</div>
											</div>
											
											<div class="form-group">
												<label for="dtp_input1" class="col-md-3 control-label">开始时间： <sup>*</sup></label>
												<div class="input-group date form_time col-md-5"
													data-date="" data-date-format="hh:ii"
													data-link-field="dtp_input1" data-link-format="hh:ii">
													<input class="form-control" type="text" value="${projectInfo.perdate }"
														readonly> <span class="input-group-addon"><span
														class="glyphicon glyphicon-remove"></span></span> <span
														class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span></span>
												</div>
												<label class="col-lg-3 control-label"></label>
												<div class="input-group col-lg-5">
													<input type="hidden" class="form-control" name="perdate" id="dtp_input1" value="${projectInfo.perdate }" />
												</div>
											</div>
											
											<div class="form-group">
												<label for="dtp_input3" class="col-md-3 control-label">结束时间： <sup>*</sup></label>
												<div class="input-group date form_time col-md-5"
													data-date="" data-date-format="hh:ii"
													data-link-field="dtp_input4" data-link-format="hh:ii">
													<input class="form-control" type="text" value="${projectInfo.reserved9 }"
														readonly> <span class="input-group-addon"><span
														class="glyphicon glyphicon-remove"></span></span> <span
														class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span></span>
												</div>
												<label class="col-lg-3 control-label"></label>
												<div class="input-group col-lg-5">
													<input type="hidden" class="form-control" name="reserved9" id="dtp_input4" value="${projectInfo.reserved9 }" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 联系人： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<input type="text" class="form-control" name="procontact" value="${projectInfo.procontact }" placeholder="请输入联系人" />
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">联系方式： <sup>*</sup></label>
													<div class="input-group col-lg-5">
														<input type="text" class="form-control" name="prophoneno" value="${projectInfo.prophoneno }" placeholder="请输入联系方式" />
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">省市区： <sup>*</sup></label>
													<div class="input-group col-lg-5">
														<select class="multiselect" name="province" >
														</select> &nbsp;&nbsp;-&nbsp;&nbsp;
														<select class="multiselect" name="city">
														</select>&nbsp;&nbsp;-&nbsp;&nbsp;
														<select class="multiselect" name="area">
														</select>
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">详细地址： <sup>*</sup></label>
													<div class="input-group col-lg-5">
														<input type="text" class="form-control" name="address" value="${projectInfo.address }" placeholder="请输入详细地址" />
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">最大人数： <sup>*</sup></label>
													<div class="input-group col-lg-5">
														<input type="number" class="form-control" name="reserved1" value="${projectInfo.reserved1 }" placeholder="请输入最大人数" />
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">报名费： <sup>*</sup></label>
													<div class="input-group col-lg-5">
														<input type="text" class="form-control" name="profee" value="${projectInfo.profee }" placeholder="请输入报名费" />
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">单位： <sup>*</sup></label>
													<div class="input-group col-lg-5">
														<select class="multiselect" name="unit">
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">是否置顶： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="input-group col-lg-5">
													<div class="radio-group">
														<c:if test="${projectInfo.istop}">
															<label>
																 <input type="radio" name="istop" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="istop" value="0" /> 否
															</label>
														</c:if>
														<c:if test="${!projectInfo.istop}">
															<label>
																 <input type="radio" name="istop" value="1" /> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="istop" value="0" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">是否推荐： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="input-group col-lg-5">
													<div class="radio-group">
														<c:if test="${projectInfo.reserved8}">
															<label>
																 <input type="radio" name="reserved8" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved8" value="0" /> 否
															</label>
														</c:if>
														<c:if test="${!projectInfo.reserved8}">
															<label>
																 <input type="radio" name="reserved8" value="1" /> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved8" value="0" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">是否独家： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="input-group col-lg-5">
													<div class="radio-group">
														<c:if test="${projectInfo.reserved7}">
															<label>
																 <input type="radio" name="reserved7" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved7" value="0" /> 否
															</label>
														</c:if>
														<c:if test="${!projectInfo.reserved7}">
															<label>
																 <input type="radio" name="reserved7" value="1" /> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved7" value="0" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">活动简讯：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<textarea class="form-control" rows="3" name="remark" >${projectInfo.remark }</textarea>
												</div>
											</div>
										   <div class="form-group">
												<label class="col-lg-3 control-label">活动亮点：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<textarea class="form-control" rows="3" name="lightspot" placeholder="请输入活动亮点,系统自动以•符号标识条目">${projectInfo.lightspot }</textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">温馨提示：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<textarea class="form-control" rows="3" name="warning" placeholder="请输入温馨提示,系统自动以•符号标识条目">${projectInfo.warning }</textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">活动详情：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
										 			<i class="fa fa-plus-circle fa-lg" onclick="addFile()"></i><a href="javascript:void(0)" onclick="addFile()">添加其他文件</a>
												</div>
											</div>
											<div id="more">
											  <c:forEach items="${details }" var="g">
													<div class="form-group detail">
														<label class="col-lg-3 control-label"> <i
															onclick="removeFile(this)"
															class="fa fa-minus-circle fa-lg"></i>
														</label>
														<div class="input-group col-lg-5"
															style="border:1px solid #b9c4d5">
															<input accept="image/*" type="file" value="${g.image }"
																name="files" placeholder="请上传资源" />
															<textarea class="form-control" rows="3" name="contents">${g.content }</textarea>
														</div>
													</div>
													<br />
												</c:forEach>
											</div>
											<div class="form-group">
												<div class="input-group col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
			<!--include fileinput.js  -->
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput.js"></script>
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
</body>
<script>
  $(function(){
	//ininDetail();
  });
function ininDetail(){
	/* var images = "${projectInfo.reserved11}".split(";");
	var _contents = "${projectInfo.reserved10}";
	
	var contents = _contents.split("$");
	for(var i = 0 ; i < images.length ; i++){
		if(images[i] != null && images[i] != ""){
			var tempContent = contents[i];//.replace("/b/g", '\r\n');
			var htmlField = '';
			htmlField += '<div class="form-group detail">';
			htmlField += '<label class="col-lg-3 control-label"> ';
			htmlField += '<i onclick="removeFile(this)" class="fa fa-minus-circle fa-lg"></i>';
			htmlField += '</label>';
			htmlField += '<div class="input-group col-lg-5" style="border:1px solid #b9c4d5" >';
			htmlField += '<input accept="image/*" type="file" value="'+images[i]+'"  name="files" placeholder="请上传资源" />';
			htmlField += '<textarea class="form-control" rows="3" name="contents" >'+tempContent+'</textarea>';
			htmlField += '</div>';
			htmlField += '</div><br/>';
			
			$("#more").append(htmlField);
		}
	} */
}	 
function addFile(){
	var htmlField = '';
	htmlField += '<div class="form-group detail">';
	htmlField += '<label class="col-lg-3 control-label"> ';
	htmlField += '<i onclick="removeFile(this)" class="fa fa-minus-circle fa-lg"></i>';
	htmlField += '</label>';
	htmlField += '<div class="input-group col-lg-5" style="border:1px solid #b9c4d5" >';
	htmlField += '<input accept="image/*" type="file"  name="files" placeholder="请上传资源" />';
	htmlField += '<textarea class="form-control" rows="3" name="contents" ></textarea>';
	htmlField += '</div>';
	htmlField += '</div><br/>';
	
	$("#more").append(htmlField);
	
}
function removeFile(obj){
	var $div = $(obj).parent().parent();
	$($div).remove();
}
</script>
</html>