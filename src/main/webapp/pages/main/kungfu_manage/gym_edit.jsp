<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-编辑场馆</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style type="text/css">
			.modal {
				margin-top: -500px;
			}
		</style>
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		<!--include fileinput.css  -->
	  	<link href="${pageContext.request.contextPath}/css/fileinput.min.css" rel="stylesheet">
		<!-- 样式冲突 -->
	    <!-- <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
	  	<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
	  	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<!--include wysiwyg.js  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-wysiwyg.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.hotkeys.min.js"></script>
	  	<!--导入省市区JSON数据  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/ProJson.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/DistrictJson.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/CityJson.js"></script>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var pageSize = QueryString.GetValue("pageSize");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var minDate = QueryString.GetValue("minDate");
		  		var maxDate = QueryString.GetValue("maxDate");
		  		var gymName = QueryString.GetValue("gymName");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='pageSize']").val(pageSize);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		$("input[name='minDate']").val(minDate);
		  		$("input[name='maxDate']").val(maxDate);
		  		$("input[name='gymName']").val(gymName);
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/mstGymnasiumController/list" + reqPara; 
		  		});
		  		
		  		$.ajax({
					url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
					data : {parentId:"134"},
					type : "GET",
					async : true,
					dataType : "json",
					success : function(data) {
						 var $gymtype = $("select[name='gymtype']");
						 
						 var gymtype = "${mstGymnasium.gymtype}";
						 $.each(data, function (idx, ele) {
							 if(gymtype == ele.id){
								 $gymtype.append("<option value='"+ele.id+"' selected='selected'>"+ele.name+"</option>");
							 } else {
								 $gymtype.append("<option value='"+ele.id+"'>"+ele.name+"</option>");
							 }
							 
					     });
						 
						 $gymtype.multiselect();
					},
					error : function(data) {
						bootbox.alert({ 
							  size: "small",
							  title: "错误",
							  message: "请求超时，请重试！", 
							  callback: function(){return; }
						});
						return;
					}
			   });	
		  		
		  		// 加载省信息
			  	var $province = $("select[name='province']");
			  	$province.append("<option value='0'>请选择</option>");
			  	$.each(province, function(k, p) {
			  			 var proId = "${mstGymnasium.province}";
			  			 if(proId == p.ProID){
			  				 option = "<option  value='"+p.ProID+"' selected='selected'>"+p.ProName+"</option>";
			  			 } else {
			  				 option = "<option  value='"+p.ProID+"'>"+p.ProName+"</option>";
			  			 }
			  			$province.append(option);
			  	});
			  	
			  	var $city = $("select[name='city']");
			  	$city.append("<option value='0'>请选择</option>");
		    	$.each(city, function(k, p) {
		    		var provinceId = "2";
		    		provinceId = "${mstGymnasium.province}";
		    		var cityId = "${mstGymnasium.city}";
					var option = "";
					if(provinceId == p.ProID){
						if(p.CityID == cityId){
							 option = "<option value='"+p.CityID+"' selected='selected'>"+p.CityName+"</option>";
						 } else {
							 option = "<option value='"+p.CityID+"'>"+p.CityName+"</option>";
						 }
					}
					
					$city.append(option);
				});
		    	
		    	var $area = $("select[name='area']");
		    	$.each(District, function(k, p) {
		    		var cityId = "3";
		    		cityId = "${mstGymnasium.city}";
		    		var areaId = "${mstGymnasium.area}";
		    		$area.append("<option value='0'>请选择</option>");
		    		
		    		var option = "";
		    		if(p.CityID == cityId){
		    			option = "<option value='"+p.Id+"'>"+p.DisName+"</option>";
						if(p.Id == areaId){
							 option = "<option value='"+p.Id+"' selected='selected'>"+p.DisName+"</option>"; 
						}
		    		}
					
					$area.append(option);
				}); 
		    	
		  		$.ajax({
						url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
						data : {parentId:"14"},
						type : "GET",
						async : true,
						dataType : "json",
					    beforeSend: function() {
					        //请求前的处理
					    },
						success : function(data) {
							
							$province.multiselect({
							      onChange: function(element, checked) {
							    	if(checked){
							    		var proId = $province.val();
							    		 //市数据加载
							    		var cityHtml = "<option value='0'>请选择</option>";
						                var areaHtml = "<option value='0'>请选择</option>";
						    		    $.each(city, function(k, p) {
						    				if (p.ProID == proId) {
						    					cityHtml += "<option value='"+p.CityID+"'>"+p.CityName+"</option>";
						    				}
						    			});
						    		    $city.html(cityHtml);
						    		    $area.html(areaHtml);
						    		    //刷新市、区下拉列表
						    		    $city.multiselect('rebuild');
						    		    $area.multiselect('rebuild');
					                }
							      },
							      maxHeight: 200,
							      filterPlaceholder: 'Search'
							});
							
							$city.multiselect({
							      onChange: function(element, checked) {
							    	if(checked){
							    		var cityId = $city.val();
							    		 //市数据加载
						                var areaHtml = "<option value='0'>请选择</option>";
						                $.each(District, function(k, p) {
											if (p.CityID == cityId) {
												areaHtml += "<option value='"+p.Id+"'>"+p.DisName+"</option>";
											}
										});
						    		    $area.html(areaHtml);
						    		    //刷新区下拉列表
						    		    $area.multiselect('rebuild');
					                }
							      },
							      maxHeight: 200
							});
							
							$area.multiselect({
							      onChange: function(element, checked) {
							      },
							      maxHeight: 200
							});
							
						},
						error : function(data) {
							bootbox.alert({ 
								  size: "small",
								  title: "错误",
								  message: "请求超时，请重试！", 
								  callback: function(){return; }
							});
							return;
						}
				   });	
		  
			  	$('form').bootstrapValidator({
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            gymtype: {
		                    validators: {
		                        callback: {
		                            message: '请选择场馆类型',
		                            callback: function(value, validator) {
		                                var options = validator.getFieldElements('gymtype').val();
		                                return (options != null );
		                            }
		                        }
		                    }
		                }, 
			            fields: {
			            	myfiles: {
			            		  validators: {
			                          file: {
			                              extension: 'jpeg,png,jpg',
			                              type: 'image/jpeg,image/png,image/jpg',
			                              maxSize: 2048 * 1024,   // 2 MB
			                              message: '文件格式非法，请重新上传'
			                          },
			                          callback: {
				                            message: '请上传文件',
				                            callback: function(value, validator) {
				                                // Get the selected options
				                                
				                                var $imagename = $("input[name='imagename']");
				                                var imgs = $(".defined");
				                                return ($imagename.val() != undefined || imgs.length != 0);
				                            }
				                     }
			                      }
				            },
			                gymname: {
			                    validators: {
			                        notEmpty: {
			                            message: '场馆名称为空'
			                        },
			                        stringLength: {
			                            min: 3,
			                            max: 20,
			                            message: '长度在为3-20位'
			                        },
			                    }
			                },
			               /*  gymcontact: {
			                    validators: {
			                        notEmpty: {
			                            message: '联系人为空'
			                        },
			                    }
			                }, 
			                gymphone: {
			                    validators: {
			                        notEmpty: {
			                            message: '联系方式为空'
			                        },
			                        regexp: {
			                            regexp: "^(13|15|18|17)[0-9]{9}$",
			                            message: '手机号格式不正确'
			                        }
			                    }
			                }, */
			                openinghours: {
			                    validators: {
			                        notEmpty: {
			                            message: '场馆营业时间为空'
			                        },
			                    }
			                },
			               /*  startdate: {
			                    validators: {
			                        notEmpty: {
			                            message: '开始时间为空'
			                        },
			                    }
			                }, 
			                enddate: {
			                    validators: {
			                        notEmpty: {
			                            message: '结束时间为空'
			                        },
			                    }
			                },  */
			                istop: {
			                    validators: {
			                        notEmpty: {
			                            message: '请选择是否精选'
			                        }
			                    }
			                },
			                address: {
			                    validators: {
			                        notEmpty: {
			                            message: '详细地址为空'
			                        },
			                    }
			                },
			                remark: {
			                    validators: {
			                        notEmpty: {
			                            message: '场馆简讯为空'
			                        },
			                        stringLength: {
			                            min: 3,
			                            max: 200,
			                            message: '简讯长度为3-200位'
			                        },
			                    }
			                },
			                content: {
			                    validators: {
			                        notEmpty: {
			                            message: '场馆内容为空'
			                        },
			                        stringLength: {
			                            min: 3,
			                            max: 200,
			                            message: '简讯长度为3-500位'
			                        },
			                    }
			                },
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	
			            	validator.defaultSubmit();
			            }
			        });
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >有点功夫管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel" >场馆管理</a></li>
		                   <li><a href="javascript:void(0)" class="active">编辑场馆</a></li>
		                 </ul>
		                 <h3 class="page-header"> 编辑场馆 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	场馆信息编辑！
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/mstGymnasiumController/save">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="pageSize" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												<input type="hidden" name="minDate" value="" /> 
												<input type="hidden" name="maxDate" value="" /> 
											    <input type="hidden" name="gymName" value="" /> 
											</div>
											
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${mstGymnasium.id }" />
											<div class="form-group">
												<label class="col-lg-3 control-label">场馆海报<span style="color:gray">(750 x 400)： <sup>*</sup></label>
												<div class="col-lg-9">
													<input type="file" class="form-control file-loading" name="myfiles" multiple placeholder="请上传资源" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">场馆类型： <sup>*</sup></label>
												<div class="col-lg-5">
													<select class="multiselect" name="gymtype"></select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">场馆名称： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="gymname"  value="${mstGymnasium.gymname }" placeholder="请输入场馆名称" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">场馆营业时间： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="openinghours"  value="${mstGymnasium.openinghours }" placeholder="请输入场馆营业时间" />
												</div>
											</div>
											<%-- <div class="form-group">
												<label class="col-lg-3 control-label">起始时间： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="datetime" class="form-control" name="startdate"  value='<fmt:formatDate value="${mstGymnasium.startdate }" type="date"/>' placeholder="请输入起始时间" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">结束时间： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="datetime" class="form-control" name="enddate"  value='<fmt:formatDate value="${mstGymnasium.enddate }" type="date"/>' placeholder="请输入结束时间" />
												</div>
											</div> --%>
											<%-- <div class="form-group">
												<label class="col-lg-3 control-label"> 联系人： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="gymcontact" value="${mstGymnasium.gymcontact }" placeholder="请输入联系人" />
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">联系方式： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="gymphone" value="${mstGymnasium.gymphone }" placeholder="请输入联系方式" />
													</div>
												</div>
											</div> --%>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">省市区： <sup>*</sup></label>
													<div class="col-lg-5">
														<select class="multiselect" name="province" >
														</select> &nbsp;&nbsp;-&nbsp;&nbsp;
														<select class="multiselect" name="city">
														</select>&nbsp;&nbsp;-&nbsp;&nbsp;
														<select class="multiselect" name="area">
														</select>
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">详细地址： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="address" value="${mstGymnasium.address }" placeholder="请输入详细地址" />
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">是否置顶： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<c:if test="${mstGymnasium.istop}">
															<label>
																 <input type="radio" name="istop" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="istop" value="0" /> 否
															</label>
														</c:if>
														<c:if test="${!mstGymnasium.istop}">
															<label>
																 <input type="radio" name="istop" value="1" /> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="istop" value="0" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">是否推荐： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<c:if test="${mstGymnasium.reserved8}">
															<label>
																 <input type="radio" name="reserved8" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved8" value="0" /> 否
															</label>
														</c:if>
														<c:if test="${!mstGymnasium.reserved8}">
															<label>
																 <input type="radio" name="reserved8" value="1" /> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved8" value="0" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div>
											<%-- <div class="form-group">
												<label class="col-lg-3 control-label">是否独家： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<c:if test="${mstGymnasium.reserved7}">
															<label>
																 <input type="radio" name="reserved7" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved7" value="0" /> 否
															</label>
														</c:if>
														<c:if test="${!mstGymnasium.reserved7}">
															<label>
																 <input type="radio" name="reserved7" value="1" /> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved7" value="0" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div> --%>
											<div class="form-group">
												<label class="col-lg-3 control-label">场馆简讯：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
														<textarea class="form-control" rows="3" name="remark" >${mstGymnasium.remark }</textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">内容：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
														<textarea class="form-control" rows="7" name="content" >${mstGymnasium.content }</textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
			<!--include fileinput.js  -->
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput.js"></script>
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
</body>
<script>
  $(function(){
	 var imsg = [];
	 var res = "${mstGymnasium.gymbill}".split(";");
	 for(var i = 0; i < res.length; i ++){
		 if(res[i] != null && res[i] != ""){
			 imsg.push("<img src='"+res[i]+"' class='file-preview-image defined' style='width:180px;height:120px;' />");
		 }
		
	 }
	 $("input[name='myfiles']").fileinput({
	        language: 'zh', //设置语言
	      	uploadUrl: "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
	        uploadAsync: true, //异步加载
	      	allowedFileExtensions : ['jpg', 'png','gif'],//接收的文件后缀
	        showUpload: false, //是否显示上传按钮
	        showCaption: false,//是否显示标题
	        overwriteInitial: true,
	        maxFileSize: 1024 * 10,
	        //maxFileCount: 1,
	        slugCallback: function(filename) {
	            return filename.replace('(', '_').replace(']', '_');
	        },
	      /*   minImageHeight:450,
	        maxImageHeight:450,
	        maxImageWidth:750, */
	        showPreview:true,
	        initialPreview: imsg,
	        allowedPreviewTypes : [ 'image' ],
	        browseClass: "btn btn-primary", //按钮样式             
	 });
	            
	 $("input[name='myfiles']").on("fileuploaded", function (event, data, previewId, index) {  
		// JSON.stringify(data),分析data数据格式
		 //alert(data.response.resName + "==" + previewId + "==" + index)
		var $input = " <input type='hidden' id='"+previewId+"' name='imagename' value='"+data.response.resName+"'/>";
		var $form = $("form");
		$form.append($input);
		
		 $('form')
	        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'myfiles');
     }); 
	 
	$("input[name='myfiles']").on("fileclear", function(event) {
		   var $imagenames = $("input[name='imagename']");
		   
		   for(var i = 0 ; i < $imagenames.length ; i ++){
			   $($imagenames[i]).remove();
		   }
		   
		   var $imgs = $(".defined");
		   for(var i = 0 ; i < $imgs.length ; i ++){
			    $($imgs[i]).remove();
		   }
		   
		   $('form')
	        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'myfiles');
	 });
	
	$("input[name='myfiles").on('filereset', function(event) {
		  $('form')
	        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'myfiles');
	});
	
	$("input[name='myfiles'").on('filesuccessremove', function(event, id) {
		$("input[id='"+id+"']").remove();
		$('form')
	     .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	     .bootstrapValidator('validateField', 'myfiles');
	});

  });
</script>
</html>