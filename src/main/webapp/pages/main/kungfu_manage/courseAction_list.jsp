<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-课程配置动作</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		

<jsp:include page="../../fragment/frag_top_css.jsp" />
<!-- 样式冲突 -->
<!--include fileinput.css  -->
<link href="${pageContext.request.contextPath}/css/fileinput.min.css"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
	rel="stylesheet">
<jsp:include page="../../fragment/frag_top_js.jsp" />

<!--include fileinput.js  -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/fileinput.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
	  	<script type="text/javascript">
	  	var paras = "";
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             					cache: true, // 设置为 true 禁用 AJAX 数据缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				              /*   pagination: true, // 开启分页功能
				               // singleSelect: false,
				                pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500], */
				                search: false, // 关闭搜索功能
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                queryParams: function (params) {
				                	//排序，并处理多表中的属性
				                	var sort = params.sort;
				                	if(sort == "rank" ){
				                		sort == "u." + sort;
				                	}
				                	var obj = {
					                        sort:sort,
							                order:params.order, 
					                        courseId:$("input[name='courseId']").val()
					                    };
				                	return obj;
				                },
				                url: "${pageContext.request.contextPath}/courseActionController/queryList",
				                columns: [{
				                    field: 'state',
				                    checkbox: true
				                }, {
				                    field: 'courseTitle',
				                    title: '课程名称',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                },{
				                    field: 'rank',
				                    title: '节点顺序',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                }, {
				                    field: 'actionId',
				                    title: '动作序号',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                },{
				                    field: 'actionTitle',
				                    title: '动作名称',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                }, 
				                {
				                    field: 'videoName',
				                    title: '视频名字',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                }, 
				                {
				                    field: 'restTime',
				                    title: '距下一个动作间隔',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                },
				                 {
				                      title: '操作',
				                      field: 'id',
				                      align: 'center',
				                      events: operateEvents,
				                      formatter:function(value,row,index){  
						                   var eles = '<button class="btn btn-danger btn-xs delList"><i class="fa fa-trash-o"></i></button>&nbsp;<button class="btn btn-success btn-xs editObj"><i class="fa fa-edit"></i></button>  '; 
						                   return eles ;
				                      } 
				                 }],
				                onLoadSuccess:function(data){
				                	var rows = $table.bootstrapTable('getData');
				                	$table.bootstrapTable('updateCell', {index:0, field: 'totalTime', value: data.totalTime});
				                  	$table.bootstrapTable('mergeCells', {index:0, field: 'totalTime', colspan: 1, rowspan: rows.length});
				                },
				                onLoadError: function () {
				             	   //    mif.showErrorMessageBox("数据加载失败！");
				                }
            });
        }
	  	
		window.operateEvents = {
	  	        'click .delList': function (e, value, row, index) {
	  	        	 $.ajax({
	  					url : "${pageContext.request.contextPath}/courseActionController/delList",
	  					data : {ids:row.id},
	  					type : "POST",
	  					async : true,
	  					dataType : "json",
	  					success : function(flag) {
	  						if(flag){
	  							bootbox.alert({ 
	  	 							  size: "small",
	  	 							  title: "正确",
	  	 							  message: "操作成功！", 
	  	 							  callback: function(){
	  	 								  $table.bootstrapTable('refresh');
	  	 								  return;
	  	 							 }
	  	 						});
	  						}else{
	  							bootbox.alert({ 
	 	 							  size: "small",
	 	 							  title: "错误",
	 	 							  message: "操作失败！", 
	 	 							  callback: function(){return; }
	 	 						});
	  						}
	  					},
	  					error : function(data) {
	  						bootbox.alert({ 
	  							  size: "small",
	  							  title: "错误",
	  							  message: "请求超时，请重试！", 
	  							  callback: function(){return; }
	  						});
	  						return;
	  					}
	  			   });	
	  	        },
	  	      'click .editObj': function (e, value, row, index) {
	            	editObj(row);
	          },
	  	};
	  	
   		$(function(){
   			initTable();
   		 	
   		  	$('form').bootstrapValidator({
	            excluded: ':disabled',
	            feedbackIcons: {
	            	 valid: 'fa fa-check',
	            	 invalid: 'fa fa-times',
	            	 validating: 'fa fa-refresh'
	            },
	            fields: {
	                rank: {
	                    validators: {
	                        notEmpty: {
	                            message: '排序号为空'
	                        },
	                        regexp: {
	                            regexp: "^[1-9]\\d*$",
	                            message: '请输入正整数'
	                        },
	                        callback: {
	                            message: '输入格式不对，或顺序重复',
	                            callback: function(value, validator) {
	                            	
	                            	var flag = true;
	                            	
	                            	var id = $("input[name='id']").val();
	                            	var rank = $("input[name='rank']").val();
	                            	if(id != "" && rank != ""){//更新
	                            		return flag;
	                            	}else{
	                            		var rows = $table.bootstrapTable('getData');
	                            		$(rows).each(function(index,element){
	                            			if(value == element.rank){
	                            				flag = false;
	                            			}
	                            		});
	                            		
	                            		return flag;
	                            	}
                            		
                            		
	                            }
	                        }
	                    }
	                },
	                actionId: {
	                    validators: {
	                        notEmpty: {
	                            message: '动作序号为空'
	                        },
	                    } ,
	                   
	                }, 
	                restTime: {
	                    validators: {
	                        notEmpty: {
	                            message: '距下一个动作播放间隔时间为空'
	                        },
	                    } ,
	                }, 
	            },
	        });
   		  	
   		 $('#btn_save_single').submit(function (ev) {
             ev.preventDefault();
         });
   		 
         $('#btn_save_single').on("click", function () {
             var bootstrapValidator = $('form').data('bootstrapValidator');
             bootstrapValidator.validate();
			
             if (bootstrapValidator.isValid()) {
            	 $.ajax({
 					url : "${pageContext.request.contextPath}/courseActionController/save",
 					data : $("form").serialize(),
 					type : "POST",
 					async : true,
 					dataType : "json",
 					success : function(flag) {
 						if(flag){
 							bootbox.alert({ 
 	 							  size: "small",
 	 							  title: "正确",
 	 							  message: "操作成功！", 
 	 							  callback: function(){
 	 								  $table.bootstrapTable('refresh');
 	 								  $("input[name='rank']").val("");
 	 								  $("input[name='actionId']").val("");
 	 								  $("input[name='restTime']").val("");
 	 								  $("input[name='id']").val("");
 	 								  return;
 	 							 }
 	 						});
 						}else{
 							bootbox.alert({ 
	 							  size: "small",
	 							  title: "错误",
	 							  message: "操作失败！", 
	 							  callback: function(){return; }
	 						});
 						}
 					},
 					error : function(data) {
 						bootbox.alert({ 
 							  size: "small",
 							  title: "错误",
 							  message: "请求超时，请重试！", 
 							  callback: function(){return; }
 						});
 						return;
 					}
 			   });	
             } else {
                 return;
             }
         });
         
     	$(".cancel").click(function(){
     		
  			var reqPara = "?" + $(".folPara").find("input").serialize();
  			reqPara = decodeURIComponent(reqPara,true);
  			window.location.href="${pageContext.request.contextPath}/onlineCourseController/list" + reqPara; 
  		});
     	
     	$("#btn_cancel_single").click(function(){
     		$("input[name='id']").val("");
     		
     		 $("input[name='rank']").val("");
			 $('form')
		     .bootstrapValidator('updateStatus', 'rank', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'rank');
			
			 $("input[name='actionId']").val("");
			 $('form')
		     .bootstrapValidator('updateStatus', 'actionId', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'actionId');
			 
			 $("input[name='restTime']").val("");
			 $('form')
		     .bootstrapValidator('updateStatus', 'restTime', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'restTime');
			 
     	});
   	});
   	function editObj(row){
   		$("input[name='rank']").val(row.rank);
		$("input[name='actionId']").val(row.actionId);
		$("input[name='restTime']").val(row.restTime);
		$("input[name='id']").val(row.id);	
   	}
   </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     		
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>有点功夫管理</span></li>
                   <li><a href="javascript:void(0)" class="active cancel">在线课程</a></li>
                   <li><a href="javascript:void(0)" class="active">在线课程配置动作</a></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
 					 </div>
                </div>

                <h3 class="page-header"> 在线课程配置动作 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

                <blockquote class="page-information hidden">
                 <p>
                  	更新、维护在线课程配置动作信息！
                </p>
              </blockquote>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
					<div class="ro">
						<div class="col-mol-md-offset-2">
							<form method="post" class="form-horizontal" action="">

								<!--附件参数  -->
								<div class="folPara" style="display: none">
									<input type="hidden" name="menuId" value="${folPara.menuId }" />
									<input type="hidden" name="page" value="${folPara.page }" /> 
									<input type="hidden" name="pageSize" value="${folPara.pageSize }" />
									<input type="hidden" name="index_exp" value="${MENU_STYLE_INDEX_EXP }" />
									<input type="hidden" name="index_che" value="${MENU_STYLE_INDEX_CHE }" />
								</div>
								<!-- 逻辑参数 -->
								<input type="hidden" name="id" value="" />
								<input type="hidden" name="courseId" value="${folPara.courseId }" />
								<div class="form-group">
									<label class="col-lg-3 control-label">动作播放顺序： <sup>*</sup></label>
									<div class="col-lg-5">
										<input type="number" min="1" class="form-control" name="rank"
											value="" placeholder="请输入正整数" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">播放动作序号： <sup>*</sup></label>
									<div class="col-lg-5">
										<input class="form-control" name="actionId" placeholder="请输入动作的序号" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">动作距下一个间隔时间： <sup>*</sup></label>
									<div class="col-lg-5">
									   <div class="input-group">
										<input class="form-control" name="restTime" placeholder="请输入动作距下一个间隔时间" value="0"/>
										<span class="input-group-addon">秒</span>
									    </div>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-lg-9 col-lg-offset-3">
										<button id="btn_save_single" type="button"
											class="btn btn-primary btn-xs">
											<i class="fa fa-check fa-lg"></i>保存
										</button>
										&nbsp;&nbsp;&nbsp;
										<button id="btn_cancel_single" type="button"
											class="btn btn-primary btn-xs">
											<i class="fa fa-remove fa-lg"></i>取消
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<table id="table">
				    </table>
		    </div>
		    <input type="button" class="btn btn-primary" onclick="javascript:history.back(-1);" value="返回上一页">
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>