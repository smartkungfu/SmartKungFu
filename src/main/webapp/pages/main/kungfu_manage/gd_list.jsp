<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-课程管理</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
	  	var paras = "";
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             					//cache: false, // 不缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				                pagination: true, // 开启分页功能
				               // singleSelect: false,
				                pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500],
				                search: false, // 关闭搜索功能
				                detailView:true,
				                detailFormatter:function detailFormatter(index, row) {
				                	  var html = [];
				                	  var tableHtml = "";
				                	  var tableHtml2 = "";
				                	  
				                      html.push('<p><b>课程类型:</b> ' + row.coursetypeName + '</p>'); 
				                      html.push('<p><b>教练:</b> ' + row.coach + '</p>'); 
				                      html.push('<p><b>教练描述:</b> ' + row.coachcontent + '</p>'); 
				                     /*  html.push('<p><b>教龄:</b> ' + row.workage + '</p>');  */
				                      html.push('<p><b>课程限制人数:</b> ' + row.limitnum + '</p>'); 
				                      html.push('<p><b>开课时间:</b> ' + row.learningtime + '</p>');
				                      html.push('<p><b>联系电话:</b> ' + row.phoneno + '</p>');
				                      
				                      var len = row.cps.length;
				                      if(row.cps.length != 0){
				                    	  tableHtml += '<table  class="table table-bordered table-hover table-striped display" id="example" >';
					                      tableHtml += '<thead>';
					                      tableHtml += '<tr>';
					                      tableHtml += '<th>套餐ID</th>';
					                      tableHtml += '<th>套餐名称</th>';
					                      tableHtml += '<th>套餐价格</th>';
					                      tableHtml += '<th>创建时间</th>';
					                      tableHtml += '<th>创建者</th>';
					                      tableHtml += '</tr>';
					                      tableHtml += '</thead>';
					                      tableHtml += '<tbody>';
					                      for(var i = 0 ; i < len ; i ++){
					                    	  tableHtml += '<tr>';
						                      tableHtml += '<td class="center">'+row.cps[i].id+'</td>';
						                      tableHtml += '<td class="center"><span class="label bg-info">'+row.cps[i].packagetypeName+'</span></td>';
						                      tableHtml += '<td class="center">'+row.cps[i].price.toFixed(2) + '</td>';
						                      tableHtml += '<td class="center">'+ timeStamp2String(row.cps[i].createdate)+'</td>';
						                      tableHtml += '<td class="center"><span class="badge bg-warning">'+row.cps[i].cUName+'</span></td>';
						                      tableHtml += '</tr>';
					                      }
					                      tableHtml += '</tbody>';
					                      tableHtml += '</table>';
				                      }else{
				                    	  tableHtml = '<p><b>无套餐信息</b></p>';
				                      }
				                      
				                      len = row.rolls.length;
				                      if(len != 0){
				                    	  tableHtml2 += '<table  class="table table-bordered table-hover table-striped display" id="example" >';
					                      tableHtml2 += '<thead>';
					                      tableHtml2 += '<tr>';
					                      tableHtml2 += '<th>优惠券标题</th>';
					                      tableHtml2 += '<th>优惠券内容</th>';
					                      tableHtml2 += '<th>开始时间</th>';
					                      tableHtml2 += '<th>结束时间</th>';
					                      tableHtml2 += '<th>创建者</th>';
					                      tableHtml2 += '<th>操作</th>';
					                      tableHtml2 += '</tr>';
					                      tableHtml2 += '</thead>';
					                      tableHtml2 += '<tbody>';
					                      for(var i = 0 ; i < len ; i ++){
					                    	  tableHtml2 += '<tr>';
					                    	  tableHtml2 += '<td class="center">'+row.rolls[i].title+'</td>';
					                    	  tableHtml2 += '<td class="center"><span class="label bg-info">'+row.rolls[i].content+'</span></td>';
					                    	  tableHtml2 += '<td class="center">'+ timeStamp2String(row.rolls[i].startdate)+'</td>';
						                      tableHtml2 += '<td class="center">'+ timeStamp2String(row.rolls[i].enddate)+'</td>';
						                      tableHtml2 += '<td class="center"><span class="badge bg-warning">'+row.rolls[i].cUName+'</span></td>';
						                      tableHtml2 += '<td class="center"><button type="button" class="btn btn-primary btn-xs" onclick="editRoll('+row.rolls[i].id+')">修改</button>  <button type="button" class="btn btn-primary btn-xs" onclick="delRoll('+row.rolls[i].id+')">删除</button></td>';
						                      tableHtml2 += '</tr>';
					                      }
					                      tableHtml2 += '</tbody>';
					                      tableHtml2 += '</table>';
				                      }else{
				                    	  tableHtml2 = '<p><b>无优惠券信息</b></p>'
				                      }
				                      
				                      return html.join('') + tableHtml + tableHtml2;
				    	   		},
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                //smartDisplay: true, // 智能显示 pagination 和 cardview 等
				                queryParams: function (params) {
									var page = params.offset;
				                	
				                	if((page * 1) != 0 ){
				            			page = page/params.limit + 1;
				            		}
				                	
				                	//排序，并处理多表中的属性
				                	var sort = params.sort;
				                	if(sort == "cUName"){
				                		sort == "u." + sort;
				                	} else if(sort == "gymName"){
				                		sort == "gym" + sort;
				                	} else if(sort == "coursetypeName"){
				                		sort == "c1." + sort;
				                	} else{
				                		sort == "gd." + sort;
				                	}
				                	var obj = {
					                        pageSize: params.limit,
					                        page:page,
					                        sort:sort,
					                        order:params.order, 
					                        minDate:$("#minDate").val(),
					                        maxDate:$("#maxDate").val(),
					                        gymName:$("#gymName").val(),
					                    };
				                	paras =  "pageSize=" + obj.pageSize + "&page=" + obj.page ;
				                	return obj;
				                },
				                url: "${pageContext.request.contextPath}/gymnasiumDetailController/queryList",
				                columns: [{
				                    field: 'state',
				                    checkbox: true
				                },  {
				                    field: 'coursebill',
				                    title: '图片',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){ 
				                    	if(value != null){
				                    		return "<img height='35px' width='50px' src='"+value.split(";")[0]+"' alt='' class='img'/> ";
				                    	} else {
				                    		return "<img height='35px' width='50px' src='' alt='' class='img'/> ";
				                    	}
				                    	
				                    }
				                }, {
				                    field: 'gymName',
				                    title: '场馆名称',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true
				                },{
				                    field: 'coursename',
				                    title: '课程名称',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                }, {
				                    field: 'coursetypeName',
				                    title: '课程类型',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                }, {
				                    field: 'pricerang',
				                    title: '价格范围',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                }, {
				                    field: 'crowd',
				                    title: '针对人群',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                },{
				                    field: 'courseresult',
				                    title: '课程目的',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                }, {
				                    field: 'reserved8',
				                    title: '是否推荐',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	if(value){
				                    		return "是";
				                    	} else {
				                    		return "否";
				                    	}
				                    }
				                },{
				                    field: 'createdate',
				                    title: '注册时间',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return timeStamp2String(value);
				                    }
				                }, {
				                    field: 'cUName',
				                    title: '创建者',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true
				                },/* {
				                      title: '操作',
				                      field: 'id',
				                      align: 'center',
				                      events: operateEvents,
				                      formatter:function(value,row,index){  
						                   var eles = '<button class="btn btn-primary btn-xs detail"><i class="fa fa-ellipsis-h"></i></button> '; 
						                   return eles ;
				                      } 
				                 } */],
				                onLoadSuccess:function(){
				                },
				                onLoadError: function () {
				             	   //    mif.showErrorMessageBox("数据加载失败！");
				                }
            });
        }
	  	window.operateEvents = {
	  	        'click .detail': function (e, value, row, index) {
		  	      	paras = "id="+ row.id + "&"+paras + "&"+  $("form").find("input").serialize();
					paras = decodeURIComponent(paras,true);
					window.location.href = "${pageContext.request.contextPath}/gymnasiumDetailController/info" + "?"+ paras;
	  	        },
	  	        /* 'click .remove': function (e, value, row, index) {
	  	        	delList({ids:row.id},'${pageContext.request.contextPath}/mstUserController/delList',$table);
	  	        } */
	  	};
	  	
	  	function addObj(url){
	  		paras = paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
			paras = decodeURIComponent(paras,true);
			window.location.href = url + "?"+ paras;
	  	}
	  	
	  	function delList(url){
	  		var rows = $table.bootstrapTable('getSelections');
			var ids = "";
			$(rows).each(function(index,element){
				ids += element.id + ",";
			});
			ids = ids.substring(0,ids.length - 1);
			delListAsUtils({ids:ids},url,$table);
	  	}
	  	
	  	function editObj(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择编辑行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条编辑数据", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "id=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	  	
   		$(function(){
   			initTable();
   			
   			$("#btn_search").click(function(){
   				$table.bootstrapTable('refresh');
   			});
   		 
   		});
   		
   		//添加优惠券
   		function addRoll(url){
		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择添加行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条添加数据", 
					  callback: function(){return; }
				});
	  		} else {
		  		paras = paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}&type=124&resourceid="+rows[0].id;
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
   		
   		//修改优惠券
	  	function editRoll(id){
  			paras = "index_exp=${MENU_STYLE_INDEX_EXP}"+"&index_che=${MENU_STYLE_INDEX_CHE}"+"&id=" + id + "&" +paras + "&"+  $("form").find("input").serialize()+"&type=124";
			paras = decodeURIComponent(paras,true);
			window.location.href = "${pageContext.request.contextPath}/mstRollController/edit" + "?"+ paras;
	  	}
   		
	  	//删除优惠券
	  	function delRoll(id){
			url = "${pageContext.request.contextPath}/mstRollController/delList" + "?"+ paras;
			delListAsUtils({ids:id},url,$table);
	  	}
	    </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>课程管理</span></li>
                   <li><a href="javascript:void(0)" class="active">课程管理</a></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
                  		<c:forEach var="g" items="${btns }">
                  			 <button type="button" class="btn btn-primary" onclick="${g.action }">
						 		<i class="${g.btnicon }"></i> ${g.btnname }
							 </button>
                  		</c:forEach>
 					 </div>
                </div>

                <h3 class="page-header"> 课程管理<i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

                <blockquote class="page-information hidden">
                 <p>
                	  更新、维护课程信息！
                </p>
              </blockquote>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
					<form class="form-inline" role="form" style="margin-top: 10px;margin-bottom: 10px;">
						  <input type="hidden" name="menuId" value="${folPara.menuId }">
						  
						  <div class="form-group">
						    <label for="minDate">发布时间：</label>
						    <input type="date" class="form-control" id="minDate" name="minDate" value="${folPara.minDate }"> -
						    <input type="date" class="form-control" id="maxDate" name="maxDate"  value="${folPara.maxDate }">
						  </div>
						  <br/><br/>
						  <div class="form-group">
						    <label  for="namecn">场馆名称：</label>
						    <input type="text" class="form-control" id="gymName" name="gymName"  value="${folPara.gymName }">
						  </div>
						  <button id="btn_search" type="button" class="btn btn-primary" >
						 	<i class="fa fa-search fa-lg"></i> 搜索
						  </button>
					</form>
				    <table id="table">
				    </table>
		    </div>
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>