<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-活动详情</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style type="text/css">
		</style>
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
		<!--include fileinput.css  -->
	  	<link href="${pageContext.request.contextPath}/css/fileinput.min.css" rel="stylesheet">
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<!--include wysiwyg.js  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-wysiwyg.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.hotkeys.min.js"></script>
	  	<!--导入省市区JSON数据  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/ProJson.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/DistrictJson.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/CityJson.js"></script>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var minDate = QueryString.GetValue("minDate");
		  		var maxDate = QueryString.GetValue("maxDate");
		  		var titleFol = QueryString.GetValue("titleFol");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		$("input[name='minDate']").val(minDate);
		  		$("input[name='maxDate']").val(maxDate);
		  		$("input[name='titleFol']").val(titleFol);
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/projectInfoController/list" + reqPara; 
		  		});
		  		
		  		// 加载省信息
			  	var $province = $("select[name='province']");
			  	$province.append("<option value='0'>请选择</option>");
			  	$.each(province, function(k, p) {
			  			 var proId = "${projectInfo.province}";
			  			 var option = "";
			  			 if(proId == p.ProID){
			  				 option = "<option  value='"+p.ProID+"' selected='selected'>"+p.ProName+"</option>";
			  			 } 
			  			$province.append(option);
			  	});
			  	
			  	var $city = $("select[name='city']");
			  	$city.append("<option value='0'>请选择</option>");
		    	$.each(city, function(k, p) {
		    		var provinceId = "2";
		    		provinceId = "${projectInfo.province}";
		    		var cityId = "${projectInfo.city}";
					var option = "";
					if(provinceId == p.ProID){
						if(p.CityID == cityId){
							 option = "<option value='"+p.CityID+"' selected='selected'>"+p.CityName+"</option>";
						 } 
					}
					
					$city.append(option);
				});
		    	
		    	var $area = $("select[name='area']");
		    	$.each(District, function(k, p) {
		    		var cityId = "3";
		    		cityId = "${projectInfo.city}";
		    		var areaId = "${projectInfo.area}";
		    		$area.append("<option value='0'>请选择</option>");
		    		
		    		var option = "";
		    		if(p.CityID == cityId){
		    			option = "<option value='"+p.Id+"'>"+p.DisName+"</option>";
						if(p.Id == areaId){
							 option = "<option value='"+p.Id+"' selected='selected'>"+p.DisName+"</option>"; 
						}
		    		}
					
					$area.append(option);
				}); 
		    	
		    	var $unit = $("select[name='unit']");
				$unit.append("<option value='${projectInfo.unit}'>${projectInfo.unitName}</option>");
		    	
				$province.multiselect();
				
				$city.multiselect();
				
				$area.multiselect();
				
				$unit.multiselect();
		  	 });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >有点功夫管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel" >活动管理</a></li>
		                   <li><a href="javascript:void(0)" class="active">活动详情</a></li>
		                 </ul>
		                 <h3 class="page-header"> 活动详情 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	活动详细信息查看！
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- 基本信息 start -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息<span class="pull-right">
									<a href="#" class="panel-minimize"><i
											class="fa fa-chevron-up"></i></a> 
									</span>
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/projectInfoController/save">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												<input type="hidden" name="minDate" value="" /> 
												<input type="hidden" name="maxDate" value="" /> 
											    <input type="hidden" name="titleFol" value="" /> 
											</div>
											
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${projectInfo.id }" />
											<textarea name="content" id="content" rows="" cols="" hidden="hidden"></textarea>
											<div class="form-group">
												<label class="col-lg-3 control-label">活动海报： <sup>*</sup></label>
												<div class="col-lg-9">
													<input type="file" class="form-control file-loading" name="myfiles" multiple readonly="readonly"/>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">活动标题： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="title"  value="${projectInfo.title }" readonly="readonly"/>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">起始时间： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="datetime" class="form-control" name="startdate"  value='<fmt:formatDate value="${projectInfo.startdate }" type="date"/>'readonly="readonly" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">结束时间： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="datetime" class="form-control" name="enddate"  value='<fmt:formatDate value="${projectInfo.enddate }" type="date"/>' readonly="readonly"/>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 联系人： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="procontact" value="${projectInfo.procontact }" readonly="readonly" />
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">联系方式： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="prophoneno" value="${projectInfo.prophoneno }" readonly="readonly"  />
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">省市区： <sup>*</sup></label>
													<div class="col-lg-5">
														<select class="multiselect" disabled name="province" >
														</select> &nbsp;&nbsp;-&nbsp;&nbsp;
														<select class="multiselect" disabled name="city">
														</select>&nbsp;&nbsp;-&nbsp;&nbsp;
														<select class="multiselect" disabled name="area">
														</select>
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">详细地址： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="address" value="${projectInfo.address }" readonly="readonly" />
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">报名费： <sup>*</sup></label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="profee" value="${projectInfo.profee }" readonly="readonly" />
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">单位： <sup>*</sup></label>
													<div class="col-lg-5">
														<select class="multiselect" disabled name="unit" >
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">是否精选： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<c:if test="${projectInfo.istop}">
															<label>
																 <input type="radio" name="istop" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
														</c:if>
														<c:if test="${!projectInfo.istop}">
															<label> 
																<input type="radio" name="istop" value="0" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">活动亮点：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-9">
													<div id="alerts"></div>
													<div class="btn-toolbar" data-role="editor-toolbar"
														data-target="#editor">
														<div class="btn-group">
															<a class="btn dropdown-toggle" data-toggle="dropdown"
																title="Font"><i class="icon-font"></i><b
																class="caret"></b></a>
															<ul class="dropdown-menu">
															</ul>
														</div>
														<div class="btn-group">
															<a class="btn dropdown-toggle" data-toggle="dropdown"
																title="Font Size"><i class="icon-text-height"></i>&nbsp;<b
																class="caret"></b></a>
															<ul class="dropdown-menu">
																<li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
																<li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
																<li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
															</ul>
														</div>
														<div class="btn-group">
															<a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i
																class="icon-bold"></i></a> <a class="btn" data-edit="italic"
																title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
															<a class="btn" data-edit="strikethrough"
																title="Strikethrough"><i class="icon-strikethrough"></i></a>
															<a class="btn" data-edit="underline"
																title="Underline (Ctrl/Cmd+U)"><i
																class="icon-underline"></i></a>
														</div>
														<div class="btn-group">
															<a class="btn" data-edit="insertunorderedlist"
																title="Bullet list"><i class="icon-list-ul"></i></a> <a
																class="btn" data-edit="insertorderedlist"
																title="Number list"><i class="icon-list-ol"></i></a> <a
																class="btn" data-edit="outdent"
																title="Reduce indent (Shift+Tab)"><i
																class="icon-indent-left"></i></a> <a class="btn"
																data-edit="indent" title="Indent (Tab)"><i
																class="icon-indent-right"></i></a>
														</div>
														<div class="btn-group">
															<a class="btn" data-edit="justifyleft"
																title="Align Left (Ctrl/Cmd+L)"><i
																class="icon-align-left"></i></a> <a class="btn"
																data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i
																class="icon-align-center"></i></a> <a class="btn"
																data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i
																class="icon-align-right"></i></a> <a class="btn"
																data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i
																class="icon-align-justify"></i></a>
														</div>
														<div class="btn-group">
															<a class="btn dropdown-toggle" data-toggle="dropdown"
																title="Hyperlink"><i class="icon-link"></i></a>
															<div class="dropdown-menu input-append">
																<input class="span2" placeholder="URL" type="text"
																	data-edit="createLink" />
																<button class="btn" type="button">Add</button>
															</div>
															<a class="btn" data-edit="unlink" title="Remove Hyperlink"><i
																class="icon-cut"></i></a>
	
														</div>
	
														<div class="btn-group">
															<a class="btn"
																title="Insert picture (or just drag & drop)"
																id="pictureBtn"><i class="icon-picture"></i></a> <input
																type="file" data-role="magic-overlay"
																data-target="#pictureBtn" data-edit="insertImage" />
														</div>
														<div class="btn-group">
															<a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i
																class="icon-undo"></i></a> <a class="btn" data-edit="redo"
																title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
														</div>
														<input type="text" data-edit="inserttext" id="voiceBtn"
															x-webkit-speech="">
													</div>
	
													<div id="editor">
														<c:if test="${projectInfo.lightspot eq null || projectInfo.lightspot eq '' }">请输入内容&hellip;</c:if>
														<c:if test="${projectInfo.lightspot ne null && projectInfo.lightspot ne '' }">${projectInfo.lightspot }</c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="button" class="btn btn-primary cancel"><i class="fa fa-reply fa-lg"></i>返回</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<!-- 基本信息  end-->
				<!-- 活动参与者信息  start-->
						<div class="panel panel-cascade">
							<div class="panel-heading text-primary">
								<h3 class="panel-title">
									活动参数者信息 <span class="pull-right">
									<a href="#" class="panel-minimize"><i
											class="fa fa-chevron-up"></i></a> 
									</span>
								</h3>
							</div>
							<div class="panel-body">
								<div class="panel panel-default">
									<div class="panel-heading">${projectInfo.title }</div>
									<table class="table">
										<thead>
											<tr>
												<th>用户头像</th>
												<th>用户昵称</th>
												<th>用户名称</th>
												<th>手机号</th>
												<th>地址</th>
												<th>参与时间</th>
											</tr>
										</thead>
										<tbody>
										<c:forEach var="g" items="${pds }">
											<tr>
												<td><img height="35px" width="50px" src="${g.mstClient.portrait }"  class="img"/> </td>
												<td>${g.mstClient.nickname }</td>
												<td>${g.mstClient.namecn }</td>
												<td>${g.mstClient.mobileno }</td>
												<td>${g.mstClient.address }</td>
												<td><fmt:formatDate value="${g.createdate }" type="date"/></td>
											</tr>
											
										</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- 活动参与者信息 end -->
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
			<!--include fileinput.js  -->
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput.js"></script>
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
</body>
<script>
  $(function(){
	 var imsg = [];
	 var res = "${projectInfo.probill}".split(";");
	 for(var i = 0; i < res.length; i ++){
		 imsg.push("<img src='"+res[i]+"' class='file-preview-image' />");
	 }
	 $("input[name='myfiles']").fileinput({
	        language: 'zh', //设置语言
	      	uploadUrl: "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
	        uploadAsync: true, //异步加载
	      	allowedFileExtensions : ['jpg', 'png','gif'],//接收的文件后缀
	        showUpload: true, //是否显示上传按钮
	        showCaption: false,//是否显示标题
	        overwriteInitial: true,
	        maxFileSize: 1024 * 10,
	        maxFileCount: 1,
	        slugCallback: function(filename) {
	            return filename.replace('(', '_').replace(']', '_');
	        },
	      
	        showPreview:true,
	        initialPreview: imsg,
	        allowedPreviewTypes : [ 'image' ],
	        browseClass: "btn btn-primary", //按钮样式             
	 });
	 $("input[name='myfiles']").on("fileuploaded", function (event, data, previewId, index) {  
		// JSON.stringify(data),分析data数据格式
		// alert(data.response.resName + "==" + previewId + "==" + index)
		var $input = " <input type='hidden' name='imagename' value='"+data.response.resName+"'/>";
		var $form = $("form");
		$form.append($input);
     });  
	$("input[name='myfiles']").on('filedeleted', function(event, key) {
		 	//alert(key)
		    console.log('Key = ' + key);
	});
    function initToolbarBootstrapBindings() {
      var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier', 
            'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
            'Times New Roman', 'Verdana'],
            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
      $.each(fonts, function (idx, fontName) {
          fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
      });
      $('a[title]').tooltip({container:'body'});
    	$('.dropdown-menu input').click(function() {return false;})
		    .change(function () {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
        .keydown('esc', function () {this.value='';$(this).change();});

      $('[data-role=magic-overlay]').each(function () { 
        var overlay = $(this), target = $(overlay.data('target')); 
        overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
      });
      if ("onwebkitspeechchange"  in document.createElement("input")) {
        var editorOffset = $('#editor').offset();
        $('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
      } else {
        $('#voiceBtn').hide();
      }
	};
	function showErrorAlert (reason, detail) {
		var msg='';
		if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
		else {
			console.log("error uploading file", reason, detail);
		}
		$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
		 '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
	};
    initToolbarBootstrapBindings();  
	//$('#editor').wysiwyg({ fileUploadError: showErrorAlert} );
    window.prettyPrint && prettyPrint();
  });
</script>
<div id="fb-root"></div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-37452180-6', 'github.io');
  ga('send', 'pageview');
</script>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="http://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</html>