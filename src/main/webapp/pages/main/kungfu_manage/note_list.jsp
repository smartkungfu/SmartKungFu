<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-在线课程视频节点</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		

<jsp:include page="../../fragment/frag_top_css.jsp" />
<!-- 样式冲突 -->
<!--include fileinput.css  -->
<link href="${pageContext.request.contextPath}/css/fileinput.min.css"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
	rel="stylesheet">
<jsp:include page="../../fragment/frag_top_js.jsp" />

<!--include fileinput.js  -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/fileinput.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
	  	<script type="text/javascript">
	  	var paras = "";
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             					cache: true, // 不缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				              /*   pagination: true, // 开启分页功能
				               // singleSelect: false,
				                pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500], */
				                search: false, // 关闭搜索功能
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                queryParams: function (params) {
				                	//排序，并处理多表中的属性
				                	var sort = params.sort;
				                	if(sort == "cUName" ){
				                		sort == "u." + sort;
				                	} else{
				                		sort == "tip." + sort;
				                	}
				                	var obj = {
					                        sort:sort,
							                order:params.order, 
					                        title:$("#title").val(),
					                        viid:$("input[name='viid']").val()
					                    };
				                	return obj;
				                },
				                url: "${pageContext.request.contextPath}/videoNoteController/queryList",
				                columns: [{
				                    field: 'state',
				                    checkbox: true
				                }, {
				                    field: 'videoTitle',
				                    title: '视频名称',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                },{
				                    field: 'reserved1',
				                    title: '节点顺序',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                }, {
				                    field: 'notetitle',
				                    title: '分段视频名称',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                }, 
				                {
				                    field: 'worktime',
				                    title: '播放时间(秒)',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
									formatter:function(value,row,index){  
						                
				                    	return (value * 1).toFixed(6) ;
				                    } 
				                },
				                {
				                    field: 'reserved2',
				                    title: '帧数',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                },
				                {
				                    field: 'reserved4',
				                    title: '毫秒',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                    formatter:function(value,row,index){  
						                
				                    	return (value * 1).toFixed(3) ;
				                    } 
				                },
				               {
				                    field: 'cUName',
				                    title: '创建者',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true
				                },{
				                      title: '操作',
				                      field: 'id',
				                      align: 'center',
				                      events: operateEvents,
				                      formatter:function(value,row,index){  
						                   var eles = '<button class="btn btn-danger btn-xs delList"><i class="fa fa-trash-o"></i></button>&nbsp;<button class="btn btn-success btn-xs editObj"><i class="fa fa-edit"></i></button>  '; 
						                   return eles ;
				                      } 
				                 }],
				                onLoadSuccess:function(data){
				                	var rows = $table.bootstrapTable('getData');
				                	$table.bootstrapTable('updateCell', {index:0, field: 'totalTime', value: data.totalTime});
				                  	$table.bootstrapTable('mergeCells', {index:0, field: 'totalTime', colspan: 1, rowspan: rows.length});
				                },
				                onLoadError: function () {
				             	   //    mif.showErrorMessageBox("数据加载失败！");
				                }
            });
        }
	  	
		window.operateEvents = {
	  	        'click .delList': function (e, value, row, index) {
	  	        	 $.ajax({
	  					url : "${pageContext.request.contextPath}/videoNoteController/delList",
	  					data : {ids:row.id},
	  					type : "POST",
	  					async : true,
	  					dataType : "json",
	  					success : function(flag) {
	  						if(flag){
	  							bootbox.alert({ 
	  	 							  size: "small",
	  	 							  title: "正确",
	  	 							  message: "操作成功！", 
	  	 							  callback: function(){
	  	 								  $table.bootstrapTable('refresh');
	  	 								  return;
	  	 							 }
	  	 						});
	  						}else{
	  							bootbox.alert({ 
	 	 							  size: "small",
	 	 							  title: "错误",
	 	 							  message: "操作失败！", 
	 	 							  callback: function(){return; }
	 	 						});
	  						}
	  					},
	  					error : function(data) {
	  						bootbox.alert({ 
	  							  size: "small",
	  							  title: "错误",
	  							  message: "请求超时，请重试！", 
	  							  callback: function(){return; }
	  						});
	  						return;
	  					}
	  			   });	
	  	        },
	  	      'click .editObj': function (e, value, row, index) {
	            	editObj(row);
	          },
	  	};
	  	
   		$(function(){
   			initTable();
   		 	
   		  	$('form').bootstrapValidator({
	            excluded: ':disabled',
	            feedbackIcons: {
	            	 valid: 'fa fa-check',
	            	 invalid: 'fa fa-times',
	            	 validating: 'fa fa-refresh'
	            },
	            fields: {
	                reserved1: {
	                    validators: {
	                        notEmpty: {
	                            message: '训练次数为空'
	                        },
	                        regexp: {
	                            regexp: "^[1-9]\\d*$",
	                            message: '请输入正整数'
	                        },
	                        callback: {
	                            message: '输入格式不对，或顺序重复',
	                            callback: function(value, validator) {
	                            	
	                            	var flag = true;
	                            	
	                            	var id = $("input[name='id']").val();
	                            	var reserved1 = $("input[name='reserved1']").val();
	                            	if(id != "" && reserved1 != ""){//更新
	                            		return flag;
	                            	}else{
	                            		var rows = $table.bootstrapTable('getData');
	                            		$(rows).each(function(index,element){
	                            			if(value == element.reserved1){
	                            				flag = false;
	                            			}
	                            		});
	                            		
	                            		return flag;
	                            	}
                            		
                            		
	                            }
	                        }
	                    }
	                },
	                notetitle: {
	                    validators: {
	                        notEmpty: {
	                            message: '分段视频名称为空'
	                        },
	                        stringLength: {
	                            min: 1,
	                            max: 50,
	                            message: '字符长度在为1-50'
	                        },
	                    } ,
	                   
	                }, 
	                minute: {
	                    validators: {
	                        notEmpty: {
	                            message: '播放分钟为空'
	                        },
		                    between: {
		                            min: 0,
		                            max:120,
		                            message: '播放分钟不能大于120'
		                    }
	                    } ,
	                }, 
	                seconds: {
	                    validators: {
	                        notEmpty: {
	                            message: '播放秒为空'
	                        },
		                    between: {
		                            min: 0,
		                            max:59,
		                            message: '播放秒不能大于60'
		                    }
	                    } ,
	                }, 
	                reserved2: {
	                    validators: {
	                        notEmpty: {
	                            message: '播放帧数为空'
	                        },
		                    between: {
		                            min: 0,
		                            max:24,
		                            message: '播放帧数不能大于25'
		                    }
	                    } ,
	                }, 
	                reserved4: {
	                    validators: {
	                        notEmpty: {
	                            message: '播放毫秒为空'
	                        },
	                        regexp: {
	                            regexp: "^([+-]?)\\d*\\.?\\d+$",
	                            message: '播放毫秒格式不正确',
	                        },
		                    between: {
		                            min: 0,
		                            max:1000,
		                            message: '播放毫秒不能大于999'
		                    }
	                    } ,
	                }, 
	               	myfiles: {
	            		  validators: {
	                          file: {
	                              extension: 'jpeg,png,jpg,gif',
	                              type: 'image/jpeg,image/png,image/jpg,image/gif',
	                              maxSize: 2048 * 1024,   // 2 MB
	                              message: '文件格式非法，请重新上传'
	                          },
	                          callback: {
		                            message: '请上传文件',
		                            callback: function(value, validator) {
		                                // Get the selected options
		                                
		                                var $imagename = $("input[name='imagename']");
		                                var imgs = $(".defined");
		                                return ($imagename.val() != undefined || imgs.length != 0);
		                            }
		                     }
	                      }
		            },
	                
	            },
	        });
   		  	
   		 $('#btn_save_single').submit(function (ev) {
             ev.preventDefault();
         });
   		 
         $('#btn_save_single').on("click", function () {
             var bootstrapValidator = $('form').data('bootstrapValidator');
             bootstrapValidator.validate();
			
             if (bootstrapValidator.isValid()) {
            	 $.ajax({
 					url : "${pageContext.request.contextPath}/videoNoteController/save",
 					data : $("form").serialize(),
 					type : "POST",
 					async : true,
 					dataType : "json",
 					success : function(flag) {
 						if(flag){
 							bootbox.alert({ 
 	 							  size: "small",
 	 							  title: "正确",
 	 							  message: "操作成功！", 
 	 							  callback: function(){
 	 								  $table.bootstrapTable('refresh');
 	 								  $("input[name='reserved1']").val("");
 	 								  $("input[name='notetitle']").val("");
 	 								  $("input[name='minute']").val("");
 	 								  $("input[name='seconds']").val("");
 	 								  $("input[name='reserved2']").val("");
 	 								  $("input[name='reserved4']").val("");
 	 								  $("input[name='id']").val("");
 	 								  //$("input[name='worktime']").val("");
 	 								  /* $("input[name='gaptime']").val(""); */
 	 								  return;
 	 							 }
 	 						});
 						}else{
 							bootbox.alert({ 
	 							  size: "small",
	 							  title: "错误",
	 							  message: "操作失败！", 
	 							  callback: function(){return; }
	 						});
 						}
 					},
 					error : function(data) {
 						bootbox.alert({ 
 							  size: "small",
 							  title: "错误",
 							  message: "请求超时，请重试！", 
 							  callback: function(){return; }
 						});
 						return;
 					}
 			   });	
             } else {
                 return;
             }
         });
         
     	$(".cancel").click(function(){
     		
  			var reqPara = "?" + $(".folPara").find("input").serialize();
  			reqPara = decodeURIComponent(reqPara,true);
  			window.location.href="${pageContext.request.contextPath}/onlineCourseController/list" + reqPara; 
  		});
     	
		$("input[name='reserved2']").blur(function(){
     		var reserved2 = $(this).val();
     		if(reserved2 != null && reserved2 != ""){
     			$("input[name='reserved4']").val(((reserved2 * 1) / 25 * 1000).toFixed(3) );
     		}
  		});
     	
     	$("#btn_cancel_single").click(function(){
     		$("input[name='id']").val("");
     		
     		 $("input[name='reserved1']").val("");
			 $('form')
		     .bootstrapValidator('updateStatus', 'reserved1', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'reserved1');
			
			 $("input[name='notetitle']").val("");
			 $('form')
		     .bootstrapValidator('updateStatus', 'notetitle', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'notetitle');
			
		/* 	 $("input[name='worktime']").val("");
			 $('form')
		     .bootstrapValidator('updateStatus', 'worktime', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'worktime');
			  */
			 $("input[name='minute']").val("");
			 $('form')
		     .bootstrapValidator('updateStatus', 'minute', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'minute');
			 
			 $("input[name='seconds']").val("");
			 $('form')
		     .bootstrapValidator('updateStatus', 'seconds', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'seconds');
			 
			 $("input[name='reserved2']").val("");
			 $('form')
		     .bootstrapValidator('updateStatus', 'reserved2', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'reserved2');
			 
			 $("input[name='reserved4']").val("");
			 $('form')
		     .bootstrapValidator('updateStatus', 'reserved4', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'reserved4');
			 
			/*  $("input[name='gaptime']").val(""); */
     	});
   	});
   	function editObj(row){
   		$("input[name='reserved1']").val(row.reserved1);
		$("input[name='notetitle']").val(row.notetitle);
		$("input[name='minute']").val(row.reserved9);
		$("input[name='seconds']").val((row.reserved10 * 1).toFixed(0));
		$("input[name='reserved2']").val(row.reserved2);
		$("input[name='reserved4']").val(row.reserved4);
		$("input[name='id']").val(row.id);	
   	}
   </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     		
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>有点功夫管理</span></li>
                   <li><a href="javascript:void(0)" class="active cancel">在线课程</a></li>
                   <li><a href="javascript:void(0)" class="active">在线课程视频节点</a></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
 					 </div>
                </div>

                <h3 class="page-header"> 在线课程视频节点 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

                <blockquote class="page-information hidden">
                 <p>
                  	更新、维护在线课程视频节点信息！请保证节点片段配置与视频剪辑顺序一致。
                </p>
              </blockquote>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
					<div class="ro">
						<div class="col-mol-md-offset-2">
							<form method="post" class="form-horizontal" action="">

								<!--附件参数  -->
								<div class="folPara" style="display: none">
									<input type="hidden" name="menuId" value="${folPara.menuId }" />
									<input type="hidden" name="page" value="${folPara.page }" /> 
									<input type="hidden" name="pageSize" value="${folPara.pageSize }" />
									<input type="hidden" name="index_exp" value="${MENU_STYLE_INDEX_EXP }" />
									<input type="hidden" name="index_che" value="${MENU_STYLE_INDEX_CHE }" />
								</div>
								<!-- 逻辑参数 -->
								<input type="hidden" name="id" value="" />
								<input type="hidden" name="viid" value="${folPara.courseId }" />
								<input type="hidden" name="vitype" value="1" />
								<div class="form-group">
									<label class="col-lg-3 control-label">节点顺序： <sup>*</sup></label>
									<div class="col-lg-5">
										<input type="number" min="1" class="form-control" name="reserved1"
											value="" placeholder="请输入正整数" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">分段视频名称： <sup>*</sup></label>
									<div class="col-lg-5">
										<input class="form-control" name="notetitle" placeholder="请输入分段视频名称(end表示结束节点)" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">分钟： <sup>*</sup></label>
									<div class="col-lg-5">
										<div class="input-group">
											<input min="0" type="number" class="form-control" name="minute"
												value="" placeholder="请输入播放时间，按分钟记录"><span
												class="input-group-addon">分钟</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">秒数： <sup>*</sup></label>
									<div class="col-lg-5">
										<div class="input-group">
											<input min="0" type="number" class="form-control" name="seconds"
												value="" placeholder="请输入播放时间，按秒记录"><span
												class="input-group-addon">秒</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">帧数： <sup>*</sup></label>
									<div class="col-lg-5">
										<div class="input-group">
											<input min="0" type="number" class="form-control" name="reserved2" value=""
												placeholder="请输入播放帧数，单位秒数播放25帧"><span
												class="input-group-addon">帧</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">毫秒： <sup>*</sup></label>
									<div class="col-lg-5">
										<div class="input-group">
											<input min="0" type="text" class="form-control" name="reserved4" value=""
												placeholder="请输入毫秒"><span
												class="input-group-addon">毫秒</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">图片： <sup>*</sup></label>
									<div class="col-lg-5">
									<input type="file" class="form-control file-loading" name="myfiles" multiple />
									</div>
							  </div>
								<!-- <div class="form-group">
									<label class="col-lg-3 control-label">链接间隔时间： <sup>*</sup><sup>&nbsp;</sup></label>
									<div class="col-lg-5">
										<div class="input-group">
											<input type="number" min="1" class="form-control"
												name="gaptime" value="" placeholder="请输入链接间隔时间" /> <span
												class="input-group-addon">微秒</span>
										</div>
									</div>
								</div> -->
								<div class="form-group">
									<div class="col-lg-9 col-lg-offset-3">
										<button id="btn_save_single" type="button"
											class="btn btn-primary btn-xs">
											<i class="fa fa-check fa-lg"></i>保存
										</button>
										&nbsp;&nbsp;&nbsp;
										<button id="btn_cancel_single" type="button"
											class="btn btn-primary btn-xs">
											<i class="fa fa-remove fa-lg"></i>取消
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<table id="table">
				    </table>
		    </div>
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
<script>
$(function(){
	 var imsg = [];
	 var res = "${videoNote.reserved11}".split(";");
	 for(var i = 0; i < res.length; i ++){
		 if(res[i] != null && res[i] != ""){
			 imsg.push("<img src='"+res[i]+"' class='file-preview-image defined' style='width:180px;height:120px;' />");
		 }
	 }
	 $("input[name='myfiles']").fileinput({
	        language: 'zh', //设置语言
	      	uploadUrl: "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
	        uploadAsync: true, //异步加载
	      	allowedFileExtensions : ['jpg', 'png','gif'],//接收的文件后缀
	        showUpload: false, //是否显示上传按钮
	        showCaption: false,//是否显示标题
	        overwriteInitial: true,
	        maxFileSize: 1024 * 10,
	        maxFileCount: 1,
	        slugCallback: function(filename) {
	            return filename.replace('(', '_').replace(']', '_');
	        },
	      
	        showPreview:true,
	        initialPreview: imsg,
	        allowedPreviewTypes : [ 'image' ],
	        browseClass: "btn btn-primary", //按钮样式             
	 });
	 $("input[name='myfiles']").on("fileuploaded", function (event, data, previewId, index) {  
		// JSON.stringify(data),分析data数据格式
		// alert(data.response.resName + "==" + previewId + "==" + index)
		var $input = " <input type='hidden' id='"+previewId+"' name='imagename' value='"+data.response.resName+"'/>";
		var $form = $("form");
		$form.append($input);
		
		$('form')
       .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
       .bootstrapValidator('validateField', 'myfiles');
    });  
	$("input[name='myfiles']").on("fileclear", function(event) {
		 var $imagenames = $("input[name='imagename']");
		 for(var i = 0 ; i < $imagenames.length ; i ++){
			  $($imagenames[i]).remove();
		 }
		 
		 var $imgs = $(".defined");
		 for(var i = 0 ; i < $imgs.length ; i ++){
			  $($imgs[i]).remove();
		 }
		 $('form')
	        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'myfiles');
	 });
	
	$("input[name='myfiles").on('filereset', function(event) {
		  $('form')
	        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'myfiles');
	});
	
	$("input[name='myfiles'").on('filesuccessremove', function(event, id) {
		$("input[id='"+id+"']").remove();
		$('form')
	     .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	     .bootstrapValidator('validateField', 'myfiles');
	});
	
   UE.getEditor('content');
 });
</script>
</html>