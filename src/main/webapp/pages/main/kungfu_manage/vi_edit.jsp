<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-编辑视频</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		<!-- 样式冲突 -->
	  	<!--include fileinput.css  -->
	  	<link href="${pageContext.request.contextPath}/css/fileinput.min.css" rel="stylesheet">
	  	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  		
	  	<!--include fileinput.js  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput.min.js"></script>
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var minDate = QueryString.GetValue("minDate");
		  		var maxDate = QueryString.GetValue("maxDate");
		  		var authorFol = QueryString.GetValue("authorFol");
		  		var titleFol = QueryString.GetValue("titleFol");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		$("input[name='minDate']").val(minDate);
		  		$("input[name='maxDate']").val(maxDate);
		  		$("input[name='authorFol']").val(authorFol);
		  		$("input[name='titleFol']").val(titleFol);
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/videoInfoController/list" + reqPara; 
		  		});
		  		
		  		$.ajax({
					url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
					data : {parentId:"105"},
					type : "GET",
					async : true,
					dataType : "json",
					success : function(data) {
						var $vitype = $("select[name='vitype']");
						
						$.each(data, function(k, p) {
				  			 var vitype = "${viInfo.vitype}";
				  			 if(vitype == p.id){
				  				 option = "<option  value='"+p.id+"' selected='selected'>"+p.name+"</option>";
				  			 } else {
				  				 option = "<option  value='"+p.id+"'>"+p.name+"</option>";
				  			 }
				  			 
				  			$vitype.append(option);
				  		});
						
						$vitype.multiselect({maxHeight: 200});
					},
					error : function(data) {
						bootbox.alert({ 
							  size: "small",
							  title: "错误",
							  message: "请求超时，请重试！", 
							  callback: function(){return; }
						});
						return;
					}
			   });	
			  	$('form').bootstrapValidator({
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            fields: {
			            	myfiles: {
			            		  validators: {
			            			  callback: {
				                            message: '请上传文件',
				                            callback: function(value, validator) {
				                                // Get the selected options
				                                
				                                var $imagename = $("input[name='imagename']");
				                                var imgs = $(".defined");
				                                return ($imagename.val() != undefined || imgs.length != 0);
				                            }
				                     }
			                      }
				            },
				            cover: {
			            		  validators: {
			                          file: {
			                              extension: 'jpeg,png,jpg',
			                              type: 'image/jpeg,image/png,image/jpg',
			                              maxSize: 2048 * 1024,   // 2 MB
			                              message: '文件格式非法，请重新上传'
			                          },
			                          callback: {
				                            message: '请上传文件',
				                            callback: function(value, validator) {
				                                
				                                var $videoname = $("input[name='videoname']");
				                                var imgs = $(".defined1");
				                                return ($videoname.val() != undefined || imgs.length != 0);
				                            }
				                     }
			                      }
				            },
			                vitype: {
			                    validators: {
			                        callback: {
			                            message: '请选择视频类型',
			                            callback: function(value, validator) {
			                                var options = validator.getFieldElements('vitype').val();
			                                return (options != null);
			                            }
			                        }
			                    }
			                },
			                title: {
			                    validators: {
			                        notEmpty: {
			                            message: '视频标题为空'
			                        },
			                        stringLength: {
			                            min: 1,
			                            max: 100,
			                            message: '长度在为1-100位'
			                        },
			                    }
			                },
			                author: {
			                    validators: {
			                        notEmpty: {
			                            message: '作者为空'
			                        },
			                    }
			                }, 
			                content: {
			                    validators: {
			                        notEmpty: {
			                            message: '内容为空'
			                        },
			                    } 
			                }, 
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	
			            	validator.defaultSubmit();
			            }
			        });
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >有点功夫管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel">视频管理</a></li>
		                   <li><a href="javascript:void(0)" class="active">编辑视频</a></li>
		                 </ul>
		                 <h3 class="page-header"> 编辑视频 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	视频信息编辑！
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/videoInfoController/save">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												<input type="hidden" name="minDate" value="" /> 
												<input type="hidden" name="maxDate" value="" /> 
												<input type="hidden" name="authorFol" value="" />
											    <input type="hidden" name="titleFol" value="" /> 
											</div>
											
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${viInfo.id }" />
											<div class="form-group">
												<label class="col-lg-3 control-label">视频<span style="color:gray">(1G,mp4)</span>： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="file"  class="form-control file-loading"  name="myfiles"/>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">视频封面<span style="color:gray">(750 * 400)</span>： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="file"  class="form-control file-loading"  name="cover"/>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">视频类型： <sup>*</sup></label>
													<div class="col-lg-5">
														<select class="multiselect" name="vitype" ></select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 标题： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="title"  value="${viInfo.title }" placeholder="请输入标题" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 作者： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="author" value="${viInfo.author }" placeholder="请输入作者" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">是否推荐： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<c:if test="${viInfo.reserved8}">
															<label>
																 <input type="radio" name="reserved8" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved8" value="0" /> 否
															</label>
														</c:if>
														<c:if test="${!viInfo.reserved8}">
															<label>
																 <input type="radio" name="reserved8" value="1" /> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved8" value="0" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div>
											<%-- <div class="form-group">
												<label class="col-lg-3 control-label">是否在线： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<c:if test="${viInfo.reserved7}">
															<label>
																 <input type="radio" name="reserved7" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved7" value="0" /> 否
															</label>
														</c:if>
														<c:if test="${!viInfo.reserved7}">
															<label>
																 <input type="radio" name="reserved7" value="1" /> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="reserved7" value="0" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div> --%>
											<div class="form-group">
												<label class="col-lg-3 control-label">内容：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<textarea class="form-control" rows="7" name="content" >${viInfo.content }</textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
<script>
  $(function(){
	 var imsg = [];
	 var res = "${viInfo.url}".split(";");
	 for(var i = 0; i < res.length; i ++){
		 if(res[i] != null && res[i] != ""){
			 imsg.push("<video class='defined' controls src='"+res[i]+"' width='100%' controls></video>");
		 }
	 }
	 $("input[name='myfiles']").fileinput({
	        language: 'zh', //设置语言
	      	uploadUrl: "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
	        uploadAsync: true, //异步加载
	        showUpload: false, //是否显示上传按钮
	        showCaption: false,//是否显示标题
	        overwriteInitial: true,
	        maxFileSize: 1024 * 1024,//文件大小，单位kb
	        maxFilesNum: 1,
	        slugCallback: function(filename) {
	            return filename.replace('(', '_').replace(']', '_').replace('-', '_');
	        },
	      
	        showPreview:true,
	        initialPreview: imsg,
	        allowedPreviewTypes : [ 'video' ],
	     	allowedFileExtensions : ['mp4','mov','flv'],
	        browseClass: "btn btn-primary", //按钮样式             
	 });
	 $("input[name='myfiles']").on("fileuploaded", function (event, data, previewId, index) {  
		// JSON.stringify(data),分析data数据格式
		// alert(data.response.resName + "==" + previewId + "==" + index)
		var $input = " <input type='hidden' id='"+previewId+"' name='imagename' value='"+data.response.resName+"'/>";
		var $form = $("form");
		$form.append($input);
		
		$('form')
        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
        .bootstrapValidator('validateField', 'myfiles');
     });  
	 $("input[name='myfiles']").on("fileclear", function(event) {
		 var $imagenames = $("input[name='imagename']");
		   
		 for(var i = 0 ; i < $imagenames.length ; i ++){
			 $($imagenames[i]).remove();
		 }
		 
		 var $imgs = $(".defined");
		 for(var i = 0 ; i < $imgs.length ; i ++){
		    $($imgs[i]).remove();
		 }
		   
		 $('form')
	        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'myfiles');
	 });
	 
	 $("input[name='myfiles").on('filereset', function(event) {
		  $('form')
	        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'myfiles');
	});
	 
	$("input[name='myfiles'").on('filesuccessremove', function(event, id) {
			$("input[id='"+id+"']").remove();
			$('form')
		     .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'myfiles');
    });
	
	 var cover = [];
	 var covers = "${viInfo.reserved9}".split(";");
	 for(var i = 0; i < covers.length; i ++){
		 if(covers[i] != null && covers[i] != ""){
			 cover.push("<img src='"+covers[i]+"' class='file-preview-image defined1' style='width:180px;height:120px;' />");
		 }
		 
	 }
	 $("input[name='cover']").fileinput({
		  language: 'zh', //设置语言
	      	uploadUrl: "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
	        uploadAsync: true, //异步加载
	      	allowedFileExtensions : ['jpg', 'png'],//接收的文件后缀
	        showUpload: false, //是否显示上传按钮
	        showCaption: false,//是否显示标题
	        overwriteInitial: true,
	        maxFileSize: 1024 * 10,
	        maxFileCount: 1,
	        slugCallback: function(filename) {
	            return filename.replace('(', '_').replace(']', '_');
	        },
	      
	        showPreview:true,
	        initialPreview: cover,
	        allowedPreviewTypes : [ 'image' ],
	        browseClass: "btn btn-primary", //按钮样式 
	 });
	            
	 $("input[name='cover']").on("fileuploaded", function (event, data, previewId, index) {  
		var $input = " <input type='hidden' id='"+previewId+"' name='videoname' value='"+data.response.resName+"'/>";
		var $form = $("form");
		$form.append($input);
		
		$('form')
        .bootstrapValidator('updateStatus', 'cover', 'NOT_VALIDATED')
        .bootstrapValidator('validateField', 'cover');
     });  
	 
	 $("input[name='cover']").on("fileclear", function(event) {

		 var $videonames = $("input[name='videoname']");
				   
		 for(var i = 0 ; i < $videonames.length ; i ++){
					   $($videonames[i]).remove();
		 }
		 
		 var $imgs = $(".defined1");
		 for(var i = 0 ; i < $imgs.length ; i ++){
		    $($imgs[i]).remove();
		 }
				   
		 $('form')
	       .bootstrapValidator('updateStatus', 'cover', 'NOT_VALIDATED')
	       .bootstrapValidator('validateField', 'cover');
	 });
	 
	 $("input[name='cover").on('filereset', function(event) {
		  $('form')
	        .bootstrapValidator('updateStatus', 'cover', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'cover');
	});
	 
	$("input[name='cover'").on('filesuccessremove', function(event, id) {
			$("input[id='"+id+"']").remove();
			$('form')
		     .bootstrapValidator('updateStatus', 'cover', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'cover');
	});

  });
</script>
</html>