<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-课程管理-套餐管理</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var parentId = QueryString.GetValue("parentId");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var codeName = QueryString.GetValue("codeName");
		  		var type = QueryString.GetValue("type");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		$("input[name='parentId']").val(parentId);
		  		$("input[name='codeName']").val(codeName);
		  		$("input[name='type']").val(type);
		  		
		  		$.ajax({
					url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
					data : {parentId:"21"},
					type : "GET",
					async : true,
					dataType : "json",
					success : function(data) {
						 var $packagetype = $("select[name='packagetype']");
						 
						 $.each(data, function (idx, ele) {
							 $packagetype.append("<option value='"+ele.id+"'>"+ele.name+"</option>");
					     });
						 
						 $packagetype.multiselect();
					},
					error : function(data) {
						bootbox.alert({ 
							  size: "small",
							  title: "错误",
							  message: "请求超时，请重试！", 
							  callback: function(){return; }
						});
						return;
					}
			   });	
		  		
		  		$("#btn_add_single").click(function(){
		  			
		  			var $table = $(".table");
		  			var $trs = $table.find("tr");
		  			if($trs.length >= 5 ){
		  				bootbox.alert({ 
							  size: "small",
							  title: "错误",
							  message: "套餐最多为4条数据", 
							  callback: function(){return; }
						});
		  				return ;
		  			} else {
		  				$(this).addClass('disabled');   
			  			$("#base_info").css("display","block");
		  			}
		  		});
		  		
		  		$("#btn_save_single").click(function(){
		  			//校验
		  			$form = $("form");
		  			var id = $form.find("input[name='id']").val();
		  			var packagetype = $form.find("select[name='packagetype']").val();
		  			var packagetypeName =  $form.find("select[name='packagetype'] option:selected").text();
		  			var price = $form.find("input[name='price']").val();
		  			var enabled = $form.find("input[name='enabled']").prop("checked");
		  			if(packagetype != "0" ){
		  				if(price != null && price != "" ){
		  					$("#base_info").css("display","none");
			  				$("#btn_add_single").removeClass("disabled");
			  				var $table = $(".table");
			  				//创建表单元素
				  			var tempTr = "<tr>";
				  			tempTr += "<td>-</td>";
				  			tempTr += "<td data-pt='"+packagetype+"'>"+packagetypeName+"</td>";
				  			tempTr += "<td>"+(price * 1).toFixed(2)+"</td>";
				  			if(enabled){
				  				tempTr += "<td data-enabled='"+enabled+"'>是</td>";
				  			} else{
				  				tempTr += "<td data-enabled='"+enabled+"'>否</td>";
				  			}
				  			
				  			tempTr += "<td>"+timeStamp2String(new Date())+"</td>";
				  			tempTr += "<td>${MY_SESSION.mstUser.namecn }</td>";
				  			
				  			if(enabled){
				  				tempTr += "<td><button class='btn btn-warning btn-xs'><i class='fa fa-unlock'></i> 禁用</button>";
				  			} else{
				  				tempTr += "<td><button class='btn btn-primary btn-xs'><i class='fa fa-unlock'></i> 启用</button>";
				  			}
				  			
				  			tempTr += "<button class='btn btn-danger btn-xs'><i class='fa fa-trash-o'> 删除</i></button>";
				  			tempTr += "</td>";
				  			
				  			$table.append(tempTr);
		  				} else {
		  					bootbox.alert({ 
								  size: "small",
								  title: "错误",
								  message: "请输入套餐价格", 
								  callback: function(){return; }
							});
			  				return ;
		  				}
		  				
		  			} else{
		  				bootbox.alert({ 
							  size: "small",
							  title: "错误",
							  message: "请选择套餐", 
							  callback: function(){return; }
						});
		  				return ;
		  			}
		  		});
		  		
		  		$("#btn_save").click(function(){
		  			
		  			var codeList = new Array();
		  			
		  			var $table = $(".table");
		  			var $trs = $table.find("tr");
		  			
		  			var len = $trs.length;
		  			for(var i = 1 ;i < len ; i++){
		  				//var $idTd = $($trs[i]).find("td")[0];
		  				//var id = $($idTd).html();
		  				
		  				var $packagetype = $($trs[i]).find("td")[1];
		  				var packagetype = $($packagetype).attr("data-pt");
		  				
		  				var $price = $($trs[i]).find("td")[2];
		  				var price = $($price).html();
		  				
		  				var $enabled = $($trs[i]).find("td")[3];
		  				var enabled = $($enabled).attr("data-enabled");
		  				
		  				//var $createdate = $($trs[i]).find("td")[4];
		  				//var createdate = $($createdate).html();
		  				
		  				///var createuser = "${MY_SESSION.mstUser.id }";
		  				codeList.push({courseid:"${gd.id}",packagetype: packagetype,price:price,enabled:enabled,});   
		  			}
	            	
	            	var codes = JSON.stringify(codeList);
	            	console.log("decPara codes:" ,codes);
			  		
	            	$.ajax({
		  				url : "${pageContext.request.contextPath}/coursePackageController/saveWithList",
		  				type : "POST",
		  				data: codes,//将对象序列化成JSON字符串  
		  			    dataType:"json",  
		  		   	    contentType : 'application/json;charset=utf-8', //设置请求头信息  
		  				success : function(data) {
		  					if(data){
		  						bootbox.alert({ 
									  size: "small",
									  title: "提示",
									  message: "保存成功", 
									  callback: function(){$("#btn_cancel").click(); }
								});
		  					} else {
		  						bootbox.alert({ 
									  size: "small",
									  title: "错误",
									  message: "数据加载失败", 
									  callback: function(){return; }
								});
		  					}
		  				},
		  				error:function(XMLHttpRequest, textStatus, errorThrown){
		  					// alert(XMLHttpRequest.status);
		  					 //alert(XMLHttpRequest.readyState);
		  					 //alert(textStatus);
		  					 
		  					bootbox.alert({ 
								  size: "small",
								  title: "错误",
								  message: "数据加载失败", 
								  callback: function(){return; }
							});
		  				}
		  			});
		  		});
		  		
		  		$("#btn_cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/gymnasiumDetailController/list" + reqPara; 
		  		});
		  		
		  		$(".btn-danger").click(function(){
		  			var $this = $(this);
		  			var $tr = $this.parent().parent();
		  			$tr.remove();
		  		});
		  		//事件委托的方式,处理新增元素无法触发事件问题
		  		$(".table").on("click", ".btn-warning", function(){
		  			var $this = $(this);
		  			//改变操作按钮
		  			$this.removeClass("btn-warning");
		  			$this.addClass("btn-primary");
		  			$this.html("<i class='fa fa-unlock'></i> 启用");
		  			//改变是否属性
		  			var $td = $this.parent().prev().prev().prev();
		  			var tdHtml = "<td data-enabled='false'>否</td>";
		  			$($td).html(tdHtml);
		  		}); 
		  		
		  		$(".table").on("click", ".btn-primary", function(){
		  			var $this = $(this);
		  			//改变操作按钮
		  			$this.removeClass("btn-primary");
		  			$this.addClass("btn-warning");
		  			$this.html("<i class='fa fa-unlock'></i> 禁用");
		  			//改变是否属性
		  			var $td = $this.parent().prev().prev().prev();
		  			var tdHtml = "<td data-enabled='true'>是</td>";
		  			$($td).html(tdHtml);
		  		}); 
		  	 });
	    </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >编码管理</span></li>
		                   <li><a href="#" >编码管理</a></li>
		                   <li><a class="active">套餐管理</a></li>
		                 </ul>
		                 <h3 class="page-header"> 套餐管理 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	套餐管理-套餐信息更新、维护！ps：套餐最多为4条记录！
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row" >
					<div class="col-md-12">
						<div class="panel panel-cascade" id="base_info" style="display: none">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="" >
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												<input type="hidden" name="parentId" value="" /> 
												<input type="hidden" name="type" value="" /> 
												<input type="hidden" name="codeName" value="" />
											</div>
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${mstCode.id }" />
											
											<div class="form-group">
													<label class="col-lg-3 control-label">套餐名称： <sup>*</sup></label>
													<div class="col-lg-5">
														<select class="multiselect" name="packagetype" >
															<option value="0">请选择</option>
														</select> 
													</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">套餐价格： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="number" class="form-control" name="price"  value="" placeholder="请输入套餐价格" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">是否可用： <sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
															<label>
																 <input type="radio" name="enabled" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="enabled" value="0" /> 否
															</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button id="btn_save_single" type="button" class="btn btn-primary btn-xs"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel_single" type="button" class="btn btn-primary btn-xs"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
											
										</form>
									</div>
								</div>

							</div>
						</div>
						
							<!-- 应用信息  start-->
				<div class="row">
						<div class="panel">
							<div class="panel-heading text-primary">
								<h3 class="panel-title">
									套餐信息 <span class="pull-right">
									<a href="#" class="panel-minimize"><i
											class="fa fa-chevron-up"></i></a> 
									</span>
								</h3>
							</div>
							<div class="panel-body">
								<div class="panel panel-default">
									<div class="panel-heading">套餐管理- <button id="btn_add_single" type="button" class="btn btn-primary btn-xs">
						 		<i class="fa fa-plus fa-lg"></i> 新增
							 </button></div>
									<table class="table">
										<thead>
											<tr>
												<th>套餐ID</th>
												<th>套餐名称</th>
												<th>套餐价格</th>
												<th>是否可用</th>
												<th>创建时间</th>
												<th>创建者</th>
												<th>操作</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${cps }" var="g">
												<tr>
													<td>${g.id }</td>
													<td data-pt="${g.packagetype }">${g.packagetypeName }</td>
													<td>${g.price }</td>
													<c:if test="${g.enabled}">
														<td data-enabled="${g.enabled}">是</td>
													</c:if>
													<c:if test="${!g.enabled}">
														<td data-enabled="${g.enabled}">否</td>
													</c:if>
													<td><fmt:formatDate value="${g.createdate }" type="date" pattern="yyyy-MM-dd HH:mm:SS"/></td>
													<td>${g.cUName }</td>
													<td>
														<c:if test="${g.enabled}"><button class="btn btn-warning btn-xs"><i class="fa fa-lock"></i> 禁用</button></c:if>
														<c:if test="${!g.enabled }"><button class="btn btn-primary btn-xs"><i class="fa fa-unlock"></i> 启用</button></c:if>
														<button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"> 删除</i></button>
													</td>
												</tr>
											</c:forEach>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- 应用信息 end -->
					</div>
					<div class="form-group">
							<div class="col-lg-9 col-lg-offset-3">
								<button id="btn_save" type="button" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
								<button id="btn_cancel" type="button" class="btn btn-primary"><i class="fa fa-remove fa-lg"></i>取消</button>
							</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>