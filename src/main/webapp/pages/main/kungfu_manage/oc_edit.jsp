<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="CN">
<head>
<title>有点功夫-有点功夫管理-在线课程</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="renderer" content="webkit">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<jsp:include page="../../fragment/frag_top_css.jsp" />
<!-- 样式冲突 -->
<!--include fileinput.css  -->
<link href="${pageContext.request.contextPath}/css/fileinput.min.css"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
	rel="stylesheet">
<jsp:include page="../../fragment/frag_top_js.jsp" />

<!--include fileinput.js  -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/fileinput.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		QueryString.Initial();

		var menuId = QueryString.GetValue("menuId");
		var page = QueryString.GetValue("page");
		var rows = QueryString.GetValue("rows");
		var index_exp = QueryString.GetValue("index_exp");
		var index_che = QueryString.GetValue("index_che");

		var minDate = QueryString.GetValue("minDate");
		var maxDate = QueryString.GetValue("maxDate");
		var authorFol = QueryString.GetValue("authorFol");
		var titleFol = QueryString.GetValue("titleFol");

		$("input[name='menuId']").val(menuId);
		$("input[name='page']").val(page);
		$("input[name='rows']").val(rows);
		$("input[name='index_exp']").val(index_exp);
		$("input[name='index_che']").val(index_che);

		$("input[name='minDate']").val(minDate);
		$("input[name='maxDate']").val(maxDate);
		$("input[name='authorFol']").val(authorFol);
		$("input[name='titleFol']").val(titleFol);

		$(".cancel").click(function() {
			var reqPara = "?" + $(".folPara").find("input").serialize();
			reqPara = decodeURIComponent(reqPara, true);
			window.location.href = "${pageContext.request.contextPath}/onlineCourseController/list" + reqPara;
		});
		
		$.ajax({
			url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
			data : {parentId:"351"},
			type : "GET",
			async : true,
			dataType : "json",
			success : function(data) {
				var $vitype = $("select[name='type']");
	  			
	 			$.each(data, function(k, p) {
	 				 var vitype = "${onlineCourse.type}".split(",");
			  		 option = "<option  value='"+p.id+"'>"+p.name+"</option>";
			  		 $vitype.append(option);
			  		 
				  		 for(var i=0;i<vitype.length;i++){
				  			if(vitype[i]==p.id){
				  		     $(" select[name='type'] option[value='"+p.id+"']").attr("selected","selected"); 
				  			 }
				  		  }
			  		   
			  			
			  		});
	 		     
				
				$vitype.multiselect({
	                onChange: function(element, checked) {
	                	if(checked){
				    		var vitype = $("select[name='type']").val();
				    	}
	                },
					maxHeight: 200
	            });
			},
			error : function(data) {
				bootbox.alert({ 
					  size: "small",
					  title: "错误",
					  message: "请求超时，请重试！", 
					  callback: function(){return; }
				});
				return;
			}
	   });	
		
		$.ajax({
			url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
			data : {parentId:"379"},
			type : "GET",
			async : true,
			dataType : "json",
			success : function(data) {
				var $vitype = $("select[name='trainType']");
				$.each(data, function(k, p) {
		  			 var vitype = "${onlineCourse.trainType}";
		  			 if(vitype == p.id){
			  				option = "<option  value='"+p.id+"' selected='selected'>"+p.name+"</option>";
			  			 } else {
			  				 option = "<option  value='"+p.id+"'>"+p.name+"</option>";
			  			 }
		  			 
		  			$vitype.append(option);
		  		});
				$vitype.multiselect({
	                onChange: function(element, checked) {
	                	if(checked){
				    		var vitype = $("select[name='trainType']").val();
				    	}
	                },
					maxHeight: 400
	            });
			},
			error : function(data) {
				bootbox.alert({ 
					  size: "small",
					  title: "错误",
					  message: "请求超时，请重试！", 
					  callback: function(){return; }
				});
				return;
			}
	   });
		
		$.ajax({
			url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
			data : {parentId:"388"},
			type : "GET",
			async : true,
			dataType : "json",
			success : function(data) {
				var $vitype = $("select[name='kfClassType']");
				$.each(data, function(k, p) {
		  			 var vitype = "${onlineCourse.kfClassType}";
		  			 if(vitype == p.id){
			  				option = "<option  value='"+p.id+"' selected='selected'>"+p.name+"</option>";
			  			 } else {
			  				 option = "<option  value='"+p.id+"'>"+p.name+"</option>";
			  			 }
		  			$vitype.append(option);
		  		});
				$vitype.multiselect({
	                onChange: function(element, checked) {
	                	if(checked){
				    		var vitype = $("select[name='kfClassType']").val();
				    	}
	                },
					maxHeight: 200
	            });
			},
			error : function(data) {
				bootbox.alert({ 
					  size: "small",
					  title: "错误",
					  message: "请求超时，请重试！", 
					  callback: function(){return; }
				});
				return;
			}
	   });
		
	$.ajax({
			url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
			data : {parentId:"393"},
			type : "GET",
			async : true,
			dataType : "json",
			success : function(data) {
				var $vitype = $("select[name='instrumentType']");
				$.each(data, function(k, p) {
		  			 var vitype = "${onlineCourse.instrumentType}";
		  			 if(vitype == p.id){
			  				option = "<option  value='"+p.id+"' selected='selected'>"+p.name+"</option>";
			  			 } else {
			  				 option = "<option  value='"+p.id+"'>"+p.name+"</option>";
			  			 }
		  			$vitype.append(option);
		  		});
				$vitype.multiselect({
	                onChange: function(element, checked) {
	                	if(checked){
				    		var vitype = $("select[name='instrumentType']").val();
				    	}
	                },
					maxHeight: 200
	            });
			},
			error : function(data) {
				bootbox.alert({ 
					  size: "small",
					  title: "错误",
					  message: "请求超时，请重试！", 
					  callback: function(){return; }
				});
				return;
			}
	   });
	
	$.ajax({
		url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
		data : {parentId:"397"},
		type : "GET",
		async : true,
		dataType : "json",
		success : function(data) {
			var $vitype = $("select[name='runType']");
			$.each(data, function(k, p) {
	  			 var vitype = "${onlineCourse.runType}";
	  			 if(vitype == p.id){
		  				option = "<option  value='"+p.id+"' selected='selected'>"+p.name+"</option>";
		  			 } else {
		  				 option = "<option  value='"+p.id+"'>"+p.name+"</option>";
		  			 }
	  			$vitype.append(option);
	  		});
			$vitype.multiselect({
                onChange: function(element, checked) {
                	if(checked){
			    		var vitype = $("select[name='runType']").val();
			    	}
                },
				maxHeight: 200
            });
		},
		error : function(data) {
			bootbox.alert({ 
				  size: "small",
				  title: "错误",
				  message: "请求超时，请重试！", 
				  callback: function(){return; }
			});
			return;
		}
   });
	
	
	$.ajax({
		url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
		data : {parentId:"465"},
		type : "GET",
		async : true,
		dataType : "json",
		success : function(data) {
			var $vitype = $("select[name='reserved1']");
			$.each(data, function(k, p) {
	  			 var vitype = "${onlineCourse.reserved1}";
	  			 if(vitype == p.id){
		  				option = "<option  value='"+p.id+"' selected='selected'>"+p.name+"</option>";
		  			 } else {
		  				 option = "<option  value='"+p.id+"'>"+p.name+"</option>";
		  			 }
	  			$vitype.append(option);
	  		});
			$vitype.multiselect({
                onChange: function(element, checked) {
                	if(checked){
			    		var vitype = $("select[name='reserved1']").val();
			    	}
                },
				maxHeight: 200
            });
		},
		error : function(data) {
			bootbox.alert({ 
				  size: "small",
				  title: "错误",
				  message: "请求超时，请重试！", 
				  callback: function(){return; }
			});
			return;
		}
   });
	
	$.ajax({
		url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
		data : {parentId:"401"},
		type : "GET",
		async : true,
		dataType : "json",
		success : function(data) {
			var $vitype = $("select[name='reserved2']");
			$.each(data, function(k, p) {
 				 var vitype = "${onlineCourse.reserved2}".split(",");
		  		 option = "<option  value='"+p.id+"'>"+p.name+"</option>";
		  		 $vitype.append(option);
			  		 for(var i=0;i<vitype.length;i++){
			  			if(vitype[i]==p.id){
			  		     $(" select[name='reserved2'] option[value='"+p.id+"']").attr("selected","selected"); 
			  			 }
			  		  }
	  		});
			$vitype.multiselect({
                onChange: function(element, checked) {
                	if(checked){
			    		var vitype = $("select[name='reserved2']").val();
			    	}
                },
				maxHeight: 300
            });
		},
		error : function(data) {
			bootbox.alert({ 
				  size: "small",
				  title: "错误",
				  message: "请求超时，请重试！", 
				  callback: function(){return; }
			});
			return;
		}
   });
	

		
		$('form').bootstrapValidator({
			excluded : ':disabled',
			feedbackIcons : {
				valid : 'fa fa-check',
				invalid : 'fa fa-times',
				validating : 'fa fa-refresh'
			},
			fields : {
	            cover: {
	          		  validators: {
	                        file: {
	                            extension: 'jpeg,png,jpg',
	                            type: 'image/jpeg,image/png,image/jpg',
	                            maxSize: 2048 * 1024,   // 2 MB
	                            message: '文件格式非法，请重新上传'
	                        },
	                        callback: {
	                            message: '请上传文件',
	                            callback: function(value, validator) {
	                                // Get the selected options
	                                
	                                var $videoname = $("input[name='videoname']");
	                                var imgs = $(".defined1");
	                                
	                                return ($videoname.val() != undefined || imgs.length != 0);
	                            }
	                        }
	                    }
		            },
		          
	            cover1: {
	          		  validators: {
	                        file: {
	                            extension: 'jpeg,png,jpg',
	                            type: 'image/jpeg,image/png,image/jpg',
	                            maxSize: 2048 * 1024,   // 2 MB
	                            message: '文件格式非法，请重新上传'
	                        },
	                        callback: {
	                            message: '请上传文件',
	                            callback: function(value, validator) {
	                                // Get the selected options
	                                
	                                var $reserved12 = $("input[name='reserved12']");
	                                var imgs = $(".defined2");
	                                return ($reserved12.val() != undefined || imgs.length != 0);
	                            }
	                        }
	                    }
		        },
		        
	            cover2: {
	          		  validators: {
	                        file: {
	                            extension: 'jpeg,png,jpg',
	                            type: 'image/jpeg,image/png,image/jpg',
	                            maxSize: 2048 * 1024,   // 2 MB
	                            message: '文件格式非法，请重新上传'
	                        },
	                        callback: {
	                            message: '请上传文件',
	                            callback: function(value, validator) {
	                                // Get the selected options
	                                
	                                var $imagename = $("input[name='imagename']");
	                                var imgs = $(".defined3");
	                                
	                                return ($imagename.val() != undefined || imgs.length != 0);
	                            }
	                        }
	                    }
		            },
		     
				url : {
					validators : {
						notEmpty : {
							message : '课程包地址为空'
						},
					}
				},
				reserved11 : {
					validators : {
						notEmpty : {
							message : '背景音乐名称为空'
						},
					}
				},
				title : {
					validators : {
						notEmpty : {
							message : '视频标题为空'
						},
					}
				},
				courseno : {
					validators : {
						notEmpty : {
							message : '课程等级为空'
						},
					}
				},
				
				calorie : {
					validators : {
						notEmpty : {
							message : '视频卡路里为空'
						}, 
						regexp: {
                            regexp: "^[1-9]\\d*$",
                            message: '请输入正整数数字'
                        }
					}
				},
				introduction : {
					validators : {
						notEmpty : {
							message : '课程简介为空'
						},
					}
				},
				people : {
					validators : {
						notEmpty : {
							message : '适应人群为空'
						},
					}
				},
				descript : {
					validators : {
						notEmpty : {
							message : '课程描述为空'
						},
					}
				},
				notice : {
					validators : {
						notEmpty : {
							message : '训练器材为空'
						},
					}
				},
			},
			submitHandler : function(validator, form, submitButton) {

				validator.defaultSubmit();
			}
		});
	});
</script>
</head>
<body>

	<div class="site-holder">
		<!-- nav -->
		<jsp:include page="../../fragment/frag_nav.jsp" />

		<!-- .box-holder -->
		<div class="box-holder">
			<!--left-sidebar  -->
			<jsp:include page="../../fragment/frag_left.jsp" />

			<!-- .content -->
			<div class="content">
				<div class="row">
					<div class="col-mod-12">
						<ul class="breadcrumb">
							<li><span>有点功夫管理</span></li>
							<li><a href="javascript:void(0)" class="cancel">在线课程</a></li>
							<li><a href="javascript:void(0)" class="active">编辑在线课程</a></li>
						</ul>
						<h3 class="page-header">
							编辑在线课程<i class="fa fa-info-circle animated bounceInDown show-info"></i>
						</h3>

						<blockquote class="page-information hidden">
							<p>在线课程信息编辑！</p>
						</blockquote>
					</div>
				</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">基本信息</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal"
											action="${pageContext.request.contextPath}/onlineCourseController/save">

											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" /> <input
													type="hidden" name="rows" value="" /> <input type="hidden"
													name="page" value="" /> <input type="hidden"
													name="index_exp" value="" /> <input type="hidden"
													name="index_che" value="" /> <input type="hidden"
													name="minDate" value="" /> <input type="hidden"
													name="maxDate" value="" /> <input type="hidden"
													name="authorFol" value="" /> <input type="hidden"
													name="titleFol" value="" />
											</div>

											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${onlineCourse.id }" />
											
											<div class="form-group">
												<label class="col-lg-3 control-label"> 课程包地址： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="url"
														value="${onlineCourse.url }" placeholder="请输入课程包地址" />
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-lg-3 control-label"> 课程背景音乐名称： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="reserved11"
														value="${onlineCourse.reserved11}" placeholder="请输入背景音乐名称" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">课程封面小图(480*480)： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="file" class="form-control file-loading"
														name="cover2" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">课程封面(656*431)： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="file" class="form-control file-loading"
														name="cover" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">课程详情背景图(607*1080)： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="file" class="form-control file-loading"
														name="cover1" />
												</div>
											</div>
										
											<div class="form-group">
												<label class="col-lg-3 control-label"> 类型(课程一级分类)： <sup>*</sup></label>
												<div class="col-lg-5">
													<select class="multiselect" name="type" multiple="multiple">
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 二级分类： <sup>*</sup></label>
												<div class="col-lg-5">
													<select class="multiselect" name="reserved2" multiple="multiple">
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 课程标签： <sup>*</sup></label>
												<div class="col-lg-5">
													<select class="multiselect" name="reserved1" >
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 是否推荐： <sup>*</sup></label>
												<div class="col-lg-5">
													<select class="multiselect" name="reserved8" >
													 <c:if test="${onlineCourse.reserved8==true}" >
													<option value="1" selected="selected">推荐</option>
													<option value="0">不推荐</option>
													</c:if>
													 <c:if test="${onlineCourse.reserved8==false}" >
													<option value="1">推荐</option>
													<option value="0" selected="selected">不推荐</option>
													</c:if>
													<c:if test="${onlineCourse.reserved8==null}" >
													<option value="1">推荐</option>
													<option value="0">不推荐</option>
													</c:if>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 是否用于制定训练计划： <sup>*</sup></label>
												<div class="col-lg-5">
													<select class="multiselect" name="reserved7" >
													 <c:if test="${onlineCourse.reserved7==true}" >
													<option value="1" selected="selected">用于</option>
													<option value="0">不用于</option>
													</c:if>
													 <c:if test="${onlineCourse.reserved7==false}" >
													<option value="1">用于</option>
													<option value="0" selected="selected">不用于</option>
													</c:if>
													<c:if test="${onlineCourse.reserved7==null}" >
													<option value="1">用于</option>
													<option value="0">不用于</option>
													</c:if>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 训练能力类型(用于制定计划)： <sup>*</sup></label>
												<div class="col-lg-5">
													<select class="multiselect" name="trainType" ></select>
												</div>
											</div>	
											<div class="form-group">
												<label class="col-lg-3 control-label"> 功夫倾向类型(用于制定计划)： <sup>*</sup></label>
												<div class="col-lg-5">
													<select class="multiselect" name="kfClassType" ></select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 器械类型(用于制定计划)： <sup>*</sup></label>
												<div class="col-lg-5">
													<select class="multiselect" name="instrumentType" >
													<option value="0">无</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 跑步类型： <sup>*</sup></label>
												<div class="col-lg-5">
													<select class="multiselect" name="runType" >
													<option value="0">不属于</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 标题： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="title"
														value="${onlineCourse.title }" placeholder="请输入标题" />
												</div>
											</div>
										 <div class="form-group">
												<label class="col-lg-3 control-label"> 课程时长： <sup>*</sup></label>
												<div class="col-lg-5">
												   <div class="input-group">
													<input type="text" class="form-control" name="viewTime"
														value="${onlineCourse.viewTime }" placeholder="请输入课程时间，如：20" />
													  	<span class="input-group-addon">分钟</span>
													</div>
												</div>
											</div>	
										 <div class="form-group">
												<label class="col-lg-3 control-label"> 课程关联文章序号： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="articleNum"
														value="${onlineCourse.articleNum }" placeholder="请输关联文章序号" />
												</div>
											</div>	
											<div class="form-group">
												<label class="col-lg-3 control-label">可消耗卡路里： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="input-group">
													  	<input class="form-control" name="calorie" value='<fmt:formatNumber type="number" value="${onlineCourse.calorie }" pattern="0" maxFractionDigits="0"></fmt:formatNumber>' placeholder="请输入可消耗卡路里,如：10"/>
													  	<span class="input-group-addon">千卡</span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">课程等级： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<textarea class="form-control" rows="3" name="courseno" placeholder="请输入课程等级,如：Lv1">${onlineCourse.courseno }</textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">课程描述： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<textarea class="form-control" rows="7" name="descript" placeholder="请输入课程描述,系统自动以•符号标识条目">${onlineCourse.descript }</textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">课程简介： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<textarea class="form-control" rows="7" name="introduction" placeholder="请输入简介,系统自动以•符号标识条目">${onlineCourse.introduction }</textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">适宜人群： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<textarea class="form-control" rows="7" name="people" placeholder="请输入适宜人群,系统自动以•符号标识条目">${onlineCourse.people }</textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">训练器材： <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<textarea class="form-control" rows="7" name="notice" placeholder="请输入简介,系统自动以•符号标识条目">${onlineCourse.notice }</textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary">
														<i class="fa fa-check fa-lg"></i>保存
													</button>
													&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button"
														class="btn btn-primary cancel">
														<i class="fa fa-remove fa-lg"></i>取消
													</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp" />
</body>
<script>
	$(function() {
		var cover = [];
		var covers = "${onlineCourse.coverurl}".split(";");
		for (var i = 0; i < covers.length; i++) {
			 if(covers[i] != null && covers[i] != ""){
				 cover.push("<img src='"+covers[i]+"' class='file-preview-image defined1' style='width:180px;height:120px;' />");
			 }
			
		}
		$("input[name='cover']").fileinput({
			language : 'zh', //设置语言
			uploadUrl : "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
			uploadAsync : true, //异步加载
			allowedFileExtensions : [ 'jpg', 'png' ],//接收的文件后缀
			showUpload : false, //是否显示上传按钮
			showCaption : false,//是否显示标题
			overwriteInitial : true,
			maxFileSize : 1024 * 10,
			maxFileCount : 1,
			slugCallback : function(filename) {
				return filename.replace('(', '_').replace(']', '_');
			},

			showPreview : true,
			initialPreview : cover,
			allowedPreviewTypes : [ 'image' ],
			browseClass : "btn btn-primary", //按钮样式 
		});
		$("input[name='cover']").on("fileuploaded", function(event, data, previewId, index) {
			var $input = " <input type='hidden' id='"+previewId+"' name='videoname' value='"+data.response.resName+"'/>";
			var $form = $("form");
			$form.append($input);
			
			$('form')
	        .bootstrapValidator('updateStatus', 'cover', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'cover');
		});
		
		$("input[name='cover']").on("fileclear", function(event) {
			   var $videonames = $("input[name='videoname']");
			   
			   for(var i = 0 ; i < $videonames.length ; i ++){
				   $($videonames[i]).remove();
			   }
			   
			   var $imgs = $(".defined1");
			   for(var i = 0 ; i < $imgs.length ; i ++){
				   $($imgs[i]).remove();
			   }
			   
			   $('form')
		        .bootstrapValidator('updateStatus', 'cover', 'NOT_VALIDATED')
		        .bootstrapValidator('validateField', 'cover');
		 });
		
		$("input[name='cover").on('filereset', function(event) {
			  $('form')
		        .bootstrapValidator('updateStatus', 'cover', 'NOT_VALIDATED')
		        .bootstrapValidator('validateField', 'cover');
		});
		
		$("input[name='cover'").on('filesuccessremove', function(event, id) {
			$("input[id='"+id+"']").remove();
			$('form')
		     .bootstrapValidator('updateStatus', 'cover', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'cover');
		});
	
	
		var cover1 = [];
		var covers1 = "${onlineCourse.reserved12}".split(";");
		for (var i = 0; i < covers1.length; i++) {
			 if(covers1[i] != null && covers1[i] != ""){
				 cover1.push("<img src='"+covers1[i]+"' class='file-preview-image defined2' style='width:180px;height:120px;' />");
			 }
			
		}
		$("input[name='cover1']").fileinput({
			language : 'zh', //设置语言
			uploadUrl : "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
			uploadAsync : true, //异步加载
			allowedFileExtensions : [ 'jpg', 'png', 'gif' ],//接收的文件后缀
			showUpload : false, //是否显示上传按钮
			showCaption : false,//是否显示标题
			overwriteInitial : true,
			maxFileSize : 1024 * 10,
			maxFileCount : 1,
			slugCallback : function(filename) {
				return filename.replace('(', '_').replace(']', '_');
			},

			showPreview : true,
			initialPreview : cover1,
			allowedPreviewTypes : [ 'image' ],
			browseClass : "btn btn-primary", //按钮样式 
		});
		$("input[name='cover1']").on("fileuploaded", function(event, data, previewId, index) {
			var $input = " <input type='hidden' id='"+previewId+"' name='reserved12' value='"+data.response.resName+"'/>";
			var $form = $("form");
			$form.append($input);
			
			$('form')
	        .bootstrapValidator('updateStatus', 'cover1', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'cover1');
		});
		
		$("input[name='cover1']").on("fileclear", function(event) {
			
			 var $reserved12s = $("input[name='reserved12']");
			   
			 for(var i = 0 ; i < $reserved12s.length ; i ++){
				   $($reserved12s[i]).remove();
			 }
			 
			 var $imgs = $(".defined2");
			 for(var i = 0 ; i < $imgs.length ; i ++){
			    $($imgs[i]).remove();
			 }
			   
			 $('form')
		       .bootstrapValidator('updateStatus', 'cover1', 'NOT_VALIDATED')
		       .bootstrapValidator('validateField', 'cover1');
		 });
		
		$("input[name='cover1").on('filereset', function(event) {
			  $('form')
		        .bootstrapValidator('updateStatus', 'cover1', 'NOT_VALIDATED')
		        .bootstrapValidator('validateField', 'cover1');
		});
		
		$("input[name='cover1'").on('filesuccessremove', function(event, id) {
			$("input[id='"+id+"']").remove();
			$('form')
		     .bootstrapValidator('updateStatus', 'cover1', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'cover1');
		});
		
		
		var cover2 = [];
		var covers2 = "${onlineCourse.remark}".split(";");
		for (var i = 0; i < covers2.length; i++) {
			 if(covers2[i] != null && covers2[i] != ""){
				 cover2.push("<img src='"+covers2[i]+"' class='file-preview-image defined3' style='width:100px;height:100px;' />");
			 }
			
		}
		$("input[name='cover2']").fileinput({
			language : 'zh', //设置语言
			uploadUrl : "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
			uploadAsync : true, //异步加载
			allowedFileExtensions : [ 'jpg', 'png', 'gif' ],//接收的文件后缀
			showUpload : false, //是否显示上传按钮
			showCaption : false,//是否显示标题
			overwriteInitial : true,
			maxFileSize : 1024 * 10,
			maxFileCount : 1,
			slugCallback : function(filename) {
				return filename.replace('(', '_').replace(']', '_');
			},

			showPreview : true,
			initialPreview : cover2,
			allowedPreviewTypes : [ 'image' ],
			browseClass : "btn btn-primary", //按钮样式 
		});
		$("input[name='cover2']").on("fileuploaded", function(event, data, previewId, index) {
			var $input = " <input type='hidden' id='"+previewId+"' name='imagename' value='"+data.response.resName+"'/>";
			var $form = $("form");
			$form.append($input);
			
			$('form')
	        .bootstrapValidator('updateStatus', 'cover2', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'cover2');
		});
		
		$("input[name='cover2']").on("fileclear", function(event) {
			
			 var $imagenames = $("input[name='imagename']");
			   
			 for(var i = 0 ; i < $imagenames.length ; i ++){
				   $($imagenames[i]).remove();
			 }
			 
			 var $imgs = $(".defined3");
			 for(var i = 0 ; i < $imgs.length ; i ++){
			    $($imgs[i]).remove();
			 }
			   
			 $('form')
		       .bootstrapValidator('updateStatus', 'cover2', 'NOT_VALIDATED')
		       .bootstrapValidator('validateField', 'cover2');
		 });
		
		$("input[name='cover2").on('filereset', function(event) {
			  $('form')
		        .bootstrapValidator('updateStatus', 'cover2', 'NOT_VALIDATED')
		        .bootstrapValidator('validateField', 'cover2');
		});
		
		$("input[name='cover2'").on('filesuccessremove', function(event, id) {
			$("input[id='"+id+"']").remove();
			$('form')
		     .bootstrapValidator('updateStatus', 'cover2', 'NOT_VALIDATED')
		     .bootstrapValidator('validateField', 'cover2');
		});
		
		
	});
</script>
</html>