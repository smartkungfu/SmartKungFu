<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-有点功夫管理-在线课程提示语</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
	  	var paras = "";
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             					//cache: false, // 不缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				                pagination: true, // 开启分页功能
				               // singleSelect: false,
				                pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500],
				                search: false, // 关闭搜索功能
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                detailView:true,
				                detailFormatter:function detailFormatter(index, row) {
				                	 var html = [];
				                     html.push('<p><b>训练提示语:</b> <br/>' + row.tips + '</p>'); 
				                     
				                     return html.join('') ;
				                },
				                queryParams: function (params) {
									var page = params.offset;
				                	
				                	if((page * 1) != 0 ){
				            			page = page/params.limit + 1;
				            		}
				                	//排序，并处理多表中的属性
				                	var sort = params.sort;
				                	if(sort == "cUName" ){
				                		sort == "u." + sort;
				                	} else{
				                		sort == "tip." + sort;
				                	}
				                	var obj = {
				                			pageSize: params.limit,
					                        page: page,
					                        sort:sort,
							                order:params.order, 
					                        title:$("#title").val(),
					                        courseId:$("input[name='courseid']").val()
					                    };
				                	paras =  "pageSize=" + obj.pageSize + "&page=" + obj.page ;
				                	return obj;
				                },
				                url: "${pageContext.request.contextPath}/onlineCourseTipsController/queryList",
				                columns: [{
				                    field: 'state',
				                    checkbox: true
				                }, {
				                    field: 'covernum',
				                    title: '训练次数',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                }, {
				                    field: 'createdate',
				                    title: '发布时间',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return timeStamp2String(value);
				                    }
				                }, {
				                    field: 'cUName',
				                    title: '创建者',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true
				                },{
				                      title: '操作',
				                      field: 'id',
				                      align: 'center',
				                      events: operateEvents,
				                      formatter:function(value,row,index){  
						                   var eles = '<button class="btn btn-danger btn-xs delList"><i class="fa fa-trash-o"></i></button> '; 
						                   return eles ;
				                      } 
				                 }],
				                onLoadSuccess:function(){
				                },
				                onLoadError: function () {
				             	   //    mif.showErrorMessageBox("数据加载失败！");
				                }
            });
        }
	  	
		window.operateEvents = {
	  	        'click .delList': function (e, value, row, index) {
	  	        	 $.ajax({
	  					url : "${pageContext.request.contextPath}/onlineCourseTipsController/delList",
	  					data : {ids:row.id},
	  					type : "POST",
	  					async : true,
	  					dataType : "json",
	  					success : function(flag) {
	  						if(flag){
	  							bootbox.alert({ 
	  	 							  size: "small",
	  	 							  title: "正确",
	  	 							  message: "操作成功！", 
	  	 							  callback: function(){
	  	 								  $table.bootstrapTable('refresh');
	  	 								  return;
	  	 							 }
	  	 						});
	  						}else{
	  							bootbox.alert({ 
	 	 							  size: "small",
	 	 							  title: "错误",
	 	 							  message: "操作失败！", 
	 	 							  callback: function(){return; }
	 	 						});
	  						}
	  					},
	  					error : function(data) {
	  						bootbox.alert({ 
	  							  size: "small",
	  							  title: "错误",
	  							  message: "请求超时，请重试！", 
	  							  callback: function(){return; }
	  						});
	  						return;
	  					}
	  			   });	
	  	        },
	  	};
	  	
   		$(function(){
   			initTable();
   		 	
   		  	$('form').bootstrapValidator({
	            excluded: ':disabled',
	            feedbackIcons: {
	            	 valid: 'fa fa-check',
	            	 invalid: 'fa fa-times',
	            	 validating: 'fa fa-refresh'
	            },
	            fields: {
	                covernum: {
	                    validators: {
	                        notEmpty: {
	                            message: '训练次数为空'
	                        },
	                        regexp: {
	                            regexp: "^[1-9]\\d*$",
	                            message: '请输入1-8的正整数'
	                        },
	                        callback: {
	                            message: '输入格式不对，或训练次数已存在',
	                            callback: function(value, validator) {
	                            	
	                            	if( value <= 8){
	                            		
	                            		var flag = true;
	                            		var rows = $table.bootstrapTable('getData');
	                            		$(rows).each(function(index,element){
	                            			if(value == element.covernum){
	                            				flag = false;
	                            			}
	                            		});
	                            		return flag;
	                            	} else{
	                            		return false;
	                            	}
	                            }
	                        }
	                    }
	                },
	                tips: {
	                    validators: {
	                        notEmpty: {
	                            message: '训练提示语为空'
	                        },
	                        stringLength: {
	                            min: 3,
	                            max: 100,
	                            message: '长度在为3-300位'
	                        },
	                    } ,
	                   
	                }, 
	            },
	        });
   		  	
   		 $('#btn_save_single').submit(function (ev) {
             ev.preventDefault();
         });
   		 
         $('#btn_save_single').on("click", function () {
             var bootstrapValidator = $('form').data('bootstrapValidator');
             bootstrapValidator.validate();
			
             if (bootstrapValidator.isValid()) {
            	 $.ajax({
 					url : "${pageContext.request.contextPath}/onlineCourseTipsController/save",
 					data : $("form").serialize(),
 					type : "POST",
 					async : true,
 					dataType : "json",
 					success : function(flag) {
 						if(flag){
 							bootbox.alert({ 
 	 							  size: "small",
 	 							  title: "正确",
 	 							  message: "操作成功！", 
 	 							  callback: function(){
 	 								  $table.bootstrapTable('refresh');
 	 								  $("input[name='covernum']").val("");
 	 								$("textarea[name='tips']").val("");
 	 								  return;
 	 							 }
 	 						});
 						}else{
 							bootbox.alert({ 
	 							  size: "small",
	 							  title: "错误",
	 							  message: "操作失败！", 
	 							  callback: function(){return; }
	 						});
 						}
 					},
 					error : function(data) {
 						bootbox.alert({ 
 							  size: "small",
 							  title: "错误",
 							  message: "请求超时，请重试！", 
 							  callback: function(){return; }
 						});
 						return;
 					}
 			   });	
             } else {
                 return;
             }
         });
         
     	$(".cancel").click(function(){
  			var reqPara = "?" + $(".folPara").find("input").serialize();
  			reqPara = decodeURIComponent(reqPara,true);
  			window.location.href="${pageContext.request.contextPath}/onlineCourseController/list" + reqPara; 
  		});
   		});
   </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>有点功夫管理</span></li>
                   <li><a href="javascript:void(0)" class="active cancel">在线课程</a></li>
                   <li><a href="javascript:void(0)" class="active">在线课程提示语</a></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
 					 </div>
                </div>

                <h3 class="page-header"> 在线课程提示语 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

                <blockquote class="page-information hidden">
                 <p>
                  	更新、维护在线课程提示语信息！ps：提示语最多为8条记录！
                </p>
              </blockquote>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
					<div class="ro">
						<div class="col-mol-md-offset-2">
							<form method="post" class="form-horizontal" action="">

								<!--附件参数  -->
								<div class="folPara" style="display: none">
									<input type="hidden" name="menuId" value="${folPara.menuId }" />
									<input type="hidden" name="page" value="${folPara.page }" /> 
									<input type="hidden" name="pageSize" value="${folPara.pageSize }" />
									<input type="hidden" name="index_exp" value="${MENU_STYLE_INDEX_EXP }" />
									<input type="hidden" name="index_che" value="${MENU_STYLE_INDEX_CHE }" />
								</div>
								<!-- 逻辑参数 -->
								<input type="hidden" name="courseid" value="${folPara.courseId }" />
								<div class="form-group">
									<label class="col-lg-3 control-label">训练次数： <sup>*</sup></label>
									<div class="col-lg-5">
										<input type="number" class="form-control" name="covernum"
											value="" placeholder="请输入1-8的正整数" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">训练提示语： <sup>*</sup></label>
									<div class="col-lg-5">
										<textarea class="form-control" rows="7" name="tips" placeholder="请输入提示语，系统自动以●符号标识" ></textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-9 col-lg-offset-3">
										<button id="btn_save_single" type="button"
											class="btn btn-primary btn-xs">
											<i class="fa fa-check fa-lg"></i>保存
										</button>
										&nbsp;&nbsp;&nbsp;
										<button id="btn_cancel_single" type="button"
											class="btn btn-primary btn-xs">
											<i class="fa fa-remove fa-lg"></i>取消
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<table id="table">
				    </table>
		    </div>
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>