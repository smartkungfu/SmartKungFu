<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-统计管理-周数据统计</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script src="${pageContext.request.contextPath}/js/echarts.min.js"></script>
	  	<script type="text/javascript">
	  	var paras = "";
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             					//cache: false, // 不缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				                pagination: true, // 开启分页功能
				               // singleSelect: false,
				                pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500],
				                search: false, // 关闭搜索功能
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                //smartDisplay: true, // 智能显示 pagination 和 cardview 等
				                queryParams: function (params) {
									var page = params.offset;
				                	
				                	if((page * 1) != 0 ){
				            			page = page/params.limit + 1;
				            		}
				                	
				                  	//排序，并处理多表中的属性vitypeName
				                	var sort = params.sort;
				                	if(sort == "cUName"){
				                		sort == "u." + sort;
				                	} else if(sort == "title"){
				                		sort == "c2." + sort;
				                	} 
				                	var obj = {
					                        rows: params.limit,
					                        page: page,
					                        sort:sort,
							                order:params.order, 
					                        title:$("#titleFol").val(),
					                    };
				                	paras =  "rows=" + obj.rows + "&page=" + obj.page ;
				                	return obj;
				                },
				               
				               
				               
            });
        }
	 
   		$(function(){
   			initTable();
   			
   		 $.ajax({                         
		    	url:'${pageContext.request.contextPath}/dataController/queryList3',
		        type:'post',
		        dataType:'json',
		        success: function(data){
		        	var arry2=data.toString();
		        	var arry=arry2.split(",");
		      // 基于准备好的dom，初始化echarts实例
		            var myChart = echarts.init(document.getElementById('weekNum'));
		            // 指定图表的配置项和数据
		            var option = {
		                title: {
		                    text: '本年新增数据统计'
		                },
		                color: ['#FF69B4','#003366', '#006699', '#4cabce', '#e5323e'],
		                tooltip: {
		                 trigger: 'axis',
		                 axisPointer : {            // 坐标轴指示器，坐标轴触发有效
		                  type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
		                   }
		                },
		                legend: {
		                    data:['用户新增数量','动态新增数量','问题新增数量','回答新增数量','嘿哈新增数量']
		                },
		                xAxis: {
		                	 data : ['一月', '二月', '三月', '四月', '五月', '六月', '7月','8月','9月','10月','11月','12月']
		                },
		                yAxis: {
		                 type: 'value',
		                 scale: true,
		                 name: '数量',
		                 max: 100,
		                 min: 0,
		                 splitNumber:10,
		                 boundaryGap: [0.2, 0.2]
		                },
		                series: [
		                {
		                    name: '用户新增数量',
		                    type: 'line',
		                   // barWidth : 30,//柱图宽度
		                    data: [arry[0],arry[1],arry[2],arry[3],arry[4],arry[5],arry[6],arry[7],arry[8],arry[9],arry[10],arry[11]]
		                },
		                {
		                    name: '动态新增数量',
		                    type: 'line',
		                   // barWidth : 30,//柱图宽度
		                    data: [arry[12],arry[13],arry[14],arry[15],arry[16],arry[17],arry[18],arry[19],arry[20],arry[21],arry[22],arry[23]]
		                },
		                {
		                    name: '问题新增数量',
		                    type: 'line',
		                   // barWidth : 30,//柱图宽度
		                    data: [arry[24],arry[25],arry[26],arry[27],arry[28],arry[29],arry[30],arry[31],arry[32],arry[33],arry[34],arry[35]]
		                },
		                {
		                    name: '回答新增数量',
		                    type: 'line',
		                   // barWidth : 30,//柱图宽度
		                    data: [arry[36],arry[37],arry[38],arry[39],arry[40],arry[41],arry[42],arry[43],arry[44],arry[45],arry[46],arry[47]]
		                },
		                {
		                    name: '嘿哈新增数量',
		                    type: 'line',
		                   // barWidth : 30,//柱图宽度
		                    data: [arry[48],arry[49],arry[50],arry[51],arry[52],arry[53],arry[54],arry[55],arry[56],arry[57],arry[58],arry[59]]
		                },
		                ]
		            };

		            // 使用刚指定的配置项和数据显示图表。
		            myChart.setOption(option);
		        	
		                }
		            });
            
            
   		 
   		});
   		
	    </script>

   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     		
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>数据统计</span></li>
                   <li><a href="javascript:void(0)" class="active">月数据统计</a></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
                  		<c:forEach var="g" items="${btns }">
                  			 <button type="button" class="btn btn-primary" onclick="${g.action }">
						 		<i class="${g.btnicon }"></i> ${g.btnname }
							 </button>
                  		</c:forEach>
 					 </div>
                </div>

                <h3 class="page-header">月数据统计<i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

                <blockquote class="page-information hidden">
                 <p>
                	查看数据管理信息！
                </p>
              </blockquote>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
  <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
    <div id="weekNum" style="margin-top:50px;width: 1200px;height:400px;"></div>
				  
		    </div>
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>