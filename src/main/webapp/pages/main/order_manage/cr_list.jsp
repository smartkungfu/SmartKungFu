<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-订单管理-退款管理</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
	  	var paras = "";
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             					//cache: false, // 不缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				                pagination: true, // 开启分页功能
				               // singleSelect: false,
				                pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500],
				                search: false, // 关闭搜索功能
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                //smartDisplay: true, // 智能显示 pagination 和 cardview 等
				                   detailView:true,
				                detailFormatter:function detailFormatter(index, row) {
				                	  var html = [];
				                	  
				                	  if( row.mstOrder.tradeno != null && row.mstOrder.tradeno != ""){
				                		  html.push('<p><b>商户交易号:</b> ' + row.mstOrder.tradeno + '</p>');  
				                	  } else {
				                		  html.push('<p><b>商户交易号:</b> - </p>');
				                	  }
				                	  
				                	  if( row.mstOrder.alipaytradeno != null && row.mstOrder.alipaytradeno != ""){
				                		  html.push('<p><b>阿里交易号:</b> ' + row.mstOrder.alipaytradeno + '</p>');  
				                	  } else {
				                		  html.push('<p><b>阿里交易号:</b> - </p>');
				                	  }
				                     
				                	  if(row.rr != null){
				                		  if(row.rr.ticprice != null && row.rr.ticprice != ""){
				                			  html.push('<p><b>返还优惠:</b>¥' + (row.rr.ticprice * 1).toFixed(2) + '</p>'); 
				                		  }else{
				                			  html.push('<p><b>返还优惠:</b>¥0.00</p>');
				                		  }
				                		  
				                		  if(row.rr.graprice != null && row.rr.graprice != ""){
				                			  html.push('<p><b>返还等级:</b>¥' + (row.rr.graprice * 1).toFixed(2) + '</p>'); 
				                		  }else{
				                			  html.push('<p><b>返还等级:</b>¥0.00</p>');
				                		  }
				                		  
				                		  if(row.rr.gradevalue != null && row.rr.gradevalue != ""){
				                			  html.push('<p><b>返功力值:</b>' + row.rr.gradevalue + '</p>'); 
				                		  }else{
				                			  html.push('<p><b>返功力值:</b>0</p>');
				                		  }
					                      html.push('<p><b>返回现金:</b> ¥' + (row.rr.refprice * 1).toFixed(2) + '</p>'); 
					                      html.push('<p><b>处理时间:</b> ' + timeStamp2String(row.rr.createdate) + '</p>'); 
					                      html.push('<p><b>处理人:</b> ' + row.rr.cUName + '</p>'); 
				                	  }
				                      
				                      return html.join('');
				    	   		},
				                queryParams: function (params) {
									var page = params.offset;
				                	
				                	if((page * 1) != 0 ){
				            			page = page/params.limit + 1;
				            		}
				                	//排序，并处理多表中的属性
				                	var sort = params.sort;
				                	if(sort == "cUName" ){
				                		sort == "u1." + sort;
				                	} else{
				                		sort == "cr." + sort;
				                	}
				                	var obj = {
					                        rows: params.limit,
					                        page: page,
					                        sort:sort,
							                order:params.order, 
					                        orderNo:$("#orderNo").val(),
					                    };
				                	paras =  "rows=" + obj.rows + "&page=" + obj.page ;
				                	return obj;
				                },
				                url: "${pageContext.request.contextPath}/clientRefundController/queryList",
				                columns: [{
				                    field: 'state',
				                    checkbox: true
				                }, {
				                    field: 'mstOrder.orderno',
				                    title: '订单号',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                }, {
				                    field: 'mstClient.namecn',
				                    title: '用户名称',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                }, {
				                    field: 'mstClient.nickname',
				                    title: '用户昵称',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                },{
				                    field: 'causeName',
				                    title: '退款原因',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                }, {
				                    field: 'refnum',
				                    title: '退款数量',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                }, {
				                    field: 'refprice',
				                    title: '退款价格',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return "¥" + (value * 1).toFixed(2);
				                    }
				                },{
				                    field: 'createdate',
				                    title: '申请日期',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return timeStamp2String(value);
				                    }
				                },{
				                    field: 'refStatusName',
				                    title: '退款状态',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                },{
				                    field: 'op',
				                    title: '操作',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    events: operateEvents,
				                    formatter:function(value,row,index){  
				                    	var htmlCell = '';
				                    	if(row.refstatus == "241"){
				                    		htmlCell = "-";
				                    	} else if(row.refstatus == "242"){
				                    		htmlCell += '<button class="btn btn-primary btn-xs going">待处理</button>&nbsp;&nbsp;';
				                    	} else if(row.refstatus == "244"){
				                    		htmlCell += '<button class="btn disabled btn-primary btn-xs detail">已处理</button>&nbsp;&nbsp;';
				                    	} else {
				                    		htmlCell = "-";
				                    	}
				                    	
				                    	return htmlCell;
				                    }
				                }],
				                onLoadSuccess:function(){
				                },
				                onLoadError: function () {
				             	   //    mif.showErrorMessageBox("数据加载失败！");
				                }
            });
        }
	  	
	  	window.operateEvents = {
	  	        'click .wait': function (e, value, row, index) {
	  	        	//更新订单状态
	  	        	 $.ajax({
	  	      		  	 type: "post",
	  	      		  	 data: {id:row.id,status:243,orderdetailid:row.orderdetailid},
	  	      		  	 url: "${pageContext.request.contextPath}/clientRefundController/UpdateStatus",
	  	      		  	 success: function (data) {
	  	      		  	bootbox.confirm({
		  	      	  		size: "small",
		  	      	  	    title: "提示",
		  	      	  	    message: "继续前往处理？",
			  	      	  	buttons: {
		  	      	  	        cancel: {
		  	      	  	            label: '<i class="fa fa-times"></i> 否'
		  	      	  	        },
		  	      	  	        confirm: {
		  	      	  	            label: '<i class="fa fa-check"></i> 是'
		  	      	  	        }
		  	      	  	    },
		  	      	  	    callback: function (result) {
		  	      	  	        //console.log('This was logged in the callback: ' + result);
		  	      	  	        if(result){
				  	      	  	  	paras = "id="+ row.id + "&"+paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
									paras = decodeURIComponent(paras,true);
									window.location.href = "${pageContext.request.contextPath}/clientRefundController/edit" + "?"+ paras; 
		  	      	  	        }else{
		  	      	  	     		 $table.bootstrapTable('refresh'); 
		  	      	  	        }
		  	      	  	    }
		  	      	  		});
	  	      		  	 }
	  	      		});
	  	        },
	  	      'click .going': function (e, value, row, index) {
		  	    	paras = "id="+ row.id + "&"+paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
					paras = decodeURIComponent(paras,true);
					window.location.href = "${pageContext.request.contextPath}/clientRefundController/edit" + "?"+ paras;
	  	        	
	  	      },
	  	};
	  	
	  	function addObj(url){
	  		paras = paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
			paras = decodeURIComponent(paras,true);
			window.location.href = url + "?"+ paras;
	  	}
	  	
	  	function delList(url){
	  		var rows = $table.bootstrapTable('getSelections');
			var ids = "";
			$(rows).each(function(index,element){
				ids += element.id + ",";
			});
			ids = ids.substring(0,ids.length - 1);
			delListAsUtils({ids:ids},url,$table);
	  	}
	  	
	  	function editObj(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择编辑行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条编辑数据", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "id=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	  	
   		$(function(){
   			initTable();
   			
   			$("#btn_search").click(function(){
   				$table.bootstrapTable('refresh');
   			});
   		 
   		});
	    </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>订单管理</span></li>
                   <li><a href="javascript:void(0)" class="active">退款管理</a></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
                  		<c:forEach var="g" items="${btns }">
                  			 <button type="button" class="btn btn-primary" onclick="${g.action }">
						 		<i class="${g.btnicon }"></i> ${g.btnname }
							 </button>
                  		</c:forEach>
 					 </div>
                </div>

                <h3 class="page-header"> 退款管理 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

                <blockquote class="page-information hidden">
                 <p>
                  	更新、维护退款申请！
                </p>
              </blockquote>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
					<form class="form-inline" role="form" style="margin-top: 10px;margin-bottom: 10px;">
						  <input type="hidden" name="menuId" value="${folPara.menuId }">
						  <input type="hidden" name="masterType" value="${folPara.masterType }">
						  <div class="form-group">
						    <label for="orderNo">订单号：</label>
						    <input type="text" class="form-control" id="orderNo" name="orderNo"  value="${folPara.orderNo }">
						  </div>
						  <button id="btn_search" type="button" class="btn btn-primary" >
						 	<i class="fa fa-search fa-lg"></i> 搜索
						  </button>
					</form>
				    <table id="table">
				    </table>
		    </div>
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>