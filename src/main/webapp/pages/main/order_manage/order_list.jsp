<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-订单管理-订单管理</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
	  	var paras = "";
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             					//cache: false, // 不缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				                detailView:true,
				                detailFormatter:function detailFormatter(index, row) {
				                	  var html = [];
				                	  var tableHtml2 = "";
				                	  
				                	  if( row.mstClient.nickname !=null &&  row.mstClient.nickname != ""){
				                		  html.push('<p><b>用户名称:</b> ' + row.mstClient.nickname + '</p>');
				                	  } else {
				                		  html.push('<p><b>用户名称:</b> - </p>');
				                	  }
				                	 
				                      html.push('<p><b>联系人:</b> ' + row.contact + '</p>'); 
				                      html.push('<p><b>联系电话:</b> ' + row.phoneno + '</p>'); 
				                      html.push('<p><b>支付方式:</b> ' + row.paytypeName + '</p>'); 
				                      
				                      if(row.paydate != null){
				                    	  html.push('<p><b>支付时间:</b> ' + timeStamp2String(row.paydate) + '</p>');	  
				                      } else {
				                    	  html.push('<p><b>支付时间:</b> - </p>');
				                      }
				                      
				                      if( row.tradeno != null && row.tradeno != ""){
				                		  html.push('<p><b>商户交易号:</b> ' + row.tradeno + '</p>');  
				                	  } else {
				                		  html.push('<p><b>商户交易号:</b> - </p>');
				                	  }
				                	  
				                	  if( row.alipaytradeno != null && row.alipaytradeno != ""){
				                		  html.push('<p><b>第三方交易号:</b> ' + row.alipaytradeno + '</p>');  
				                	  } else {
				                		  html.push('<p><b>第三方交易号:</b> - </p>');
				                	  }
				                     
				                      len = row.ods.length;
				                      if(len != 0){
				                    	  tableHtml2 += '<table  class="table table-bordered table-hover table-striped display" id="example" >';
					                      tableHtml2 += '<thead>';
					                      tableHtml2 += '<tr>';
					                      tableHtml2 += '<th>数量</th>';
					                      tableHtml2 += '<th>预算价格</th>';
					                      tableHtml2 += '<th>优惠卷</th>';
					                      tableHtml2 += '<th>优惠金额</th>';
					                      tableHtml2 += '<th>功夫等级</th>';
					                      tableHtml2 += '<th>等级价格</th>';
					                      tableHtml2 += '<th>实际价格</th>';
					                      tableHtml2 += '<th>状态</th>';
					                      tableHtml2 += '</tr>';
					                      tableHtml2 += '</thead>';
					                      tableHtml2 += '<tbody>';
					                      for(var i = 0 ; i < len ; i ++){
					                    	  tableHtml2 += '<tr>';
					                    	  tableHtml2 += '<td class="center">'+row.ods[i].num+'</td>';
					                    	  tableHtml2 += '<td class="center">¥'+(row.ods[i].preprice).toFixed(2)+'</td>';
					                    	  if(row.ods[i].cr != null){
					                    		  tableHtml2 += '<td class="center"><span class="label bg-info">'+row.ods[i].cr.content+'</span></td>';
					                    	  } else{
					                    		  tableHtml2 += '<td class="center"><span class="label bg-info">-</span></td>';
					                    	  }
					                    	 
					                    	  tableHtml2 += '<td class="center">¥'+(row.ods[i].ticprice * 1).toFixed(2)+'</td>';
					                    	  tableHtml2 += '<td class="center">'+ row.ods[i].graName +'</td>';
						                      tableHtml2 += '<td class="center">¥'+ (row.ods[i].graprice * 1).toFixed(2) +'</td>';
						                      tableHtml2 += '<td class="center">¥'+ (row.ods[i].actprice * 1).toFixed(2) +'</td>';
						                      tableHtml2 += '<td class="center"><span class="badge bg-warning">'+row.ods[i].orderStatusName+'</span></td>';
						                      tableHtml2 += '</tr>';
					                      }
					                      tableHtml2 += '</tbody>';
					                      tableHtml2 += '</table>';
				                      }else{
				                    	  tableHtml2 = '<p><b>找不到订单详情信息</b></p>';
				                      }
				                      
				                      return html.join('') + tableHtml2;
				    	   		},
				                pagination: true, // 开启分页功能
				               // singleSelect: false,
				                pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500],
				                search: false, // 关闭搜索功能
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                //smartDisplay: true, // 智能显示 pagination 和 cardview 等
				                queryParams: function (params) {
									var page = params.offset;
				                	
				                	if((page * 1) != 0 ){
				            			page = page/params.limit + 1;
				            		}
				                	//排序，并处理多表中的属性
				                	var sort = params.sort;
				                	if(sort == undefined){
				                		sort = "";
				                	} else if(sort == "mstClient.namecn"){
				                		sort = "cli.namecn";
				                	} else if(sort == "mstClient.nickname"){
				                		sort = "cli.nickname";
				                	} else if(sort == "mstClient.gender"){
				                		sort = "cli.gender";
				                	} else if(sort == "mstClient.mobileno"){
				                		sort = "cli.mobileno";
				                	} else if(sort == "mstClient.thirdno"){
				                		sort = "cli.thirdno";
				                	} else if(sort == "packageTypeName" ){
				                		sort = "paytypeName";
				                	} else if(sort == "packageName" ){
				                		sort = "packageName";
				                	} else if(sort == "acttypeName" ){
				                		sort = "acttypeName";
				                	} else{
				                		sort = "o." + sort;
				                	}
				                	var obj = {
					                        pageSize: params.limit,
					                        page: page,
					                        sort:sort,
							                order:params.order, 
					                        minDate:$("#minDate").val(),
					                        maxDate:$("#maxDate").val(),
					                        account:$("#usrAccount").val(),
					                        orderNo:$("#orderNo").val(),
					                        mobileNo:$("#mobileNo").val(),
					                    };
				                	paras =  "pageSize=" + obj.pageSize + "&page=" + obj.page ;
				                	
				                	return obj;
				                },
				                url: "${pageContext.request.contextPath}/mstOrderController/queryList",
				                columns: [{
				                    field: 'state',
				                    checkbox: true
				                }, {
				                    field: 'orderno',
				                    title: '订单号',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                }, {
				                    field: 'acttypeName',
				                    title: '活动/场馆',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                }, {
				                    field: 'actname',
				                    title: '标题',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                }, {
				                    field: 'actbill',
				                    title: '海报',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){ 
				                    	if(value != null){
				                    		return "<img height='35px' width='50px' src='"+value.split(";")[0]+"' alt='' class='img'/> ";
				                    	} else {
				                    		return "<img height='35px' width='50px' src='' alt='' class='img'/> ";
				                    	}
				                    }
				                },{
				                    field: 'orderStatusName',
				                    title: '订单状态',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){ 
				                    	var len = row.ods.length;
				                    	var orderStatusNames = "";
				                    	for(var i = 0 ;i < len ; i ++ ){
				                    		orderStatusNames += row.ods[i].orderStatusName +",";
				                    	}
				                    	return '<span class="badge bg-warning">'+orderStatusNames.substring(0,orderStatusNames.length - 1)+'</span>';
				                    }
				                },{
				                    field: 'packageTypeName',
				                    title: '套餐',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                },{
				                    field: 'price',
				                    title: '单价',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){ 
				                    	return  "¥" + value.toFixed(2);
				                    }
				                },{
				                    field: 'preprice',
				                    title: '预算价格',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){ 
				                    	return  "¥" + value.toFixed(2);
				                    }
				                },  {
				                    field: 'actprice',
				                    title: '付款价格',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return  "¥" + value.toFixed(2);
				                    }
				                }, {
				                    field: 'createdate',
				                    title: '下单时间',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return timeStamp2String(value);
				                    }
				                }, ],
				                onLoadSuccess:function(){
				                },
				                onLoadError: function () {
				             	   //    mif.showErrorMessageBox("数据加载失败！");
				                }
            });
        }
	  	
	  	function addObj(url){
	  		paras = paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
			paras = decodeURIComponent(paras,true);
			window.location.href = url + "?"+ paras;
	  	}
	  	
	  	function delList(url){
	  		var rows = $table.bootstrapTable('getSelections');
			var ids = "";
			$(rows).each(function(index,element){
				ids += element.id + ",";
			});
			ids = ids.substring(0,ids.length - 1);
			delListAsUtils({ids:ids},url,$table);
	  	}
	  	
	  	function editObj(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择编辑行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条编辑数据", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "id=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize() + "&index_exp=${MENU_STYLE_INDEX_EXP}&index_che=${MENU_STYLE_INDEX_CHE}";
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	  	
	  	function updateObj(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择编辑行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条编辑数据", 
					  callback: function(){return; }
				});
	  		} else {
	  			var oid =rows[0].id;
	  			var orderStatus = rows[0].orderStatus;
	  			$.ajax({
 	      		  	 type: "post",
  	      		  	 data: {id:oid},
  	      		  	 url: url,
  	      		  	 success:function(data){

  	      		  		 if(data =="success"){
 		  	   	  			bootbox.alert({ 
 			  					  size: "small",
 			  					  title: "提示",
 			  					  message: "操作成功", 
 			  					  callback: function(){return; }
 			  				});
  	      		  			$table.bootstrapTable('refresh');
  	      		  		 }else{
 		  	   	  			bootbox.alert({ 
			  					  size: "small",
			  					  title: "提示",
			  					  message: "操作失败", 
			  					  callback: function(){return; }
			  				}); 	      		  			 
  	      		  		 }
  	      		  	 }
	  			})
	  		}
	  	}
	  	
   		$(function(){
   			initTable();
   			
   			$("#btn_search").click(function(){
   				$table.bootstrapTable('refresh');
   			});
   		 
   		});
	    </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>订单管理</span></li>
                   <li><a href="javascript:void(0)" class="active">订单管理</a></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
                  		<c:forEach var="g" items="${btns }">
                  			 <button type="button" class="btn btn-primary" onclick="${g.action }">
						 		<i class="${g.btnicon }"></i> ${g.btnname }
							 </button>
                  		</c:forEach>
 					 </div>
                </div>

                <h3 class="page-header"> 订单管理<i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

                <blockquote class="page-information hidden">
                 <p>
                  	编辑、维护订单信息！
                </p>
              </blockquote>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
					<form class="form-inline" role="form" style="margin-top: 10px;margin-bottom: 10px;">
						  <input type="hidden" name="menuId" value="${folPara.menuId }">
						  
						  <div class="form-group">
						    <label for="minDate">下单时间：</label>
						    <input type="date" class="form-control" id="minDate" name="minDate" value="${folPara.minDate }"> -
						    <input type="date" class="form-control" id="maxDate" name="maxDate"  value="${folPara.maxDate }">
						  </div>
						  <br/><br/>
						  <div class="form-group">
						    <label for="usrAccount">订单编号：</label>
						    <input type="text" class="form-control" id="orderNo" name="orderNo"  value="${folPara.orderNo }">
						  </div>
						<%--   <div class="form-group">
						    <label  for="namecn">用户账号：</label>
						    <input type="text" class="form-control" id="usrAccount" name="usrAccount"  value="${folPara.usrAccount }">
						  </div>
						   <div class="form-group">
						    <label  for="mobileNo">手机号：</label>
						    <input type="text" class="form-control" id="mobileNo" name="mobileNo"  value="${folPara.mobileNo }">
						  </div> --%>
						  <!-- <div class="checkbox">
						    <label>
						      <input type="checkbox"> Remember me
						    </label>
						  </div> -->
						  <button id="btn_search" type="button" class="btn btn-primary" >
						 	<i class="fa fa-search fa-lg"></i> 搜索
						  </button>
					</form>
				    <table id="table">
				    </table>
		    </div>
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>