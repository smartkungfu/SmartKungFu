<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-订单管理-退款管理</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var orderNo = QueryString.GetValue("orderNo");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);

		  		$("input[name='orderNo']").val(orderNo);
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize() ;
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/clientRefundController/list" + reqPara; 
		  		});
			  	  $('form').bootstrapValidator({
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh',
			            },
			            fields: {
			                enabled: {
			                    validators: {
			                        notEmpty: {
			                            message: '请选择可用性',
			                        }
			                    }
			                },
			                refPrice: {
			                    validators: {
			                        notEmpty: {
			                            message: '返还现金为空',
			                        },
			                        regexp: {
			                            regexp: "^([+-]?)\\d*\\.?\\d+$",
			                            message: '返还现金格式不正确',
			                        },
			                        between: {
			                            min: 0.00,
			                            max: ${cr.mstOrder.actprice},
			                            message: '单价范围在¥0.00-¥${cr.mstOrder.actprice}'
			                        }
			                    }
			                },
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	validator.defaultSubmit();
			            }
			        });
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >订单管理</span></li>
		                   <li><a  href="javascript:void(0)" class="cancel">退款管理</a></li>
		                   <li><a  href="javascript:void(0)" class="active">退款处理</a></li>
		                 </ul>
		                 <h3 class="page-header"> 退款处理<i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	退出处理信息编辑！
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<table id="teacher_table" data-toggle="table" data-page-size="5"
					data-card-view="true">
					<thead>
						<tr>
							<th data-sortable="true" data-align="center" data-field="t1">订单号</th>
							<th data-sortable="true" data-align="center" data-field="t2">用户名称</th>
							<th data-sortable="true" data-align="center" data-field="t3">活动/场馆</th>
							<th data-sortable="true" data-align="center" data-field="t4">标题</th>
							<th data-sortable="true" data-align="center" data-field="t5">付款价格</th>
							<th data-sortable="true" data-align="center" data-field="t6">支付日期</th>
							<th data-sortable="true" data-align="center" data-field="t7">优惠价格</th>
							<th data-sortable="true" data-align="center" data-field="t8">等级优惠</th>
							<th data-sortable="true" data-align="center" data-field="t9">成长功力值</th>
							<th data-sortable="true" data-align="center" data-field="t10">退款数量</th>
							<th data-sortable="true" data-align="center" data-field="t11">单价</th>
							<th data-sortable="true" data-align="center" data-field="t12">申请退款金额</th>
							<th data-sortable="true" data-align="center" data-field="t13">可退金额</th>
						</tr>
					</thead>
					<tr>
						<td>${cr.mstOrder.orderno }</td>
						<td>${cr.mstClient.namecn }</td>
						<c:if test="${cr.mstOrder.acttype eq 11 }">
							<td>活动</td>
						</c:if>
							<c:if test="${cr.mstOrder.acttype ne 11 }">
							<td>场馆</td>
						</c:if>
						<td>${cr.mstOrder.actname }</td>
						<td>¥${cr.mstOrder.actprice }</td>
						<td><fmt:formatDate value="${cr.mstOrder.paydate }"  pattern="yyyy-MM-dd HH:mm"/></td>
						<td>¥${cr.od.ticprice }</td>
						<td>¥${cr.od.graprice }</td>
						<td>${cr.cg.growthvalue }</td>
						<td>${cr.refnum }</td>
						<td>¥${cr.mstOrder.price }</td>
						<td>¥${cr.refprice }</td>
						<td>¥${cr.mstOrder.actprice }</td>
					</tr>
				</table>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									退款信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/refundResultController/save">
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												<input type="hidden" name="orderNo" value="" /> 
											</div>
											<!-- 逻辑参数 -->
											<!-- 退款申请信息 -->
											<input type="hidden" name="id" value="${cr.id }" />
											<input type="hidden" name="clientid" value="${cr.clientid }" />
											<!--订单信息  -->
											<input type="hidden" name="mstOrder.orderno" value="${cr.mstOrder.orderno }" />
											<input type="hidden" name="mstOrder.phoneno" value="${cr.mstOrder.phoneno }" />
											<!--订单详情信息 -->
											<input type="hidden" name="od.id" value="${cr.od.id }" />
											<input type="hidden" name="od.orderid" value="${cr.od.orderid }" />
											<input type="hidden" name="od.ticket" value="${cr.od.ticket }" />
											<input type="hidden" name="od.ticprice" value="${cr.od.ticprice }" />
											<input type="hidden" name="od.grade" value="${cr.od.grade }" />
											<input type="hidden" name="od.reserved1" value="${cr.od.reserved1 }" />
											<input type="hidden" name="od.reserved2" value="${cr.od.reserved2 }" />
											<input type="hidden" name="od.graprice" value="${cr.od.graprice }" />
											<!--用户信息 -->
											<input type="hidden" name="mstClient.mobileno" value="${cr.mstClient.mobileno }" />
											<!--用户功力值 -->
											<input type="hidden" name="cg.id" value="${cr.cg.id }" />
											<input type="hidden" name="cg.growthid" value="${cr.cg.growthid }" />
											<input type="hidden" name="cg.growthtype" value="${cr.cg.growthtype }" />
											<input type="hidden" name="cg.growthvalue" value="${cr.cg.growthvalue }" />
											<div class="form-group">
												<label class="col-lg-3 control-label">退还优惠卷： <sup>*</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<label>
															<input type="radio" name="isTicket" value="1" checked="checked"/> 是
														</label>&nbsp;&nbsp;&nbsp; 
														<label> 
															<input type="radio" name="isTicket" value="0" /> 否
														</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">退还功力等级优惠： <sup>*</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<label>
															 <input type="radio" name="isGrade" value="1" checked="checked"/> 是
														</label>&nbsp;&nbsp;&nbsp; 
														<label> 
															<input type="radio" name="isGrade" value="0" /> 否
														</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">退还功力值： <sup>*</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<label>
															 <input type="radio" name="isGradeValue" value="1" checked="checked"/> 是
														</label>&nbsp;&nbsp;&nbsp; 
														<label> 
															<input type="radio" name="isGradeValue" value="0" /> 否
														</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">返还现金： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="refPrice" value="" placeholder="请输入返还现金" />
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>提交</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>