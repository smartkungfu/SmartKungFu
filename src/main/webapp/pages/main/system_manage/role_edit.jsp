<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-系统管理-编辑角色</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var pageSize = QueryString.GetValue("pageSize");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var rolName = QueryString.GetValue("rolName");
		  		
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='pageSize']").val(pageSize);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);

		  		$("input[name='rolName']").val(rolName);
		  		
		  	  $.ajax({
					url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
					data : {parentId:75},
					type : "GET",
					async : true,
					dataType : "json",
				    beforeSend: function() {
				        //请求前的处理
				    },
					success : function(data) {
						var $select = $(".multiselect");
						var roltype = "${mstRole.roltype}";
						
						var tempArr = new Array();
						$.each(data, function(index, ele) {
							if(roltype == ele.id){
					            var tempObj = {label: ele.name,value:ele.id,selected:"selected" };
					            tempArr.push(tempObj);
							} else {
								var tempObj = {label: ele.name,value:ele.id };
					            tempArr.push(tempObj);
							}
				              
				        });
						
						$select.multiselect('dataprovider',tempArr,{
			                onChange: function(element, checked) {
			                	//当选项改变重新校验select元素
			                    $('form').data('bootstrapValidator')                 // Get plugin instance
			                        	 .updateStatus('roltype', 'NOT_VALIDATED')  // Update field status
			                             .validateField('roltype');                 // and re-validate it
			                }
			            });
					},
				    complete: function() {
				    },
					error : function(data) {
						bootbox.alert({ 
							  size: "small",
							  title: "错误",
							  message: "请求超时，请重试！", 
							  callback: function(){return; }
						});
						return;
					}
			   });	
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize() ;
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/mstRoleController/list" + reqPara; 
		  		});
			  	  $('form').bootstrapValidator({
			            // Exclude only disabled fields
			            // The invisible fields set by Bootstrap Multiselect must be validated
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            fields: {
			                enabled: {
			                    validators: {
			                        notEmpty: {
			                            message: '请选择可用性'
			                        }
			                    }
			                },
			                rolno: {
			                    validators: {
			                        notEmpty: {
			                            message: '角色编号为空'
			                        },
			                        stringLength: {
			                            min: 3,
			                            max: 10,
			                            message: '编号长度为3-10'
			                        }, 
			                        callback: {
			                            message: '角色编码已存在',
			                            callback: function(value, validator) {
			                            	  var rolno = validator.getFieldElements('rolno').val();
			                            	  var flag = false;
			                            	  $.ajax({
			                  					url : "${pageContext.request.contextPath}/mstRoleController/isUnique",
			                  					data : {property:rolno,id:"${mstRole.id}"},
			                  					type : "GET",
			                  					async : false,
			                  					dataType : "json",
			                  					success : function(data) {
			                  						flag = data;
			                  					},
			                  					error : function(data) {
			                  						bootbox.alert({ 
			                  							  size: "small",
			                  							  title: "错误",
			                  							  message: "请求超时，请重试！", 
			                  							  callback: function(){return; }
			                  						});
			                  						return;
			                  					}
			                  					
			                  			   	  });
			                            	  return flag;
			                            }
			                        }
			                        
			                    }
			                },
			                rolname: {
			                    validators: {
			                        notEmpty: {
			                            message: '角色名称为空'
			                        },
			                    }
			                }, 
			                roltype: {
			                    validators: {
			                        callback: {
			                            message: '请选择角色类型',
			                            callback: function(value, validator) {
			                                // Get the selected options
			                                var options = validator.getFieldElements('roltype').val();
			                                return (options != null);
			                            }
			                        }
			                    }
			                }
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	validator.defaultSubmit();
			            }
			        });
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >系统管理</span></li>
		                   <li><a  href="javascript:void(0)" class="cancel">角色管理</a></li>
		                   <li><a  href="javascript:void(0)" class="active">编辑角色</a></li>
		                 </ul>
		                 <h3 class="page-header"> 编辑角色 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	角色信息编辑！
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									角色信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/mstRoleController/save">
											<!--附件参数  -->
											
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="pageSize" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												<input type="hidden" name="rolName" value="" /> 
											</div>
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${mstRole.id }" />
											
											<div class="form-group">
												<label class="col-lg-3 control-label">角色编号： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="rolno"  value="${mstRole.rolno }" placeholder="请输入角色编号" />
												</div>
											</div>

											<div class="form-group">
												<label class="col-lg-3 control-label"> 角色名称： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="rolname" value="${mstRole.rolname }" placeholder="请输入角色名称" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">是否可用： <sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<c:if test="${mstRole.enabled}">
															<label>
																 <input type="radio" name="enabled" value="1" checked="checked"/> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="enabled" value="0" /> 否
															</label>
														</c:if>
														<c:if test="${!mstRole.enabled}">
															<label>
																 <input type="radio" name="enabled" value="1" /> 是
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="enabled" value="0" checked="checked" /> 否
															</label>
														</c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">角色类型： <sup>*</sup></label>
												<div class="col-lg-5">
													<select class="multiselect" name="roltype">
													</select>
												</div>
											</div>

											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>