<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-系统管理-权限编辑</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		<!-- loadding easyui css-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/easyui/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/easyui/themes/icon.css">

		<jsp:include page="../../fragment/frag_top_js.jsp"/>
		<!-- include easyui.js -->
		<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/locale/easyui-lang-zh_CN.js"></script>
		
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var masterType = QueryString.GetValue("masterType");
		  		var masterValue = QueryString.GetValue("id");
		  		var rolName = QueryString.GetValue("rolName");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		$("input[name='masterType']").val(masterType);
		  		$("input[name='masterValue']").val(masterValue);
		  		$("input[name='rolName']").val(rolName);
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/mstPermissionController/list" + reqPara; 
		  		});
		  		
		  		//加载权限树
		  		$("#treeMenu").tree({
		  			url : "${pageContext.request.contextPath}/mstPermissionController/queryMenuPermission?masterType=" + masterType + "&masterValue=" + masterValue,
		  			lines:true,//显示虚线效果 
		  			onBeforeExpand : function(node) {
		  				if (node.parentid == null) {
		  					$("#treeMenu").tree("options").url = "${pageContext.request.contextPath}/mstPermissionController/queryMenuPermission?masterType=" + masterType + "&masterValue=" + masterValue + "&parentId=" + node.id;
		  				} else {
		  					$("#treeMenu").tree("options").url = "${pageContext.request.contextPath}/mstPermissionController/queryBtnPermission?masterType=" + masterType + "&masterValue=" + masterValue + "&menuId=" + node.id;
		  				}
		  			}
		  		});
		  		
		  	 });
		  	//展开树
		  	function ExpandAll() {
		  		$("#treeMenu").tree("expandAll");
		  	}
		  	function formSubmit() {
		  		//所选节点
		  		var nodes = $('#treeMenu').tree('getChecked', [ 'checked', 'indeterminate' ]);

		  		var menuIds = "";
		  		var buttonIds = "";

		  		for (var i = 0; i < nodes.length; i++) {
		  			if (nodes[i].treeType == "MENU") {
		  				if (menuIds != "") {
		  					menuIds = menuIds + ",";
		  				}
		  				menuIds = menuIds + nodes[i].id;
		  			} else if (nodes[i].treeType == "BUTTON") {
		  				if (buttonIds != "") {
		  					buttonIds = buttonIds + ",";
		  				}
		  				buttonIds = buttonIds + nodes[i].id;
		  			}
		  		}	
		  		
		  		$.ajax({
		  				url : "${pageContext.request.contextPath}/mstPermissionController/savePermission",
		  				type : "post",
		  				data : {
		  					masterType : $("#masterType").val(),
		  					masterValue : $("#masterValue").val(),
		  					menuIds : menuIds,
		  					buttonIds : buttonIds
		  				},
		  				success : function(data) {
		  					if (data == "SUCCESS") {
		  						
		  						bootbox.alert({ 
									  size: "small",
									  title: "提示",
									  message: "操作成功！", 
									  callback: function(){return; }
								});
		  					} else {
		  						bootbox.alert({ 
									  size: "small",
									  title: "错误",
									  message: "请求超时，请重试！", 
									  callback: function(){return; }
								});
		  					}
		  				}
		  		});
		  	}
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >系统管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel">权限管理</a></li>
		                   <li><a href="javascript:void(0)" class="active">编辑权限</a></li>
		                 </ul>
		                 <h3 class="page-header"> 编辑权限 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	角色权限维护！
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									角色名：${tempName }
								</h3>
							</div>
							<div class="panel-body" style="overflow-x: hidden; ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/mstUserController/save">
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>

												<input id="masterType" name="masterType" type="hidden" /> 
												<input id="masterValue" name="masterValue" type="hidden" /> 
												<input type="hidden" name="rolName" value="" /> 
											</div>
											<!--表单提交参数  -->
											<input id="menuIds" name="menuIds" type="hidden" />
											<input id="buttonIds" name="buttonIds" type="hidden" />
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<ul id="treeMenu" class="easyui-tree"
														data-options="checkbox:true,onLoadSuccess:ExpandAll">
													</ul>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="button" onclick="formSubmit()" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>