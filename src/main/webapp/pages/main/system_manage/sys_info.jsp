<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-系统管理-系统信息</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  	/* 	
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var minDate = QueryString.GetValue("minDate");
		  		var maxDate = QueryString.GetValue("maxDate");
		  		var usrAccount = QueryString.GetValue("usrAccount");
		  		var mobileNo = QueryString.GetValue("mobileNo");
		  		var nameCn = QueryString.GetValue("nameCn");
		  		
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);

		  		$("input[name='minDate']").val(minDate);
		  		$("input[name='maxDate']").val(maxDate);
		  		$("input[name='usrAccount']").val(usrAccount);
		  		$("input[name='mobileNo']").val(mobileNo);
		  		$("input[name='nameCn']").val(nameCn); */
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/mstUserController/list" + reqPara; 
		  		});
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >系统管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel" >系统信息</a></li>
		                   <li><a href="javascript:void(0)" class="active">系统信息</a></li>
		                 </ul>
		                 <h3 class="page-header"> 系统信息 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                	  系统详情信息
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>
				<!--系统介绍 start -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
							<div class="panel-heading">
								<h3 class="panel-title">
									系统介绍 <span class="pull-right">
										<a href="#" class="panel-minimize"><i
											class="fa fa-chevron-up"></i></a> 
									</span>
								</h3>
							</div>
							<div class="panel-body">
								<div class="panel panel-default">
									<div class="panel-heading">系统介绍</div>
									<div class="panel-body">
										<p>&nbsp;&nbsp;&nbsp;&nbsp;通过建设APP实现终端客户了解功夫的特色、发现功夫的魅力并可体验功夫的过程，让用户近距离体会功夫，实现终端用户资料维护、功夫视频、图片资源分享、达人功夫展秀、场馆功夫培训、开展功夫活动等功能。
										同时对用户功夫问卷分析、订单、活动、场馆信息数据统计，提供数据质量。</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /col-md-12 -->
				</div>
				<!-- 系统介绍 end -->
				<!-- 应用信息  start-->
				<div class="row">
						<div class="panel">
							<div class="panel-heading text-primary">
								<h3 class="panel-title">
									应用信息 <span class="pull-right">
									<a href="#" class="panel-minimize"><i
											class="fa fa-chevron-up"></i></a> 
									</span>
								</h3>
							</div>
							<div class="panel-body">
								<div class="panel panel-default">
									<div class="panel-heading">应用信息</div>
									<div class="panel-body">
										<p>有点功夫应用信息列表</p>
									</div>
									<table class="table">
										<thead>
											<tr>
												<th>应用编码</th>
												<th>应用名称</th>
												<th>应用版本</th>
												<th>发布时间</th>
												<th>最后更新时间</th>
												<th>支持系统</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>SmartKungFu</td>
												<td>有点功夫后台管理系统</td>
												<td>1.0.0.161018</td>
												<td>2016-10-18</td>
												<td>2016-10-18</td>
												<td><span class="label bg-success">windows 、ios、linux</span></td>
											</tr>
											<tr>
												<td>SmartKungFuService</td>
												<td>有点功夫接口项目</td>
												<td>1.0.0.161018</td>
												<td>2016-10-18</td>
												<td>2016-10-18</td>
												<td><span class="label bg-primary">windows 、ios、linux</span></td>
											</tr>
											<tr>
												<td>SmartKungFu</td>
												<td>IOS版终端应用</td>
												<td>1.0.0.161018</td>
												<td>2016-10-18</td>
												<td>2016-10-18</td>
												<td><span class="label bg-warning">ios</span></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- 应用信息 end -->

				</div>
				<!-- /panels with tables and lists -->
				<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>