<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-系统管理-用户信息</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var minDate = QueryString.GetValue("minDate");
		  		var maxDate = QueryString.GetValue("maxDate");
		  		var usrAccount = QueryString.GetValue("usrAccount");
		  		var mobileNo = QueryString.GetValue("mobileNo");
		  		var nameCn = QueryString.GetValue("nameCn");
		  		
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);

		  		$("input[name='minDate']").val(minDate);
		  		$("input[name='maxDate']").val(maxDate);
		  		$("input[name='usrAccount']").val(usrAccount);
		  		$("input[name='mobileNo']").val(mobileNo);
		  		$("input[name='nameCn']").val(nameCn);
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/mstUserController/list" + reqPara; 
		  		});
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >系统管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel" >用户管理</a></li>
		                   <li><a href="javascript:void(0)" class="active">用户信息</a></li>
		                 </ul>
		                 <h3 class="page-header"> 用户信息 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                	   用户详情信息
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/mstUserController/save">
											<!--附件参数  -->
											
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>

												<input type="hidden" name="minDate" value="" /> 
												<input type="hidden" name="maxDate" value="" /> 
												<input type="hidden" name="usrAccount" value="" />
											    <input type="hidden" name="mobileNo" value="" /> 
											    <input type="hidden" name="nameCn" value="" /> 
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">账号： </label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="account" value="${mstUser.account }"  readonly="readonly"/>
												</div>
											</div>

											<div class="form-group">
												<label class="col-lg-3 control-label"> 密码： </label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="password" value="${mstUser.password }"  readonly="readonly"/>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">用户名： </label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="namecn" value="${mstUser.namecn}"  readonly="readonly"/>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">性 别： <sup>&nbsp;</sup></label>
												<div class="col-lg-5">
													<div class="radio-group">
														<c:if test="${!mstUser.gender}"><input type="text" class="form-control" name="gender" value="女"  readonly="readonly"/></c:if>
														<c:if test="${mstUser.gender}"><input type="text" class="form-control" name="gender" value="男"  readonly="readonly"/></c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">角色： </label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="namecn" value="${mstUser.rolnames}"  readonly="readonly"/>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">出生日期： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="birthday" value='<fmt:formatDate value="${mstUser.birthday}" />' readonly="readonly"/>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">电话号码：</label>
												<div class="col-lg-5">
													<input type="tel" class="form-control" name="mobileno" value="${mstUser.mobileno}" readonly="readonly"/>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">居住地址： </label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="address" value="${mstUser.address}" readonly="readonly"/>
												</div>
											</div>

											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="button" class="btn btn-primary cancel"><i class="fa fa-reply fa-lg"></i>返回</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>