<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-系统管理-编辑用户</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		
		<!--include fileinput.css  -->
	  	<link href="${pageContext.request.contextPath}/css/fileinput.min.css" rel="stylesheet">
	  	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	  	<!-- include datetimepicker.css  -->
	  	<link href="${pageContext.request.contextPath}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	    <!-- include datetimepicker.js  -->
	  	<script src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.min.js"></script>
	    <script src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.zh-CN.js"></script>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  	  QueryString.Initial();
		  		
		  	  var menuId = QueryString.GetValue("menuId");
		  	  var page = QueryString.GetValue("page");
		  	  var rows = QueryString.GetValue("rows");
		  	  var index_exp = QueryString.GetValue("index_exp");
		  	  var index_che = QueryString.GetValue("index_che");
		  		
		  	  var minDate = QueryString.GetValue("minDate");
		  	  var maxDate = QueryString.GetValue("maxDate");
		  	  var usrAccount = QueryString.GetValue("usrAccount");
		  	  var mobileNo = QueryString.GetValue("mobileNo");
		  	  var nameCn = QueryString.GetValue("nameCn");
		  		
		  	  $("input[name='menuId']").val(menuId);
		  	  $("input[name='page']").val(page);
		  	  $("input[name='rows']").val(rows);
		  	  $("input[name='index_exp']").val(index_exp);
		  	  $("input[name='index_che']").val(index_che);
		  		
		  	  $("input[name='minDate']").val(minDate);
		  	  $("input[name='maxDate']").val(maxDate);
		  	  $("input[name='usrAccount']").val(usrAccount);
		  	  $("input[name='mobileNo']").val(mobileNo);
		  	  $("input[name='nameCn']").val(nameCn);
		  		
		  	  $.ajax({
					url : "${pageContext.request.contextPath}/mstRoleController/initRoleWithUser",
					data : {userId:"${mstUser.id}"},
					type : "GET",
					async : true,
					dataType : "json",
					success : function(data) {
						var $select = $(".multiselect");
						
						$.each(data, function(index, ele) {
							if(ele.flag){
								$select.append("<option  selected='selected' value='"+ele.id+"'>"+ele.rolname+"</option>");
							} else {
								$select.append("<option value='"+ele.id+"'>"+ele.rolname+"</option>");
							}
				        });
						
						$select.multiselect({
			                onChange: function(element, checked) {
			                	//当选项改变重新校验select元素
			                    $('form').data('bootstrapValidator')                 // Get plugin instance
			                        	 .updateStatus('rolids', 'NOT_VALIDATED')  // Update field status
			                             .validateField('rolids');                 // and re-validate it
			                }
			            });
					},
				    complete: function() {
				    },
					error : function(data) {
						bootbox.alert({ 
							  size: "small",
							  title: "错误",
							  message: "请求超时，请重试！", 
							  callback: function(){return; }
						});
						return;
					}
			   });	
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/mstUserController/list" + reqPara; 
		  		});
		  		
			  	$('form').bootstrapValidator({
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            fields: {
			            	myfiles: {
			            		  validators: {
			                          file: {
			                              extension: 'jpeg,png,jpg',
			                              type: 'image/jpeg,image/png,image/jpg',
			                              maxSize: 2048 * 1024,   // 2 MB
			                              message: '文件格式非法，请重新上传'
			                          },
				                      callback: {
				                            message: '请上传文件',
				                            callback: function(value, validator) {
				                                // Get the selected options
				                                
				                                var $imagename = $("input[name='imagename']");
				                                var imgs = $(".defined");
				                                return ($imagename.val() != undefined || imgs.length != 0);
				                            }
				                     }
			                      }
				            },
			                gender: {
			                    validators: {
			                        notEmpty: {
			                            message: '请选择性别'
			                        }
			                    }
			                },
			                account: {
			                    validators: {
			                        notEmpty: {
			                            message: '用户账号为空'
			                        },
			                        regexp: {
			                            regexp: "^\\w+$",
			                            message: '账号由数字、26个英文字母或者下划线组成'
			                        }
			                    }
			                },
			                password: {
			                    validators: {
			                        notEmpty: {
			                            message: '用户密码为空'
			                        },
			                        stringLength: {
			                            min: 6,
			                            max: 30,
			                            message: '密码长度为6-30位'
			                        },
			                        regexp: {
			                            regexp: "^[\\x00-\\xFF]+$",
			                            message: '密码必须是字符',
			                        }
			                    }
			                }, 
			                namecn: {
			                    validators: {
			                        notEmpty: {
			                            message: '用户名称为空'
			                        },
			                        stringLength: {
			                            min: 2,
			                            max: 20,
			                            message: '用户名长度为2-20'
			                        },
			                    }
			                },
			                birthday: {
			                    validators: {
			                        notEmpty: {
			                            message: '出生日期为空'
			                        },
			                    }
			                },
			                mobileno: {
			                    validators: {
			                        notEmpty: {
			                            message: '电话号码为空'
			                        },
			                        regexp: {
			                            regexp: "^(13|15|18|17)[0-9]{9}$",
			                            message: '手机号格式不正确'
			                        }
			                    }
			                },
			                address: {
			                    validators: {
			                        notEmpty: {
			                            message: '地址详情为空'
			                        },
			                        stringLength: {
			                        	  min: 1,
				                          max: 50,
				                          message: '地址详情长度为1-50'
			                        }
			                    }
			                },
			                rolids: {
			                    validators: {
			                        callback: {
			                            message: '至少选择一个角色',
			                            callback: function(value, validator) {
			                                // Get the selected options
			                                var options = validator.getFieldElements('rolids').val();
			                                return (options != null && options.length >= 1);
			                            }
			                        }
			                    }
			                }
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	validator.defaultSubmit();
			            }
			    });
			  	$('.form_date').datetimepicker({
			        language:  'zh-CN',
			        weekStart: 1,
			        todayBtn:  1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					minView: 2,
					forceParse: 0,
					startDate: "1970-01-01",
					endDate:new Date(),
			    }).on('hide', function(ev){
			        $('form')
			        .bootstrapValidator('updateStatus', 'birthday', 'NOT_VALIDATED')
			        .bootstrapValidator('validateField', 'birthday');
			    });
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >系统管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel">用户管理</a></li>
		                   <li><a href="javascript:void(0)" class="active">编辑用户</a></li>
		                 </ul>
		                 <h3 class="page-header">编辑用户 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	用户资料信息编辑
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/mstUserController/save" enctype="multipart/form-data">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
					
												<input type="hidden" name="minDate" value="" /> 
												<input type="hidden" name="maxDate" value="" /> 
												<input type="hidden" name="usrAccount" value="" />
											    <input type="hidden" name="mobileNo" value="" /> 
											    <input type="hidden" name="nameCn" value="" /> 
											</div>
											
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${mstUser.id }" />
											<div class="form-group">
												<label class="col-lg-3 control-label">头像： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<input class="form-control file-loading"  name="myfiles"
														type="file">
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">账号： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<input type="text" class="form-control" name="account"  value="${mstUser.account }" placeholder="请输入账号" />
												</div>
											</div>

											<div class="form-group">
												<label class="col-lg-3 control-label"> 密码： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<input type="text" class="form-control" name="password" value="${mstUser.password }" placeholder="请输入密码" />
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">用户名： <sup>*</sup></label>
													<div class="input-group col-lg-5">
														<input type="text" class="form-control" name="namecn" value="${mstUser.namecn }" placeholder="请输入用户名" />
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">性 别： <sup>&nbsp;</sup></label>
												<div class="input-group col-lg-5">
													<div class="radio-group">
														<c:if test="${mstUser.gender}">
															<label>
																 <input type="radio" name="gender" value="1" checked="checked"/> 男
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="gender" value="0" /> 女
															</label>
														</c:if>
														<c:if test="${!mstUser.gender}">
															<label>
																 <input type="radio" name="gender" value="1" /> 男
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="gender" value="0" checked="checked" /> 女
															</label>
														</c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">角色： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<select class="multiselect" name="rolids" multiple>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="dtp_input3" class="col-md-3 control-label">出生日期： <sup>*</sup></label>
												<div class="input-group date form_date col-md-5"
													data-date="" data-date-format="yyyy-mm-dd"
													data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
													<input class="form-control" type="text" value="<fmt:formatDate value="${mstUser.birthday }" pattern="yyyy-MM-dd"/>"
														readonly> <span class="input-group-addon"><span
														class="glyphicon glyphicon-remove"></span></span> <span
														class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span></span>
												</div>
												<label class="col-lg-3 control-label"></label>
												<div class="input-group col-lg-5">
													<input type="hidden" class="form-control" name="birthday" id="dtp_input2" value="<fmt:formatDate value="${mstUser.birthday }" pattern="yyyy-MM-dd"/>" />
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-lg-3 control-label">电话号码： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<input type="tel" class="form-control" name="mobileno" value="${mstUser.mobileno }" placeholder="请输入电话号码"/>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">居住地址： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<input type="text" class="form-control" name="address" value="${mstUser.address }" placeholder="请输入居住地址"/>
												</div>
											</div>

											<div class="form-group">
												<div class="input-group col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
			<!--include fileinput.js  -->
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput.js"></script>
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
</body>
<script type="text/javascript">
$(function(){
	 var imsg = [];
	 var res = "${mstUser.portrait}".split(";");
	 for(var i = 0; i < res.length; i ++){
		 if(res[i] != null && res[i] != ""){
			 imsg.push("<img src='"+res[i]+"' class='file-preview-image defined' style='width:180px;height:120px;' />");
		 }
	 }
	 $("input[name='myfiles']").fileinput({
	        language: 'zh', //设置语言
	      	uploadUrl: "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
	        uploadAsync: true, //异步加载
	      	allowedFileExtensions : ['jpg', 'png'],//接收的文件后缀
	        showUpload: false, //是否显示上传按钮
	        showCaption: false,//是否显示标题
	        overwriteInitial: true,
	        maxFileSize: 1024 * 10,
	        maxFileCount: 1,
	        slugCallback: function(filename) {
	            return filename.replace('(', '_').replace(']', '_');
	        },
	      
	        showPreview:true,
	        initialPreview: imsg,
	        allowedPreviewTypes : [ 'image' ],
	        browseClass: "btn btn-primary", //按钮样式             
	 });
	 $("input[name='myfiles']").on("fileuploaded", function (event, data, previewId, index) {  
	//	console.log("previewId:",previewId )//,分析data数据格式
		var $input = " <input type='hidden' id='"+previewId+"' name='imagename' value='"+data.response.resName+"'/>";
		var $form = $("form");
		$form.append($input);
		
		$('form')
        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
        .bootstrapValidator('validateField', 'myfiles');
     });  
	 $("input[name='myfiles']").on("fileclear", function(event) {
		  
		   var $imagenames = $("input[name='imagename']");
		   
		   for(var i = 0 ; i < $imagenames.length ; i ++){
			   $($imagenames[i]).remove();
		   }
		   
		   var $imgs = $(".defined");
		   for(var i = 0 ; i < $imgs.length ; i ++){
			    $($imgs[i]).remove();
		   }
		   
		   $('form')
	        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'myfiles');
	 });
	 
	$("input[name='myfiles").on('filereset', function(event) {
		  $('form')
	        .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	        .bootstrapValidator('validateField', 'myfiles');
	});
	 
	$("input[name='myfiles'").on('filesuccessremove', function(event, id) {
		$("input[id='"+id+"']").remove();
		$('form')
	     .bootstrapValidator('updateStatus', 'myfiles', 'NOT_VALIDATED')
	     .bootstrapValidator('validateField', 'myfiles');
	});
});
</script>
</html>