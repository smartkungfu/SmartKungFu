<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-消息管理-消息推送</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		<!--include fileinput.css  -->
	  	<link href="${pageContext.request.contextPath}/css/fileinput.min.css" rel="stylesheet">
	  	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	  	<!-- include datetimepicker.css  -->
	  	<link href="${pageContext.request.contextPath}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<!--include wysiwyg.js  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-wysiwyg.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.hotkeys.min.js"></script>
		<!-- include datetimepicker.js  -->
	  	<script src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.min.js"></script>
	    <script src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.zh-CN.js"></script>
		
	  	<script type="text/javascript">
	  		 
		  	 $(document).ready(function() {
		  		var kungfutype = [ { "type":"wenw", "name":"文章" }, { "type":"video", "name":"视频" }, { "type":"online_course", "name":"在线课程" }, { "type":"project", "name":"活动" }];
		  	
		  		//功夫数据加载
		  		$.ajax({
					url : "${pageContext.request.contextPath}/mstMessageController/loadkungfu",
					data : {},
					type : "POST",
					async : false,
					dataType : "json",
					success : function(data) {
						kfData = data;
					},
					error : function(data) {
						bootbox.alert({ 
							  size: "small",
							  title: "错误",
							  message: "请求超时，请重试！", 
							  callback: function(){return; }
						});
						return;
					}
		   		});
		  		
		  		//功夫数据读取
		  		var $kungfutype = $("select[name='kungfutype']");
		  		
		  		$kungfutype.append("<option value='0'>请选择</option>");
				$.each(kungfutype,function(index,obj){ 
					var option = "<option value="+obj.type+">"+obj.name+"</option>";
					$kungfutype.append(option);
				}) ;
			  
			   $proxy = $("select[name='proxy']");
			   $kungfutype.multiselect({
					onChange: function(element, checked) {
							if(checked){
							    var type = $kungfutype.val();
							    
							    var proxyHtml = "<option value='0'>请选择</option>";
							    if("wenw" == type){
							    	$.each(kfData.wenws, function(index, obj) {
							    		proxyHtml += "<option value='"+obj.id+"'>"+obj.title+"</option>";
						    		});
							    } else if("video" == type){
							    	$.each(kfData.vis, function(index, obj) {
							    		proxyHtml += "<option value='"+obj.id+"'>"+obj.title+"</option>";
						    		});
							    } else if("online_course" == type){
							    	$.each(kfData.ocs, function(index, obj) {
							    		proxyHtml += "<option value='"+obj.id+"'>"+obj.title+"</option>";
						    		});
							    } else if("project" == type){
							    	$.each(kfData.pis, function(index, obj) {
							    		proxyHtml += "<option value='"+obj.id+"'>"+obj.title+"</option>";
						    		});
							    } else {
							    	//proxyHtml += "<option value='0'>请选择</option>";
							    	$("textarea[name='reserved10']").val("");
							    }
							   
						    	$proxy.html(proxyHtml);
						    		  //刷新市、区下拉列表
						    	$proxy.multiselect('rebuild');
					          }
					},
					maxHeight: 200,
					filterPlaceholder: 'Search'
				});
			  		
			   $proxy.multiselect({
				      onChange: function(element, checked) {
				    	if(checked){
				    		var id = $proxy.val();
				    		var type = $kungfutype.val();
				    		var $reserved10 = $("textarea[name='reserved10']");
				    		var url = "http://skf.smartkungfu.com/SmartKungFu/shareContentController/view?type="+type+"&id=" + id;
				    		
				    		$reserved10.val(url);
		                }
				      },
				      maxHeight: 200,
			          filterPlaceholder: 'Search'
				});
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var minDate = QueryString.GetValue("minDate");
		  		var maxDate = QueryString.GetValue("maxDate");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		$("input[name='minDate']").val(minDate);
		  		$("input[name='maxDate']").val(maxDate);
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/mstMessageController/Main" + reqPara; 
		  		});
		  		
			  	$('form').bootstrapValidator({
			            // Exclude only disabled fields
			            // The invisible fields set by Bootstrap Multiselect must be validated
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            fields: {
			            	content: {
				                    validators: {
				                        notEmpty: {
				                            message: '消息内容为空'
				                        }
				                    }
				            }
			            },
			            submitHandler: function (validator, form, submitButton) {
			       			$.ajax({
			    				url : "${pageContext.request.contextPath}/mstMessageController/pushMessage",
			    				type : "get",
			    				async : false,
			    				dataType : "text",
			    				data : {
			    					content : $("textarea[name='content']").val(),
			    					reserved10 : $("textarea[name='reserved10']").val()
			    				},
			    				success : function(data) {
			    					if(data == "SUCCESS"){
			    						bootbox.alert({ 
			    						  size: "small",
			    						  title: "提示",
			    						  message: "推送成功！", 
			    						  callback: function(){
			    							  $(".cancel").click();
			    						  }
			    						}); 
			    					}else{
			    						bootbox.alert({ 
			    						  size: "small",
			    						  title: "提示",
			    						  message: "推送失败！", 
			    						  callback: function(){return; }
			    						});  
			    					}
			    				}
			    		   });
			            }
			        });
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >消息管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel">系统消息</a></li>
		                   <li><a href="javascript:void(0)" class="active">消息推送</a></li>
		                 </ul>
		                 <h3 class="page-header">消息编辑<i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	消息编辑
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/mstMessageController/save">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												<input type="hidden" name="minDate" value="" /> 
												<input type="hidden" name="maxDate" value="" /> 
											</div>
											
											<!-- 逻辑参数 -->
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">消息内容： </label>
													<div class="input-group col-lg-6">
												    	<textarea class="form-control" rows="5" name="content" ></textarea>
													</div>
												</div>
											</div>
											
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">推送目标：</label>
													<div class="input-group col-lg-6">
														<select class="multiselect" name="kungfutype" >
														</select> &nbsp;&nbsp;-&nbsp;&nbsp;
														<select class="multiselect" name="proxy">
														</select>&nbsp;&nbsp;
													</div>
												</div>
											</div>
											
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">目标链接： </label>
													<div class="input-group col-lg-6">
														<textarea type="text" row="3" class="form-control" name="reserved10" value="" placeholder="请输入地址" >
														</textarea>
													</div>
												</div>
											</div>
											<div>
											<div class="form-group">
												<div class="input-group col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>推送</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
			<!--include fileinput.js  -->
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput.js"></script>
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
</body>
</html>