<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-消息管理-系统消息</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
	  	var paras = "";
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             					//cache: false, // 不缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				                pagination: true, // 开启分页功能
				               // singleSelect: false,
				                pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500],
				                search: false, // 关闭搜索功能
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                //smartDisplay: true, // 智能显示 pagination 和 cardview 等
				                queryParams: function (params) {
				                	//处理当前页
									var page = params.offset;
				                	if((page*1) !=0 ){
				            			page = page/params.limit;
				            		}
				            		page += 1;
				            		
				            		//排序，并处理多表中的属性
				                	var sort = params.sort;
				                	if(sort == undefined){
				                		sort ="";
				                	}else {
				                		if(sort == "typeName"){
				                			sort = "c1.name";
				                		}else if(sort == "sendTypeName"){
				                			sort = "c2.name";
				                		}else if(sort == "receiveTypeName"){
				                			sort = "c3.name";
				                		}else {
				                			sort = "m." + sort;
				                		}
				                	}
				            		
				                	var obj = {
					                        rows: params.limit,
					                        page: page,
					                        sort:sort,
					                        order:params.order,
					                        minDate:$("#minDate").val(),
					                        maxDate:$("#maxDate").val(),
					                        type:$("select[name='type']").val(),
					                        sendtype:$("select[name='sendtype']").val(),
					                        receivetype:$("select[name='receivetype']").val()
					                    };
				                	paras =  "rows=" + obj.rows + "&page=" + obj.page ;
				                	return obj;
				                },
				                url: "${pageContext.request.contextPath}/mstMessageController/queryList",
				                detailView:true,
				                detailFormatter:function detailFormatter(index, row) {
				                	var html = [];
				                    html.push('<textarea class="form-control" readonly="readonly" rows="5">' + (row.content == null ? '' : row.content) + '</textarea>'); 
				                    return html;
				    	   		},
				                columns: [{
				                    field: 'state',
				                    checkbox: true
				                },{
				                    field: 'typeName',
				                    title: '消息类型',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                },{
				                    field: 'sendName',
				                    title: '发送人',
				                    align: 'center',
				                    valign: 'middle'
				                },{
				                    field: 'sendTypeName',
				                    title: '发送人类型',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                },{
				                    field: 'content',
				                    title: '消息内容',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                },{
				                    field: 'receiveName',
				                    title: '接收人',
				                    align: 'center',
				                    valign: 'middle'
				                },{
				                    field: 'receiveTypeName',
				                    title: '接收人类型',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                },{
				                    field: 'createdate',
				                    title: '消息时间',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return timeStamp2String(value);
				                    }
				                }],
				                onLoadSuccess:function(){
				                },
				                onLoadError: function () {
				             	   //    mif.showErrorMessageBox("数据加载失败！");
				                }
            });
        }
	  	
	  	//推送消息
	  	function pushMessage(url){
	  		paras = paras +"&index_exp=${MENU_STYLE_INDEX_EXP}"+ "&index_che=${MENU_STYLE_INDEX_CHE}"+"&"+  $("form").find("input").serialize();
			paras = decodeURIComponent(paras,true);
			
			window.location.href = url + "?"+ paras;
	  	}
	  	
   		$(function(){
   			
  			//消息类型
   			$.ajax({
				url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
				data : {parentId:"78"},
				type : "GET",
				async : false,
				dataType : "json",
				success : function(data) {
					 var $type = $("select[name='type']");
					 $type.append("<option selected value=''>请选择</option>");
					 $.each(data, function (idx, ele) {
						$type.append("<option value='"+ele.id+"'>"+ele.name+"</option>");
				     });
					 
					 $type.multiselect();
				},
				error : function(data) {
					bootbox.alert({ 
						  size: "small",
						  title: "错误",
						  message: "请求超时，请重试！", 
						  callback: function(){return; }
					});
					return;
				}
		   });
  			
  			//发送人类型
   			$.ajax({
				url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
				data : {parentId:"91"},
				type : "GET",
				async : false,
				dataType : "json",
				success : function(data) {
					 var $type = $("select[name='sendtype']");
					 $type.append("<option selected value=''>请选择</option>");
					 $.each(data, function (idx, ele) {
						$type.append("<option value='"+ele.id+"'>"+ele.name+"</option>");
				     });
					 
					 $type.multiselect();
				},
				error : function(data) {
					bootbox.alert({ 
						  size: "small",
						  title: "错误",
						  message: "请求超时，请重试！", 
						  callback: function(){return; }
					});
					return;
				}
		   });
  			
  			//接收人类型
   			$.ajax({
				url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
				data : {parentId:"87"},
				type : "GET",
				async : false,
				dataType : "json",
				success : function(data) {
					 var $type = $("select[name='receivetype']");
					 $type.append("<option selected value=''>请选择</option>");
					 $.each(data, function (idx, ele) {
						$type.append("<option value='"+ele.id+"'>"+ele.name+"</option>");
				     });
					 
					 $type.multiselect();
				},
				error : function(data) {
					bootbox.alert({ 
						  size: "small",
						  title: "错误",
						  message: "请求超时，请重试！", 
						  callback: function(){return; }
					});
					return;
				}
		   });
   			
   		    initTable();
   		 
   			$("#btn_search").click(function(){
   				$table.bootstrapTable('destroy');
   			 	initTable();
   			});
   		 
   		});
	    </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>消息管理</span></li>
                   <li><a href="javascript:void(0)" class="active">系统消息</a></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
                  		<c:forEach var="g" items="${btns }">
                  			 <button type="button" class="btn btn-primary" onclick="${g.action }">
						 		<i class="${g.btnicon }"></i> ${g.btnname }
							 </button>
                  		</c:forEach>
 					 </div>
                </div>

                <h3 class="page-header"> 系统消息 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

                <blockquote class="page-information hidden">
                 <p>
                	  系统消息
                </p>
              </blockquote>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
					<form class="form-inline" role="form" style="margin-top: 10px;margin-bottom: 10px;">
						  <input type="hidden" name="menuId" value="${folPara.menuId }">
						  <div class="form-group">
						    <label for="minDate">消息时间：</label>
						    <input type="date" class="form-control" id="minDate" name="minDate" value="${folPara.minDate }"> -
						    <input type="date" class="form-control" id="maxDate" name="maxDate"  value="${folPara.maxDate }">
						  </div>
						  <br><br>
						  <div class="form-group">
						    <label for="usrAccount">消息类型：</label>
						    <select class="multiselect" name="type"></select>
						  </div>
						  <div class="form-group">
						    <label for="usrAccount">发送人类型：</label>
						    <select class="multiselect" name="sendtype"></select>
						  </div>
						  <div class="form-group">
						    <label for="usrAccount">接收人类型：</label>
						    <select class="multiselect" name="receivetype"></select>
						  </div>
						  <button id="btn_search" type="button" class="btn btn-primary" >
						 	<i class="fa fa-search fa-lg"></i> 搜索
						  </button>
					</form>
				    <table id="table">
				    </table>
		    </div>
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>