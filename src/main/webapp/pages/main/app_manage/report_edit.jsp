<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-app管理-举报回复</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		<!-- 样式冲突 -->
	  	<!--include fileinput.css  -->
	  	<link href="${pageContext.request.contextPath}/css/fileinput.min.css" rel="stylesheet">
	  	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  		
	  	<!--include fileinput.js  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput.min.js"></script>
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/reportController/Main" + reqPara; 
		  		});
		  		
			  	$('form').bootstrapValidator({
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	
			            	validator.defaultSubmit();
			            }
			        });
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >app管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel">举报管理</a></li>
		                   <li><a href="javascript:void(0)" class="active">举报回复</a></li>
		                 </ul>
		                 <h3 class="page-header">举报回复 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	举报回复信息编辑！
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/reportController/save">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
											</div>
											
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${report.id }" />
											<div class="form-group">
												<label class="col-lg-3 control-label"> 举报类型： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="reportType"  value="${report.reportType}" readonly="readonly"/>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 举报描述： <sup>*</sup></label>
												<div class="col-lg-5">
													<input type="text" class="form-control" name="reportContent" value="${report.reportContent}" readonly="readonly"/>
												</div>
											</div>
											
										   <div class="form-group">
												<label class="col-lg-3 control-label">举报回复：  <sup>*</sup><sup>&nbsp;</sup></label>
												<div class="col-lg-5">
												<input type="text" class="form-control" name="replyContent" value="${report.replyContent}" placeholder="请输入对举报的回复" >
												</div>
											</div>
										    
											<div class="form-group">
												<div class="col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>