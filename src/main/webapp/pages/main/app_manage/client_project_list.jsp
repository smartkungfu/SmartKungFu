<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-APP管理-用户活动</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
	  	var paras = "";
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             					//cache: false, // 不缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				                pagination: true, // 开启分页功能
				               // singleSelect: false,
				                pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500],
				                search: false, // 关闭搜索功能
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                //smartDisplay: true, // 智能显示 pagination 和 cardview 等
				                queryParams: function (params) {
				                	//处理当前页
									var page = params.offset;
				                	if((page*1) !=0 ){
				            			page = page/params.limit;
				            		}
				            		page += 1;
				            		
				            		//排序，并处理多表中的属性
				                	var sort = params.sort;
				                	if(sort == undefined){
				                		sort ="";
				                	}else {
				                		if(sort == "nickName"){
				                			sort = "c.nickName";
				                		}else if(sort == "orderno"){
				                			sort = "o.orderno";
				                		}else{
				                			sort = "pd." + sort;
				                		}
				                	}
				            		
				                	var obj = {
					                        rows: params.limit,
					                        page: page,
					                        sort:sort,
					                        order:params.order,
					                        clientid:"${clientid}",
					                        minDate:$("#minDate").val(),
					                        maxDate:$("#maxDate").val()
					                    };
				                	paras =  "rows=" + obj.rows + "&page=" + obj.page ;
				                	return obj;
				                },
				                url: "${pageContext.request.contextPath}/clientProjectController/queryList",
				                columns: [{
				                    field: 'state',
				                    checkbox: true
				                },{
				                    field: 'nickName',
				                    title: '用户昵称',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                }, {
				                    field: 'protitle',
				                    title: '活动标题',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                }, {
				                    field: 'orderno',
				                    title: '订单编号',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                }, {
				                    field: 'createdate',
				                    title: '时间',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return timeStamp2String(value);
				                    }
				                }],
				                onLoadSuccess:function(){
				                },
				                onLoadError: function () {
				             	   //    mif.showErrorMessageBox("数据加载失败！");
				                }
            });
        }
	  	
   		$(function(){
   			initTable();
   			
   			$("#btn_search").click(function(){
   				$table.bootstrapTable('refresh');
   			});
   			
   			QueryString.Initial();
	  		
	  		var menuId = QueryString.GetValue("menuId");
	  		var page = QueryString.GetValue("page");
	  		var rows = QueryString.GetValue("rows");
	  		var index_exp = QueryString.GetValue("index_exp");
	  		var index_che = QueryString.GetValue("index_che");
	  		
	  		var minDate = QueryString.GetValue("minDate");
	  		var maxDate = QueryString.GetValue("maxDate");
	  		var nickName = QueryString.GetValue("nickName");
	  		var mobileNo = QueryString.GetValue("mobileNo");
	  		var nameCn = QueryString.GetValue("nameCn");
	  		
	  		$("input[name='menuId']").val(menuId);
	  		$("input[name='page']").val(page);
	  		$("input[name='rows']").val(rows);
	  		$("input[name='index_exp']").val(index_exp);
	  		$("input[name='index_che']").val(index_che);
	  		
	  		$("input[name='minDate']").val(minDate);
	  		$("input[name='maxDate']").val(maxDate);
	  		$("input[name='nickName']").val(nickName);
	  		$("input[name='mobileNo']").val(mobileNo);
	  		$("input[name='nameCn']").val(nameCn);
   			
   			$(".cancel").click(function(){
   	  			var reqPara = "?" + $(".folPara").find("input").serialize();
   	  			reqPara = decodeURIComponent(reqPara,true);
   	  			window.location.href="${pageContext.request.contextPath}/mstClientController/Main" + reqPara;
   	  		});
   		});
	    </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>APP管理</span></li>
                   <li><a href="javascript:void(0)" class="cancel">APP管理</a></li>
		           <li><a href="javascript:void(0)" class="active">用户活动</a></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
                  		<c:forEach var="g" items="${btns }">
                  			 <button type="button" class="btn btn-primary" onclick="${g.action }">
						 		<i class="${g.btnicon }"></i> ${g.btnname }
							 </button>
                  		</c:forEach>
 					 </div>
                </div>

                <h3 class="page-header"> 用户活动 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

                <blockquote class="page-information hidden">
                 <p>
                  APP前台用户活动
                </p>
              </blockquote>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
					<form class="form-inline" role="form" style="margin-top: 10px;margin-bottom: 10px;">
					
					     <!--附件参数  -->
						 <div class="folPara" style="display: none">
							<input type="hidden" name="menuId" value="" />
							<input type="hidden" name="rows" value="" />
							<input type="hidden" name="page" value="" /> 
							<input type="hidden" name="index_exp" value=""/>
							<input type="hidden" name="index_che" value=""/>
							
							<input type="hidden" name="minDate" value="" /> 
							<input type="hidden" name="maxDate" value="" /> 
							<input type="hidden" name="nickName" value="" />
						    <input type="hidden" name="mobileNo" value="" /> 
						    <input type="hidden" name="nameCn" value="" /> 
					      </div>
											
						  <div class="form-group">
						    <label for="minDate">时间：</label>
						    <input type="date" class="form-control" id="minDate" name="minDate"> -
						    <input type="date" class="form-control" id="maxDate" name="maxDate">
						  </div>
						  <button id="btn_search" type="button" class="btn btn-primary" >
						 	<i class="fa fa-search fa-lg"></i> 搜索
						  </button>
					</form>
				    <table id="table">
				    </table>
		    </div>
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>