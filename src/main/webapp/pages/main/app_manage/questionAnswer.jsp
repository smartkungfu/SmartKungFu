<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-app管理-问题回答</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
	  	var paras = "";
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             				    cache: true, // 不缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				                pagination: true, // 开启分页功能
				               // singleSelect: false,
				                pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500],
				                search: false, // 关闭搜索功能
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                //smartDisplay: true, // 智能显示 pagination 和 cardview 等
				                queryParams: function (params) {
				                	//处理当前页
									var page = params.offset;
				                	if((page*1) !=0 ){
				            			page = page/params.limit;
				            		}
				            		page += 1;
				            		
				            		//排序，并处理多表中的属性
				                	var sort = params.sort;
				                	if(sort == undefined){
				                		sort ="";
				                	}else {
				                		if(sort == "nickName"){
				                			sort = "c.nickName";
				                		}else {
				                			sort = "d." + sort;
				                		}
				                	}
				            		
				                	var obj = {
					                        rows: params.limit,
					                        page: page,
					                        sort:sort,
					                        order:params.order,
					                        questionId:"${questionId}"
					                    };
				                	paras =  "rows=" + obj.rows + "&page=" + obj.page ;
				                	return obj;
				                },
				                url: "${pageContext.request.contextPath}/questionController/queryAnswerList",
				                columns: [{
				                    field: 'state',
				                    checkbox: true
				                },{
				                    field: 'isDelete',
				                    title: '是否删除',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){ 
				                    	if(value ==0){
				                    		return "否";
				                    	}
				                    	if(value ==1){
				                    		return "是";
				                    	} 
				                    }
				                },{
				                    field: 'answerTitle',
				                    title: '回答的问题',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                },{
				                    field: 'answerDetail',
				                    title: '问题描述',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                },{
				                    field: 'answerName',
				                    title: '发布人昵称',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                },{
				                    field: 'answerCreateDate',
				                    title: '发布时间',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return timeStamp2String(value);
				                    }
				                },{
				                    	  field: 'answerId',  
				                    	  title: '操作',
					                      align: 'center',
					                      events: operateEvents,
					                      formatter:function(value,row,index){  
							                   var eles = '<button class="btn btn-danger btn-xs delList"><i class="fa fa-trash-o"></i></button> '; 
							                   return eles ;
					                      }
					                 }
				                ],
				                onLoadSuccess:function(){
				                },
				                onLoadError: function () {
				             	   //    mif.showErrorMessageBox("数据加载失败！");
				                }
            });
        }
	  	
           	window.operateEvents = {
    	  	        'click .delList': function (e, value, row, index) {
    	  	        	 $.ajax({
    	  					url : "${pageContext.request.contextPath}/answerController/delList",
    	  					data : {ids:row.answerId},
    	  					type : "POST",
    	  					async : true,
    	  					dataType : "json",
    	  					success : function(flag) {
    	  						if(flag){
    	  							bootbox.alert({ 
    	  	 							  size: "small",
    	  	 							  title: "正确",
    	  	 							  message: "操作成功！", 
    	  	 							  callback: function(){
    	  	 								  $table.bootstrapTable('refresh');
    	  	 								  return;
    	  	 							 }
    	  	 						});
    	  						}else{
    	  							bootbox.alert({ 
    	 	 							  size: "small",
    	 	 							  title: "错误",
    	 	 							  message: "操作失败！", 
    	 	 							  callback: function(){return; }
    	 	 						});
    	  						}
    	  					},
    	  					error : function(data) {
    	  						bootbox.alert({ 
    	  							  size: "small",
    	  							  title: "错误",
    	  							  message: "请求超时，请重试！", 
    	  							  callback: function(){return; }
    	  						});
    	  						return;
    	  					}
    	  			   });	
    	  	        },
    	  	};
           	 
   		$(function(){
   			initTable();
   			
   			$("#btn_search").click(function(){
   				$table.bootstrapTable('refresh');
   			});
   			
   			QueryString.Initial();
	  		
	  		var menuId = QueryString.GetValue("menuId");
	  		var page = QueryString.GetValue("page");
	  		var rows = QueryString.GetValue("rows");
	  		var index_exp = QueryString.GetValue("index_exp");
	  		var index_che = QueryString.GetValue("index_che");
	  		
	  		
	  		$("input[name='menuId']").val(menuId);
	  		$("input[name='page']").val(page);
	  		$("input[name='rows']").val(rows);
	  		$("input[name='index_exp']").val(index_exp);
	  		$("input[name='index_che']").val(index_che);
   			
   		});
	    </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>问题管理</span></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
                  		<c:forEach var="g" items="${btns }">
                  			 <button type="button" class="btn btn-primary" onclick="${g.action }">
						 		<i class="${g.btnicon }"></i> ${g.btnname }
							 </button>
                  		</c:forEach>
 					 </div>
                </div>

                <h3 class="page-header"> 查看问题回答列表</h3>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
					<form class="form-inline" role="form" style="margin-top: 10px;margin-bottom: 10px;">
					
					     <!--附件参数  -->
						 <div class="folPara" style="display: none">
							<input type="hidden" name="menuId" value="" />
							<input type="hidden" name="rows" value="" />
							<input type="hidden" name="page" value="" /> 
							<input type="hidden" name="index_exp" value=""/>
							<input type="hidden" name="index_che" value=""/>
							
					      </div>
					</form>
				    <table id="table">
				    </table>
		    </div>
		    <input type="button" class="btn btn-primary" onclick="javascript:history.back(-1);" value="返回上一页">
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>