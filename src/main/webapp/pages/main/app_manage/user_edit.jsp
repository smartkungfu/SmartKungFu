<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-APP管理-编辑用户</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
		<!--include fileinput.css  -->
	  	<link href="${pageContext.request.contextPath}/css/fileinput.min.css" rel="stylesheet">
	  	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	  	<!-- include datetimepicker.css  -->
	  	<link href="${pageContext.request.contextPath}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<!--include wysiwyg.js  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-wysiwyg.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.hotkeys.min.js"></script>
	  	<!--导入省市区JSON数据  -->
	  	<script type="text/javascript" src="${pageContext.request.contextPath}/js/ProJson.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/DistrictJson.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/CityJson.js"></script>
		<!-- include datetimepicker.js  -->
	  	<script src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.min.js"></script>
	    <script src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.zh-CN.js"></script>
		
	  	<script type="text/javascript">
		  	 $(document).ready(function() {
		  		QueryString.Initial();
		  		
		  		var menuId = QueryString.GetValue("menuId");
		  		var page = QueryString.GetValue("page");
		  		var rows = QueryString.GetValue("rows");
		  		var index_exp = QueryString.GetValue("index_exp");
		  		var index_che = QueryString.GetValue("index_che");
		  		
		  		var minDate = QueryString.GetValue("minDate");
		  		var maxDate = QueryString.GetValue("maxDate");
		  		var nickName = QueryString.GetValue("nickName");
		  		var mobileNo = QueryString.GetValue("mobileNo");
		  		var nameCn = QueryString.GetValue("nameCn");
		  		
		  		$("input[name='menuId']").val(menuId);
		  		$("input[name='page']").val(page);
		  		$("input[name='rows']").val(rows);
		  		$("input[name='index_exp']").val(index_exp);
		  		$("input[name='index_che']").val(index_che);
		  		
		  		$("input[name='minDate']").val(minDate);
		  		$("input[name='maxDate']").val(maxDate);
		  		$("input[name='nickName']").val(nickName);
		  		$("input[name='mobileNo']").val(mobileNo);
		  		$("input[name='nameCn']").val(nameCn);
		  		
		  		// 加载省信息
			  	var $province = $("select[name='province']");
			  	$province.append("<option value='0'>请选择</option>");
			  	$.each(province, function(k, p) {
			  			 var proId = "${mstClient.province}";
			  			 if(proId == p.ProID){
			  				 option = "<option  value='"+p.ProID+"' selected='selected'>"+p.ProName+"</option>";
			  			 } else {
			  				 option = "<option  value='"+p.ProID+"'>"+p.ProName+"</option>";
			  			 }
			  			$province.append(option);
			  	});
			  	
			  	var $city = $("select[name='city']");
			  	$city.append("<option value='0'>请选择</option>");
		    	$.each(city, function(k, p) {
		    		var provinceId = "2";
		    		provinceId = "${mstClient.province}";
		    		var cityId = "${mstClient.city}";
					var option = "";
					if(provinceId == p.ProID){
						if(p.CityID == cityId){
							 option = "<option value='"+p.CityID+"' selected='selected'>"+p.CityName+"</option>";
						 } else {
							 option = "<option value='"+p.CityID+"'>"+p.CityName+"</option>";
						 }
					}
					
					$city.append(option);
				});
		    	
		    	var $area = $("select[name='area']");
		    	$.each(District, function(k, p) {
		    		var cityId = "3";
		    		cityId = "${mstClient.city}";
		    		var areaId = "${mstClient.area}";
		    		$area.append("<option value='0'>请选择</option>");
		    		
		    		var option = "";
		    		if(p.CityID == cityId){
		    			option = "<option value='"+p.Id+"'>"+p.DisName+"</option>";
						if(p.Id == areaId){
							 option = "<option value='"+p.Id+"' selected='selected'>"+p.DisName+"</option>"; 
						}
		    		}
					
					$area.append(option);
				}); 
		    	
		  		$.ajax({
						url : "${pageContext.request.contextPath}/mstCodeController/queryListByParentId",
						data : {parentId:"14"},
						type : "GET",
						async : true,
						dataType : "json",
					    beforeSend: function() {
					        //请求前的处理
					    },
						success : function(data) {
							
							$province.multiselect({
							      onChange: function(element, checked) {
							    	if(checked){
							    		var proId = $province.val();
							    		 //市数据加载
							    		var cityHtml = "<option value='0'>请选择</option>";
						                var areaHtml = "<option value='0'>请选择</option>";
						    		    $.each(city, function(k, p) {
						    				if (p.ProID == proId) {
						    					cityHtml += "<option value='"+p.CityID+"'>"+p.CityName+"</option>";
						    				}
						    			});
						    		    $city.html(cityHtml);
						    		    $area.html(areaHtml);
						    		    //刷新市、区下拉列表
						    		    $city.multiselect('rebuild');
						    		    $area.multiselect('rebuild');
					                }
							      },
							      maxHeight: 200,
							      filterPlaceholder: 'Search'
							});
							
							$city.multiselect({
							      onChange: function(element, checked) {
							    	if(checked){
							    		var cityId = $city.val();
							    		 //市数据加载
						                var areaHtml = "<option value='0'>请选择</option>";
						                $.each(District, function(k, p) {
											if (p.CityID == cityId) {
												areaHtml += "<option value='"+p.Id+"'>"+p.DisName+"</option>";
											}
										});
						    		    $area.html(areaHtml);
						    		    //刷新区下拉列表
						    		    $area.multiselect('rebuild');
					                }
							      },
							      maxHeight: 200
							});
							
							$area.multiselect({
							      onChange: function(element, checked) {
							      },
							      maxHeight: 200
							});
							
						},
						error : function(data) {
							bootbox.alert({ 
								  size: "small",
								  title: "错误",
								  message: "请求超时，请重试！", 
								  callback: function(){return; }
							});
							return;
						}
				});
		  		
		  		$(".cancel").click(function(){
		  			var reqPara = "?" + $(".folPara").find("input").serialize();
		  			reqPara = decodeURIComponent(reqPara,true);
		  			window.location.href="${pageContext.request.contextPath}/mstClientController/Main" + reqPara; 
		  		});
		  		
			  	$('form').bootstrapValidator({
			            // Exclude only disabled fields
			            // The invisible fields set by Bootstrap Multiselect must be validated
			            excluded: ':disabled',
			            feedbackIcons: {
			            	 valid: 'fa fa-check',
			            	 invalid: 'fa fa-times',
			            	 validating: 'fa fa-refresh'
			            },
			            fields: {
			                nickname: {
				                    validators: {
				                        notEmpty: {
				                            message: '用户昵称为空'
				                        }
				                    }
				            },
				            password: {
			                    validators: {
			                        notEmpty: {
			                            message: '用户密码为空'
			                        },
			                        stringLength: {
			                            min: 6,
			                            max: 30,
			                            message: '密码长度至为6-30位'
			                        },
			                        regexp: {
			                            regexp: "^[\\x00-\\xFF]+$",
			                            message: '密码必须是字符'
			                        }
			                    }
			                }, 
			               /*  namecn: {
			                    validators: {
			                        notEmpty: {
			                            message: '中文姓名'
			                        },
			                        stringLength: {
			                            min: 2,
			                            max: 20,
			                            message: '用户名长度为2-20'
			                        },
			                    }
			                }, */
			                gender: {
			                    validators: {
			                        notEmpty: {
			                            message: '请选择性别'
			                        }
			                    }
			                },
			                birthday: {
			                    validators: {
			                        notEmpty: {
			                            message: '出生日期为空'
			                        },
			                    }
			                },
			                mobileno: {
			                    validators: {
			                        notEmpty: {
			                            message: '手机号'
			                        },
			                        regexp: {
			                            regexp: "^(13|15|18|17)[0-9]{9}$",
			                            message: '手机号格式不正确'
			                        }
			                    }
			                },
			                address: {
			                    validators: {
			                        notEmpty: {
			                            message: '地址详情为空'
			                        },
			                        stringLength: {
			                        	  min: 1,
				                          max: 50,
				                          message: '地址详情长度为1-50'
			                        }
			                    }
			                },
			            },
			            submitHandler: function (validator, form, submitButton) {
			            	validator.defaultSubmit();
			            }
			        });
		  	  });
	    </script>
   </head>
<body>
	
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">
                <div class="row">
	                 <div class="col-mod-12">
		                 <ul class="breadcrumb">
		                   <li><span >系统管理</span></li>
		                   <li><a href="javascript:void(0)" class="cancel">APP管理</a></li>
		                   <li><a href="javascript:void(0)" class="active">编辑用户</a></li>
		                 </ul>
		                 <h3 class="page-header">编辑用户 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
		
		                 <blockquote class="page-information hidden">
			                 <p>
			                  	用户资料信息编辑
			                 </p>
		                 </blockquote>
	          	    </div>
          		</div>

				<!-- Form elements -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									基本信息
								</h3>
							</div>
							<div class="panel-body ">
								<div class="ro">
									<div class="col-mol-md-offset-2">
										<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/mstClientController/save">
											
											<!--附件参数  -->
											<div class="folPara" style="display: none">
												<input type="hidden" name="menuId" value="" />
												<input type="hidden" name="rows" value="" />
												<input type="hidden" name="page" value="" /> 
												<input type="hidden" name="index_exp" value=""/>
												<input type="hidden" name="index_che" value=""/>
												
												<input type="hidden" name="minDate" value="" /> 
												<input type="hidden" name="maxDate" value="" /> 
												<input type="hidden" name="nickName" value="" />
											    <input type="hidden" name="mobileNo" value="" /> 
											    <input type="hidden" name="nameCn" value="" /> 
											</div>
											
											<!-- 逻辑参数 -->
											<input type="hidden" name="id" value="${mstClient.id }" />
											
											<div class="form-group">
												<label class="col-lg-3 control-label">头像： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<input class="form-control file-loading"  name="myfiles"
														type="file">
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">昵称： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<input type="text" class="form-control" name="nickname"  value="${mstClient.nickname }" placeholder="请输入昵称" />
												</div>
											</div>

											<div class="form-group">
												<label class="col-lg-3 control-label"> 密码： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<input type="text" class="form-control" name="password" value="${mstClient.password }" placeholder="请输入密码" />
												</div>
											</div>
											<%-- <div>
												<div class="form-group">
													<label class="col-lg-3 control-label">中文姓名： <sup>*</sup></label>
													<div class="input-group col-lg-5">
														<input type="text" class="form-control" name="namecn" value="${mstClient.namecn }" placeholder="请输入中文姓名" />
													</div>
												</div>
											</div> --%>
											<div class="form-group">
												<label class="col-lg-3 control-label">性 别： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<div class="radio-group">
														<c:if test="${mstClient.gender}">
															<label>
																 <input type="radio" name="gender" value="1" checked="checked"/> 男
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="gender" value="0" /> 女
															</label>
														</c:if>
														<c:if test="${!mstClient.gender}">
															<label>
																 <input type="radio" name="gender" value="1" /> 男
															</label>&nbsp;&nbsp;&nbsp; 
															<label> 
																<input type="radio" name="gender" value="0" checked="checked" /> 女
															</label>
														</c:if>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 身高(cm)： <sup>&nbsp;</sup></label>
												<div class="input-group col-lg-5">
													<input type="text" class="form-control" name="height" value="${mstClient.height }" placeholder="请输入身高" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 体重(kg)： <sup>&nbsp;</sup></label>
												<div class="input-group col-lg-5">
													<input type="text" class="form-control" name="weight" value="${mstClient.weight }" placeholder="请输入体重" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label"> 个性签名： <sup>&nbsp;</sup></label>
												<div class="input-group col-lg-5">
													<input type="text" class="form-control" name="sign" value="${mstClient.sign }" placeholder="请输入个性签名" />
												</div>
											</div>
											<div class="form-group">
												<label for="dtp_input3" class="col-md-3 control-label">出生日期： <sup>*</sup></label>
												<div class="input-group date form_date col-md-5"
													data-date="" data-date-format="yyyy-mm-dd"
													data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
													<input class="form-control" type="text" value="<fmt:formatDate value="${mstClient.birthday }" pattern="yyyy-MM-dd"/>"
														readonly> <span class="input-group-addon"><span
														class="glyphicon glyphicon-remove"></span></span> <span
														class="input-group-addon"><span
														class="glyphicon glyphicon-calendar"></span></span>
												</div>
												<label class="col-lg-3 control-label"></label>
												<div class="input-group col-lg-5">
													<input type="hidden" class="form-control" name="birthday" id="dtp_input2" value="<fmt:formatDate value="${mstClient.birthday }" pattern="yyyy-MM-dd"/>" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-lg-3 control-label">手机： <sup>*</sup></label>
												<div class="input-group col-lg-5">
													<input type="tel" class="form-control" name="mobileno" value="${mstClient.mobileno }" placeholder="请输入手机"/>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">省市区： <sup>*</sup></label>
													<div class="input-group col-lg-5">
														<select class="multiselect" name="province" >
														</select> &nbsp;&nbsp;-&nbsp;&nbsp;
														<select class="multiselect" name="city">
														</select>&nbsp;&nbsp;-&nbsp;&nbsp;
														<select class="multiselect" name="area">
														</select>
													</div>
												</div>
											</div>
											<div>
												<div class="form-group">
													<label class="col-lg-3 control-label">地址： <sup>*</sup></label>
													<div class="input-group col-lg-5">
														<input type="text" class="form-control" name="address" value="${mstClient.address }" placeholder="请输入详细地址" />
													</div>
												</div>
											</div>

											<div class="form-group">
												<div class="input-group col-lg-9 col-lg-offset-3">
													<button type="submit" class="btn btn-primary"><i class="fa fa-check fa-lg"></i>保存</button>&nbsp;&nbsp;&nbsp;
													<button id="btn_cancel" type="button" class="btn btn-primary cancel"><i class="fa fa-remove fa-lg"></i>取消</button>
												</div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../../fragment/frag_bottom_js.jsp"/>
			<!--include fileinput.js  -->
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput.js"></script>
	  		<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileinput_locale_zh.js"></script>
</body>
<script type="text/javascript">
$(function(){
	$('.form_date').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		startDate: "1970-01-01",
		endDate:new Date(),
    }).on('hide', function(ev){
        $('form')
        .bootstrapValidator('updateStatus', 'birthday', 'NOT_VALIDATED')
        .bootstrapValidator('validateField', 'birthday');
    });
	
	 var imsg = [];
	 var res = "${mstClient.portrait}".split(";");
	 for(var i = 0; i < res.length; i ++){
		 if(res[i] != null && res[i] != ""){
			 imsg.push("<img src='"+res[i]+"' class='file-preview-image' style='width:180px;height:120px;' />");
		 }
	 }
	 $("input[name='myfiles']").fileinput({
	        language: 'zh', //设置语言
	      	uploadUrl: "${pageContext.request.contextPath}/resourcesController/saveTempImage", //上传的地址
	        uploadAsync: true, //异步加载
	      	allowedFileExtensions : ['jpg', 'png'],//接收的文件后缀
	        showUpload: true, //是否显示上传按钮
	        showCaption: false,//是否显示标题
	        overwriteInitial: true,
	        maxFileSize: 1024 * 10,
	        maxFilesNum: 10,
	        maxFileCount: 1,
	        slugCallback: function(filename) {
	            return filename.replace('(', '_').replace(']', '_');
	        },
	      
	        showPreview:true,
	        initialPreview: imsg,
	        allowedPreviewTypes : [ 'image' ],
	        browseClass: "btn btn-primary", //按钮样式             
	 });
	 $("input[name='myfiles']").on("fileuploaded", function (event, data, previewId, index) {  
		// JSON.stringify(data),分析data数据格式
		// alert(data.response.resName + "==" + previewId + "==" + index)
		var $input = " <input type='hidden' name='imagename' value='"+data.response.resName+"'/>";
		var $form = $("form");
		$form.append($input);
     });   
});
</script>
</html>