<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-APP管理-用户管理</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../../fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="../../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
	  	var paras = "";
	  	
	  	var a = 1;
	  	
	  	function initTable() {
           	 $table = $('#table').bootstrapTable({
             					method: 'post',
             					//cache: false, // 不缓存
				                //height: $(window).height() - 200,
				              	 /*TODO: queryParams传参数到服务器中，bootstrap_table.js源码默认contentType=application/json，这里需要改为 application/x-www-form-urlencoded以便参数传递*/
				                contentType:"application/x-www-form-urlencoded",
				                striped: true,// 隔行加亮
				                pagination: true, // 开启分页功能
				               // singleSelect: false,
				              //  pageNumber: 1,
				                pageSize: 10,//设置默认分页为 50
				                pageList: [10, 50, 100, 200, 500],
				                search: false, // 关闭搜索功能
				                showColumns: false, //不显示下拉框（选择显示的列）
				                sidePagination: "server", //服务端处理分页
				                minimunCountColumns: 2,// 设置最少显示列个数
				                //smartDisplay: true, // 智能显示 pagination 和 cardview 等
				                queryParams: function (params) {
				                	//处理当前页
									var page = params.offset;
				                	if((page*1) !=0 ){
				            			page = page/params.limit;
				            		}
				            		page += 1;
				            		
				            		//排序，并处理多表中的属性
				                	var sort = params.sort;
				                	if(sort == undefined){
				                		sort ="";
				                	}else {
				                		if(sort = "cUName"){
				                			sort = "u1.nameCn";
				                		}else{
				                			sort = "u." + sort;
				                		}
				                	}
				                	
				                	var obj = {
					                        rows: params.limit,
					                        page: page,
					                        sort:sort,
					                        order:params.order,
					                        minDate:$("#minDate").val(),
					                        maxDate:$("#maxDate").val(),
					                        nickname:$("#nickName").val(),
					                        namecn:$("#nameCn").val(),
					                        mobileno:$("#mobileNo").val()
					                    };
				                	paras =  "rows=" + obj.rows + "&page=" + obj.page ;
				                	return obj;
				                },
				                detailView:true,
				                detailFormatter:function detailFormatter(index, row) {
				                	 var html = [];
				                	  
				                     html.push('<p><b>密码:</b> ' + (row.password == null || row.password == ''? '-' : row.password) + '</p>'); 
				                     html.push('<p><b>身高:</b> ' + (row.height == null || row.height == ''? '-' : row.height + 'cm') + '</p>'); 
				                     html.push('<p><b>体重:</b> ' + (row.weight == null || row.weight == ''? '-' : row.weight + 'kg') + '</p>'); 
				                     html.push('<p><b>个性签名:</b> ' + (row.sign == null || row.sign ==''? '-' : row.sign) + '</p>'); 
				                     html.push('<p><b>地址:</b> ' + (row.address == null || row.address == ''? '-' : row.address) + '</p>'); 
				                     
				                     return html;
				                },
				                url: "${pageContext.request.contextPath}/mstClientController/queryList",
				                columns: [{
				                    field: 'state',
				                    checkbox: true
				                },  {
				                    field: 'portrait',
				                    title: '头像',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	
				                    	if(value == null || value == ""){
				                    		return "<img height='35px' width='50px' src='${pageContext.request.contextPath}/images/portrait.png' alt='' class='img-circle'/> ";
				                    	} else{
				                    		return "<img height='35px' width='50px' src='"+value+"' alt='' class='img-circle'/> ";
				                    	}
				                    	
				                    }
				 
				                },{
				                    field: 'nickname',
				                    title: '昵称',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                }, 
				               /*  {
				                    field: 'namecn',
				                    title: '中文名',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				 
				                },  */
				                {
				                    field: 'gender',
				                    title: '性别',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	if(value){
				                    		return "男";
				                    	} else {
				                    		return "女";
				                    	}
				                    }
				                }, {
				                    field: 'birthday',
				                    title: '出生日期',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return dateStamp2String(value);
				                    }
				                }, {
				                    field: 'mobileno',
				                    title: '手机号码',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true
				                },  {
				                    field: 'logintype',
				                    title: '注册方式',
				                    align: 'center',
				                    valign: 'middle',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	if(value == 2){
				                    		return "手机号注册";
				                    	} else if(value == 4){
				                    		return "QQ账号注册";
				                    	} else if(value == 296){
				                    		return "微信注册";
				                    	} else if(value == 297){
				                    		return "新浪注册";
				                    	} else {
				                    		return "";
				                    	}
				                    }
				                },{
				                    field: 'createdate',
				                    title: '注册时间',
				                    align: 'center',
				                    valign: 'top',
				                    sortable: true,
				                    formatter:function(value,row,index){  
				                    	return timeStamp2String(value);
				                    }
				                }],
				                onLoadSuccess:function(){
				                },
				                onLoadError: function () {
				             	   //    mif.showErrorMessageBox("数据加载失败！");
				                }
            });
        }
	  	
	  	function addObj(url){
	  		paras = paras +"&index_exp=${MENU_STYLE_INDEX_EXP}"+ "&index_che=${MENU_STYLE_INDEX_CHE}"+"&"+  $("form").find("input").serialize();
			paras = decodeURIComponent(paras,true);
			
			window.location.href = url + "?"+ paras;
	  	}
	  	
	  	function delList(url){
	  		var rows = $table.bootstrapTable('getSelections');
			var ids = "";
			$(rows).each(function(index,element){
				ids += element.id + ",";
			});
			ids = ids.substring(0,ids.length - 1);
			delListAsUtils({ids:ids},url,$table);
	  	}
	  	
	  	function editObj(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择编辑行", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一条编辑数据", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "index_exp=${MENU_STYLE_INDEX_EXP}"+"&index_che=${MENU_STYLE_INDEX_CHE}"+"&id=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize();
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	  	
	  	//用户收藏
	  	function favorite(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择用户", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一个用户", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "index_exp=${MENU_STYLE_INDEX_EXP}"+"&index_che=${MENU_STYLE_INDEX_CHE}"+"&clientid=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize();
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	  	
	    //用户发布问题
	  	function clientQuestion(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择用户", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一个用户", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "index_exp=${MENU_STYLE_INDEX_EXP}"+"&index_che=${MENU_STYLE_INDEX_CHE}"+"&clientid=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize();
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	    
	  	 //用户发布动态
	  	function clientDynamic(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择用户", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一个用户", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "index_exp=${MENU_STYLE_INDEX_EXP}"+"&index_che=${MENU_STYLE_INDEX_CHE}"+"&clientid=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize();
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	  	
	    //用户发布回答
	  	function clientAnswer(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择用户", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一个用户", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "index_exp=${MENU_STYLE_INDEX_EXP}"+"&index_che=${MENU_STYLE_INDEX_CHE}"+"&clientid=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize();
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	    
	    //用户优惠
	  	function roll(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择用户", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一个用户", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "index_exp=${MENU_STYLE_INDEX_EXP}"+"&index_che=${MENU_STYLE_INDEX_CHE}"+"&clientid=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize();
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}

	    //用户活动
	  	function project(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择用户", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一个用户", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "index_exp=${MENU_STYLE_INDEX_EXP}"+"&index_che=${MENU_STYLE_INDEX_CHE}"+"&clientid=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize();
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	    
	    //用户课程
	  	function course(url){
	  		var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择用户", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一个用户", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "index_exp=${MENU_STYLE_INDEX_EXP}"+"&index_che=${MENU_STYLE_INDEX_CHE}"+"&clientid=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize();
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	  	}
	  	
	    //用户等级优惠
	    function growthUsed(url){
			var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择用户", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一个用户", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "index_exp=${MENU_STYLE_INDEX_EXP}"+"&index_che=${MENU_STYLE_INDEX_CHE}"+"&clientid=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize();
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	    }
	    
	    //在线课程
	    function onlineCourse(url){
			var rows = $table.bootstrapTable('getSelections');
	  		
	  		if(rows.length == 0){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "请选择用户", 
					  callback: function(){return; }
				});
	  		} else if(rows.length > 1){
	  			bootbox.alert({ 
					  size: "small",
					  title: "提示",
					  message: "只能选择一个用户", 
					  callback: function(){return; }
				});
	  		} else {
	  			paras = "index_exp=${MENU_STYLE_INDEX_EXP}"+"&index_che=${MENU_STYLE_INDEX_CHE}"+"&clientid=" + rows[0].id + "&" +paras + "&"+  $("form").find("input").serialize();
				paras = decodeURIComponent(paras,true);
				window.location.href = url + "?"+ paras;
	  		}
	    }
	    
   		$(function(){
   			initTable();
   			
   			$("#btn_search").click(function(){
   				$table.bootstrapTable('refresh');
   			});
   		 
   		});
	    </script>
   </head>
<body>
    <div class="site-holder">
      <!-- nav -->
	  <jsp:include page="../../fragment/frag_nav.jsp" />
     
          <!-- .box-holder -->
          <div class="box-holder">
		  <!--left-sidebar  -->
		  <jsp:include page="../../fragment/frag_left.jsp" />

            <!-- .content -->
            <div class="content">

                <div class="row">
                 <div class="col-mod-12">
                  <ul class="breadcrumb">
                   <li><span>APP管理</span></li>
                   <li><a href="javascript:void(0)" class="active">用户管理</a></li>
                 </ul>
                 
                 <div class="form-group hiddn-minibar pull-right">
                  	<div id="toolbar" class="btn-group">
                  		<!-- 摆放按钮 -->
                  		<c:forEach var="g" items="${btns }" varStatus="i">
                  			 <button type="button" class="btn btn-primary" onclick="${g.action }">
						 		<i class="${g.btnicon }"></i> ${g.btnname }
							 </button>
                  		</c:forEach>
 					 </div>
                </div>

                <h3 class="page-header"> 用户管理 <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>

                <blockquote class="page-information hidden">
                 <p>
                  APP前台用户信息
                </p>
              </blockquote>
            </div>
          </div>

	        <!-- Users widget -->
	        <div class="panel-body">
					<form class="form-inline" role="form" style="margin-top: 10px;margin-bottom: 10px;">
						  <input type="hidden" name="menuId" value="${folPara.menuId }">
						  <div class="form-group">
						    <label for="minDate">注册时间：</label>
						    <input type="date" class="form-control" id="minDate" name="minDate" value="${folPara.minDate }"> -
						    <input type="date" class="form-control" id="maxDate" name="maxDate"  value="${folPara.maxDate }">
						  </div>
						  <br/><br/>
						  <div class="form-group">
						    <label for="usrAccount">用户昵称：</label>
						    <input type="text" class="form-control" id="nickName" name="nickName"  value="${folPara.nickName }">
						  </div>
						 <%--  <div class="form-group">
						    <label  for="namecn">中文名：</label>
						    <input type="text" class="form-control" id="nameCn" name="nameCn"  value="${folPara.nameCn }">
						  </div> --%>
						   <div class="form-group">
						    <label  for="mobileNo">手机号：</label>
						    <input type="text" class="form-control" id="mobileNo" name="mobileNo"  value="${folPara.mobileNo }">
						  </div>
						  <!-- <div class="checkbox">
						    <label>
						      <input type="checkbox"> Remember me
						    </label>
						  </div> -->
						  <button id="btn_search" type="button" class="btn btn-primary" >
						 	<i class="fa fa-search fa-lg"></i> 搜索
						  </button>
					</form>
				    <table id="table">
				    </table>
		    </div>
	    </div>
	   </div>
	 </div>
	 <jsp:include page="../../fragment/frag_bottom_js.jsp"/>
</body>
</html>