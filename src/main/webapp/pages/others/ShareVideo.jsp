<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="分享视频">
<meta name="description" content="分享视频">
<meta name="apple-mobile-web-app-capable"  content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no"/>
<title>分享视频</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/style.css">
</head>
<body >
<section class="wrap_box"> 
  <div class="video_box">
<%--   <a href="javascript:void(0)" class="vedio_play">
  	<video poster="http://skf.smartkungfu.com/${videoInfo.reserved9 }" width="100%" height="100%" src="http://skf.smartkungfu.com/${videoInfo.url }" controls="controls"></video>
  </a> --%>
  	<video poster="http://skf.smartkungfu.com/${videoInfo.reserved9 }" width="100%" height="100%" src="http://skf.smartkungfu.com/${videoInfo.url }" controls="controls"></video>
  </div>
  <div class="vedio_detail_box clearfix">
  	<h3 class="details_til">${videoInfo.title }</h3>
    <p class="author">作者：${videoInfo.author } <fmt:formatDate value="${videoInfo.createdate }" pattern="MM.dd"/></p>
    <p>${videoInfo.content }</p>
  </div>
  <div class="control_box">
  	<a href="javascript:void(0)" class="attention">${videoInfo.browsenum }</a>
    <a href="javascript:void(0)" class="share">${videoInfo.sharenum }</a>
    <a href="javascript:void(0)" class="collect">${videoInfo.favoritenum }</a>
    <a href="javascript:void(0)" class="collect_2">${videoInfo.disNum }</a>
  </div>
</section>
</body>
</html>
