<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><!doctype html>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="文章详情">
<meta name="description" content="文章详情">
<meta name="apple-mobile-web-app-capable"  content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no"/>
<title>有点功夫</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/style.css">
<style type="text/css">
body{ padding-bottom:55px}

.mask_bg{ position:fixed;top:0;right:0;bottom:0; left:0; background:rgba(0,0,0,.8); z-index:90; display:none}
.ball_box{ position:fixed; bottom:60px;left:0;right:0; z-index:96; text-align:center;color:#fff;font-size:14px;line-height:1.6; display:none}
.lightball_img{width:50.666%; margin-left:auto; margin-right:auto; margin-top:-20px}
.lightball_img img{width:100%}
.tip_pop{ text-align:center;width:200px;height:100px;font-size:14px;position:fixed;left:50%; margin-left:-100px;top:50%; margin-top:-50px; z-index:97; background:#fff; border-radius:4px; display:none}
.tip_pop p{color:#333;line-height:56px;}
.tip_pop a{ border-top:1px #e5e5e5 solid;line-height:44px; display:block;color:#007aff; text-decoration:none}
@media screen and (max-width:375px){
	.tip_pop{width:280px; margin-left:-140px}
}
.copyright{
    font-family: sans-serif,"Helvetica Neue",'微软雅黑','黑体',Gotham,Helvetica,Arial;
    font-size: 12px;
    color: #666;
    text-align:center;
}
ol, ul, li {
    list-style:none;
}
.found_detail_box p {
padding-bottom: 12px;
}
</style>
</head>

<body>
<article>

  <div class="found_detail_box photo_details_box clearfix">
  	<h3 class="details_til">${wenwInfo.title }</h3>
    <p class="author">作者：${wenwInfo.author } <fmt:formatDate value="${wenwInfo.createdate }" pattern="yyyy.MM.dd"/></p>
    ${wenwInfo.content }
  	<div class="copyright">— 投稿或商务合作 —<br/>请发邮件至 article@smartkungfu.com </div><br/>
  </div>
  		<div>
		<p style="margin-left:20px;font-size:18px;color:black;">${wenwInfo.reserved1}条评论</p>
		</div>
		 <!-- 
  <div  class="control_box clearfix" style="left:50px;height:30px;">
			<a href="javascript:void(0)" class="collect_2">${wenwInfo.reserved1}条评论</a>
 </div>
 
  <div class="control_box clearfix" style="left:30px;margin-bottom:30px;">
		  <ul>
		    <c:forEach items="${mainList}" var="mstDiscuss">
                <li>
                <p><img src="${mstDiscuss.sendPortrait }" alt="" width="35px" height="35px" style="border-radius:50%;"/></p>
                <span style="position:relative;top:-30px;left:45px;font-size:15px;">${mstDiscuss.sendName}</span>
                <p style="position:relative;top:-10px;left:45px;font-size:15px;color:black;width:280px;">${mstDiscuss.content}</p>
                <c:if test="${mstDiscuss.mds[0]!=null}">
                 <div style="position:relative;top:-10px;left:45px;font-size:15px;background:#f1f1f1;width:280px;">
                 <p style="position:relative;top:0px;left:20px;line-height:25px;">回复<span style="color:#ffdf16">${mstDiscuss.mds[0].sendName}</span>:</p>
                 <p style="position:relative;top:2px;left:20px;line-height:25px;">${mstDiscuss.mds[0].content}</p>
                 </div>
                </c:if>
                </li>
             </c:forEach>
		  </ul>
	</div>
	-->
		<div class="control_box clearfix" style="height:80px;">
		<a href="javascript:void(0)" class="detail_btn_more">登陆app查看评论</a>
		</div>
	<div class="xz_btn" style="height:60px; line-height:60px;flex: auto;width: 100%;">
	<img src="${pageContext.request.contextPath}/pages/others/images/xz.png" alt="" width="50px" height="50px" style="position:absolute;top:6px;left:10px;"/>
	<span style="position:absolute;top:-10px;left:70px;color:black;">有点功夫</span>
	<span style="position:absolute;top:15px;left:70px;font-size:12px;">让你有点自己的功夫</span>
	<!--  
	<button class="dkbutton">打开app</button>
	-->
	<button class="xzbutton">打开app</button>
	</div>
</article>

<script src="${pageContext.request.contextPath}/pages/others/js/jquery.min.2.0.js"></script>
<script src="${pageContext.request.contextPath}/pages/others/js/swiper.min.js"></script>
<script>
$(function(){
	
	var str="";
	if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
	    str="ios";  
	} else if (/(Android)/i.test(navigator.userAgent)) {
	    str="android"; 
	} else {
	   str="pc";
	};	
	
	// Initialize Swiper
	var activitySwiper = new Swiper('.swiper-container', {
		//pagination: '.swiper-pagination',
		slidesPerView: 2,
		paginationClickable: true,
		spaceBetween: 5,
		freeMode: true
	});
	
	$('.detail_btn_more,.xzbutton').click(
			function() {
				if(str=="ios"){
			    	if(is_weixn()){
			    		alert("请在浏览器中打开");
			    	}else{
			    		window.location.href = 'smartkungfu://www.smartkungfu.com?type=wen&id='+${wenwInfo.id};
					    t = Date.now();
					    setTimeout(function(){
					        if (Date.now() - t < 1200) {
					        	window.location.href = 'https://itunes.apple.com/cn/app/id1190945824';
					        }
					    }, 1000);
					    return false;
			    	 } 	
				}else if(str=="android"){
			    	if(is_weixn()){
			    		alert("请在浏览器中打开");
			    	}else{
			    		window.location.href = 'smk://com.smk.fitness:8888/web?type=wen&id='+${wenwInfo.id};
					    t = Date.now();
					    setTimeout(function(){
					        if (Date.now() - t < 1200) {
					        	alert("android下载地址敬请期待");
					        }
					    }, 1000);
					    return false;
			    	 } 	
					
					}else{
					  window.location.href = "http://www.smartkungfu.com/";
					}
	});
	
});
function is_weixn(){

	 var ua = navigator.userAgent.toLowerCase();

	 if(ua.match(/MicroMessenger/i)=="micromessenger") {
			return true;
		} else {
			return false;
			}
	}
</script>
</body>
</html>
