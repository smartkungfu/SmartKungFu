<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="文章详情">
<meta name="description" content="文章详情">
<meta name="apple-mobile-web-app-capable"  content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no"/>
<title>分享场馆</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/style.css">
<style type="text/css">
.ball_box{ position:fixed; bottom:60px;left:0;right:0; z-index:96; text-align:center;color:#fff;font-size:14px;line-height:1.6; display:none}
.lightball_img{width:50.666%; margin-left:auto; margin-right:auto; margin-top:-20px}
.lightball_img img{width:100%}
.tip_pop{ text-align:center;width:200px;height:100px;font-size:14px;position:fixed;left:50%; margin-left:-100px;top:50%; margin-top:-50px; z-index:97; background:#fff; border-radius:4px; display:none}
.tip_pop p{color:#333;line-height:56px;}
.tip_pop a{ border-top:1px #e5e5e5 solid;line-height:44px; display:block;color:#007aff; text-decoration:none}
 @media screen and (max-width:375px){
	.tip_pop{width:280px; margin-left:-140px}
}
</style>
</head>
  
<body class="pb49">

<section class="wrap_box"> 
  <div class="venue_kv">
  	<!-- Swiper -->
    <div class="swiper-container">
        <ul class="swiper-wrapper">
        	<c:forEach items="${mstGymnasium.gymBills }" var="g" >
        		<li class="swiper-slide bill"><a href="javascript:void(0)"><img src="${g }"  alt=""/></a></li>
        	</c:forEach>
        </ul>
        <!-- Add Pagination -->
      <div class="swiper-pagination"></div>
    </div>
  </div>
  
  <div class="cont_box clearfix">
    <div class="bot_line"><a href="javascript:void(0)" class="detail_btn top_line"><span>场馆介绍</span></a></div>
    <p class="venue_des">${mstGymnasium.content }</p>
    <div class="venue_info bot_line">
        <p class="address">${mstGymnasium.address }</p>
      <!--   <p class="icon_tel">电话：+86-021-3133-1203</p> -->
      </div>
      
      <!--课程列表 start-->
      <div class="course_list">
      	<ul>
      		<c:forEach items="${mstGymnasium.speGds }" var="g" varStatus="status">
      			<li class="features_item"><a href="javascript:void(0)">
		           	<img src="${g.coursebill }"  alt="${g.coursename }"/>
		            <span class="num">  ${status.index + 1}</span>
		            <h4>${g.coursename }</h4>
		            </a>
	            </li>
      		</c:forEach>
      		<c:forEach items="${mstGymnasium.otherGds }" var="g"  begin="0" end="0" >
           		<li>
           			<a href="javascript:void(0)">
		           	<img src="${g.coursebill }"  alt="${g.coursename }"/>
		            <span class="qita_tab">其他课程</span>
		            <h4>${g.coursename }</h4>
		            </a>
	            </li>	
            </c:forEach>
        </ul>
      </div>
      
  </div>
</section>
<!--在线咨询弹出框 start-->
<div class="pop_box">
    <div class="mask_bg"></div>
    <div class="pop_con">
        <p>喜欢我们吗？赶快下载吧！</p>
        <a href="javascript:void(0)" class="comfirm_btn">确定</a>
    </div>
</div>

<footer>
	<a href="javascript:void(0)" class="sign_up_btn">立即报名</a>
</footer>
<script src="${pageContext.request.contextPath}/pages/others/js/jquery.min.2.0.js"></script>
<script src="${pageContext.request.contextPath}/pages/others/js/swiper.min.js"></script>
<script src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript">
$(function(){
	
	// Initialize Swiper
	 var venueKvSwiper = new Swiper('.swiper-container', {
         pagination: '.swiper-pagination',
         paginationClickable: true,
         centeredSlides: true,
         autoplay: 2500,
		 loop: true,
         autoplayDisableOnInteraction: false
    });
	
	$('a').click(function(){
		if($(this).attr('class') == "comfirm_btn" ){
			window.location.href="https://itunes.apple.com/cn/app/id1190945824";
		}
		
		if($(this).attr('class') != "message"){
			$('.pop_box').fadeIn();
		}
		
	});
		
	$('.mask_bg,.comfirm_btn').click(function(){
		$('.pop_box').fadeOut();	
	});
});

</script>
</body>
</html>
