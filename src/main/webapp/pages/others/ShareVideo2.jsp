<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="分享视频">
<meta name="description" content="分享视频">
<meta name="apple-mobile-web-app-capable"  content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no"/>
<title>有点功夫</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/style.css">
</head>
<body >
<section class="wrap_box"> 
  <div class="video_box">
  	<video poster="http://skf.smartkungfu.com/${videoInfo.reserved9 }" width="100%" height="100%" src="http://skf.smartkungfu.com/${videoInfo.url }" controls="controls"></video>
  </div>
  <div class="vedio_detail_box clearfix">
  	<h3 class="details_til">${videoInfo.title }</h3>
    <p class="author">作者：${videoInfo.author } <fmt:formatDate value="${videoInfo.createdate }" pattern="yyyy.MM.dd"/></p>
    <p>${videoInfo.content }</p>
  </div>
  		<div>
		<p style="position:relative;top:15px;left:20px;font-size:18px;color:black;">${videoInfo.disNum }条评论</p>
		</div>
   <!-- 
  <div  class="control_box clearfix" style="left:50px;height:30px;">
    <a href="javascript:void(0)" class="collect_2">${videoInfo.disNum }条评论</a>
  </div>
 
  <div class="control_box clearfix" style="left:30px;margin-bottom:30px;">
		  <ul>
		    <c:forEach items="${mainList}" var="mstDiscuss">
                <li>
                <p><img src="${mstDiscuss.sendPortrait }" alt="" width="35px" height="35px" style="border-radius:50%;"/></p>
                <span style="position:relative;top:-30px;left:45px;font-size:15px;">${mstDiscuss.sendName}</span>
                <p style="position:relative;top:-10px;left:45px;font-size:15px;color:black;width:280px;">${mstDiscuss.content}</p>
                <c:if test="${mstDiscuss.mds[0]!=null}">
                 <div style="position:relative;top:-10px;left:45px;font-size:15px;background:#f1f1f1;width:280px;">
                 <p style="position:relative;top:0px;left:20px;line-height:25px;">回复<span style="color:#ffdf16">${mstDiscuss.mds[0].sendName}</span>:</p>
                 <p style="position:relative;top:2px;left:20px;line-height:25px;">${mstDiscuss.mds[0].content}</p>
                 </div>
                </c:if>
                </li>
             </c:forEach>
		  </ul>
	</div>
	 -->
		<div class="control_box clearfix" style="height:80px;">
		<a href="javascript:void(0)" class="detail_btn_more">登陆app查看评论</a>
		</div>
	
	<div class="xz_btn" style="height:60px; line-height:60px;flex: auto;width: 100%;">
	<img src="${pageContext.request.contextPath}/pages/others/images/xz.png" alt="" width="50px" height="50px" style="position:absolute;top:6px;left:10px;"/>
	<span style="position:absolute;top:-10px;left:70px;color:black;">有点功夫</span>
	<span style="position:absolute;top:15px;left:70px;font-size:12px;">让你有点自己的功夫</span>
	<!--  
	<button class="dkbutton">打开app</button>
	-->
	<button class="xzbutton">打开app</button>
	</div>

</section>
</body>
<script src="${pageContext.request.contextPath}/pages/others/js/jquery.min.2.0.js"></script>
<script src="${pageContext.request.contextPath}/pages/others/js/swiper.min.js"></script>
<script type="text/javascript">
$(function(){

	var str="";
	if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
	    str="ios";  
	} else if (/(Android)/i.test(navigator.userAgent)) {
	    str="android"; 
	} else {
	   str="pc";
	};	
	
	$('.detail_btn_more,.xzbutton').click(
			function() {
				if(str=="ios"){
					if(is_weixn()){
			    		alert("请在浏览器中打开");
			    	}else{
			    		window.location.href = 'smartkungfu://www.smartkungfu.com?type=video&id='+${videoInfo.id};
					    t = Date.now();
					    setTimeout(function(){
					        if (Date.now() - t < 1200) {
					        	window.location.href = 'https://itunes.apple.com/cn/app/id1190945824';
					        }
					    }, 1000);
					    return false;
			    	 } 
					
				}else if(str=="android"){
			    	if(is_weixn()){
			    		alert("请在浏览器中打开");
			    	}else{
			    		window.location.href = 'smk://com.smk.fitness:8888/web?type=video&id='+${videoInfo.id};
					    t = Date.now();
					    setTimeout(function(){
					        if (Date.now() - t < 1200) {
					        	alert("android下载地址敬请期待");
					        }
					    }, 1000);
					    return false;
			    	 } 	
					
					}else{
					  window.location.href = "http://www.smartkungfu.com/";
					}
	});
	
});
function is_weixn(){

	 var ua = navigator.userAgent.toLowerCase();

	 if(ua.match(/MicroMessenger/i)=="micromessenger") {
			return true;
		} else {
			return false;
			}
	}
</script>
</html>
