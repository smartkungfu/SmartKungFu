<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
	<html lang="CN">
	<head>
		<title>有点功夫-资源下载库</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<jsp:include page="../fragment/frag_top_css.jsp"/>
	  
	  	<jsp:include page="../fragment/frag_top_js.jsp"/>
	  	<script type="text/javascript">
	  	
	    </script>
   </head>
<body>
	<div class="panel-body ">
		<h3 class="page-header">有点功夫APP安装包下载</h3>
		<table id="teacher_table" data-toggle="table" data-page-size="5">
			<thead>
				<tr>
					<th data-align="center" data-field="t1">名称</th>
					<th data-align="center" data-field="t2">链接</th>
					<th data-align="center" data-field="t3">版本</th>
					<th data-align="center" data-field="t4">大小</th>
					<th data-align="center" data-field="t6">更新日期</th>
					<th data-align="center" data-field="t7">兼容性</th>
				</tr>
			</thead>
			<tr>
				<td>有点功夫安装包IOS版</td>
				<td><a style="color:blue;text-decoration: underline;"
					href="${pageContext.request.contextPath}/download/KungFu.ipa">KungFu.ipa</a></td>
				<td>1.0.1</td>
				<td>27.4 MB</td>
				<td>2016-12-22</td>
				<td>支持ios8.0或更高版本。与iPhone、iPad兼容</td>
			</tr>
		</table>
	</div>
	<jsp:include page="../fragment/frag_bottom_js.jsp" />
</body>
</html>