<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><!doctype html>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="分享测试结果">
<meta name="description" content="分享测试结果">
<meta name="apple-mobile-web-app-capable"  content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no"/>
<title>功夫测试报告</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/style.css">
</head>
<body>
<section class="wrap_box"> 
	<div class="report_top">
    	<div class="user_info">
   	   		<p class="user_avatar">
   	   			<c:if test="${mstClient.portrait ne null and mstClient.portrait ne '' }">
   	   				<img src="${mstClient.portrait }" alt=""/>
   	   			</c:if>
   	   			<c:if test="${mstClient.portrait ne null and mstClient.portrait ne '' }">
   	   				<img src="${mstClient.portrait }" alt=""/>
   	   			</c:if>
   	   		</p>
            <p class="user_name">${mstClient.nickname }</p>
        </div>
    	<img src="${pageContext.request.contextPath}/pages/others/images/report_top_bg.png" alt=""/>
    </div>
    <div class="body_type">
    	<c:if test="${tt.cv.bodyStatus eq -1 }">
    		<img src="${pageContext.request.contextPath}/pages/others/images/body_type_02.png" alt="您的体型偏瘦"/>
    	</c:if>
    	
    	<c:if test="${tt.cv.bodyStatus eq 0 }">
    		<img src="${pageContext.request.contextPath}/pages/others/images/body_type_03.png" alt="您的体型正常"/>
    	</c:if>
    	
    	<c:if test="${tt.cv.bodyStatus eq 1 }">
    		<img src="${pageContext.request.contextPath}/pages/others/images/body_type_01.png" alt="您的体型偏胖"/>
    	</c:if>
    </div>
    
    <div class="report_box clearfix">
    	<!--你的身体机能表现优秀的是-->
    	<div class="report_con">
            <h3 class="report_til"><img src="${pageContext.request.contextPath}/pages/others/images/report_til.png" alt="你的身体机能表现优秀的是"/></h3>
            <div class="reoprt_body_img"><img src="${pageContext.request.contextPath}/pages/others/images/report_body_img.png" alt=""/></div>
            <!--所有标题icon样式 start-->
            <div class="reprot_item">
            	<c:forEach items="${bodylistGoods }" var="g">
            		 <h4 class="${g.css }"><span class="txt">${g.name }</span></h4>
            		 <p>${g.note }</p>
            	</c:forEach>
            </div>
        </div>
        
        <!--你的身体机能有待提高的是-->
        <div class="report_con">
            <h3 class="report_til"><img src="${pageContext.request.contextPath}/pages/others/images/report_til-02.png" alt="你的身体机能有待提高的是"/></h3>
            <div class="reoprt_body_img"><img src="${pageContext.request.contextPath}/pages/others/images/report_body_img-02.png" alt=""/></div>
             <div class="reprot_item">
            	<c:forEach items="${bodylistExpects }" var="g">
            		 <h4 class="${g.css }"><span class="txt">${g.name }</span></h4>
            		 <p>${g.note }</p>
            	</c:forEach>
            </div>
        </div>
        
        <!--你最适合的功夫项目是-->
        <div class="report_con">
            <h3 class="report_til"><img src="${pageContext.request.contextPath}/pages/others/images/report_til-03.png" alt="你最适合的功夫项目是"/></h3>
            <div class="reoprt_body_img"><img src="${pageContext.request.contextPath}/pages/others/images/report_body_img-03.png" alt=""/></div>
            <div class="reprot_item">
            	<c:forEach items="${kungfuOks }" var="g">
            		 <h4 class="${g.css }"><span class="txt">${g.name }</span></h4>
            		 <p>${g.note }</p>
            	</c:forEach>
            </div>
        </div>
        
        <!--训练建议-->
        <div class="report_con">
            <h3 class="report_til"><img src="${pageContext.request.contextPath}/pages/others/images/report_til-04.png" alt="训练建议"/></h3>
            <div class="reprot_item">
                <p class="pt20">您可以先练习综合类的功夫基本功，培养肌肉记忆，提高身体综合素质，然后便可在以上最适合的项目中选一个你最感兴趣的进行系统、深入地学习了。</p>
            </div>
        </div>
        <div class="report_date">
        	<p>有点功夫</p>
            <p>${tt.cv.testDate }</p>
        </div>
        
       	
    </div>
    <c:if test="${op eq 'h5' }">
		<a href="https://itunes.apple.com/cn/app/you-dian-gong-fu/id1190945824?l=en&mt=8"
			class="training_btn"><span class="txt">开始训练</span></a>
	</c:if>
	<c:if test="${op ne 'h5' }">
		<a href="start()" class="training_btn"><span class="txt">开始训练</span></a>
	</c:if>
</section>
</body>
</html>

