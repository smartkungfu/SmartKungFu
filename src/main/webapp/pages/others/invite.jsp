<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="邀请好友">
<meta name="description" content="邀请好友">
<meta name="apple-mobile-web-app-capable"  content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no"/>
<title>邀请好友</title>
<style type="text/css">
</style>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/style.css">
</head>
<body class="bg_gray2">

<section class="wrap_box"> 
  <div class="invite_card_bg">
  	<div class="invite_card">
  		<div class="kungfu_logo"><img src="${pageContext.request.contextPath}/pages/others/images/kongfu_logo.jpg" alt=""/></div>
        <h3>一个专注功夫的兴趣健身平台</h3>
        <p>
        	<span class="name">
	        	<c:if test="${mstClient.mobileno ne null and mstClient.mobileno ne ''}">${mstClient.mobileno }</c:if>
	        	<c:if test="${mstClient.mobileno eq null or mstClient.mobileno eq ''}">${mstClient.nickname }</c:if>
        	</span>送你
        </p>
        <div class="kungfu_value">
        	<p>50功力值</p>
        </div>
        <p>邀你加入有点功夫</p>
		<p class="em">让你有点自己的功夫</p>
        <p class="btn_box">
        	<a href="javascript:void(0)" class="btn J_join_btn">立刻加入</a>
        </p>
    </div>
  </div>
</section>

<div class="mask_bg" style="display:none"></div>
<div class="invite_pop" style="display:none">
	<a href="javascript:void(0)" class="pop_close"></a>
	<div class="invite_form">
    	<h4>加入有点功夫</h4>
    	<input type="hidden" name="service_captcha">
        <p class="form_item icon_phone"><input type="number" name="mobileno" placeholder="请输入手机号"><span id="mobileno_msg" style="color:#a94442;display: none">手机号为空</span></p>
        <p class="form_item icon_password"><input type="password" name="password" placeholder="请输入密码"><span id="password_msg" style="color:#a94442;display: none">密码为空</span></p>
        <p class="form_item icon_code"><input type="number" name="captcha" placeholder="请输入验证码"><a href="javascript:void(0)" class="get_code">获取验证码</a><span id="captcha_msg" style="color:#a94442;display: none">密码为空</span></p>
    </div>
    <div class="btn_box">
        <input type="submit" name="register" value="开启功夫之旅" class="btn" />
    </div>
</div>

<script src="${pageContext.request.contextPath}/pages/others/js/jquery.min.2.0.js"></script>
<script>
	$(function(){
		$('.J_join_btn').click(function(){
			$('.mask_bg').fadeIn();
			$('.invite_pop').fadeIn();
		});
		$('.pop_close').click(function(){
			$('.mask_bg').fadeOut();
			$('.invite_pop').fadeOut();
		});
		
		//获取短信验证码
		var validCode=true;
		$(".get_code").click (function(){
			$("#mobileno_msg").hide();
			var mobileno = $("input[name='mobileno']").val();
			var reg = "^(13|15|18|17)[0-9]{9}$";
			
			if(mobileno != ""){
				var patt = new RegExp(reg);
				if(patt.test(mobileno)){
					//清空服务端验证码记录
					$("input[name='service_captcha']").val("");
					//请求短信接口,
					var flag = getServiceCode();
					if(flag == "SUCCESS"){
						var time= 60;
						var code=$(this);
						code.html("60秒");
						if (validCode) {
							validCode=false;
						var t=setInterval(function  () {
							time--;
							code.html(time+"秒");
							if (time == 0) {
								clearInterval(t);
								code.html("重新获取");
								validCode=true;
								//验证码失效
								$("input[name='service_captcha']").val("");
							}
						},1000);
						}
					}else{
						$("#captcha_msg").text(flag);
						$("#captcha_msg").show();
					}
				
				}else{
					$("#mobileno_msg").text("手机号格式不对");
					$("#mobileno_msg").show();
				}
				
			}else{
				$("#mobileno_msg").text("手机号为空");
				$("#mobileno_msg").show();
			}
	});
		
	$("input[name='register']").click(function(){
		$("#mobileno_msg").hide();
		$("#password_msg").hide();
		$("#captcha_msg").hide();
		
		var mobileno = $("input[name='mobileno']").val();
		var password = $("input[name='password']").val();
		var captcha = $("input[name='captcha']").val();
		var service_captcha = $("input[name='service_captcha']").val();
		var reg = "^(13|15|18|17)[0-9]{9}$";
		var patt = new RegExp(reg);
	
		if(mobileno == null || mobileno == ""){
			$("#mobileno_msg").text("手机号为空");
			$("#mobileno_msg").show();
			
			return ;
		}
		
	    if(!patt.test(mobileno)){
	    	$("#mobileno_msg").text("手机号格式不对");
			$("#mobileno_msg").show();
			
			return ;
	    }
		
		if(password == null || password == ""){
			$("#password_msg").text("密码为空");
			$("#password_msg").show();
			
			return ;
		}
		
		if(password.length <6 ){
			$("#password_msg").text("密码长度至少为6位");
			$("#password_msg").show();
			
			return ;
		}
		
		if(captcha == null || captcha == ""){
			$("#captcha_msg").text("验证码为空");
			$("#captcha_msg").show();
			
			return ;
		}
		
		if(service_captcha != captcha){
			$("#captcha_msg").text("验证码输入错误");
			$("#captcha_msg").show();
			
			return ;
		}
		
		var url = "${pageContext.request.contextPath}/inviteController/inviteRegister?userid=${mstClient.id}&mobileno="+mobileno+"&password=" + password;
		$.ajax({
			url : url,
			type : "POST",
			async : true,
			dataType: "json",
			success : function(data) {
				if("111111" == data.code && "SUCCESS" == data.message){
					window.location.href="https://itunes.apple.com/cn/app/id1190945824";
				}else{
					$("#captcha_msg").text(data.message);
					$("#captcha_msg").show();
				}
			},
			error : function(data) {
				$("#captcha_msg").text("网络繁忙，请重试");
				$("#captcha_msg").show();
			}
		});	
	});
});
	
	//请求接口验证码
function getServiceCode(){
	var flag = "网络超时，请重试";
	var mobileno = $("input[name='mobileno']").val();
	var url = "${pageContext.request.contextPath}/inviteController/sendSms?userid=0&mobileno="+mobileno+"&receivetype=89&mirror=captcha";
	$.ajax({
		url : url,
		type : "POST",
		async : false,
		dataType: "json",
		success : function(data) {
			flag = data.message;
			if("111111" == data.code && "SUCCESS" == data.message){
				$("input[name='service_captcha']").val(data.captcha);
			}
		},
		error : function(data) {
			
		}
	});	
	return flag;
}
</script>
</body>
</html>