<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="功夫养成秘籍">
<meta name="description" content="功夫养成秘籍">
<meta name="apple-mobile-web-app-capable"  content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no"/>
<title>功夫养成秘籍</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/style.css">
</head>

<body class="bg_orange">

<section class="wrap_box"> 
  <div class="avatar_box animated"><img src="${pageContext.request.contextPath}/pages/others/images/avatar.png" class="animated"  alt=""/></div>
  <div class="my_help_box">
		<h3 class="bot_line"><i>1</i>.什么是功力值？</h3>
        <div class="help_infor">
        	<p>想从「菜鸟」成长为「武林高手」，你需要不断提高自己「功力值」。</p>
            <p>你的「功力值」来源于你在「有点功夫」里的每一次学习、体验、分享，它直接体现了你功夫水平提升的程度，赶快让自己有点功夫吧。</p>
            <p>「功力值」能准确地告诉你，你的功夫水平到底提升了多少。</p>
        </div>
        <a class="page_next" href="next()"><span>下一章：如何提升功力值</span></a>
  </div>
</section>
<script src="${pageContext.request.contextPath}/pages/others/js/jquery.min.2.0.js"></script>
<script>
	$(function(){
		var timer;
		
		$('.avatar_box').click(function(){
			clearTimeout(timer);
			$(this).addClass("bounceIn");
			$(this).find("img").addClass("bounce");
			timer = setTimeout(function(){
				$('.avatar_box').removeClass("bounceIn");
				$('.avatar_box img').removeClass("bounce");
			},1000);
		});
		
		$('.avatar_box').click();
	})
	
</script>
</body>
</html>
