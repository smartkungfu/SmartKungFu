﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="分享回答">
<meta name="description" content="分享回答">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no" />
<title>有点功夫</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/pages/others/css/swiper.min.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/pages/others/css/style.css">
	<style type="text/css">
	#popweixin {
    width:100%;
    height:100%;
    overflow:hidden;
    position:fixed;
    z-index:1000;
    background:rgba(0,0,0,.5);
    top:0;
    left:0;
    display:none;
}
#popweixin .tip {
    width:100%;
    background:#fff;
    z-index:1001;
}
.top2bottom {
    -webkit-animation:top2bottom 1.2s ease;
    -moz-animation:top2bottom 1.2s ease;
    -o-animation:top2bottom 1.2s ease;
    animation:top2bottom 1.2s ease;
    -webkit-animation-fill-mode:backwards;
    -moz-animation-fill-mode:backwards;
    -o-animation-fill-mode:backwards;
    animation-fill-mode:backwards
}
.animate-delay-1 {
    -webkit-animation-delay:1s;
    -moz-animation-delay:1s;
    -o-animation-delay:1s;
    animation-delay:1s
}
@-webkit-keyframes top2bottom {
    0% {
    -webkit-transform:translateY(-300px);
    opacity:.6
}
100% {
    -webkit-transform:translateY(0px);
    opacity:1
}
}@keyframes top2bottom {
    0% {
    transform:translateY(-300px);
    opacity:.6
}
100% {
    transform:translateY(0px);
    opacity:1
}
	</style>
</head>
<body>
	<div id='popweixin'>
    <div class='tip top2bottom animate-delay-1'>
        <img src='../pages/others/images/wechat_appstore_popup.jpg'/>
    </div>
	</div>
	<section class="wrap_box">
		<div class="add_course_detail">
			<h3 class="detail_til2">
				<span class="txt">${answer.answerTitle }</span>
			</h3>
		
			<div style="height:60px;">
			<c:if test="${answer.answerPortrait!=null }">
			<img src="${answer.answerPortrait }" alt="" width="50px" height="50px" style="border-radius:50%;"/>
			<span style="position:relative;top:-40px;left:10px;font-size:15px;color:black;">${answer.answerName }</span>
			<span style="position:relative;top:-15px;left:-20px;font-size:12px;">回答</span>
			</c:if>
			<c:if test="${answer.answerPortrait==null }">
			<img src="${pageContext.request.contextPath}/pages/others/images/xz.jpg" alt="" width="50px" height="50px" style="border-radius:50%;"/>
			</c:if>
			</div>
		
			<p>${answer.answerDetail }</p>
			<p style="margin-top:20px;font-size:18px;color:black;">${answer.answerDiscussNum }条评论</p>
		</div>
	<!--  
		<div class="control_box clearfix" style="height:50px;">
			<a href="javascript:void(0)" class="collect_2">${answer.answerDiscussNum }条评论</a>
			<a href="javascript:void(0)" class="attention">${onlineCourse.browsenum }</a>
			<a href="javascript:void(0)" class="share">${onlineCourse.sharenum }</a>
			<a href="javascript:void(0)" class="collect_2">${onlineCourse.disNum }</a>
			<a href="javascript:void(0)" class="detail_btn_more">详情</a>
		</div>
		-->
		<!--  
		<div class="control_box clearfix" style="margin-bottom:30px;">
		  <ul>
		    <c:forEach items="${mainList}" var="mstDiscuss">
                <li>
                <p><img src="${mstDiscuss.sendPortrait }" alt="" width="35px" height="35px" style="border-radius:50%;"/></p>
                <span style="position:relative;top:-30px;left:45px;font-size:15px;">${mstDiscuss.sendName}</span>
                <p style="position:relative;top:-10px;left:45px;font-size:15px;color:black;width:280px;">${mstDiscuss.content}</p>
                <c:if test="${mstDiscuss.mds[0]!=null}">
                 <div style="position:relative;top:-10px;left:45px;font-size:15px;background:#f1f1f1;width:280px;">
                 <p style="position:relative;top:0px;left:20px;line-height:25px;">回复<span style="color:#ffdf16">${mstDiscuss.mds[0].sendName}</span>:</p>
                 <p style="position:relative;top:2px;left:20px;line-height:25px;">${mstDiscuss.mds[0].content}</p>
                 </div>
                </c:if>
                </li>
             </c:forEach>
		  </ul>
		</div>
		-->
		<div class="control_box clearfix" style="height:80px;">
		<a href="javascript:void(0)" class="detail_btn_more">登陆app查看评论</a>
		</div>
	</section>
	<footer>
	<div class="xz_btn" style="height:60px; line-height:60px;flex: auto;width: 100%;">
	<img src="${pageContext.request.contextPath}/pages/others/images/xz.png" alt="" width="50px" height="50px" style="position:absolute;top:6px;left:10px;"/>
	<span style="position:absolute;top:-10px;left:70px;color:black;">有点功夫</span>
	<span style="position:absolute;top:15px;left:70px;font-size:12px;">让你有点自己的功夫</span>
	<!--  
	<button class="dkbutton">打开app</button>
	-->
	<button class="xzbutton">打开app</button>
	</div>
    </footer>
</body>
<script
	src="${pageContext.request.contextPath}/pages/others/js/jquery.min.2.0.js"></script>
<script
	src="${pageContext.request.contextPath}/pages/others/js/swiper.min.js"></script>
<script type="text/javascript">
	$(function() {
		
		var str="";
		if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
		    str="ios";  
		} else if (/(Android)/i.test(navigator.userAgent)) {
		    str="android"; 
		} else {
		   str="pc";
		};	
		
		$('.detail_btn_more,.xzbutton').click(
				function() {
					if(str=="ios"){
				    	if(is_weixn()){
				    		alert("请在浏览器中打开");
				    	}else{
							window.location.href = 'smartkungfu://www.smartkungfu.com?type=answer&id='+${answer.answerId};
						    t = Date.now();
						    setTimeout(function(){
						        if (Date.now() - t < 1200) {
						        	window.location.href = 'https://itunes.apple.com/cn/app/id1190945824';
						        }
						    }, 1000);
						    return false;
				    	}
					    
					}else if(str=="android"){
					    	if(is_weixn()){
					    		alert("请在浏览器中打开");
					    	}else{
					    		window.location.href = 'smk://com.smk.fitness:8888/web?type=answer&id='+${answer.answerId};
								t = Date.now();
								 setTimeout(function(){
								        if (Date.now() - t < 1200) {
								        	alert("android下载地址敬请期待");
								        }
								    }, 1000);
								    return false;
					    	}
						   
						}else{
						  window.location.href = "http://www.smartkungfu.com/";
						}
		});
		
		
	});
	
	function is_weixn(){

	 var ua = navigator.userAgent.toLowerCase();

	 if(ua.match(/MicroMessenger/i)=="micromessenger") {
			return true;
		} else {
			return false;
			}
	}
	
	function testDevice(){
	    var ua = navigator.userAgent.toLowerCase();
	    if (/iphone|ipod/.test(ua)) {
	        if(/micromessenger/.test(ua)){
	             document.getElementById("popweixin").style.display = "block";
	        }
	    }
	}

</script>
</html>
