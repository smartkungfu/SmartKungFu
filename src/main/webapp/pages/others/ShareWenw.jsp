<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><!doctype html>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="文章详情">
<meta name="description" content="文章详情">
<meta name="apple-mobile-web-app-capable"  content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no"/>
<title>有点功夫</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/style.css">
<style type="text/css">
.wrap_box{ max-width:768px; margin:0 auto}

.copyright{ text-align:center;color:#a4a4a4; padding:15px 15px 20px 15px;left:0;right:0; background:#fff; bottom:0px;margin-bottom: -100px}

.mask_bg{ position:fixed;top:0;right:0;bottom:0; left:0; background:rgba(0,0,0,.8); z-index:90; display:none}
.ball_box{ position:fixed; bottom:60px;left:0;right:0; z-index:96; text-align:center;color:#fff;font-size:14px;line-height:1.6; display:none}
.lightball_img{width:50.666%; margin-left:auto; margin-right:auto; margin-top:-20px}
.lightball_img img{width:100%}
.tip_pop{ text-align:center;width:200px;height:100px;font-size:14px;position:fixed;left:50%; margin-left:-100px;top:50%; margin-top:-50px; z-index:97; background:#fff; border-radius:4px; display:none}
.tip_pop p{color:#333;line-height:56px;}
.tip_pop a{ border-top:1px #e5e5e5 solid;line-height:44px; display:block;color:#007aff; text-decoration:none}
@media screen and (max-width:375px){
	.tip_pop{width:280px; margin-left:-140px}
}
.copyright{
    font-family: sans-serif,"Helvetica Neue",'微软雅黑','黑体',Gotham,Helvetica,Arial;
    font-size: 12px;
    color: #666;
}
ol, ul, li {
    list-style:inherit;
}
.found_detail_box p {
padding-bottom: 12px;
}
</style>
</head>

<body>
<section class="wrap_box"> 
  <div class="found_detail_box photo_details_box clearfix">
	${wenwInfo.content }
  	<div class="copyright">— 投稿或商务合作 —<br/>请发邮件至 article@smartkungfu.com
    </div><br/>
  </div>
  <div class="mask_bg"></div>
  <div class="tip_pop">
  	<p>喜欢我们吗，赶快下载吧^_^</p>
    <a href="javascript:void(0)" class="pop_close">确定</a>
  </div>
</section>
<script src="${pageContext.request.contextPath}/pages/others/js/jquery.min.2.0.js"></script>
<script src="${pageContext.request.contextPath}/pages/others/js/swiper.min.js"></script>
<script>
$(function(){
	// Initialize Swiper
	var activitySwiper = new Swiper('.swiper-container', {
		//pagination: '.swiper-pagination',
		slidesPerView: 2,
		paginationClickable: true,
		spaceBetween: 5,
		freeMode: true
	});
	
});

</script>
</body>
</html>
