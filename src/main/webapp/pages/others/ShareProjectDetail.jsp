<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><!doctype html>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="文章详情">
<meta name="description" content="文章详情">
<meta name="apple-mobile-web-app-capable"  content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no"/>
<title>分享活动详情</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/style.css">
</head>
<body>

<section class="wrap_box"> 
  <div class="found_detail_box photo_details_box clearfix">
  	
  	<c:forEach items="${irs }" var="g">
  		<p class="detail_img"><img src="${g.image }" alt=""/></p>
    	<p>${g.remark }</p>
  	</c:forEach>
    
 <!--    <p class="detail_img"><img src="images/detail_img-02.jpg" alt=""/></p>
    <p>2016 年 9 月 2 日，电影《铁拳》在中国大陆地区首映。虽说是首映，部分中国。</p>
    <p class="detail_img"><img src="images/detail_img-03.jpg" alt=""/></p>
    <p>2016 年 9 月 2 日，电影《铁拳》在中国大陆地区首映。虽说是首映，部分中国观众对于这部片子，已经不陌生了。在第十八届上海国际电影节上，该片是众多展映影片之一，
并入围了金爵奖最佳影片的提名。</p>
	<p class="detail_img"><img src="images/detail_img-04.jpg" alt=""/></p>
    <p>不得不说，如今的电影观众，大多都有着一双挑剔的眼睛，就像在古代文人骚客的眼里，「名门闺秀」纵使百般好，但也抵不过青楼里浮夸又调皮的「小野花」。无奈，电影《铁拳》偏偏是那个中规中矩的「名门闺秀」。在传统的叙事模式
下，一个拳手重振旗鼓的故事，犹如一碗不咸不淡</p> -->
  </div>
 <!--  <a href="javascript:void(0)" class="close_btn"></a> -->
</section>

</body>
</html>
