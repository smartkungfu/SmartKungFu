﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="功夫养成秘籍">
<meta name="description" content="功夫养成秘籍">
<meta name="apple-mobile-web-app-capable"  content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no"/>
<title>功夫养成秘籍</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/style.css">
</head>

<body class="bg_orange">

<section class="wrap_box"> 
  <div class="avatar_box animated"><img src="${pageContext.request.contextPath}/pages/others/images/avatar.png" class="animated"  alt=""/></div>
  <div class="my_help_box">
		<h3 class="bot_line"><i>2</i>.如何提升你的功力值</h3>
        <div class="help_infor_item">
        	<h4><span>下定决心</span></h4>
        	<p>坚定的决心，是进步的基石。立即完成注册，开启功夫之旅。</p>
            <p class="em">完成注册，功力值+10</p>
        </div>
        <div class="help_infor_item">
        	<h4><span>多练</span></h4>
        	<p>“身体力行，业精于勤”才是硬道理我们准备了使用的在线课程，在家也能高效训练，行动起来吧。</p>
            <p class="em">完成一次在线课程训练</p>
<p class="em">功力值+训练时长</p>
          <!--   <p class="aid"> X = 训练时间，如：学完20分钟的课程，功力值+20</p> -->
        </div>
        <div class="help_infor_item">
        	<h4><span>多看</span></h4>
        	<p>“触内旁通，行成与思”才能更扎实，我们准备了丰富的文章、视频资料，训练之余，别忘了涨知识。</p>
            <p class="em">读完1篇文章/看完1个视频</p>
            <p class="em">功力值+3</p>
        </div>
        <div class="help_infor_item">
        	<h4><span>参加活动</span></h4>
        	<p>有师友同行，才能走得更远。我们组织了大量有趣的活动，快出来感受功夫互动的乐趣吧。</p>
            <p class="em">90分钟的活动，功力值+90</p>
        </div>
      <!--   <div class="help_infor_item">
        	<h4><span>系统学习</span></h4>
        	<p>我们精选了一批优质的功夫训练场馆，想系统学习一门功夫，就立刻报名吧。</p>
            <p class="em">购买一次场馆课程，功力值+900</p>
        </div> -->
        <div class="help_infor_item">
        	<h4><span>分享</span></h4>
        	<p>“分享”会让我们获得更多。如果你在这里看到了好的内容，请分享给你的好朋友；如果你在这里有收获，请分享给你的朋友，并邀请TA们一起加入训练吧。</p>
            <p class="em">邀请注册，功力值+50</p>
            <p class="em">分享精彩内容，功力值+5</p>
        </div>
  </div>
</section>
<script src="${pageContext.request.contextPath}/pages/others/js/jquery.min.2.0.js"></script>
<script>
	$(function(){
		var timer;
		$('.avatar_box').click(function(){
			clearTimeout(timer);
			$(this).addClass("bounceIn");
			$(this).find("img").addClass("bounce");
			timer = setTimeout(function(){
				$('.avatar_box').removeClass("bounceIn");
				$('.avatar_box img').removeClass("bounce");
			},1000);
		})
		
		$('.avatar_box').click();
	})
</script>
</body>
</html>
