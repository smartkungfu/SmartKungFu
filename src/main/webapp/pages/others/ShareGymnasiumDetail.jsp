<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="文章详情">
<meta name="description" content="文章详情">
<meta name="apple-mobile-web-app-capable"  content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no"/>
<title>分享课程</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/style.css">
<style type="text/css">
.ball_box{ position:fixed; bottom:60px;left:0;right:0; z-index:96; text-align:center;color:#fff;font-size:14px;line-height:1.6; display:none}
.lightball_img{width:50.666%; margin-left:auto; margin-right:auto; margin-top:-20px}
.lightball_img img{width:100%}
.tip_pop{ text-align:center;width:200px;height:100px;font-size:14px;position:fixed;left:50%; margin-left:-100px;top:50%; margin-top:-50px; z-index:97; background:#fff; border-radius:4px; display:none}
.tip_pop p{color:#333;line-height:56px;}
.tip_pop a{ border-top:1px #e5e5e5 solid;line-height:44px; display:block;color:#007aff; text-decoration:none}
 @media screen and (max-width:375px){
.tip_pop{width:280px; margin-left:-140px}
}
</style>
</head>

<body class="bg_gray pb49">

<section class="wrap_box"> 
  <div class="ad_box"><img width="750px" height="450px" src="${gd.coursebill }" alt=""/></div>
  
  <div class="cont_box clearfix">
      <div class="detail_til bot_line">
      	<h3>${gd.coursename }<span class="price">${gd.pricerang }<i>/课时</i></span></h3>
        <p class="like"><span class="txt">${gd.favoritenum }人喜欢</span></p>
      </div>
      <div class="detail_con">
      	<p><span class="item">针对人群：</span><span class="item_txt">${gd.crowd }</span></p>
        <p><span class="item">课程效果：</span><span class="item_txt">${gd.courseresult }</span></p>
        <p><span class="item">教练：</span><span class="item_txt">${gd.coach }</span></p>
        <p><span class="item">教练简介：</span><span class="item_txt">${gd.coachcontent }</span></p>
      </div>
  </div>

  <!--温馨提示-->
  <div class="cont_box mt6 top_line clearfix">
  	<div class="detail_con bot_line">
      	<p><span class="item">人数：</span><span class="item_txt">${gd.limitnum }人</span></p>
        <p><span class="item">可选时间：</span><span class="item_txt">${gd.learningtime }</span></p>
        <p><span class="item">可选套餐：</span><span class="item_txt">${gd.cpStr }</span></p>
    </div>
    <div class="bot_line op_box details_op">
    	<a href="javascript:void(0)" class="online_btn"><span><i>在线咨询</i></span></a>
        <a href="tel:15842932586" class="tel"><span><i>电话咨询</i></span></a>
    </div>
  </div>
  
  
  <!--更多有趣的活动 -->
  <div class="activity_con top_line clearfix">
  	<h4 class="cont_til2">更多有趣的课程</h4>
    <!-- Swiper -->
    <div class="activity_list">
        <div class="swiper-container">
            <ul class="swiper-wrapper">
               <c:forEach items="${gd.moreGds }" var="g" begin="0" end="4" >
               		<li class="swiper-slide">
	                	<a href="javascript:void(0)"><img width="200px" height="100px" src="${g.coursebill }"  alt=""/>
	                    <p>${g.coursename }</p>
	                    </a>
              		</li>
               </c:forEach>
            </ul>
        </div>
    </div>
  </div>
</section>
<!--在线咨询弹出框 start-->
<div class="pop_box">
    <div class="mask_bg"></div>
    <div class="pop_con">
        <p>喜欢我们吗？赶快下载吧！</p>
        <a href="javascript:void(0)" class="comfirm_btn">确定</a>
    </div>
</div>
<footer>
	<a href="javascript:void(0)" class="sign_up_btn">立即报名</a>
</footer>
<script src="${pageContext.request.contextPath}/pages/others/js/jquery.min.2.0.js"></script>
<script src="${pageContext.request.contextPath}/pages/others/js/swiper.min.js"></script>
<script>
$(function(){
	// Initialize Swiper
	var activitySwiper = new Swiper('.swiper-container', {
		//pagination: '.swiper-pagination',
		slidesPerView: 2,
		paginationClickable: true,
		spaceBetween: 5,
		freeMode: true
	});
	
	$('a').click(function(){
		
		if($(this).attr('class') == "comfirm_btn" ){
			window.location.href="https://itunes.apple.com/cn/app/id1190945824";
		}
		
		if($(this).attr('class') != "tel" && $(this).attr('class') != "message"){
			$('.pop_box').fadeIn();
		}
	});
	
	$('.mask_bg,.comfirm_btn').click(function(){
		$('.pop_box').fadeOut();	
	});
	
});

</script>
</body>
</html>
