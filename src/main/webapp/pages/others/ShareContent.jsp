<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><!doctype html>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="文章详情">
<meta name="description" content="文章详情">
<meta name="apple-mobile-web-app-capable"  content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no"/>
<title>分享视频</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/style.css">
<style type="text/css">
html,body,ol,ul,dl,dt,dd,h1,h2,h3,h4,h5,h6,p,form,table,tr,td,header,section,footer,nav{padding:0;margin:0}
body{ padding-bottom:55px}
.wrap_box{ max-width:768px; margin:0 auto}
.cont_box{ padding:20px 25px 10px 25px}
.cont_box p{ line-height:1.7;padding-bottom:10px;font-size:13px}
.cont_box h2{font-size:16px; font-weight:normal; text-align:center;padding-bottom:10px;color:#202020}

/* .copyright{ text-align:center;color:#a4a4a4; padding:15px 15px 20px 15px; position:fixed;left:0;right:0; background:#fff; bottom:0} */
.copyright{ text-align:center;color:#a4a4a4; padding:15px 15px 20px 15px;left:0;right:0; background:#fff; bottom:0px;margin-bottom: -100px}

.control{ padding:0 25px 20px 25px; text-align:right}
.control a{color:#434343;font-size:13px; line-height:38px; text-decoration:none; display:inline-block;padding-right:10px;}
.control a:last-child{padding-right:0}
.control .a:before{ width:16px;height:16px; content:''; float:left; margin-top:12px; margin-right:2px; background: url(${pageContext.request.contextPath}/pages/others/images/icon_skill.png) no-repeat; background-size:16px 80px; display:inline-block}
.control a.skill:before{ background-position:0 0}
.control a.skill_selected:before{ background-position:0 -16px}
.control a.comment:before{ background-position:0 -32px}
.control a.collect:before{ background-position:0 -48px}
.control a.collected:before{background-position:0 -64px}
.look{background: url(${pageContext.request.contextPath}/pages/others/images/look02.png) no-repeat}
.mask_bg{ position:fixed;top:0;right:0;bottom:0; left:0; background:rgba(0,0,0,.8); z-index:90; display:none}
.ball_box{ position:fixed; bottom:60px;left:0;right:0; z-index:96; text-align:center;color:#fff;font-size:14px;line-height:1.6; display:none}
.lightball_img{width:50.666%; margin-left:auto; margin-right:auto; margin-top:-20px}
.lightball_img img{width:100%}
.tip_pop{ text-align:center;width:200px;height:100px;font-size:14px;position:fixed;left:50%; margin-left:-100px;top:50%; margin-top:-50px; z-index:97; background:#fff; border-radius:4px; display:none}
.tip_pop p{color:#333;line-height:56px;}
.tip_pop a{ border-top:1px #e5e5e5 solid;line-height:44px; display:block;color:#007aff; text-decoration:none}
 
@media screen and (max-width:375px){
	.tip_pop{width:280px; margin-left:-140px}
}
.copyright{
    font-family: sans-serif,"Helvetica Neue",'微软雅黑','黑体',Gotham,Helvetica,Arial;
    font-size: 12px;
    color: #666;
}

</style>
<link href="${pageContext.request.contextPath}/css/ShareContent/page.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/ShareContent/improve.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/ShareContent/not_in_mm.css" rel="stylesheet" type="text/css" />
</head>
<body style="overflow:-Scroll;overflow-y:hidden">
<section class="wrap_box" > 
  <video poster="http://skf.smartkungfu.com/${videoInfo.reserved9 }" width="100%" height="100%" src="http://skf.smartkungfu.com/${videoInfo.url }" controls="controls"></video>
  <div class="found_detail_box photo_details_box clearfix">
  	<h3 class="details_til" style="margin-top: -30px">${videoInfo.title }</h3>
    <p class="author" style="margin-top: -20px"><c:if test="${videoInfo.author ne null and videoInfo.author ne ''}">作者：videoInfo.author</c:if> <fmt:formatDate value="${videoInfo.createdate }" pattern="MM.dd"/></p>
	 ${videoInfo.content }
	<div class="control" style="text-align: left;">
		 <ul>
		 	<li>
				  <a href="javascript:void(0)" style="width:16px;height:16px; content:''; float:left; margin-top:12px; margin-right:2px; background: url(${pageContext.request.contextPath}/pages/others/images/look02.png) no-repeat;display:inline-block"></a>
			  	  <a href="javascript:void(0)" class="comment a"></a>
			  	  <a href="javascript:void(0)" class="collect a"></a>
		  		  <a href="javascript:void(0)" style="width:16px;height:16px; content:''; margin-top:12px; margin-right:2px; background: url(${pageContext.request.contextPath}/pages/others/images/discuss02.png) no-repeat;display:inline-block"></a></li>
			<li style="margin-top: -5px;"><a>&nbsp;${videoInfo.browsenum }</a> &nbsp;&nbsp;<a>${videoInfo.sharenum }</a>&nbsp;&nbsp;&nbsp;<a>${videoInfo.favoritenum }</a>&nbsp;&nbsp;&nbsp;<a>${videoInfo.disNum }</a></li>
		 </ul>
  	</div>
  </div>
  <div class="mask_bg"></div>
  <div class="tip_pop">
  	<p>喜欢我们吗，赶快下载吧^_^</p>
    <a href="javascript:void(0)" class="pop_close">确定</a>
  </div>
</section>
</body>
<script src="${pageContext.request.contextPath}/pages/others/js/jquery.min.2.0.js"></script>
<script src="${pageContext.request.contextPath}/pages/others/js/swiper.min.js"></script>
<script>
$(function(){
	// Initialize Swiper
	var activitySwiper = new Swiper('.swiper-container', {
		//pagination: '.swiper-pagination',
		slidesPerView: 2,
		paginationClickable: true,
		spaceBetween: 5,
		freeMode: true
	});
	
});

</script>
</body>
</html>
