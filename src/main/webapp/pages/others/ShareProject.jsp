<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta name="keywords" content="文章详情">
<meta name="description" content="文章详情">
<meta name="apple-mobile-web-app-capable"  content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no"/>
<title>分享活动</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/pages/others/css/style.css">
<style type="text/css">
.ball_box{ position:fixed; bottom:60px;left:0;right:0; z-index:96; text-align:center;color:#fff;font-size:14px;line-height:1.6; display:none}
.lightball_img{width:50.666%; margin-left:auto; margin-right:auto; margin-top:-20px}
.lightball_img img{width:100%}
.tip_pop{ text-align:center;width:200px;height:100px;font-size:14px;position:fixed;left:50%; margin-left:-100px;top:50%; margin-top:-50px; z-index:97; background:#fff; border-radius:4px; display:none}
.tip_pop p{color:#333;line-height:56px;}
.tip_pop a{ border-top:1px #e5e5e5 solid;line-height:44px; display:block;color:#007aff; text-decoration:none}
 @media screen and (max-width:375px){
.tip_pop{width:280px; margin-left:-140px}
}
</style>
</head>

<body class="bg_gray pb49">

<section class="wrap_box"> 
  	<!-- Swiper -->
    <div class="swiper-container">
        <ul class="swiper-wrapper">
        	<c:forEach items="${projectInfo.probills }" var="g" >
        		<li class="swiper-slide bill"><a href="javascript:void(0)"><img width="768px" height="450px" src="http://skf.smartkungfu.com/${g }"  alt=""/></a></li>
        	</c:forEach>
        </ul>
        <!-- Add Pagination -->
      <div class="swiper-pagination"></div>
   </div>
  <%-- <div class="ad_box"><img src="${pageContext.request.contextPath}/pages/others/images/activity_ad.jpg" alt=""/></div> --%>
  <h3 class="activity_til bot_line">${projectInfo.title }<span class="price">¥<fmt:formatNumber type="number" value="${projectInfo.profee }" maxFractionDigits="0"/>/人</span></h3>
  <div class="activity_info">
  	<p class="address bot_line">${projectInfo.address }</p>
    <p class="date bot_line"><fmt:formatDate value="${projectInfo.startdate }" pattern="yyyy.MM.dd"/> － <fmt:formatDate value="${projectInfo.enddate }" pattern="yyyy.MM.dd"/></p>
    <p class="time bot_line">${projectInfo.perdate }</p>
  </div>
  <!--活动亮点-->
  <div class="activity_con top_line clearfix">
  	<h4 class="cont_til bot_line">
    	<span>活动亮点</span>
    </h4>
    <div class="infor">
    	<!-- <p>1. 咏春传人亲授咏春</p>
        <p>2. 功夫女王叫你如何用功夫玩转撕名牌</p>
        <p>3. 领略真实的武林秘籍、宝刀神剑</p>
        <p>4. 亲身体验咏春大片</p> -->
        <c:forEach items="${projectInfo.lightspots }" var="g">
        	<p><span style="font-size:10px;">•</span> ${g }</p>
        </c:forEach>
    <div class="bot_line"><a href="javascript:void(0)" class="detail_btn top_line"><span>查看详情</span></a></div>
  </div>
  <!--温馨提示-->
  <div class="activity_con top_line clearfix">
  	<h4 class="cont_til bot_line">
    	<span class="tips">温馨提示</span>
    </h4>
    <div class="infor bot_line">
      	<c:forEach items="${projectInfo.warnings }" var="g">
        	<p><span style="font-size:10px;">•</span> ${g }</p>
        </c:forEach>
    </div>
    <div class="bot_line op_box">
    	<a href="javascript:void(0)" class="online_btn"><span><i>在线咨询</i></span></a>
        <a href="tel:15842932586" class="tel"><span><i>电话咨询</i></span></a>
    </div>
  </div>
  
  <!--更多有趣的活动 -->
  <div class="activity_con top_line clearfix">
  	<h4 class="cont_til2">更多活动</h4>
    <!-- Swiper -->
    <div class="activity_list">
        <div class="swiper-container">
            <ul class="swiper-wrapper">
                <c:forEach items="${projectInfo.morePro }"  begin="0" end="4" var="g">
                	 <li class="swiper-slide">
	                	<a href="javascript:void(0)"><img  width="200px" height="100px"  src="http://skf.smartkungfu.com/${g.probill }"  alt=""/>
	                   	<div>
	                   	</div>
	                    <p style="text-align: left;line-height: 20px"><span>${g.title } </span><br/><span>¥<fmt:formatNumber type="number" value="${g.profee }" maxFractionDigits="0"/>元 </span></p>
	                    </a>
               		 </li>
                </c:forEach>
            </ul>
        </div>
    </div>
  </div>
</section>
<!--在线咨询弹出框 start-->
<div class="pop_box">
    <div class="mask_bg"></div>
    <div class="pop_con">
        <p>喜欢我们吗？赶快下载吧！</p>
        <a href="javascript:void(0)" class="comfirm_btn">确定</a>
    </div>
</div>
<footer>
	<a href="javascript:void(0)" class="sign_up_btn">立即报名</a>
</footer>
<script src="${pageContext.request.contextPath}/pages/others/js/jquery.min.2.0.js"></script>
<script src="${pageContext.request.contextPath}/pages/others/js/swiper.min.js"></script>
<script>
$(function(){
	
	var str="";
	if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
	    str="ios";  
	} else if (/(Android)/i.test(navigator.userAgent)) {
	    str="android"; 
	} else {
	   str="pc";
	};	
	
	var activitySwiper = $($('.swiper-container')[1]).swiper({
		//pagination: '.swiper-pagination',
		slidesPerView: 2,
		paginationClickable: true,
		spaceBetween: 5,
		freeMode: true
	});
	var venueKvSwiper = $($('.swiper-container')[0]).swiper({
		 	pagination: '.swiper-pagination ',
	        paginationClickable: true,
	        centeredSlides: true,
	        autoplay: 2500,
			 loop: true,
	        autoplayDisableOnInteraction: false
	});
	
	$('a').click(function(){
		if($(this).attr('class') == "comfirm_btn" ){
			if(str=="ios"){
				  window.location.href = "https://itunes.apple.com/cn/app/id1190945824";	
				}else if(str=="android"){
				  alert("android");	
				}else{
				  window.location.href = "http://www.smartkungfu.com/";
				}
		}
		
		if($(this).attr('class') != "tel" && $(this).attr('class') != "message"){
			$('.pop_box').fadeIn();
		}
		
	});
	
	$('.mask_bg,.comfirm_btn').click(function(){
		$('.pop_box').fadeOut();	
	});
	
	//图片变形处理
	var imgs = $("img");
	for(var i = 0 ; i < imgs.length ; i ++){
		$(imgs[i]).attr("src" , $(imgs[i]).attr("src") + "?" + Math.random()) ;
	}

});

</script>
</body>
</html>
