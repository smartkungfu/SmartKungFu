<!--bootstrap核心js及各插件js导入  -->

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/js/html5shiv.js"></script>
<![endif]-->
<!-- Load JS here for Faster site load =============================-->
<%-- <script src="${pageContext.request.contextPath}/js/less-1.5.0.min.js"></script> --%>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-select.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-switch.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.tagsinput.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.placeholder.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-typeahead.js"></script>
<script src="${pageContext.request.contextPath}/js/application.js"></script>
<script src="${pageContext.request.contextPath}/js/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.sortable.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.gritter.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery.nicescroll.min.js"></script>
<script src="${pageContext.request.contextPath}/js/prettify.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.noty.js"></script>
<script src="${pageContext.request.contextPath}/js/bic_calendar.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.accordion.js"></script>
<script src="${pageContext.request.contextPath}/js/skylo.js"></script>

<%-- <script src="${pageContext.request.contextPath}/js/theme-options.js"></script> --%>

<script src="${pageContext.request.contextPath}/js/bootstrap-progressbar.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-progressbar-custom.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-colorpicker.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-colorpicker-custom.js"></script>

<script src="${pageContext.request.contextPath}/js/raphael-min.js"></script>
<%--  <script src="${pageContext.request.contextPath}/js/morris-0.4.3.min.js"></script> --%>
<%-- <script src="${pageContext.request.contextPath}/js/morris-custom.js"></script> --%>

<script src="${pageContext.request.contextPath}/js/charts/jquery.sparkline.min.js"></script>

<!-- NVD3 graphs  =============================-->
<script src="${pageContext.request.contextPath}/js/nvd3/lib/d3.v3.js"></script>
<script src="${pageContext.request.contextPath}/js/nvd3/nv.d3.js"></script>
<script src="${pageContext.request.contextPath}/js/nvd3/src/models/legend.js"></script>
<script src="${pageContext.request.contextPath}/js/nvd3/src/models/pie.js"></script>
<script src="${pageContext.request.contextPath}/js/nvd3/src/models/pieChart.js"></script>
<script src="${pageContext.request.contextPath}/js/nvd3/src/utils.js"></script>
<script src="${pageContext.request.contextPath}/js/nvd3/sample.nvd3.js"></script>

<!-- Core Jquery File  =============================-->
<script src="${pageContext.request.contextPath}/js/core.js"></script>
<script src="${pageContext.request.contextPath}/js/dashboard-custom.js"></script>

<!--bootbox.js  -->
<script src="${pageContext.request.contextPath}/js/bootbox.min.js"></script>
<!-- <script src="http://bootboxjs.com/bootbox.js"></script>  --> 

<!--bootstrap-table.js  -->
<script src="${pageContext.request.contextPath}/js/bootstrap-table.js"></script>
<!-- Latest compiled and minified JavaScript -->
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script> -->

<!-- Latest compiled and minified Locales -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/locale/bootstrap-table-zh-CN.min.js"></script>

<!-- Latest multiselect Locales -->
<script src="${pageContext.request.contextPath}/js/bootstrap-multiselect.js"></script>

<!-- Latest bootstrapValidator Locales -->
<script src="${pageContext.request.contextPath}/js/bootstrapValidator.min.js"></script>
