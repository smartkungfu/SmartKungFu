<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- .navbar -->
<nav class="navbar" role="navigation">

	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<a class="navbar-brand" href="#"><i
			class="fa fa-list btn-nav-toggle-responsive text-white"></i> <span
			class="logo">有点<strong>功夫</strong><i class="fa fa-bookmark"></i></span></a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav user-menu navbar-right ">

			<li><a href="#" class="user dropdown-toggle"
				data-toggle="dropdown"><span class="username"> 
					<c:if test="${MY_SESSION.mstUser.portrait eq null or MY_SESSION.mstUser.portrait eq ''}">
						<img src="${pageContext.request.contextPath}/images/portrait1.jpg" alt="" class="user-avatar" alt="">Hi,${MY_SESSION.mstUser.namecn }
					</c:if>
					<c:if test="${MY_SESSION.mstUser.portrait ne null and MY_SESSION.mstUser.portrait ne ''}">
					 <img src="${MY_SESSION.mstUser.portrait}" alt="" class="user-avatar" alt="">Hi,${MY_SESSION.mstUser.namecn }
					</c:if>
				</span></a>
				<ul class="dropdown-menu">
					<li><a href="${pageContext.request.contextPath}/mstUserController/info?id=${MY_SESSION.mstUser.id }&menuId=5&index_exp=1&index_che=0"><i class="fa fa-user"></i>我的资料</a></li>
				<!-- 	<li><a href="#"><i class="fa fa-envelope"></i>收件箱</a></li>
					<li><a href="#"><i class="fa fa-cogs"></i>设置</a></li> -->
					<li class="divider"></li>
					<li><a href="${pageContext.request.contextPath}/mstUserController/logout" class="text-danger"><i class="fa fa-lock"></i>退出</a></li>
				</ul>
			<!-- <li><a href="#" class="settings dropdown-toggle"
				data-toggle="dropdown"><i class="fa fa-envelope"></i><span
					class="badge bg-pink">4</span></a> -->
				<%-- <ul class="dropdown-menu inbox">
					<li><a href="inbox.php"> 
							<c:if test="${MY_SESSION.mstUser.portrait eq null or MY_SESSION.mstUser.portrait eq ''}">
								<img src="${pageContext.request.contextPath}/images/portrait1.jpg" alt="" class="avatar">
							</c:if>
							<c:if test="${MY_SESSION.mstUser.portrait ne null and MY_SESSION.mstUser.portrait ne ''}">
								<img src="${MY_SESSION.mstUser.portrait}" alt="" class="avatar">
							</c:if>
							<div class="message">
								<span class="username">盖伦</span> <span
									class="mini-details">(6) <i class="fa fa-paper-clip"></i></span>
								<span class="time pull-right"> <i class="fa fa-clock-o"></i>
									06:58 PM
								</span>
								<p>正义与我同在!</p>
							</div>
					</a></li>
					<li><a href="#"> <img
							src="${pageContext.request.contextPath}/images/portrait2.jpg" alt="" class="avatar">
							<div class="message">
								<span class="username">赵信</span> <span
									class="mini-details">(6) <i class="fa fa-paper-clip"></i></span>
								<span class="time pull-right"> <i class="fa fa-clock-o"></i>
									02:58 PM
								</span>
								<p>一点寒芒先到,随后枪出如龙...</p>
							</div>
					</a></li>
					<li><a href="#"> <img
							src="${pageContext.request.contextPath}/images/portrait3.jpg" alt="" class="avatar">
							<div class="message">
								<span class="username">嘉文</span> <span
									class="mini-details">(3) <i class="fa fa-paper-clip"></i></span>
								<span class="time pull-right"> <i class="fa fa-clock-o"></i>
									08:58 PM
								</span>
								<p>我不需要躲在草丛里！呃，我不是在贬低盖伦……</p>
							</div>
					</a></li>
					<li><a href="${pageContext.request.contextPath}/mstMessageController/Main?menuId=20&index_exp=4&index_che=0" class="btn bg-primary">显示所有</a></li>
				</ul> --%>
			<!-- <li><a href="#" class="settings dropdown-toggle"
				data-toggle="dropdown"><i class="fa fa-bell animated shake"></i><span
					class="badge bg-success">10</span></a>
				<ul class="dropdown-menu notifications">
					<li><a href="#"> <i
							class="fa fa-user noty-icon bg-primary"></i> <span
							class="description">10个用户完成注册</span> <span
							class="time"> <i class="fa fa-clock-o"></i> 06:58 PM
						</span>
					</a></li>
					<li><a href="#" class="text-danger"> <i
							class="fa fa-inbox noty-icon bg-pink"></i> <span
							class="description">收到一份来自远方的漂流瓶</span> <span
							class="time"> <i class="fa fa-clock-o"></i> 06:58 PM
						</span>
					</a></li>
					<li><a href="#" class="text-info"> <i
							class="fa fa-comment noty-icon bg-purple"></i> <span
							class="description">58条新内容</span> <span class="time">
								<i class="fa fa-clock-o"></i> 06:58 PM
						</span>
					</a></li>

					<li><a href="javascript:void(0)" class="btn bg-primary">显示所有</a></li>
				</ul></li> -->
			<!-- <li><a href="#" class="settings"><i
					class="fa fa-cogs settings-toggle"></i><span class="badge bg-info">20</span></a></li> -->
		</ul>
	</div>
	<!-- /.navbar-collapse -->
</nav>
<!-- /.navbar -->