<!-- Loading Bootstrap -->
<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">

<!-- Loading font-awesome Stylesheets -->
<link href="${pageContext.request.contextPath}/css/font-awesome.min.css" rel="stylesheet">
<!-- Loading style Stylesheets -->
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css">

<!-- Loading Custom Stylesheets -->
<link href="${pageContext.request.contextPath}/css/custom.css" rel="stylesheet">

<!-- Loading Table Stylesheets -->
<link href="${pageContext.request.contextPath}/css/bootstrap-table.css" rel="stylesheet">
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css"> -->

<!-- Loading multiselect Stylesheets -->
<link href="${pageContext.request.contextPath}/css/bootstrap-multiselect.css" rel="stylesheet">

<!-- Loading validation Stylesheets -->
<link href="${pageContext.request.contextPath}/css/bootstrapValidator.min.css" rel="stylesheet">