<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<script type="text/javascript">
$(function(){
	 $.ajax({
			url : "${pageContext.request.contextPath}/mstMenuController/initMenus",
			data : {},
			type : "POST",
			async : false,
			dataType : "json",
		    beforeSend: function() {
		        //请求前的处理
		    },
			success : function(data) {
				//alert(JSON.stringify(data));
				var len = data.length;
				
				var cxts = "";
				
				var exp = "${MENU_STYLE_INDEX_EXP}";
				var che = "${MENU_STYLE_INDEX_CHE}";
				
				//向左隐藏
				cxts += "<li class='nav-toggle'><button class='btn  btn-nav-toggle text-primary'><i class='fa fa-angle-double-left toggle-left'></i></button></li>";
				
				for(var i = 0 ; i < len;i ++){
					
					var _mstMenus = data[i]._mstMenus;
					
					if(_mstMenus.length == 0){
						cxts += "<li class='active'><a href='"+data[i].url+"&index_exp="+i+"&index_che="+j+"' data-original-title='Dashboard'><i class='"+data[i].icon+"'></i><span class='hidden-minibar'> "+data[i].name+" </span></a></li>";
					}else{
						if( i == exp){
							cxts += "<li class='active'><a class='dropdown' href='javascript:' data-original-title='"+data[i].name+"'>";
							cxts += "<i class='"+data[i].icon+"'></i>";
							cxts += "<span class='hidden-minibar'> "+data[i].name+" </a>";
						} else {
							cxts += "<li class='submenu'><a class='dropdown' href='javascript:' data-original-title='"+data[i].name+"'>";
							cxts += "<i class='"+data[i].icon+"'></i>";
							cxts += "<span class='hidden-minibar'> "+data[i].name+" </a>";
						}
						
							
						cxts += "<ul>";
						for(var j = 0 ; j < _mstMenus.length ; j ++){
							if(i == exp && j == che){
								cxts += "<li class='active'><a href='"+_mstMenus[j].url+"&index_exp="+i+"&index_che="+j+"' data-original-title='"+_mstMenus[j].name+"'><i class='"+_mstMenus[j].icon+"'></i><span> "+_mstMenus[j].name+" </span></a></li>";
							} else{
								cxts += "<li><a href='"+_mstMenus[j].url+"&index_exp="+i+"&index_che="+j+"' data-original-title='"+_mstMenus[j].name+"'><i class='"+_mstMenus[j].icon+"'></i><span> "+_mstMenus[j].name+" </span></a></li>";
							}
							
						}
						cxts += "</ul>";
						
						cxts += "</li>";
					}
				}
				
				//console.log('cxts:',cxts); 
				$(".left-menu").html(cxts);
				
			},
		    complete: function() {

		    },
			error : function(data) {
				bootbox.alert({ 
							  size: "small",
							  title: "错误",
							  message: "请求超时，请重试！", 
							  callback: function(){ /* your callback code */ }
						});
				return;
			}
	  });	
});
</script>
<!-- .left-sidebar -->
<div class="left-sidebar">
	 <div class="sidebar-holder">
	 	 <ul class="nav nav-list left-menu">
		</ul>  
	</div> 
</div>
<!-- /.left-sidebar -->
