	function ajaxLoading(msg){
  		$('input[type=submit], input[type=button]', this).each(function() {
	        $(this).attr("disabled", "disabled");
	      });
	     
	      $.messager.progress({ 
	    		title:'请稍后', 
	    		msg: msg === undefined ? '正在上传文件...' : msg
	      }); 
	}
	
	function ajaxLoaded(){
		$('input[type=submit], input[type=button]', this).each(function() {
	        $(this).attr("disabled", "false");
	      });
		$.messager.progress('close');
	}
	
	function ResDelete(contextPath,resType,cfgKey,refId,appendShow){
		ajaxLoading("正在删除文件...");
		$.ajax({
			url:contextPath+'/menu/EmSysResource/resDelete?resType='+resType+'&refId='+refId+'&cfgKey='+cfgKey, 
			type : "post",
			cache : false,
			data : "",
			success : function(data) {
				var result = data.substring(0,1);
			    if(result=="0"){
					readjustDisplay(false,resType,cfgKey,'','',appendShow);
			    	//$.messager.alert('操作提示', '删除成功','info');
			    	ajaxLoaded();
			    }
			    else{
			    	$.messager.alert('操作提示', '删除操作失败!','error');
			    	ajaxLoaded();
			    }
			}
		});
	}
	
	function ResUpload(contextPath,resType,cfgKey,refId,isResource,w,h){
		
		 ajaxLoading();
		 
		 $.ajaxFileUpload({ 
		        //处理文件上传操作的服务器端地址(可以传参数,已亲测可用) 
		        url:contextPath+'/menu/EmSysResource/resUpload?resType='+resType+'&refId='+refId+'&cfgKey='+cfgKey, 
		        secureuri:false,                       //是否启用安全提交,默认为false   
		        fileElementId:cfgKey,           //文件选择框的id属性  
		        dataType:'json',                       //服务器返回的格式,可以是json或xml等 
		        success:function(data, status){        //服务器响应成功时的处理函数 
		        	
		        	//alert(status);
		        	//alert(data);
		        	//alert(data.success);
		            //alert(data.message);
		        	//data = data.replace("<PRE>", '');  //ajaxFileUpload会对服务器响应回来的text内容加上<pre>text</pre>前后缀 
		            //data = data.replace("</PRE>", ''); 
		            //data = data.replace("<pre>", '');  
		            //data = data.replace("</pre>", ''); //本例中设定上传文件完毕后,服务端会返回给前台[0`filepath]  
		            //alert(data);
		            var flag = "";
		            if(data.success == true){     //0表示上传成功(后跟上传后的文件路径),1表示失败(后跟失败描述) 
		            	flag = "0";
		            	readjustDisplay(true,resType,cfgKey,isResource,data.message,true,w,h);
		                //$.messager.alert('操作提示', '上传成功','info');
		                ajaxLoaded();
		            }else{ 
		                $.messager.alert('操作提示', '上传失败，请重试！！','error');
		                ajaxLoaded();
		            } 
		            if(typeof(customFileUploadAfter)=="function") {
		            	customFileUploadAfter(flag);
					}
		        }, 
		        error:function(data, status, e){ //服务器响应失败时的处理函数 
		            $.messager.alert('操作提示', '上传失败，请重试！！','error');
		            ajaxLoaded();
		        } 
		    }); 
		 
		
	}
	
	function readjustDisplay(show,resType,cfgKey,isResource,showVal,appendShow,w,h){
		var divId = "#show_"+cfgKey;
		$(divId).empty();
		if(show){
			if(isResource == '1'){// img
				w = w === undefined ? 80:w;
				h = h === undefined ? 80:h;
				$(divId).append("<img id=\"img_"+cfgKey+"\" width=\""+w+"\" height=\""+h+"\" src=\""+showVal+"\">&nbsp;"); 
				$(divId).append("<input type=\"button\" value=\"删除\" onclick=\"deleteFile('"+resType+"','"+cfgKey+"')\"/>"); 
			}
			else if(isResource == '2'){ //href
				$(divId).append("<input type=\"button\" value=\"下载\" onclick=\"window.open('"+showVal+"')\"/>&nbsp;"); 
				$(divId).append("<input type=\"button\" value=\"删除\" onclick=\"deleteFile('"+resType+"','"+cfgKey+"')\"/>"); 
			}
		}
		else{
			appendShow = appendShow === undefined ? true : appendShow ;
			if(appendShow)
			$(divId).append("<span style=\"background-position: right center; border-color: #ffa8a8; background-color: #fff3f3; color: #000; font-size: 14px;\">系统默认</span>");
		}
	}

jQuery.extend({
	

    createUploadIframe: function(id, uri)
	{
			//create frame
            var frameId = 'jUploadFrame' + id;
            var iframeHtml = '<iframe id="' + frameId + '" name="' + frameId + '" style="position:absolute; top:-9999px; left:-9999px"';
			if(window.ActiveXObject)
			{
                if(typeof uri== 'boolean'){
					iframeHtml += ' src="' + 'javascript:false' + '"';

                }
                else if(typeof uri== 'string'){
					iframeHtml += ' src="' + uri + '"';

                }	
			}
			iframeHtml += ' />';
			jQuery(iframeHtml).appendTo(document.body);

            return jQuery('#' + frameId).get(0);			
    },
    createUploadForm: function(id, fileElementId, data)
	{
		//create form	
		var formId = 'jUploadForm' + id;
		var fileId = 'jUploadFile' + id;
		var form = jQuery('<form  action="" method="POST" name="' + formId + '" id="' + formId + '" enctype="multipart/form-data"></form>');	
		if(data)
		{
			for(var i in data)
			{
				jQuery('<input type="hidden" name="' + i + '" value="' + data[i] + '" />').appendTo(form);
			}			
		}		
		var oldElement = jQuery('#' + fileElementId);
		var newElement = jQuery(oldElement).clone();
		jQuery(oldElement).attr('id', fileId);
		jQuery(oldElement).before(newElement);
		jQuery(oldElement).appendTo(form);
		
		//add text parameters
        if (data) {  
            for (var i in data) {  
                $('<input type="hidden" name="' + i + '" value="' + data[i] + '" />').appendTo(form);  
            }  
        } 


		
		//set attributes
		jQuery(form).css('position', 'absolute');
		jQuery(form).css('top', '-1200px');
		jQuery(form).css('left', '-1200px');
		jQuery(form).appendTo('body');		
		return form;
    },
    handleError:function(a,b,d,f){if(a.error)a.error.call(a.context||a,b,d,f);if(a.global)(a.context?c(a.context):c.event).trigger("ajaxError",[b,a,f])},
    ajaxFileUpload: function(s) {
        // TODO introduce global settings, allowing the client to modify them for all requests, not only timeout	
        s = jQuery.extend({}, jQuery.ajaxSettings, s);
        var id = new Date().getTime()        
		var form = jQuery.createUploadForm(id, s.fileElementId, (typeof(s.data)=='undefined'?false:s.data));
		var io = jQuery.createUploadIframe(id, s.secureuri);
		var frameId = 'jUploadFrame' + id;
		var formId = 'jUploadForm' + id;		
        // Watch for a new set of requests
        if ( s.global && ! jQuery.active++ )
		{
			jQuery.event.trigger( "ajaxStart" );
		}            
        var requestDone = false;
        // Create the request object
        var xml = {}   
        if ( s.global )
            jQuery.event.trigger("ajaxSend", [xml, s]);
        // Wait for a response to come back
        var uploadCallback = function(isTimeout)
		{			
			var io = document.getElementById(frameId);
            try 
			{				
				if(io.contentWindow)
				{
					 xml.responseText = io.contentWindow.document.body?io.contentWindow.document.body.innerHTML:null;
                	 xml.responseXML = io.contentWindow.document.XMLDocument?io.contentWindow.document.XMLDocument:io.contentWindow.document;
					 
				}else if(io.contentDocument)
				{
					 xml.responseText = io.contentDocument.document.body?io.contentDocument.document.body.innerHTML:null;
                	xml.responseXML = io.contentDocument.document.XMLDocument?io.contentDocument.document.XMLDocument:io.contentDocument.document;
				}						
            }catch(e)
			{
				jQuery.handleError(s, xml, null, e);
			}
            if ( xml || isTimeout == "timeout") 
			{				
                requestDone = true;
                var status;
                try {
                    status = isTimeout != "timeout" ? "success" : "error";
                    // Make sure that the request was successful or notmodified
                    if ( status != "error" )
					{
                        // process the data (runs the xml through httpData regardless of callback)
                        var data = jQuery.uploadHttpData( xml, s.dataType );    
                        // If a local callback was specified, fire it and pass it the data
                        if ( s.success )
                            s.success( data, status );
    
                        // Fire the global callback
                        if( s.global )
                            jQuery.event.trigger( "ajaxSuccess", [xml, s] );
                    } else
                        jQuery.handleError(s, xml, status);
                } catch(e) 
				{
                    status = "error";
                    jQuery.handleError(s, xml, status, e);
                }

                // The request was completed
                if( s.global )
                    jQuery.event.trigger( "ajaxComplete", [xml, s] );

                // Handle the global AJAX counter
                if ( s.global && ! --jQuery.active )
                    jQuery.event.trigger( "ajaxStop" );

                // Process result
                if ( s.complete )
                    s.complete(xml, status);

                jQuery(io).unbind()

                setTimeout(function()
									{	try 
										{
											jQuery(io).remove();
											jQuery(form).remove();	
											
										} catch(e) 
										{
											jQuery.handleError(s, xml, null, e);
										}									

									}, 100)

                xml = null

            }
        }
        // Timeout checker
        if ( s.timeout > 0 ) 
		{
            setTimeout(function(){
                // Check to see if the request is still happening
                if( !requestDone ) uploadCallback( "timeout" );
            }, s.timeout);
        }
        try 
		{

			var form = jQuery('#' + formId);
			jQuery(form).attr('action', s.url);
			jQuery(form).attr('method', 'POST');
			jQuery(form).attr('target', frameId);
            if(form.encoding)
			{
				jQuery(form).attr('encoding', 'multipart/form-data');      			
            }
            else
			{	
				jQuery(form).attr('enctype', 'multipart/form-data');			
            }			
            jQuery(form).submit();

        } catch(e) 
		{			
            jQuery.handleError(s, xml, null, e);
        }
		
		jQuery('#' + frameId).load(uploadCallback	);
        return {abort: function () {}};	

    },

    uploadHttpData: function( r, type ) {
        var data = !type;
        data = type == "xml" || data ? r.responseXML : r.responseText;
        // If the type is "script", eval it in global context
        if ( type == "script" )
            jQuery.globalEval( data );
        // Get the JavaScript object, if JSON is used.
        if ( type == "json" )
            eval( "data = \"" + data +"\"");
        // evaluate scripts within html
        if ( type == "html" )
            jQuery("<div>").html(data).evalScripts();

        return data;
    }
})

