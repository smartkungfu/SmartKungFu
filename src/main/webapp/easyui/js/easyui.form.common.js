	if (window.Event){  
		document.captureEvents(Event.MOUSEUP);  
	}
	
	function nocontextmenu() { 
		event.cancelBubble = true; 
		event.returnValue = false;  
		return false;
	} 
	
	function norightclick(e) { 
		if (window.Event)  {  
			if (e.which == 2 || e.which == 3)   
			return false; 
		} 
		else if (event.button == 2 || event.button == 3)  {  
			event.cancelBubble = true;
			event.returnValue = false;
			return false;  
		} 
	} 
	
	document.oncontextmenu = nocontextmenu;  // for IE5+
	document.onmousedown = norightclick;  // for all others

	var isDebug = true;
	var theFormId = "#myForm";
	var theProcessServlet = "";

	// prepare the form when the DOM is ready 
	$(document).ready(function() {
		
		theFormId = "#"+document.forms[0].id;
		theProcessServlet = document.forms[0].action;
		
	    var options = { 
	    	type: "POST",   //访问WebService使用Post方式请求
	        contentType: "application/json; charset=utf-8",
	        url:        theProcessServlet,   // target element(s) to be updated with server response 
	        success:       showResponse,  // post-submit callback 
	        dataType: 'json',
	        data:''
	        // other available options: 
	        //url:       url         // override for form's 'action' attribute 
	        //type:      type        // 'get' or 'post', override for form's 'method' attribute 
	        //dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
	        //clearForm: true        // clear all form fields after successful submit 
	        //resetForm: true        // reset the form after successful submit 
	 
	        // $.ajax options can be used here too, for example: 
	        //timeout:   3000 
	    }; 
	    
	 
	    // bind to the form's submit event 
	    $(theFormId).submit(function() { 
	        // inside event callbacks 'this' is the DOM element so we first 
	        // wrap it in a jQuery object and then invoke ajaxSubmit 
	        
	    	 if(typeof(customBeforeSubmit)=="function") {
		        	if(!customBeforeSubmit()){
		        		return false;	
		        	}
		     }
	    	 
	    	 if(!$(theFormId).form('validate')){
	 	    	return false;	
	 	     }
	    	 
	    	
	    	     
	    	      $('input[type=submit], input[type=button]', this).each(function() {
	    	        $(this).attr("disabled", "disabled");
	    	      });
	    	      
	    	      $.messager.progress({ 
	    	    		title:'请稍后', 
	    	    		msg:'正在提交数据...' 
	    	      }); 
	    	      
	    	     options.data = JSON.stringify($(theFormId).serializeObject()); 
	 	    	 ajaxSubmit(options);
	    	
	    	 
	        // !!! Important !!! 
	        // always return false to prevent standard browser submit and page navigation 
	        return false; 
	    }); 
	    
	    loadFormData();
	    
	}); 
	 
	function ajaxSubmit(frm) {
	    $.ajax({
	        url: frm.url,
	        type: frm.type,
	        contentType:frm.contentType,
	        dataType:frm.dataType,
	        data: frm.data,
	        success: frm.success
	    });
	}
	
	// post-submit callback 
	function showResponse(responseText, statusText)  { 
	    // for normal html responses, the first argument to the success callback 
	    // is the XMLHttpRequest object's responseText property 
	 
	    // if the ajaxSubmit method was passed an Options Object with the dataType 
	    // property set to 'xml' then the first argument to the success callback 
	    // is the XMLHttpRequest object's responseXML property 
	 
	    // if the ajaxSubmit method was passed an Options Object with the dataType 
	    // property set to 'json' then the first argument to the success callback 
	    // is the json data object returned by the server 
//	    if(isDebug){
//		    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
//		        '\n\nThe output div should have already been updated with the responseText.'); 
//	    }
	    
	    if(statusText=="success"){
			var result = responseText ;
			
			if (result.success) {
				
				if(typeof(customSubmitAfter)=="function") {
					customSubmitAfter(result.message);
					//$.messager.progress('close');
				}
				else{
			    	if(typeof(parent.window.closeMyWindow)=="function"){
				    	parent.window.showMsg('操作提示', '操作成功!');
				    	parent.window.closeMyWindow(result.message);
				    	
			    	}
					$.messager.alert('操作提示', '操作成功','info');
			    	$.messager.progress('close');
				} 	
			} else {
	    		$.messager.alert('操作提示', '操作失败, '+result.message,'error');
		    	$.messager.progress('close');
			}   
	    }
	    else{
	    	$.messager.alert('操作提示', '操作失败! ','error');
	    	$.messager.progress('close');
	    }
	    
	    $('input[type=submit], input[type=button]', this).each(function() {
	        $(this).attr("disabled", "false");
	      });
	} 

	function submitForm(){
		$(theFormId).submit();
	}
    
	function resetForm() {
		if (_acttype == "edit") {
			loadFormData();
		} else {
			$(theFormId).form('clear');
			if(typeof(custominit)=="function") {
				custominit();
			}
		}
    }
	
	function closeForm(){
		parent.window.closeMyWindow();
	}
	
	function loadFormData(){
		if(typeof(customOnLoad)=="function") {
	    	customOnLoad();
	    }
		  
		if(_acttype == "edit"){
			if(_data){
				var jsonObj = eval("(" + _data + ")");
				//var jsonObj = $.parseJSON(_data);  
				
				$(theFormId).form('load', jsonObj);
				
				if(typeof(customOnLoadAfter)=="function") {
				   customOnLoadAfter();
				   $(theFormId).form('load', jsonObj);
				}
			}
		} 
		
		if(typeof(custominit)=="function") {
			custominit();
		}
		
	}
	
	$.fn.serializeObject = function()    
	{    
	   var o = {};    
	   var a = this.serializeArray();    
	   $.each(a, function() {    
	       if (o[this.name]) {    
	           if (!o[this.name].push) {    
	               o[this.name] = [o[this.name]];    
	           }    
	           o[this.name].push(this.value || '');    
	       } else {    
	           o[this.name] = this.value || '';    
	       }    
	   });    
	   return o;    
	}; 
	
	$.extend($.fn.validatebox.defaults.rules, {
		selectValueRequired: {  
			validator: function(value,param){ 
		    //alert( $(param[0]).combobox('getValue') );
			return $(param[0]).combobox('getValue') != '';  
			},  
			message: '请选择一个选项'  
	} ,
	selectTreeValueRequired: {  
		validator: function(value,param){ 
	    //alert( $(param[0]).combotree('tree') );
		var t = $(param[0]).combotree('tree');	// 获取树对象
		var n = t.tree('getSelected');		// 获取选择的节点
		return n.text != '';
		},  
		message: '请至少选择一个选项'  
	} ,
		idcard : {// 验证身份证 
        validator : function(value) { 
            return /^\d{15}(\d{2}[A-Za-z0-9])?$/i.test(value); 
        }, 
        message : '身份证号码格式不正确' 
    },
      minLength: {
        validator: function(value, param){
            return value.length >= param[0];
        },
        message: '请输入至少（2）个字符.'
    },
    length:{validator:function(value,param){ 
        var len=$.trim(value).length; 
            return len>=param[0]&&len<=param[1]; 
        }, 
            message:"输入内容长度必须介于{0}和{1}之间." 
        }, 
    phone : {// 验证电话号码 
        validator : function(value) { 
            return /^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/i.test(value); 
        }, 
        message : '格式不正确,请使用下面格式:020-88888888' 
    }, 
    mobile : {// 验证手机号码 
        validator : function(value) { 
            return /^(13|15|18)\d{9}$/i.test(value); 
        }, 
        message : '手机号码格式不正确' 
    }, 
    number : {// 验证整数或小数 
        validator : function(value) { 
            return /^\d+(\.\d+)?$/i.test(value); 
        }, 
        message : '请输入数字，并确保格式正确' 
    }, 
    currency : {// 验证货币 
        validator : function(value) { 
            return /^\d+(\.\d+)?$/i.test(value); 
        }, 
        message : '货币格式不正确' 
    }, 
    qq : {// 验证QQ,从10000开始 
        validator : function(value) { 
            return /^[1-9]\d{4,9}$/i.test(value); 
        }, 
        message : 'QQ号码格式不正确' 
    }, 
    integer : {// 验证整数 
        validator : function(value) { 
            return /^[+]?[0-9]+\d*$/i.test(value); 
        }, 
        message : '请输入整数' 
    }, 
    age : {// 验证年龄
        validator : function(value) { 
            return /^(?:[1-9][0-9]?|1[01][0-9]|120)$/i.test(value); 
        }, 
        message : '年龄必须是0到120之间的整数' 
    }, 
    
    chinese : {// 验证中文 
        validator : function(value) { 
            return /^[\Α-\￥]+$/i.test(value); 
        }, 
        message : '请输入中文' 
    }, 
    english : {// 验证英语 
        validator : function(value) { 
            return /^[A-Za-z]+$/i.test(value); 
        }, 
        message : '请输入英文' 
    }, 
    unnormal : {// 验证是否包含空格和非法字符 
        validator : function(value) { 
            return /.+/i.test(value); 
        }, 
        message : '输入值不能为空和包含其他非法字符' 
    }, 
    username : {// 验证用户名 
        validator : function(value) { 
            return /^[a-zA-Z][a-zA-Z0-9_]{5,15}$/i.test(value); 
        }, 
        message : '用户名不合法（字母开头，允许6-16字节，允许字母数字下划线）' 
    }, 
    faxno : {// 验证传真 
        validator : function(value) { 
//            return /^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/i.test(value); 
            return /^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/i.test(value); 
        }, 
        message : '传真号码不正确' 
    }, 
    zip : {// 验证邮政编码 
        validator : function(value) { 
            return /^[1-9]\d{5}$/i.test(value); 
        }, 
        message : '邮政编码格式不正确' 
    }, 
    ip : {// 验证IP地址 
        validator : function(value) { 
            return /d+.d+.d+.d+/i.test(value); 
        }, 
        message : 'IP地址格式不正确' 
    }, 
    name : {// 验证姓名，可以是中文或英文 
            validator : function(value) { 
                return /^[\Α-\￥]+$/i.test(value)|/^\w+[\w\s]+\w+$/i.test(value); 
            }, 
            message : '请输入姓名' 
    },
    date : {// 日期格式
        validator : function(value) { 
         //格式yyyy-MM-dd或yyyy-M-d
            return /^(?:(?!0000)[0-9]{4}([-]?)(?:(?:0?[1-9]|1[0-2])\1(?:0?[1-9]|1[0-9]|2[0-8])|(?:0?[13-9]|1[0-2])\1(?:29|30)|(?:0?[13578]|1[02])\1(?:31))|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)([-]?)0?2\2(?:29))$/i.test(value); 
        },
        message : '清输入合适的日期格式'
    },
    email:{ 
        validator : function(value){ 
        return /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value); 
    }, 
    message : '请输入有效的邮箱地址(例：abc@hotmail.com)' 
    },
    same:{ 
        validator : function(value, param){ 
            if($("#"+param[0]).val() != "" && value != ""){ 
                return $("#"+param[0]).val() == value; 
            }else{ 
                return true; 
            } 
        }, 
        message : '两次输入的密码不一致！'    
    },
    remote: { 
        validator: function(value, url){
        	var flag = true;
            $.post(url+"",{name:value},function(data){
               if(data=="unexist"){
                  
               }else if(data=="exist"){
                  flag = false;
                  
               }
            });
            return  flag;
        },
        message: '已被占使用'
    } 
    
});
	
	function myformatter(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
	}
	
	function myparser(s){
		if (!s) return new Date();
		var ss = s.split('-');
		var y = parseInt(ss[0],10);
		var m = parseInt(ss[1],10);
		var d = parseInt(ss[2],10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
			return new Date(y,m-1,d);
		} else {
			return new Date();
		}
	}
	
	function getTreeCheckedVals(objID){
	    var t = $('#'+objID).combotree('tree'); 
			var nodes = t.tree('getChecked');
			var s = '';
			for(var i=0; i<nodes.length; i++){
				if (s != '') s += ',';
				s += nodes[i].id + ','+ nodes[i].text;
			}
		 if(isDebug){
			alert(s);
		 }
		 return s;
	}
