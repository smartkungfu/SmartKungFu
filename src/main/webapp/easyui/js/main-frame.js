var currentMenuId = "";

function toggle(id){
	$("li[id!='top']").each(function(){
		$(this).removeClass('current');
	});
	$("#"+id).addClass('current');
}

$(function() {
    $('#nav a').click(function() {
    	var d = $(this).attr('name');
    	if(currentMenuId == d){
    		return;
    	}
    	toggle(d);
        refreshMenu(d);
    });

    var firstMenu = $('#nav a:first').attr('name');
    refreshMenu(firstMenu); //首次加载
});

//左侧导航加载
function refreshMenu(menuId) {
	currentMenuId = menuId;
	var url = ctx+"/menu/EmSysUser/center";
    url += "?tag=" + Math.random();
	$("#mainFrame").attr("src", url);
}

