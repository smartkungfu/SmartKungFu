/**   
 *    
 * 项目名称：HelpService   
 * 类名称：JPushEnum   
 * 类描述：   
 * 创建人：Zhanglin   
 * 创建时间：2015年12月21日 下午12:26:45   
 * 修改人：Zhanglin    
 * 修改时间：2015年12月21日 下午12:26:45   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.common;

/**
 * @ClassName: JPushEnum
 * @Description: 这是一个极光推送的方式类型
 * @author Zhanglin
 * @date 2015年12月21日 下午12:26:45
 *
 */
public enum JPushEnum {
	/**
	 * ALL_ALL:所有平台，所有设备 : 所有平台，所有设备
	 * ALL_ALIAS: 所有平台，推送方式按别名
	 * ANDROID_ALIAS:平台是 Android，推送方式按别名
	 * IOS_ALIAS:平台是 iOS，推送方式按别名, 
	 * TODO:其他推送方式后续在这里添加
	 */
	ALL_ALL,ALL_ALIAS,ANDROID_ALIAS,IOS_ALIAS
}
