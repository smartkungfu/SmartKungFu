package com.lst.common;

import org.springframework.beans.propertyeditors.PropertiesEditor;

import com.lst.utils.DateUtil;


public class DateEditor extends PropertiesEditor {

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (text == null || text.equals("")) {
			setValue(null);
		} else {
			if(text.length() == 16){
				setValue(DateUtil.str2Date(text, "yyyy-MM-dd HH:mm"));
			} else if(text.indexOf(":") == 19) {
				setValue(DateUtil.str2Date(text, "yyyy-MM-dd HH:mm:ss"));
			} else{
				setValue(DateUtil.str2Date(text, "yyyy-MM-dd"));
			}

		}
	}

	@Override
	public String getAsText() {
		return getValue().toString();
	}

}
