/**   
 *    
 * 项目名称：MedicalTeamService   
 * 类名称：MD5Utils   
 * 类描述：   
 * 创建人：zhangl  
 * 创建时间：2016年7月28日 上午11:13:25   
 * 修改人：zhangl   
 * 修改时间：2016年7月28日 上午11:13:25   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.common;

import java.security.MessageDigest;

import org.apache.log4j.Logger;

/**
 * @ClassName: MD5Utils
 * @Description: 激活码MD5算法加密
 * @author zhangl
 * @date 2016年7月28日 上午11:13:25
 *
 */
public class MD5Utils {

	protected static Logger logger = Logger.getLogger("log");
	
	
	public static String producAct(String devNo,char type , int random){
		String originKey = devNo + type;
		
		try {
			Long evenNum = getStrByMd5(originKey);
			
			Long randomNum = evenNum << random;//与随机参数运算结果
			
			logger.info("decPara randomNum: "+ randomNum);
			
			if(randomNum.toString().length() > 6){
				do {
					randomNum = randomNum >>> 2;
					logger.info("decPara randomNum >>> 2: " + randomNum);
				}while (randomNum.toString().length() != 6);
			}
			 //将奇数位换为字母
            String result = "";
            char  [] c =  randomNum.toString().toCharArray();
            for(int j = 0 ; j < c.length ;j ++){
            	if(j % 2 != 0){
            		result += (char) (c[j] + 64);
            	}else{
            		result += c[j];
            	}
            }
            
            //“6 + 3 + 1”。6：md5变化后的6位，3：随机参数3位，1：激活码类型1位
           String secretKey = result + String.valueOf(random) + type;
           
           logger.info("decPara secretKey: " + secretKey);
           
           return secretKey;
		} catch (Exception e) {
			logger.error("producAct error: " ,e);
		}
		
		return null;
	}
	/***
	 * 
	 * @Title: getStrByMd5
	 * @Description: MD5方式获取纯数字
	 * @param @param originKey
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return Long    返回类型
	 */
	public static Long getStrByMd5(String originKey) throws Exception{
		//指定加密的方式为MD5
		MessageDigest md = MessageDigest.getInstance("MD5");
		// 进行加密运算
		byte bytes[] = md.digest(originKey.getBytes());
		String evenNum = "";

		//取偶数位，保证能正常转成Long型
		int i ;
		int len = bytes.length ;
		for (i = 0; i < len; i++) {  
			if(i % 2 == 0){
				evenNum += Math.abs(bytes[i]);
			}
		} 

		return Long.valueOf(evenNum);
	}
	
	public static void main(String[] args) {
		producAct("2b2b45b9-927e-48c5-9c25-df6b57363119", '1', 123);
	}
}
