/**   
*    
* 项目名称：BBM_Manage   
* 类名称：HorseLampController   
* 类描述：基础控制类   
* 创建人：Zhanglin   
* 创建时间：2015年12月22日 下午8:26:59   
* 修改人：Zhanglin    
* 修改时间：2015年12月22日 下午8:26:59   
* 修改备注：   
* @version    
*    
*/
package com.lst.common;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.lst.exception.MyException;
import com.lst.model.MyApplication;
import com.lst.model.MySession;

/**
 * 
* @ClassName: BaseController
* @Description: 基础控制类
* @author Echo
* @date 2016年4月25日 上午10:32:40
*
 */
public abstract class BaseController {
	protected static Logger logger = Logger.getLogger("log");

	/**
	 *
	 * @Title: getMySession
	 * @Description: 取得自定义Session
	 * @param @return
	 * @param @throws MyException 设定文件
	 * @return MySession 返回类型
	 * @throws
	 */
	protected MySession getMySession() throws MyException {
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = ((ServletRequestAttributes) ra)
				.getRequest();

		MySession mySession = (MySession) request.getSession().getAttribute(
				CommonEnum.SESSION_KEY);
		
		if(mySession == null){
			throw new MyException("请求超时！");
		}

		return mySession;
	}
	
	/**
	 *
	 * @Title: setMySession
	 * @Description: 设置自定义Session
	 * @param @param mySession
	 * @param @throws MyException 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	protected void setMySession(MySession mySession) throws MyException {
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = ((ServletRequestAttributes) ra)
				.getRequest();

		request.getSession(true).setMaxInactiveInterval(60 * 30);//60 * 30

		request.getSession().setAttribute(CommonEnum.SESSION_KEY, mySession);
	}
	
	/**
	 *
	 * @Title: SessionExpired
	 * @Description: Session超时判断
	 * @param @return
	 * @param @throws MyException 设定文件
	 * @return boolean 返回类型
	 * @throws
	 */
	protected boolean SessionExpired() throws MyException {
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = ((ServletRequestAttributes) ra)
				.getRequest();

		if (request.getSession(false) == null) {
			System.out.println("Session has been invalidated!");
			return true;
		} else {
			System.out.println("Session is active!");
			return false;
		}
	}

	/**
	 * 
	* @Title: clearSession
	* @Description: clear Session
	* @param     设定文件
	* @return void    返回类型
	* @throws
	 */
	protected void clearSession() {
		ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes();
		HttpSession session = sra.getRequest().getSession();
		Enumeration<String> enumeration = session.getAttributeNames();
		while (enumeration.hasMoreElements()) {
			session.removeAttribute(enumeration.nextElement());
		}
		// session.invalidate();
	}
	
	/**
	 * 
	* @Title: getMyApplication
	* @Description: 获取全局值
	* @param @return    设定文件
	* @return WebApplication    返回类型
	* @throws
	 */
	protected MyApplication getMyApplication()  {
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = ((ServletRequestAttributes) ra)
				.getRequest();

		ServletContext application = request.getServletContext();
		
		MyApplication myApplication = (MyApplication)application.getAttribute(CommonEnum.APPLICATION_KEY);
		
		if (myApplication == null) {
			myApplication = new MyApplication();

			application.setAttribute(CommonEnum.APPLICATION_KEY,
					myApplication);
		}
		
		return myApplication;
	}
	
	/**
	 * 
	* @Title: setMyApplication
	* @Description: 设置全局值
	* @param @param myApplication
	* @param @throws MyException    设定文件
	* @return void    返回类型
	* @throws
	 */
	protected void setMyApplication(MyApplication myApplication) throws MyException {
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = ((ServletRequestAttributes) ra)
				.getRequest();

		request.getServletContext().setAttribute(CommonEnum.APPLICATION_KEY, myApplication);
	}

	/**
	 *
	 * @Title: BinderDate
	 * @Description: 页面String类型转换为合适的类型
	 * @param @param binder 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	@InitBinder
	protected void BinderDate(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new DateEditor());
		binder.registerCustomEditor(BigDecimal.class, new DecimalEditor());
		binder.registerCustomEditor(Integer.class, new IntegerEditor());
		binder.registerCustomEditor(Long.class, new LongEditor());
		binder.registerCustomEditor(Double.class, new DoubleEditor());
		binder.registerCustomEditor(Float.class, new FloatEditor());
	}

}
