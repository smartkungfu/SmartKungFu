package com.lst.common;

import java.math.BigDecimal;

import org.springframework.beans.propertyeditors.PropertiesEditor;

public class DecimalEditor extends PropertiesEditor {

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (text == null || text.equals("")) {
			setValue(null);
		} else {
			setValue(new BigDecimal(text));
		}
	}

	@Override
	public String getAsText() {
		return getValue().toString();
	}

}
