/**   
 *    
 * 项目名称：MedicalTeam   
 * 类名称：CommonEnum   
 * 类描述：编码维护
 * 创建人：Zhanglin   
 * 创建时间：2016年4月29日 上午10:42:22   
 * 修改人：Zhanglin    
 * 修改时间：2016年4月29日 上午10:42:22   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.common;

/**
 * 
 * @ClassName: CommonEnum
 * @Description:编码维护
 * @author zhangl
 * @date 2016年4月29日 上午10:42:22
 * 
 */
public class CommonEnum {

	// --------------------------------Session KEY--------------------------------
	public final static String SESSION_KEY = "MY_SESSION";
	public final static String APPLICATION_KEY = "WEB_APPLICATIOIN";

	//内部跳转
	public final static String FORWARD_VIEW_MAIN = "pages/main";//main文件夹
	public final static String FORWARD_VIEW_OTHERS = "pages/others";//others文件夹

	public final static String FORWARD_VIEW_MAIN_ORDER = FORWARD_VIEW_MAIN + "/order_manage/";
	public final static String FORWARD_VIEW_MAIN_KUNGFU = FORWARD_VIEW_MAIN + "/kungfu_manage/";
	public final static String FORWARD_VIEW_MAIN_MESSAGE = FORWARD_VIEW_MAIN + "/message_manage/";
	public final static String FORWARD_VIEW_MAIN_SHOP = FORWARD_VIEW_MAIN + "/shop_manage/";
	public final static String FORWARD_VIEW_MAIN_STATISTICS = FORWARD_VIEW_MAIN + "/statistical_manage/";
	public final static String FORWARD_VIEW_MAIN_SYSTEM = FORWARD_VIEW_MAIN + "/system_manage/";
	public final static String FORWARD_VIEW_MAIN_KIND = FORWARD_VIEW_MAIN + "/kind_manage/";
	public final static String FORWARD_VIEW_MAIN_APP_MANAGE = FORWARD_VIEW_MAIN + "/app_manage/";
	public final static String FORWARD_VIEW_MAIN_STORE = FORWARD_VIEW_MAIN + "/store_manage/";
	public final static String FORWARD_VIEW_MAIN_CODE = FORWARD_VIEW_MAIN + "/code_manage/";
	public final static String FORWARD_VIEW_MAIN_RENT = FORWARD_VIEW_MAIN + "/rent_manage/";
	public final static String FORWARD_VIEW_MAIN_USER = FORWARD_VIEW_MAIN + "/user_manage/";

	public final static String FORWARD_VIEW_MAIN_STATISTICS_DEVICE = FORWARD_VIEW_MAIN + "/statistics_manage/statistics_device/";
	// 验证码图片绘制大小
	public final static Integer SAFETYCODE_HEIGHT = 37;// 高度37
	public final static Integer SAFETYCODE_WIDHT = 100;// 宽度100

	public final static Integer RGB_ROUND = 255;// GRB涂料范围值

	// 分页
	public final static String SYSTEMPAGE_PAGE = "1";
	public final static String SYSTEMPAGE_PAGESIZE = "10";

	//排序
	public final static String SYSTEM_ORDER_DESC = "DESC";
	public final static String SYSTEM_ORDER_ASC = "ASC";

	// AJAX返回码
	public final static String MSGCODE_SUCCESS = "SUCCESS";
	public final static String MSGCODE_FAILED = "FAILED";
	// 资源路径
	public static final String RESOURCE_URL_IMAGE = "/UploadImages/KUNGFU/IMG/";// 图片路径"
	public static final String RESOURCE_URL_VIDEO = "/UploadImages/KUNGFU/VIDEO/";// 视频路径"
	// 菜单树展开样式
	public static final String MENU_STYLE_INDEX_EXP = "-1";// 默认展开下标为1的一级菜单
	public static final String MENU_STYLE_INDEX_CHE = "-1";// 默认选中下标为1的一级菜单并且下标为3的二级菜单
	// 设备别名配置格式
	public static final String JPUSH_ALIAS_ANYONE = "alias_";
	// excel模板
	public static final String MODEL_EXCEL_ACT = "201607191643";// 激活码导出模板

	// 带时分秒的计算
	public static final String START_DATE_HMS = " 00:00:00";
	public static final String END_DATE_HMS = " 23:59:59";

	//服务器IP
	//public static final String SERVICE_IP = "http://203.156.203.153:20005/";
	
	//public static final String SMS_URL = "http://203.156.203.153:20005/SmartKungFuService/SendSmsServlet";//"http://localhost:8080/SmartKungFuService/SendSmsServlet";
	
	//public static final String REGISTER_URL = "http://203.156.203.153:20005/SmartKungFuService/RegisterServlet";
	
	//public static final String SERVICE_PORT = "8080";

	/***
	 ***************************** 正则表达式验证规则*************************************
	 */
	// 邮件
	public static final String REGEX_EMAIL = "^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$";
	// 颜色
	public static final String REGEX_COLOR = "^[a-fA-F0-9]{6}$";
	// url
	public static final String REGEX_URL = "^http[s]?:\\/\\/([\\w-]+\\.)+[\\w-]+([\\w-./?%&=]*)?$";
	// 仅中文
	public static final String REGEX_CHINESE = "^[\\u4E00-\\u9FA5\\uF900-\\uFA2D]+$";
	// 仅ACSII字符
	public static final String REGEX_ASCII = "^[\\x00-\\xFF]+$";
	// 邮编
	public static final String REGEX_ZIPCODE = "^\\d{6}$";
	// 手机号
	public static final String REGEX_MOBILE = "^(13|15|18|17)[0-9]{9}$";
	// ip地址
	public static final String REGEX_IP4 = "^(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)$";
	// 非空
	public static final String REGEX_NOTEMPTY = "^\\S+$";
	// 图片
	public static final String REGEX_PICTURE = "(.*)\\.(jpg|bmp|gif|ico|pcx|jpeg|tif|png|raw|tga)$";
	// 压缩文件
	public static final String REGEX_RAR = "(.*)\\.(rar|zip|7zip|tgz)$";
	// 日期
	// public static final String REGEX_DATE = "^\\d{4}(\\-|\\/|\.)\\d{1;2}\\1\\d{1;2}$";
	// QQ号码
	public static final String REGEX_QQ = "^[1-9]*[1-9][0-9]*$";
	// 电话号码的函数(包括验证国内区号;国际区号;分机号)
	public static final String REGEX_TEL = "^(([0\\+]\\d{2;3}-)?(0\\d{2;3})-)?(\\d{7;8})(-(\\d{3;}))?$";
	// 用来用户注册。匹配由数字、26个英文字母或者下划线组成的字符串
	public static final String REGEX_ACCOUNT = "^\\w+$";
	// 字母
	public static final String REGEX_LETTER = "^[A-Za-z]+$";
	// 大写字母
	public static final String REGEX_LETTER_U = "^[A-Z]+$";
	// 小写字母
	public static final String REGEX_LETTER_L = "^[a-z]+$";
	// 身份证
	public static final String REGEX_IDCARD = "^[1-9]([0-9]{14}|[0-9]{17})$";
	// 正浮点数
	public static final String REGEX_POSITIVE_NUM = "^[1-9]\\d*.\\d*|0.\\d*[1-9]\\d*$";
	/***
	 ***************************** 系统编码规范*************************************
	 */
	// 权限code
	public final static Integer SYSCODE_ROOTBELONG_2 = 41;// 个人
	public final static Integer SYSCODE_ROOTBELONG_3 = 42;// 角色

	public final static Integer SYSCODE_ROOTDISTRI_5 = 44;// 菜单
	public final static Integer SYSCODE_ROOTDISTRI_6 = 45;// 按钮
	// 角色类型
	public final static Integer SYSCODE_USERTYPE_WEB = 76;// 后端角色
	public final static Integer SYSCODE_USERTYPE_APP = 77;// 前端角色

	// 资源类型
	public static final Integer SYSCODE_REOURCES_TYPE_IMAGE = 84;// 图片资源
	public static final Integer SYSCODE_REOURCES_TYPE_VIDEO = 85;// 视频资源
	public static final Integer SYSCODE_REOURCES_TYPE_TXT = 86;// 文档资源

	// 消息类型
	public static final Integer SYSCODE_MSG_TYPE_SMS = 79;// 短信消息
	public static final Integer SYSCODE_MSG_TYPE_EMAIL = 81;// 邮件消息
	public static final Integer SYSCODE_MSG_TYPE_PUSH = 80;// 推送消息

	// 发送者类型
	public static final Integer SYSCODE_MSG_SEND_TYPE_SYSTEM = 92;// 系统
	public static final Integer SYSCODE_MSG_SEND_TYPE_CLIENT = 93;// 前端用户
	public static final Integer SYSCODE_MSG_SEND_TYPE_USER = 94;// 后端用户

	// 接收者类型
	public static final Integer SYSCODE_MSG_RECEIVE_TYPE_SYSTEM = 88;// 系统
	public static final Integer SYSCODE_MSG_RECEIVE_TYPE_CLIENT = 89;// 前端用户
	public static final Integer SYSCODE_MSG_RECEIVE_TYPE_USER = 90;// 后端用户

	//功夫板块
	public static final int SYSCODE_GFBK_WZBK = 9;// 文章板块
	public static final int SYSCODE_GFBK_SPBK = 10;// 视频板块
	public static final int SYSCODE_GFBK_HDBK = 11;// 活动板块
	public static final int SYSCODE_GFBK_CGBK = 12;// 场馆板块
	public static final int SYSCODE_GFBK_DRBK = 13;// 达人板块
	public static final int SYSCODE_GFBK_PLBK = 295;// 评论版块
	public static final int SYSCODE_GFBK_KCBK = 298;// 课程版块
	public static final int SYSCODE_GFBK_ZXKC = 369;// 在线课程版块
	
    //社区板块
    public static final Integer SYSTEM_CARESEL_RANGE_ANSWER = 50;//回答版块
    public static final Integer SYSTEM_CARESEL_RANGE_COMPANION = 51;//嘿哈版块
    public static final Integer SYSTEM_CARESEL_RANGE_DYNAMIC = 52;//动态版块
    public static final Integer SYSTEM_CARESEL_RANGE_QUESTION = 53;//问题版块

	//优惠券类型
	public static final Integer SYSCODE_YHQ_TYPE_KC = 124;// 课程
	public static final Integer SYSCODE_YHQ_TYPE_HD = 125;// 活动

	//优惠类型
	public static final Integer SYSCODE_YH_TYPE_MJ = 127;// 满减
	public static final Integer SYSCODE_YH_TYPE_DZ = 128;// 打折
	public static final Integer SYSCODE_YH_TYPE_ZJ = 129;// 直减
	
	//优惠卷状态
	public static final Integer SYSCODE_TICKET_USEED = 131;//已使用
	public static final Integer SYSCODE_TICKET_USEING = 132;//未使用
	public static final Integer SYSCODE_TICKET_DATED = 133;//已过期
	
	//订单状态
	public static final Integer SYSCODE_ORDERSTATUS_PAYING = 27;//待付款
	public static final Integer SYSCODE_ORDERSTATUS_PAYED = 29;//已付款
	public static final Integer SYSCODE_ORDERSTATUS_CANCEL = 30;//已取消
	public static final Integer SYSCODE_ORDERSTATUS_COMPLETEED = 209;//已完成
	public static final Integer SYSCODE_ORDERSTATUS_GOING = 210;//进行中
	public static final Integer SYSCODE_ORDERSTATUS_JOINING = 234;//待参与
	public static final Integer SYSCODE_ORDERSTATUS_REFUNDING = 236;//待退款
	public static final Integer SYSCODE_ORDERSTATUS_REFUND = 237;//退款中
	public static final Integer SYSCODE_ORDERSTATUS_REFUNDED = 238;//已退款
	public static final Integer SYSCODE_ORDERSTATUS_NONEREFUND = 239;//未退款
	
	//退款申请状态
	public static final Integer SYSCODE_REFUNDSTATUS_APPLYING = 241;//待申请
	public static final Integer SYSCODE_REFUNDSTATUS_APPLIED = 242;//已申请
	public static final Integer SYSCODE_REFUNDSTATUS_DEALING = 243;//待处理
	public static final Integer SYSCODE_REFUNDSTATUS_DEALED = 244;//已处理
	
	//课程状态
	public static final Integer SYSCODE_COURSETYPE_SPE = 19;//特色课程
	public static final Integer SYSCODE_COURSETYPE_OTHERS = 20;//其他课程
	
	//变化类型
	public static final Integer SYSCODE_COURSETYPE_PRAISE = 35;//点赞
	public static final Integer SYSCODE_COURSETYPE_LOOK = 36;//浏览
	public static final Integer SYSCODE_COURSETYPE_FAVORITE = 37;//收藏
	public static final Integer SYSCODE_COURSETYPE_FORWARD = 38;//转发
	
	//在线视频类型
	public static final Integer SYSCODE_ONLINECOURSETYPE_QINGGONG = 352;
	public static final Integer SYSCODE_ONLINECOURSETYPE_CRAZYFIGHT = 353;
	
	/**
	 * 统计
	 */
	public static final String[] X_AXIS_WEEKNAME = { "周一", "周二", "周三", "周四", "周五", "周六", "周日" };// X轴-星期
	
	// --------------------------------成功失败--------------------------------
	public final static String DO_SUCCESS = "SUCCESS";
	public final static String DO_FAILED = "FAILED";
	
	// 功力值类型
    public static final Integer SYSTEM_GROWTH_RANGE_REGISTER = 288;// 注册
    
    //头像url
    public static final String portraitUrl = "http://skf.smartkungfu.com";

}
