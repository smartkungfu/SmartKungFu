/**   
 *    
 * 项目名称：SmartKungFuService   
 * 类名称：ImgCompress   
 * 类描述：   
 * 创建人：zhangl  
 * 创建时间：2017年1月9日 下午8:51:30   
 * 修改人：zhangl   
 * 修改时间：2017年1月9日 下午8:51:30   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.common;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import javax.imageio.ImageIO;

/**
 * @ClassName: ImgCompress
 * @Description:图片压缩处理
 * @author zhangl
 * @date 2017年1月9日 下午8:51:30
 *
 */
public class ImgCompress {
	
	private Image img;  
	
	private int width;  
	
	private int height; 
	
	private String newUrl;
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {  
		System.out.println("开始：" + new Date().toLocaleString());  
		ImgCompress imgCom = new ImgCompress("C://Users//Administrator//Desktop//20170106174957221_656.jpg");  
		//imgCom.resizeFix(400, 400);  
		imgCom.resize(200, 200);  
		System.out.println("结束：" + new Date().toLocaleString());  
	}  
	/** 
	 * 构造函数 
	 */  
	public ImgCompress(String fileName) throws IOException {  
		File file = new File(fileName);// 读入文件  
		img = ImageIO.read(file);      // 构造Image对象  
		width = img.getWidth(null);    // 得到源图宽  
		height = img.getHeight(null);  // 得到源图长  
		
		String parentPath = file.getParent();
		
		String _fileName = file.getName();
		String prefix = _fileName.substring(0,_fileName.lastIndexOf("."));  
		
		String suffix = _fileName.substring(_fileName.lastIndexOf(".") + 1);  
		
		String newfileName = parentPath + "\\" + prefix + "_zip." + suffix; 
		
		newUrl  = newfileName;
	}  
	/** 
	 * 按照宽度还是高度进行压缩 
	 * @param w int 最大宽度 
	 * @param h int 最大高度 
	 */  
	public void resizeFix(int w, int h) throws IOException {  
		if (width / height > w / h) {  
			resizeByWidth(w);  
		} else {  
			resizeByHeight(h);  
		}  
	}  
	/** 
	 * 以宽度为基准，等比例放缩图片 
	 * @param w int 新宽度 
	 */  
	public void resizeByWidth(int w) throws IOException {  
		int h = (int) (height * w / width);  
		resize(w, h);  
	}  
	/** 
	 * 以高度为基准，等比例缩放图片 
	 * @param h int 新高度 
	 */  
	public void resizeByHeight(int h) throws IOException {  
		int w = (int) (width * h / height);  
		resize(w, h);  
	}  
	/** 
	 * 强制压缩/放大图片到固定的大小 
	 * @param w int 新宽度 
	 * @param h int 新高度 
	 */  
	public String resize(int w, int h) throws IOException {  
		// SCALE_SMOOTH 的缩略算法 生成缩略图片的平滑度的 优先级比速度高 生成的图片质量比较好 但速度慢  
		BufferedImage image = new BufferedImage(w, h,BufferedImage.SCALE_SMOOTH);   
		image.getGraphics().drawImage(img, 0, 0, w, h, null); // 绘制缩小后的图  
		File destFile = new File(newUrl);  
		FileOutputStream out = new FileOutputStream(destFile); // 输出到文件流  
		// 可以正常实现bmp、png、gif转jpg  
		ImageIO.write(image, "JPEG",out) ; 
		out.close();  
		
		return destFile.getName();
	}  
}
