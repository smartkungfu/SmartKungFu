/**
 *
 * 项目名称：BBM_Manage
 * 类名称：MyException
 * 类描述： 自定义异常类
 * 创建人：Echo
 * 创建时间：2015年10月9日 上午2:55:21
 * 修改人：Echo
 * 修改时间：2015年10月9日 上午2:55:21
 * 修改备注：
 * @version
 *
 */
package com.lst.exception;

/**
 * @ClassName: MyException
 * @Description: 自定义异常类
 * @author Echo
 * @date 2015年10月9日 上午2:55:21
 *
 */
public class MyException extends Exception {

	/**
	 * @Fields serialVersionUID : 序列化版本标示
	 */
	private static final long serialVersionUID = 1L;
	// 异常信息
	public String message;

	public MyException(String message) {
		super(message);
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
