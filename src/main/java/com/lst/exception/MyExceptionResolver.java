/**
 *
 * 项目名称：AppRoveManage
 * 类名称：MyExceptionResolver
 * 类描述：全局异常处理器
 * 创建人：Echo
 * 创建时间：2015年10月9日 上午2:57:39
 * 修改人：Echo
 * 修改时间：2015年10月9日 上午2:57:39
 * 修改备注：
 * @version
 *
 */
package com.lst.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

/**
 * @ClassName: MyExceptionResolver
 * @Description: 全局异常处理器
 * @author Echo
 * @date 2015年10月9日 上午2:57:39
 *
 */
public class MyExceptionResolver implements HandlerExceptionResolver {
	protected static Logger logger = Logger.getLogger("log");

	/**
	 * （非 Javadoc）
	 * <p>
	 * Title: resolveException
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 *
	 * @param request
	 * @param response
	 * @param handler
	 * @param ex
	 *            系统 抛出的异常
	 * @return
	 * @see org.springframework.web.servlet.HandlerExceptionResolver#resolveException(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
	 */
	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

		logger.error("Err class and method : " + handler.toString() + " Err Info:  " + ex);

		ModelAndView modelAndView = new ModelAndView();
		
		if (ex instanceof MyException) {
			// 如果该 异常类型是系统 自定义的异常，直接取出异常信息，在错误页面展示
			modelAndView.setViewName("login");
			
			// 解析出异常类型
			MyException myException = (MyException) ex;
			// 错误信息	
			String message = myException.getMessage();
			
			modelAndView.addObject("msgInfo", message);
			logger.error("Err message : " + message);
		
		} else if(ex instanceof Error404Exception) {
			// 如果该 异常类型是系统 自定义的异常，直接取出异常信息，在错误页面展示
			modelAndView.setViewName("404");
			
			// 解析出异常类型
			Error404Exception err404 = (Error404Exception) ex;
			// 错误信息
			String message = err404.getMessage();
			
			modelAndView.addObject("msgInfo", message);
			logger.error("Err message : " + message);
			
		} else {
			modelAndView.setViewName("404");
			
			String message = ex.getMessage();
			logger.error("Err message : " + message);
		}

		return modelAndView;
	}
}
