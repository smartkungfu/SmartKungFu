/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：Error404Exception   
 * 类描述：404报错类   
 * 创建人：zhangl  
 * 创建时间：2017年1月13日 下午5:18:20   
 * 修改人：zhangl   
 * 修改时间：2017年1月13日 下午5:18:20   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.exception;

/**
 * @ClassName: Error404Exception
 * @Description:404报错类
 * @author zhangl
 * @date 2017年1月13日 下午5:18:20
 *
 */
public class Error404Exception extends Exception {
	/**
	 * @Fields serialVersionUID : 序列化版本标示
	 */
	private static final long serialVersionUID = 1L;

	// 异常信息
	public String message;

	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

}
