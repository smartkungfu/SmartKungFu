/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：ClientRefundController   
 * 类描述：用户退款控制类   
 * 创建人：zhangl  
 * 创建时间：2016年12月5日 下午5:19:22   
 * 修改人：zhangl   
 * 修改时间：2016年12月5日 下午5:19:22   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.ClientRefund;
import com.lst.model.FolPara;
import com.lst.model.MstButton;
import com.lst.model.MstUser;
import com.lst.model.OrderDetail;
import com.lst.service.ClientRefundService;
import com.lst.service.MstButtonService;
import com.lst.service.OrderDetailService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: ClientRefundController
 * @Description:用户退款控制类
 * @author zhangl
 * @date 2016年12月5日 下午5:19:22
 *
 */
@Controller
@RequestMapping("clientRefundController")
public class ClientRefundController extends BaseController {

	@Autowired
	private MstButtonService mstButtonService;

	@Autowired
	private ClientRefundService clientRefundService;
	
	@Autowired
	private OrderDetailService orderDetailService;

	/**
	 * 
	 * @Title: list
	 * @Description:  页面加载
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @param minDate
	 * @param @param maxDate
	 * @param @param orderNo
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(
			@RequestParam(required = true) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE) int page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) int rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "") String orderNo) throws MyException {
		Date startDate = new Date();
		logger.info("list begin: " + startDate);

		FolPara folPara = new FolPara();
		folPara.setMenuId(menuId);
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setOrderNo(orderNo);

		folPara.setMinDate(minDate);
		folPara.setMaxDate(maxDate);

		MstUser mstUser = this.getMySession().getMstUser();
		List<MstButton> btns = CommonUtils.readPermissionWithBtn(menuId, mstUser, mstButtonService);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_ORDER + "cr_list");
		mv.addObject("btns", btns);
		mv.addObject("folPara",folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("list end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: queryList
	 * @Description:数据加载
	 * @param @param page
	 * @param @param pageSize
	 * @param @param sort
	 * @param @param order
	 * @param @param minDate
	 * @param @param maxDate
	 * @param @param orderNo
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return Object    返回类型
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "")String orderNo) throws MyException {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("isDeleted", 0);
		reqMap.put("enabled", 1);

		if(StringUtils.isEmpty(sort)){
			reqMap.put("orderByClause","cr.createdate desc");
		} else{
			reqMap.put("orderByClause",sort + " " + order);
		}

		if(!StringUtils.isBlank(orderNo)){
			reqMap.put("_orderNo", orderNo.trim());
		}

		if(!StringUtils.isBlank(minDate) && !StringUtils.isBlank(maxDate)){
			try {
				if(DateUtil.parse(minDate).after(DateUtil.parse(maxDate))){
					reqMap.put("minDate", maxDate + DateUtil.START_DATE_HMS);
					reqMap.put("maxDate", minDate + DateUtil.END_DATE_HMS) ;
				}else{
					reqMap.put("minDate", minDate + DateUtil.START_DATE_HMS);
					reqMap.put("maxDate", maxDate + DateUtil.END_DATE_HMS);
				}
			} catch (ParseException e) {
				logger.error("queryList error: " ,e);
			}
		}	

		PageBounds pageBounds = new PageBounds(page, rows);
		List<ClientRefund> list = clientRefundService.queryByList(reqMap, pageBounds);

		PageList<ClientRefund> pageList = (PageList<ClientRefund>) list;
		Paginator paginator = pageList.getPaginator();

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		indexPages.put("total", paginator.getTotalCount());	
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}
	
	
	/**
	 * 	
	 * @Title: edit
	 * @Description:编辑页面
	 * @param @param id 
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView edit(
			@RequestParam(required = true) Integer id,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) {
		Date startDate = new Date();
		logger.info("edit begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_ORDER + "rr_edit");

		ClientRefund cr = clientRefundService.selectByPrimaryKey(id);
		mv.addObject("cr",cr);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("edit end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: UpdateStatus
	 * @Description:更新退款状态
	 * @param @param id
	 * @param @param status
	 * @param @return    设定文件
	 * @return boolean    返回类型
	 * @throws MyException 
	 */
	@RequestMapping("UpdateStatus")
	@ResponseBody
	public boolean UpdateStatus(@RequestParam(required = true)Integer id,@RequestParam(required = true)Integer status,@RequestParam(required = true)Integer orderdetailid) throws MyException{
		Date startDate = new Date();
		logger.info("UpdateStatus begin: " + startDate);
		
		boolean flag = false;
		
		MstUser mstUser = this.getMySession().getMstUser();
		
		try {
			
			//更新退款申请状态
			ClientRefund cr = new ClientRefund();
			cr.setId(id);
			cr.setRefstatus(status);
			cr.setUpdateuser(mstUser.getId());
			cr.setUpdatedate(startDate);
			
			clientRefundService.updateByPrimaryKeySelective(cr);
			
			//更新订单详情状态
			
			OrderDetail od = new OrderDetail();
			od.setId(orderdetailid);
			od.setOrderstatus(CommonEnum.SYSCODE_ORDERSTATUS_REFUND);
			od.setUpdatedate(startDate);
			od.setUpdateuser(mstUser.getId());
			
			orderDetailService.updateByPrimaryKeySelective(od);
			
			flag = true;
		} catch (Exception e) {
			logger.error("UpdateStatus error: ",e);
		}
		
		logger.info("UpdateStatus end run(s): " + DateUtil.calLastedTime(startDate));

		return flag;
	}

}
