/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstDiscussController   
 * 类描述：   
 * 创建人：Wdd   
 * 创建时间：2016年10月26日 下午4:24:49   
 * 修改人：Wdd  
 * 修改时间：2016年10月26日 下午4:24:49   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.FolPara;
import com.lst.model.MstButton;
import com.lst.model.MstDiscuss;
import com.lst.model.MstUser;
import com.lst.service.MstButtonService;
import com.lst.service.MstDiscussService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: MstDiscussController
 * @Description: 评论消息
 * @author Wdd
 * @date 2016年10月26日 下午4:24:49
 *
 */
@Controller
@RequestMapping("/mstDiscussController")
public class MstDiscussController extends BaseController {

	@Autowired
	private MstDiscussService mstDiscussService;

	@Autowired
	private MstButtonService mstButtonService;

	/**
	 * 
	 * @Title: Main
	 * @Description: 页面加载
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @param minDate
	 * @param @param maxDate
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 */

	@RequestMapping(value = "/Main")
	public ModelAndView Main(
			@RequestParam(required = false) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE)Integer page,
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = "") String type,
			@RequestParam(required = false,defaultValue = "") String title) throws MyException {
		Date startDate = new Date();
		logger.info("Main begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_MESSAGE + "/discuss_list");

		// 查询用户权限按钮
		MstUser mstUser = this.getMySession().getMstUser();
		List<MstButton> btns = CommonUtils.readPermissionWithBtn(menuId, mstUser, mstButtonService);

		//写参数
		FolPara folPara = new FolPara();
		folPara.setMenuId(menuId);
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setType(type);
		folPara.setTitleFol(title);

		mv.addObject("folPara", folPara);
		mv.addObject("btns", btns);
		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("Main end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: queryList
	 * @Description: 查询分页数据
	 * @param @param page
	 * @param @param rows
	 * @param @return    设定文件
	 * @return Object    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "") String type,
			@RequestParam(required = false,defaultValue = "") String title) {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("enabled", 1);
		
		if(!StringUtils.isBlank(type)){
			Integer typeNum=Integer.parseInt(type);
			reqMap.put("type",typeNum );
		}
		if(!StringUtils.isBlank(title)){
			reqMap.put("searchwork", title);
		}

		PageBounds pageBounds = new PageBounds(page, rows);
		List<MstDiscuss> list =  mstDiscussService.queryByList(reqMap, pageBounds);

		PageList<MstDiscuss> pageList = (PageList<MstDiscuss>) list;
		Paginator paginator = pageList.getPaginator();

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		indexPages.put("total", paginator.getTotalCount());
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}

	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param request
	 * @param @param response
	 * @param @param ids
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping("delList")
	@ResponseBody
	public boolean delList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) String ids) throws MyException {
//		Date startDate = new Date();
//		logger.info("delList begin: " + startDate);

		boolean flag = false;

	//	MstUser mstUser = this.getMySession().getMstUser();

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("ids", ids);

		try {
			mstDiscussService.delList(reqMap);
			flag = true;
		} catch (Exception e) {
			logger.error("delList error: " ,e);
		}

	//	logger.info("delList end run(s): " + DateUtil.calLastedTime(startDate));

		return flag;
	}


}
