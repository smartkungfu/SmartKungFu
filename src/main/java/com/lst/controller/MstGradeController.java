/**   
*    
* 项目名称：SmartKungFu   
* 类名称：MstGradeController   
* 类描述：   
* 创建人：Wdd   
* 创建时间：2016年12月13日 下午9:52:54   
* 修改人：Wdd  
* 修改时间：2016年12月13日 下午9:52:54   
* 修改备注：   
* @version    
*    
*/
package com.lst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.common.BaseController;
import com.lst.model.MstGrade;
import com.lst.service.MstGradeService;
import com.lst.utils.DateUtil;

/**
 * @ClassName: MstGradeController
 * @Description: 功夫等级
 * @author Wdd
 * @date 2016年12月13日 下午9:52:54
 *
 */
@Controller
@RequestMapping("/mstGradeController")
public class MstGradeController extends BaseController {
	
	@Autowired
	private MstGradeService mstGradeService;
	
	/**
	 * 
	* @Title: queryList
	* @Description: 查询功夫等级
	* @param @return    设定文件
	* @return Object    返回类型
	* @throws
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList() {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		/**
		 * 查询功夫等级信息
		 */
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("isDeleted", 0);
		reqMap.put("enabled", 1);
		reqMap.put("orderByClause","g.gravalue asc");
		
		PageBounds pageBounds = new PageBounds();
		List<MstGrade> list = mstGradeService.queryByList(reqMap, pageBounds);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return list;
	}

}
