/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstClientController   
 * 类描述：   
 * 创建人：Wdd   
 * 创建时间：2016年10月10日 下午6:29:19   
 * 修改人：Wdd  
 * 修改时间：2016年10月10日 下午6:29:19   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.FolPara;
import com.lst.model.MstButton;
import com.lst.model.MstClient;
import com.lst.model.MstUser;
import com.lst.model.Resources;
import com.lst.service.MstButtonService;
import com.lst.service.MstClientService;
import com.lst.service.ResourcesService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: MstClientController
 * @Description: 前台用户管理
 * @author Wdd
 * @date 2016年10月10日 下午6:29:19
 * 
 */
@Controller
@RequestMapping("/mstClientController")
public class MstClientController extends BaseController {

	@Autowired
	private MstClientService mstClientService;

	@Autowired
	private MstButtonService mstButtonService;
	
	@Autowired
	private ResourcesService resourcesService;

	/**
	 * 
	 * @Title: Main
	 * @Description: 页面跳转
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @param minDate
	 * @param @param maxDate
	 * @param @param nickName
	 * @param @param mobileNo
	 * @param @param nameCn
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 */

	@RequestMapping(value = "/Main")
	public ModelAndView Main(
			@RequestParam(required = false) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE)Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "") String nickName,
			@RequestParam(required = false,defaultValue = "") String mobileNo,
			@RequestParam(required = false,defaultValue = "") String nameCn) throws MyException {
		Date startDate = new Date();
		logger.info("Main begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_APP_MANAGE + "/user_list");

		// 查询用户权限按钮
		MstUser mstUser = this.getMySession().getMstUser();
		List<MstButton> btns = CommonUtils.readPermissionWithBtn(menuId, mstUser, mstButtonService);

		//写参数
		FolPara folPara = new FolPara();
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setNickName(nickName);;
		folPara.setMinDate(minDate);
		folPara.setMaxDate(maxDate);
		folPara.setNameCn(nameCn);
		folPara.setMobileNo(mobileNo);
		folPara.setMenuId(menuId);

		mv.addObject("btns", btns);
		mv.addObject("folPara", folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("Main end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: queryList
	 * @Description: 查询分页数据
	 * @param @param page
	 * @param @param rows
	 * @param @param minDate
	 * @param @param maxDate
	 * @param @param nickname
	 * @param @param mobileno
	 * @param @param namecn
	 * @param @return    设定文件
	 * @return Object    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "")String minDate,
			@RequestParam(required = false,defaultValue = "")String maxDate,
			@RequestParam(required = false,defaultValue = "")String nickname,
			@RequestParam(required = false,defaultValue = "")String mobileno,
			@RequestParam(required = false,defaultValue = "")String namecn) {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		/**
		 * 查询用户信息
		 */
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("isDeleted", 0);
		reqMap.put("enabled", 1);
		
		if(StringUtils.isEmpty(sort)){
			reqMap.put("orderByClause","u.createdate desc");
		} else{
			reqMap.put("orderByClause",sort + " " + order);
		}

		if(!StringUtils.isBlank(nickname)){
			reqMap.put("_nickname", nickname.trim());
		}

		if(!StringUtils.isBlank(mobileno)){
			reqMap.put("_mobileno", mobileno.trim());
		}

		if(!StringUtils.isBlank(namecn)){
			reqMap.put("_namecn", namecn.trim());
		}

		if(!StringUtils.isBlank(minDate) && !StringUtils.isBlank(maxDate)){
			reqMap.put("minDate", minDate + DateUtil.START_DATE_HMS);
			reqMap.put("maxDate", maxDate + DateUtil.END_DATE_HMS);
		}

		PageBounds pageBounds = new PageBounds(page, rows);
		List<MstClient> list = mstClientService.queryByList(reqMap, pageBounds);

		PageList<MstClient> pageList = (PageList<MstClient>) list;
		Paginator paginator = pageList.getPaginator();

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		// indexPages.put("paginator", paginator);
		// indexPages.put("totalRows", paginator.getTotalCount());
		// indexPages.put("pageSize", paginator.getLimit());
		// indexPages.put("pageNumber", paginator.getPage());
		indexPages.put("total", paginator.getTotalCount());
		indexPages.put("page", page);
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}

	/**
	 * 
	 * @Title: edit
	 * @Description: 添加编辑跳转
	 * @param @param id
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView edit(
			@RequestParam(required = false, defaultValue = "") String id,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) {
		Date startDate = new Date();
		logger.info("edit begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_APP_MANAGE + "user_edit");

		if (!StringUtils.isBlank(id)) {
			// 获取用户信息
			MstClient mstClient = mstClientService.selectByPrimaryKey(Integer.parseInt(id));
			mv.addObject("mstClient", mstClient);
		}

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("edit end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: save
	 * @Description: 表单提交保存用户
	 * @param @param mstClient
	 * @param @param br
	 * @param @param folPara
	 * @param @param imagename
	 * @param @param rolids
	 * @param @param request
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return String    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/save",method = RequestMethod.POST)
	public String save(
			@Valid MstClient mstClient,
			BindingResult br,
			FolPara folPara, 
			String [] imagename,
			Integer [] rolids,
			HttpServletRequest request) throws MyException {
		Date startDate = new Date();
		logger.info("save begin: " + startDate);

		MstUser sysUser = this.getMySession().getMstUser();

		//执行业务层代码
		try {
			//拦截非法请求参数
			if (br.hasErrors()){
				List<ObjectError> ers = br.getAllErrors();
				logger.info("--------------form parameter error start-----------------------");

				for(ObjectError er : ers){
					logger.info("save faile with parameter illegal: " + er.getDefaultMessage() );
				}

				logger.info("--------------form parameter error end-----------------------");
			} else {
				mstClientService.saveInfo(mstClient, sysUser);
				
				//图片资源信息维护
				if(imagename != null && imagename.length != 0){
					Map<String, String> resMap = new HashMap<String,String>();
					resMap.put("userId", sysUser.getId().toString());
					resMap.put("ownerId", mstClient.getId().toString());
					resMap.put("tableName", "MST_CLIENT");

					Map<String, Object> dataMap = CommonUtils.getResourcesData(request, imagename, resMap,CommonEnum.SYSCODE_REOURCES_TYPE_IMAGE,false);
					
					@SuppressWarnings("unchecked")
					List<Resources> resources = (List<Resources>) dataMap.get("resources");

					resourcesService.saveList(resources);

					String imagePath = "";
					for(Resources resource : resources){
						imagePath += resource.getRespath() + resource.getResname() + ";";
					}

					MstClient imgUser = new MstClient();
					imgUser.setUpdatedate(startDate);
					imgUser.setUpdateuser(sysUser.getId());
					imgUser.setId(mstClient.getId());
					imgUser.setPortrait(imagePath);

					mstClientService.updateByPrimaryKeySelective(imgUser);
				}
			}
		} catch (Exception e) {
			logger.error("save error: " , e);
		}

		StringBuffer sb = new StringBuffer("?");
		sb.append("menuId=" + folPara.getMenuId());
		sb.append("&page=" + folPara.getPage());
		sb.append("&rows=" + folPara.getRows());
		sb.append("&minDate=" + folPara.getMinDate());
		sb.append("&maxDate=" + folPara.getMaxDate());
		sb.append("&nickName=" + folPara.getNickName());
		sb.append("&mobileNo=" + folPara.getMobileNo());
		sb.append("&nameCn=" + folPara.getNameCn());
		
		sb.append("&index_exp=" + folPara.getIndex_exp());
		sb.append("&index_che=" + folPara.getIndex_che());

		logger.info("save end run(s): " + DateUtil.calLastedTime(startDate));

		return "redirect:Main" + sb.toString();
	}

	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除用户
	 * @param @param ids
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 * @throws
	 */
	@RequestMapping("delList")
	@ResponseBody
	public boolean delList(
			@RequestParam(required = true) String  ids ) throws MyException{
		Date startDate = new Date();
		logger.info("delList begin: "  + startDate);

		boolean flag = false;

		MstUser mstUser = this.getMySession().getMstUser();

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("updateUser", mstUser.getId());
		reqMap.put("ids", ids);

		try {
			mstClientService.delList(reqMap);
			flag  = true;
		} catch (Exception e) {
			logger.error("delList error: " ,e);
		}

		logger.info("delList end run(s): " + DateUtil.calLastedTime(startDate));

		return flag;
	}
}
