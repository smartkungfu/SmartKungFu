/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstMessageController   
 * 类描述：   
 * 创建人：Wdd   
 * 创建时间：2016年10月31日 上午10:33:11   
 * 修改人：Wdd  
 * 修改时间：2016年10月31日 上午10:33:11   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.FolPara;
import com.lst.model.MstButton;
import com.lst.model.MstClient;
import com.lst.model.MstMessage;
import com.lst.model.MstUser;
import com.lst.model.OnlineCourse;
import com.lst.model.ProjectInfo;
import com.lst.model.VideoInfo;
import com.lst.model.WenwInfo;
import com.lst.service.MstButtonService;
import com.lst.service.MstClientService;
import com.lst.service.MstMessageService;
import com.lst.service.MstUserService;
import com.lst.service.OnlineCourseService;
import com.lst.service.ProjectInfoService;
import com.lst.service.VideoInfoService;
import com.lst.service.WenwInfoService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;
/**
 * @ClassName: MstMessageController
 * @Description: 系统消息
 * @author Wdd
 * @date 2016年10月31日 上午10:33:11
 *
 */
@Controller
@RequestMapping("/mstMessageController")
public class MstMessageController extends BaseController {

	@Autowired
	private MstButtonService mstButtonService;

	@Autowired
	private MstMessageService mstMessageService;

	@Autowired
	private MstClientService mstClientService;

	@Autowired
	private MstUserService mstUserService;

	@Autowired
	private WenwInfoService wenwInfoService;

	@Autowired
	private ProjectInfoService projectInfoService;

	@Autowired
	private VideoInfoService videoInfoService;

	@Autowired
	private OnlineCourseService onlineCourseService;


	/**
	 * 
	 * @Title: Main
	 * @Description: 页面跳转
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @param minDate
	 * @param @param maxDate
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/Main")
	public ModelAndView Main(
			@RequestParam(required = false) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE)Integer page,
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate) throws MyException {
		Date startDate = new Date();
		logger.info("Main begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_MESSAGE + "/message_list");

		// 查询用户权限按钮
		MstUser mstUser = this.getMySession().getMstUser();
		List<MstButton> btns = CommonUtils.readPermissionWithBtn(menuId, mstUser, mstButtonService);

		//写参数
		FolPara folPara = new FolPara();
		folPara.setMenuId(menuId);
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setMinDate(minDate);
		folPara.setMaxDate(maxDate);

		mv.addObject("folPara", folPara);
		mv.addObject("btns", btns);
		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("Main end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: queryList
	 * @Description: 查询分页数据
	 * @param @param page
	 * @param @param rows
	 * @param @param minDate
	 * @param @param maxDate
	 * @param @param type
	 * @param @return    设定文件
	 * @return Object    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "") String type,
			@RequestParam(required = false,defaultValue = "") String sendtype,
			@RequestParam(required = false,defaultValue = "") String receivetype) {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		/**
		 * 查询消息
		 */
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("isDeleted", 0);
		reqMap.put("enabled", 1);

		if(StringUtils.isEmpty(sort)){
			reqMap.put("orderByClause","m.createdate desc");
		} else{
			reqMap.put("orderByClause",sort + " " + order);
		}

		if(!StringUtils.isBlank(type)){
			reqMap.put("type", Integer.parseInt(type));
		}
		if(!StringUtils.isBlank(sendtype)){
			reqMap.put("sendtype", Integer.parseInt(sendtype));
		}
		if(!StringUtils.isBlank(receivetype)){
			reqMap.put("receivetype", Integer.parseInt(receivetype));
		}

		if(!StringUtils.isBlank(minDate) && !StringUtils.isBlank(maxDate)){
			reqMap.put("minDate", minDate + DateUtil.START_DATE_HMS);
			reqMap.put("maxDate", maxDate + DateUtil.END_DATE_HMS);
		}

		PageBounds pageBounds = new PageBounds(page, rows);

		HashMap<String, Object> indexPages = new HashMap<String, Object>();

		List<MstMessage> list = mstMessageService.queryByList(reqMap, pageBounds);

		MstClient mstClient = null;
		MstUser mstUser = null;

		//根据用户类型查出用户名
		for (MstMessage mstMessage : list) {
			//发送人
			if(mstMessage.getSendtype().equals(CommonEnum.SYSCODE_MSG_SEND_TYPE_CLIENT)){ //app
				mstClient = mstClientService.selectByPrimaryKey(mstMessage.getSendid());
				if(mstClient != null){
					mstMessage.setSendName(mstClient.getNickname() == null ? "" : mstClient.getNickname());
				}
			}else if(mstMessage.getSendtype().equals(CommonEnum.SYSCODE_MSG_SEND_TYPE_USER)){ //后台
				mstUser = mstUserService.selectByPrimaryKey(mstMessage.getSendid());
				if(mstUser != null){
					mstMessage.setSendName(mstUser.getAccount() == null ? "" : mstUser.getAccount());
				}
			}
			//接收人
			if(mstMessage.getReceivetype().equals(CommonEnum.SYSCODE_MSG_RECEIVE_TYPE_CLIENT)){ //app
				mstClient = mstClientService.selectByPrimaryKey(mstMessage.getReceiveid());
				if(mstClient != null){
					mstMessage.setReceiveName(mstClient.getNickname() == null ? "" : mstClient.getNickname());
				}
			}else if(mstMessage.getReceivetype().equals(CommonEnum.SYSCODE_MSG_RECEIVE_TYPE_USER)){ //后台
				mstUser = mstUserService.selectByPrimaryKey(mstMessage.getReceiveid());
				if(mstUser != null){
					mstMessage.setReceiveName(mstUser.getAccount() == null ? "" : mstUser.getAccount());
				}
			}
		}

		PageList<MstMessage> pageList = (PageList<MstMessage>) list;
		Paginator paginator = pageList.getPaginator();

		indexPages.put("total", paginator.getTotalCount());
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}

	/**
	 * 
	 * @Title: edit
	 * @Description: 跳转消息推送页
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView edit(
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) {
		Date startDate = new Date();
		logger.info("edit begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_MESSAGE + "Message_edit");

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("edit end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: pushMessage
	 * @Description:消息推送
	 * @param @param content
	 * @param @param grade
	 * @param @param reserved10
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return String    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/pushMessage")
	@ResponseBody
	public String pushMessage(
			@RequestParam(required = false,defaultValue = "") String content,
			@RequestParam(required = false,defaultValue = "") String reserved10) throws MyException {
		Date startDate = new Date();
		logger.info("pushMessage begin: " + startDate);

		MstUser sysUser = this.getMySession().getMstUser();

		String tag = "";

		try {
			//推送消息及添加消息表
			mstMessageService.saveInfo(content,sysUser,reserved10);

			tag = CommonEnum.DO_SUCCESS;
		} catch (Exception e) {
			tag = CommonEnum.DO_FAILED;

			logger.error("pushMessage is error ", e);
		}

		logger.info("pushMessage end run(s): " + DateUtil.calLastedTime(startDate));

		return tag;
	}

	/**
	 * 
	 * @Title: loadKungfu
	 * @Description: 功夫数据加载
	 * @param @return    设定文件
	 * @return Object    返回类型
	 * @throws
	 */
	@RequestMapping("loadkungfu")
	@ResponseBody
	public Object loadKungfu(){
		Date startDate = new Date();
		logger.info("loadKungfu start:" + startDate);

		Map<String, Object> kfMap = new HashMap<String,Object>();

		PageBounds pageBounds = new PageBounds();

		Map<String, Object> sqlMap = new HashMap<String,Object>();
		sqlMap.put("isDeleted", false);
		sqlMap.put("enabled", true);
		sqlMap.put("orderByClause", "wi.id desc");

		List<WenwInfo> wenws = wenwInfoService.queryByList(sqlMap, pageBounds);

		sqlMap.put("orderByClause", "vi.id desc");
		List<VideoInfo> vis = videoInfoService.queryByList(sqlMap, pageBounds);

		sqlMap.put("orderByClause", "pi.id desc");
		List<ProjectInfo> pis = projectInfoService.queryByList(sqlMap, pageBounds);

		sqlMap.put("orderByClause", "oc.id desc");
		List<OnlineCourse> ocs = onlineCourseService.queryByList(sqlMap, pageBounds);

		kfMap.put("wenws", wenws);
		kfMap.put("vis", vis);
		kfMap.put("pis", pis);
		kfMap.put("ocs", ocs);

		logger.info("loadKungfu end spent time(s):" + DateUtil.calLastedTime(startDate));

		return kfMap;
	} 

}
