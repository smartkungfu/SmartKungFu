/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstGymnasiumController   
 * 类描述：场馆控制类   
 * 创建人：zhangl  
 * 创建时间：2016年10月21日 上午10:11:31   
 * 修改人：zhangl   
 * 修改时间：2016年10月21日 上午10:11:31   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.FolPara;
import com.lst.model.MstButton;
import com.lst.model.MstGymnasium;
import com.lst.model.MstUser;
import com.lst.model.Resources;
import com.lst.service.MstButtonService;
import com.lst.service.MstGymnasiumService;
import com.lst.service.ResourcesService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: MstGymnasiumController
 * @Description: 场馆控制类
 * @author zhangl
 * @date 2016年10月21日 上午10:11:31
 *
 */
@Controller
@RequestMapping("mstGymnasiumController")
public class MstGymnasiumController extends BaseController {

	@Autowired
	private MstButtonService mstButtonService;

	@Autowired
	private MstGymnasiumService mstGymnasiumService;

	@Autowired
	private ResourcesService resourcesService;

	/**
	 * @Title: list
	 * @Description: 页面加载
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @param gymName
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(
			@RequestParam(required = true) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE) int page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) int rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "") String gymName) throws MyException {
		Date startDate = new Date();
		logger.info("list begin: " + startDate);

		FolPara folPara = new FolPara();
		folPara.setMenuId(menuId);
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setGymName(gymName);
		folPara.setMinDate(minDate);
		folPara.setMaxDate(maxDate);

		MstUser mstUser = this.getMySession().getMstUser();
		List<MstButton> btns = CommonUtils.readPermissionWithBtn(menuId, mstUser, mstButtonService);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_KUNGFU + "gym_list");
		mv.addObject("btns", btns);
		mv.addObject("folPara",folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("list end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: queryList
	 * @Description: 查询结果集
	 * @param @param page
	 * @param @param pageSize
	 * @param @param minDate
	 * @param @param maxDate
	 * @param @param gymName
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return Object    返回类型
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer pageSize,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "")String gymName) throws MyException {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("isDeleted", 0);
		reqMap.put("enabled", 1);
		
		if(StringUtils.isEmpty(sort)){
			reqMap.put("orderByClause","gym.createdate desc");
		} else{
			reqMap.put("orderByClause",sort + " " + order);
		}

		if(!StringUtils.isBlank(gymName)){
			reqMap.put("_gymName", gymName.trim());
		}

		if(!StringUtils.isBlank(minDate) && !StringUtils.isBlank(maxDate)){
			try {
				if(DateUtil.parse(minDate).after(DateUtil.parse(maxDate))){
					reqMap.put("minDate", maxDate + DateUtil.START_DATE_HMS);
					reqMap.put("maxDate", minDate + DateUtil.END_DATE_HMS) ;
				}else{
					reqMap.put("minDate", minDate + DateUtil.START_DATE_HMS);
					reqMap.put("maxDate", maxDate + DateUtil.END_DATE_HMS);
				}
			} catch (ParseException e) {
				logger.error("queryList error: " ,e);
			}
		}	

		PageBounds pageBounds = new PageBounds(page, pageSize);
		List<MstGymnasium> list = mstGymnasiumService.queryByList(reqMap, pageBounds);

		PageList<MstGymnasium> pageList = (PageList<MstGymnasium>) list;
		Paginator paginator = pageList.getPaginator();

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		indexPages.put("total", paginator.getTotalCount());	
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}

	/**
	 * 
	 * @Title: edit
	 * @Description: edit
	 * @param @param id
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView edit(
			@RequestParam(required = false) String id,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) {
		Date startDate = new Date();
		logger.info("edit begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_KUNGFU + "gym_edit");

		if (!StringUtils.isBlank(id)) {
			MstGymnasium mstGymnasium = mstGymnasiumService.selectByPrimaryKey(Integer.parseInt(id));
			mv.addObject("mstGymnasium", mstGymnasium);
		}

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("edit end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: save
	 * @Description: 保存
	 * @param @param mstGymnasium 场馆
	 * @param @param br           校验错误信息
	 * @param @param folPara	     携参
	 * @param @param imagename    图片集
	 * @param @param request	     请求
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return String    返回类型
	 */
	@RequestMapping(value = "/save",method = RequestMethod.POST)
	public String save(@Valid MstGymnasium mstGymnasium,BindingResult br,
			FolPara folPara,
			String [] imagename,
			HttpServletRequest request) throws MyException {
		Date startDate = new Date();
		logger.info("save begin: " + startDate);

		MstUser mstUser = this.getMySession().getMstUser();
		
		//处理\t问题
		if(!StringUtils.isEmpty(mstGymnasium.getContent())){
			mstGymnasium.setContent(mstGymnasium.getContent().trim());
		}

		//拦截非法请求参数
		try {
			if (br.hasErrors()){
				List<ObjectError> ers = br.getAllErrors();
				logger.info("--------------form parameter error start-----------------------");

				for(ObjectError er : ers){
					logger.info("save faile with parameter illegal: " + er.getDefaultMessage() );
				}

				logger.info("--------------form parameter error end-----------------------");
			}
			if (mstGymnasium.getId() != null) {
				mstGymnasium.setUpdatedate(startDate);
				mstGymnasium.setUpdateuser(mstUser.getId());

				mstGymnasiumService.updateByPrimaryKeySelective(mstGymnasium);
			} else {
				mstGymnasium.setEnabled(true);
				mstGymnasium.setIsdeleted(false);
				mstGymnasium.setCreatedate(startDate);
				mstGymnasium.setUpdatedate(startDate);
				mstGymnasium.setCreateuser(mstUser.getId());

				mstGymnasiumService.insert(mstGymnasium);
			}

			//图片资源信息维护
			if(imagename != null && imagename.length != 0){
				Map<String, String> resMap = new HashMap<String,String>();
				resMap.put("userId", mstUser.getId().toString());
				resMap.put("ownerId", mstGymnasium.getId().toString());
				resMap.put("tableName", "MST_GYMNASIUM");

				Map<String, Object> dataMap = CommonUtils.getResourcesData(request, imagename, resMap,CommonEnum.SYSCODE_REOURCES_TYPE_IMAGE,true);

				@SuppressWarnings("unchecked")
				List<Resources> resources = (List<Resources>) dataMap.get("resources");
				
				String reserved12 = (String) dataMap.get("zipFile");

				resourcesService.saveList(resources);

				//默认获取第一次作为常用图：String imagePath = resources.get(0).getRespath() + resources.get(0).getResname();
				//将所有图片获取出来
				String imagePath = "";
				for(Resources resource : resources){
					imagePath += resource.getRespath() + resource.getResname() + ";";
				}

				MstGymnasium imgGym = new MstGymnasium();
				imgGym.setUpdatedate(startDate);
				imgGym.setUpdateuser(mstUser.getId());
				imgGym.setId(mstGymnasium.getId());
				imgGym.setGymbill(imagePath);
				imgGym.setReserved12(reserved12);

				mstGymnasiumService.updateByPrimaryKeySelective(imgGym);
			}
		} catch (Exception e) {
			logger.error("save error： " ,e);
		}

		StringBuffer sb = new StringBuffer("?");
		sb.append("menuId=" + folPara.getMenuId());
		sb.append("&page=" + folPara.getPage());
		sb.append("&pageSize=" + folPara.getPageSize());
		sb.append("&gymName=" + folPara.getGymName());
		
		sb.append("&minDate=" + folPara.getMinDate());
		sb.append("&maxDate=" + folPara.getMaxDate());
		
		sb.append("&index_exp=" + folPara.getIndex_exp());
		sb.append("&index_che=" + folPara.getIndex_che());

		logger.info("save end run(s):" + DateUtil.calLastedTime(startDate));

		return "redirect:list" + sb.toString();
	}

	/**
	 * 
	 * @Title: isUnique
	 * @Description: 验证属性唯一性
	 * @param @param property 
	 * @param @param id
	 * @param @return    设定文件
	 * @return Boolean    返回类型
	 */
	@RequestMapping(value = "/isUnique",method = RequestMethod.GET)
	@ResponseBody
	public Boolean isUnique(
			@RequestParam(required = true)String property,
			@RequestParam(required = false,defaultValue = "")String id){
		Date startDate = new Date();
		logger.info("isUnique begin: " + startDate);

		boolean flag = false;

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("enabled", 1);
		reqMap.put("isDeleted", 0);
		reqMap.put("rolNo", property);

		List<MstGymnasium> gyms= mstGymnasiumService.queryByList(reqMap, new PageBounds());

		if(gyms.isEmpty()) flag = true;

		else{
			if(!StringUtils.isBlank(id)){//当为编辑操作时，存在唯一一个相同的名称名，验证通过
				if(gyms.get(0).getId().equals(Integer.valueOf(id))){
					flag = true;
				}
			}
		}
		logger.info("isUnique end run(s): " 
				+ DateUtil.calLastedTime(startDate));

		return flag;
	}


	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param request
	 * @param @param response
	 * @param @param ids
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping("delList")
	@ResponseBody
	public boolean delList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) String ids) throws MyException {
		Date startDate = new Date();
		logger.info("delList begin: " + startDate);

		boolean flag = false;

		MstUser mstUser = this.getMySession().getMstUser();

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("ids", ids);
		reqMap.put("updateUser", mstUser.getId());

		try {
			mstGymnasiumService.delList(reqMap);
			flag = true;
		} catch (Exception e) {
			logger.error("delList error: " ,e);
		}

		logger.info("delList end run(s): " + DateUtil.calLastedTime(startDate));

		return flag;
	}


	/**
	 * 
	 * @Title: initData
	 * @Description: 初始化数据
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return List<MstGymnasium>    返回类型
	 */
	@RequestMapping(value = "/initData" ,method = RequestMethod.GET)
	@ResponseBody
	public List<MstGymnasium> initData() throws MyException {
		Date startDate = new Date();
		logger.info("initData begin: " + startDate);

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("isDeleted", 0);
		reqMap.put("enabled", 1);
		reqMap.put("orderByClause", "gym.id desc");

		PageBounds pageBounds = new PageBounds();
		List<MstGymnasium> list = mstGymnasiumService.queryByList(reqMap, pageBounds);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return list;
	}
}
