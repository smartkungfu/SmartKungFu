/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstMenuController   
 * 类描述：  菜单控制层 
 * 创建人：zhangl  
 * 创建时间：2016年10月8日 上午9:31:34   
 * 修改人：zhangl   
 * 修改时间：2016年10月8日 上午9:31:34   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.MstMenu;
import com.lst.model.MstUser;
import com.lst.model.UserRole;
import com.lst.service.MstMenuService;
import com.lst.utils.DateUtil;

/**
 * @ClassName: MstMenuController
 * @Description: 菜单控制层
 * @author zhangl
 * @date 2016年10月8日 上午9:31:34
 *
 */
@Controller
@RequestMapping("/mstMenuController")
public class MstMenuController extends BaseController {

	@Autowired
	private MstMenuService mstMenuService;
	/**
	 * @throws MyException 
	 * 
	 * @Title: initMenus
	 * @Description: 初始化用户菜单信息
	 * @param @param request
	 * @param @param response
	 * @param @param mstmenu
	 * @param @return    设定文件
	 * @return List<MstMenu>    返回类型
	 * @throws
	 */
	@RequestMapping("initMenus")
	@ResponseBody
	public List<MstMenu> initMenus() throws MyException{
		Date startDate = new Date();
		logger.info("initMenus begin: " +  startDate);

		MstUser mstUser = this.getMySession().getMstUser();

		Map<String, Object> decMap = new HashMap<String,Object>();
		decMap.put("enabled", 1);
		decMap.put("isDeleted", 0);

		List<MstMenu> menus = this.getMySession().getMenus();
		if(menus == null || menus.size() == 0){
			menus = new ArrayList<MstMenu>();
			//初始化左侧菜单信息
			decMap.put("alMasterType", CommonEnum.SYSCODE_ROOTBELONG_2);
			decMap.put("alMasterValue", mstUser.getId());
			decMap.put("roleMasterType", CommonEnum.SYSCODE_ROOTBELONG_3);
			//附加角色权限
			for(UserRole ur : mstUser.getUrs()){
				decMap.put("roleMasterValue", ur.getRolid());
				List<MstMenu> tempMenus = mstMenuService.queryListForUserRoots(decMap,new PageBounds());
				if(!tempMenus.isEmpty()){
					menus.addAll(tempMenus);
				}
			}

			//筛选不同用户组、不同角色可能有重叠的权限
			Map<Integer, MstMenu> tempMap = new HashMap<Integer,MstMenu>();
			for(MstMenu menu : menus){
				if(!tempMap.containsKey(menu.getId())){
					tempMap.put(menu.getId(), menu);
				}
			}

			List<MstMenu> _menus = new ArrayList<MstMenu>();
			Set<Entry<Integer, MstMenu>> set = tempMap.entrySet();
			for(Entry<Integer, MstMenu> entry: set){
				_menus.add(entry.getValue());
			}

			//重组菜单（且包含一级菜单、二级菜单）
			List<MstMenu> tempList = arithMenuLv(_menus);

			menus = tempList;

			//重写Collections.sort()方法排序方式
			Collections.sort(menus, new Comparator<MstMenu>() {
				public int compare(MstMenu arg0, MstMenu arg1) {
					return arg0.getId().compareTo(arg1.getId());
				}
			});

			//排序
			/*Collections.sort(menus);*/

			this.getMySession().setMenus(menus);
		}

		logger.info("initMenus end run(s): " + DateUtil.calLastedTime(startDate));

		return  menus;
	}

	/**
	 * @Title: arithMenuLv
	 * @Description: 自定义算法，将得到菜单数据按根菜单和二级菜单等级重组
	 * @param @param menus
	 * @param @return    设定文件
	 * @return List<MstMenu>    返回类型
	 */
	private List<MstMenu> arithMenuLv(List<MstMenu> menus) {
		Map<Integer, MstMenu> mainMap =  new HashMap<Integer,MstMenu>();//存放根菜单
		Map<Integer, MstMenu> auxMap =  new HashMap<Integer,MstMenu>();//存放二级菜单

		List<MstMenu> tempList = new ArrayList<MstMenu>();

		for(MstMenu mstMenu : menus){
			if(mstMenu.getParentid() == null){
				mainMap.put(mstMenu.getId(), mstMenu);
			} else {
				auxMap.put(mstMenu.getId(), mstMenu);
			}
		}

		for (Entry<Integer, MstMenu> mainObj : mainMap.entrySet()) {
			MstMenu  menu = mainObj.getValue();

			List<MstMenu> _tempList = new ArrayList<MstMenu>();//存放临时二级菜单
			for (Entry<Integer, MstMenu> auxObj : auxMap.entrySet()){
				Integer id = menu.getId();
				Integer parentId = auxObj.getValue().getParentid();

				if(id.equals(parentId)){
					_tempList.add(auxObj.getValue());
				}
			} 
			menu.set_mstMenus(_tempList);

			tempList.add(menu);
		}

		return tempList;
	}

}
