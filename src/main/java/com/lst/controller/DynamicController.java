package com.lst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.Dynamic;
import com.lst.model.FolPara;
import com.lst.model.JiluLog;
import com.lst.model.MstButton;
import com.lst.model.MstDiscuss;
import com.lst.model.MstUser;
import com.lst.service.DynamicService;
import com.lst.service.JiluLogService;
import com.lst.service.MstButtonService;
import com.lst.service.MstDiscussService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: DynamicController
 * @Description: 动态管理
 *
 */
@Controller
@RequestMapping("/dynamicController")
public class DynamicController extends BaseController {
	
	@Autowired
	private MstButtonService mstButtonService;
	
	@Autowired
	private DynamicService dynamicService;
	
	@Autowired
	private MstDiscussService mstDiscussService;
	
	@Autowired
	private JiluLogService jiluLogService;
	
	/**
	 * 
	* @Title: Main
	* @Description: 列表页跳转
	* @param @param menuId
	* @param @param page
	* @param @param rows
	* @param @param index_exp
	* @param @param index_che
	* @param @param minDate
	* @param @param maxDate
	* @param @param typeFol
	* @param @param ariTypeFol
	* @param @return
	* @param @throws MyException    设定文件
	* @return ModelAndView    返回类型
	* @throws
	 */
	@RequestMapping(value = "/Main")
	public ModelAndView Main(
			@RequestParam(required = false) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE)Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "") String type,
			@RequestParam(required = false,defaultValue = "") String title) throws MyException {
		Date startDate = new Date();
		logger.info("Main begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_APP_MANAGE + "/dynamic_list");

		// 查询用户权限按钮
		MstUser mstUser = this.getMySession().getMstUser();
		List<MstButton> btns = CommonUtils.readPermissionWithBtn(menuId, mstUser, mstButtonService);

		//写参数
		FolPara folPara = new FolPara();
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setMinDate(minDate);
		folPara.setMaxDate(maxDate);
		folPara.setMenuId(menuId);
		folPara.setType(type);
		folPara.setTitleFol(title);

		mv.addObject("btns", btns);
		mv.addObject("folPara", folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("Main end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	* @Title: queryList
	* @Description: 查询分页list
	* @param @param page
	* @param @param rows
	* @param @param sort
	* @param @param order
	* @param @param minDate
	* @param @param maxDate
	* @param @param type
	* @param @param ariType
	* @param @return    设定文件
	* @return Object    返回类型
	* @throws
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "") String title) {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		/**
		 * 查询课程动作信息
		 */
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("orderByClause","md.createDate desc");
	
		if(!StringUtils.isBlank(title)){
			reqMap.put("searchwork", title);
		}

		PageBounds pageBounds = new PageBounds(page, rows);
		List<Dynamic> list =  dynamicService.queryByList(reqMap, pageBounds);

		PageList<Dynamic> pageList = (PageList<Dynamic>) list;
		Paginator paginator = pageList.getPaginator();

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		indexPages.put("total", paginator.getTotalCount());
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}
	
	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param request
	 * @param @param response
	 * @param @param ids
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping("delList")
	@ResponseBody
	public boolean delList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) String ids) throws MyException {
		Date startDate = new Date();
		logger.info("delList begin: " + startDate);

		boolean flag = false;

	//	MstUser mstUser = this.getMySession().getMstUser();
  
		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("ids", ids);
		try {
			dynamicService.delList(reqMap);
			flag = true;
		} catch (Exception e) {
			logger.error("delList error: " ,e);
		}

		logger.info("delList end run(s): " + DateUtil.calLastedTime(startDate));

		return flag;
	}
	
	
	/**
	 * 
	 * @Title: queryDynamicDiscuss
	 * @Description: 动态评论
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/queryDynamicDiscuss")
	public ModelAndView queryDynamicDiscuss(
			@RequestParam(required = false) int id,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) throws MyException {

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_APP_MANAGE + "/dynamicDiscuss");

		mv.addObject("dynamicId", id);
		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);
		

		return mv;
	}

	/**
	 * 
	* @Title: queryDiscuss
	* @Description: 查询动态评论
	* @param @param page
	* @param @param rows
	* @param @param minDate
	* @param @param maxDate
	* @param @param type
	* @param @param dynamicId
	* @param @return    设定文件
	* @return Object    返回类型
	* @throws
	 */
	@RequestMapping(value = "/queryDiscuss")
	@ResponseBody
	public Object queryDiscuss(@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "") String dynamicId) {
		/**
		 *  查询动态评论程信息
		 */
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("enabled", 1);
		reqMap.put("type",CommonEnum.SYSTEM_CARESEL_RANGE_DYNAMIC);
		if(!StringUtils.isBlank(dynamicId)){
			reqMap.put("tarid", dynamicId);
		}
		
    
		reqMap.put("orderByClause","d.CreateDate desc");


		PageBounds pageBounds = new PageBounds(page, rows);

		HashMap<String, Object> indexPages = new HashMap<String, Object>();

		List<MstDiscuss> list =  mstDiscussService.queryByList(reqMap, pageBounds);
		PageList<MstDiscuss> pageList = (PageList<MstDiscuss>) list;
		Paginator paginator = pageList.getPaginator();

		indexPages.put("total", paginator.getTotalCount());
		indexPages.put("rows", list);

		return indexPages;
	}
	
	
	/**
	 * 
	 * @Title: queryDynamicLove
	 * @Description: 动态喜欢者页面
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/queryDynamicLove")
	public ModelAndView queryDynamicLove(
			@RequestParam(required = false) int id,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) throws MyException {

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_APP_MANAGE + "/dynamicLove");

		mv.addObject("dynamicId", id);
		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);
		

		return mv;
	}

	/**
	 * 
	* @Title: queryDynamicLoveList
	* @Description: 查询动态喜欢者列表
	* @param @param page
	* @param @param rows
	* @param @param minDate
	* @param @param maxDate
	* @param @param type
	* @param @param dynamicId
	* @param @return    设定文件
	* @return Object    返回类型
	* @throws
	 */
	@RequestMapping(value = "/queryDynamicLoveList")
	@ResponseBody
	public Object queryDynamicLoveList(@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "") String dynamicId) {
		/**
		 *  查询动态喜欢者列表信息
		 */
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("jiluType", 1);
		if(!StringUtils.isBlank(dynamicId)){
		reqMap.put("sourceId", dynamicId);
		}
    
		reqMap.put("orderByClause", "md.createDate desc");


		PageBounds pageBounds = new PageBounds(page, rows);

		HashMap<String, Object> indexPages = new HashMap<String, Object>();

		List<JiluLog> list = jiluLogService.queryByList(reqMap, pageBounds);
		PageList<JiluLog> pageList = (PageList<JiluLog>) list;
		Paginator paginator = pageList.getPaginator();

		indexPages.put("total", paginator.getTotalCount());
		indexPages.put("rows", list);

		return indexPages;
	}
	/**
	 * 
	 * @Title: delLove
	 * @Description: 修改喜欢状态
	 * @param @param request
	 * @param @param response
	 * @param @param ids
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping("delLove")
	@ResponseBody
	public boolean delLove(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) String ids) throws MyException {

		boolean flag = false;
		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("ids", ids);
		try {
			jiluLogService.updateByKey(reqMap);
			flag = true;
		} catch (Exception e) {
			logger.error("delList error: " ,e);
		}

		return flag;
	}
	
	/**
	 * 
	 * @Title: clientDynamic
	 * @Description: 查询用户发布动态
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/clientDynamic")
	public ModelAndView clientDynamic(
			@RequestParam(required = false) int clientid,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) throws MyException {

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_APP_MANAGE + "/client_dynamic_list");

		mv.addObject("userId", clientid);
		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);
		

		return mv;
	}

	/**
	 * 
	* @Title: clientDynamicList
	* @Description: 查询用户发布动态
	* @param @param page
	* @param @param rows
	* @param @param minDate
	* @param @param maxDate
	* @param @param type
	* @param @param userId
	* @param @return    设定文件
	* @return Object    返回类型
	* @throws
	 */
	@RequestMapping(value = "/clientDynamicList")
	@ResponseBody
	public Object clientDynamicList(@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "") String userId) {
		/**
		 * 查询用户发布动态
		 */
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("createuser",userId);
		reqMap.put("orderByClause","md.createDate desc");

		PageBounds pageBounds = new PageBounds(page, rows);
		List<Dynamic> list =  dynamicService.queryByList(reqMap, pageBounds);

		PageList<Dynamic> pageList = (PageList<Dynamic>) list;
		Paginator paginator = pageList.getPaginator();

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		indexPages.put("total", paginator.getTotalCount());
		indexPages.put("rows", list);

		return indexPages;
	}
	
	
}
