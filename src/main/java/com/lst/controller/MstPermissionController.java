/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstPermissionController   
 * 类描述：权限控制层   
 * 创建人：zhangl  
 * 创建时间：2016年10月12日 下午4:35:29   
 * 修改人：zhangl   
 * 修改时间：2016年10月12日 下午4:35:29   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.FolPara;
import com.lst.model.MstPermission;
import com.lst.model.MstRole;
import com.lst.model.MstUser;
import com.lst.service.MstButtonService;
import com.lst.service.MstPermissionService;
import com.lst.service.MstRoleService;
import com.lst.service.MstUserService;
import com.lst.service.UserRoleService;
import com.lst.utils.DateUtil;

/**
 * @ClassName: MstPermissionController
 * @Description: 权限控制层
 * @author zhangl
 * @date 2016年10月12日 下午4:35:29
 *
 */
@Controller
@RequestMapping("mstPermissionController")
public class MstPermissionController extends BaseController {

	@Autowired
	private MstPermissionService mstPermissionService;

	@Autowired
	private MstUserService mstUserService;

	@Autowired
	private MstButtonService mstButtonService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private MstRoleService mstRoleService;

	/**
	 * 
	 * @Title: list
	 * @Description:页面加载
	 * @param @param menuId
	 * @param @param masterType
	 * @param @param page
	 * @param @param rows
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @param rolName
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(
			@RequestParam(required = true)Integer menuId,
			@RequestParam(required = true)Integer masterType,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE)Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false ,defaultValue = "")String rolName) throws MyException {
		Date startDate = new Date();
		logger.info("list begin: " + startDate);

		//写参数
		FolPara folPara = new FolPara();
		folPara.setMenuId(menuId);
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setMasterType(masterType);
		folPara.setRolName(rolName);

		ModelAndView mv = new ModelAndView();
		mv.setViewName(CommonEnum.FORWARD_VIEW_MAIN_SYSTEM + "permission_list");

		mv.addObject("folPara", folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("list end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: queryListByRole
	 * @Description: 查询角色权限
	 * @param @param userName
	 * @param @param page
	 * @param @param pageSize
	 * @param @return    设定文件
	 * @return Object    返回类型
	 */
	@RequestMapping(value = "/queryListByRole")
	@ResponseBody
	public Object queryListByRole(
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE) int page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) int rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false ,defaultValue = "")String rolName) {
		Date startDate = new Date();
		logger.info("queryListByRole begin: " + startDate);

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("enabled","1");
		reqMap.put("isDeleted","0");
		
		if(StringUtils.isEmpty(sort)){
			reqMap.put("orderByClause","r.createdate desc");
		} else{
			reqMap.put("orderByClause",sort + " " + order);
		}

		if(!StringUtils.isBlank(rolName)){
			reqMap.put("_rolName", rolName);
		}

		PageBounds pageBounds = new PageBounds(page, rows);
		List<MstRole> list = mstRoleService.queryByList(reqMap, pageBounds);

		Map<String, Object> decMap = new HashMap<String, Object>();
		decMap.put("isDeleted", 0);
		decMap.put("enabled", 1);

		PageList<MstRole> pageList = (PageList<MstRole>) list;
		Paginator paginator = pageList.getPaginator();

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		//indexPages.put("paginator",paginator);
		indexPages.put("total",paginator.getTotalCount());
		indexPages.put("rows", list);

		logger.info("queryListByRole end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}



	/**
	 * 
	 * @Title: save
	 * @Description:save
	 * @param @param request
	 * @param @param response
	 * @param @param mstrole
	 * @param @return    设定文件
	 * @return String    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, MstPermission mstpermission) {
		if (mstpermission.getId() != null) {
			mstpermission.setUpdatedate(new Date());
			mstpermission.setUpdateuser(1);

			mstPermissionService.updateByPrimaryKeySelective(mstpermission);
		} else {
			mstpermission.setEnabled(true);
			mstpermission.setIsdeleted(false);
			mstpermission.setCreatedate(new Date());
			mstpermission.setCreateuser(1);

			mstPermissionService.insert(mstpermission);
		}

		return "redirect:list";
	}

	/**
	 * @Title: queryMenuPermission
	 * @Description: 查询用户或角色的菜单权限
	 * @param @param request
	 * @param @return    设定文件
	 * @return Object    返回类型
	 * @throws
	 */
	@RequestMapping("queryMenuPermission")
	@ResponseBody
	public Object queryMenuPermission(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = false) Integer parentId,
			@RequestParam(required = true) int masterValue,
			@RequestParam(required = true) int masterType) {
		Date startDate = new Date();
		logger.info("queryMenuPermission begin : " + startDate);

		Map<String, Object> queryMap = new HashMap<String, Object>();
		queryMap.put("parentId", parentId);
		queryMap.put("mastervalue", masterValue);
		queryMap.put("mastertype", masterType);
		queryMap.put("actiontype", CommonEnum.SYSCODE_ROOTDISTRI_5);

		List<Map<String, Object>> list = mstPermissionService.queryMenuPermission(queryMap);

		logger.info("queryMenuPermission run end(s) :　" + DateUtil.calLastedTime(startDate));

		return list;
	}

	/**
	 * @Title: queryBtnPermission
	 * @Description: 查询用户或角色的btn权限
	 * @param @param request
	 * @param @return    设定文件
	 * @return Object    返回类型
	 */
	@RequestMapping("queryBtnPermission")
	@ResponseBody
	public Object queryBtnPermission(HttpServletRequest request) {
		Date startDate = new Date();
		logger.info("queryBtnPermission begin : " + startDate);

		String menuId = request.getParameter("menuId");
		String masterValue = request.getParameter("masterValue");
		String masterType = request.getParameter("masterType");

		Map<String, Object> queryMap = new HashMap<String, Object>();
		queryMap.put("menuId", menuId);
		queryMap.put("mastervalue", masterValue);
		queryMap.put("mastertype", masterType);
		queryMap.put("actiontype", CommonEnum.SYSCODE_ROOTDISTRI_6);

		List<Map<String, Object>> list = mstPermissionService.queryBtnPermission(queryMap);

		logger.info("queryBtnPermission run end(s) :　" + DateUtil.calLastedTime(startDate));

		return list;
	}

	/**
	 * 
	 * @Title: edit
	 * @Description: 编辑页面
	 * @param @param masterType
	 * @param @param id
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView edit(
			@RequestParam(required = true)Integer masterType,
			@RequestParam(required = true)Integer id,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) {
		Date startDate = new Date();
		logger.info("edit begin: "  + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_SYSTEM + "permission_edit");
		String tempName = "";

		if(masterType.equals(CommonEnum.SYSCODE_ROOTBELONG_3)){//角色
			MstRole role = mstRoleService.selectByPrimaryKey(id);
			tempName = role.getRolname();
		} else{												   //个人
			MstUser mstUser = mstUserService.selectByPrimaryKey(id);
			tempName = mstUser.getNamecn();
		}

		mv.addObject("tempName",tempName);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("edit end run(s): " + 
				DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * @Title: savePermission
	 * @Description: 保存权限信息
	 * @param @param request
	 * @param @return    设定文件
	 * @return Object    返回类型
	 * @throws MyException 
	 */
	@RequestMapping("savePermission")
	@ResponseBody
	public Object savePermission(HttpServletRequest request) throws MyException {
		Date startDate = new Date();
		logger.info("savePermission begin : " + startDate);

		String result = CommonEnum.MSGCODE_FAILED; 

		String masterType = request.getParameter("masterType");
		String masterValue = request.getParameter("masterValue");
		String menuIds = request.getParameter("menuIds");
		String buttonIds = request.getParameter("buttonIds");

		MstUser mstUser = this.getMySession().getMstUser();

		try {
			mstPermissionService.savePermission(masterType, masterValue, menuIds, buttonIds,mstUser);

			result = CommonEnum.MSGCODE_SUCCESS; 
		} catch (Exception e) {
			logger.error("savePermission error :　", e);

			result = CommonEnum.MSGCODE_FAILED; 
		}

		logger.info("savePermission run end(s) : " + DateUtil.calLastedTime(startDate));

		return result;
	}
}
