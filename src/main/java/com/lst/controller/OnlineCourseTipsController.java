/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：OnlineCourseTipsController   
 * 类描述： 在线课程训练提示语  
 * 创建人：zhangl  
 * 创建时间：2016年12月30日 上午9:34:18   
 * 修改人：zhangl   
 * 修改时间：2016年12月30日 上午9:34:18   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.FolPara;
import com.lst.model.MstUser;
import com.lst.model.OnlineCourseTips;
import com.lst.service.OnlineCourseTipsService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: OnlineCourseTipsController
 * @Description:在线课程训练提示语
 * @author zhangl
 * @date 2016年12月30日 上午9:34:18
 *
 */
@Controller
@RequestMapping("onlineCourseTipsController")
public class OnlineCourseTipsController extends BaseController {

	@Autowired
	private OnlineCourseTipsService onlineCourseTipsService;

	/**
	 * 
	 * @Title: list
	 * @Description: 页面加载
	 * @param @param request
	 * @param @param response
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @param title
	 * @param @param courseId
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE) int page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) int rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = "") String title,
			@RequestParam(required = true) Integer courseId) throws MyException {
		Date startDate = new Date();
		logger.info("list begin: " + startDate);

		FolPara folPara = new FolPara();
		folPara.setMenuId(menuId);
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setTitleFol(title);
		folPara.setCourseId(courseId);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_KUNGFU + "tip_list");
		mv.addObject("folPara",folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("list end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: queryList
	 * @Description: 数据集
	 * @param @param page
	 * @param @param pageSize
	 * @param @param sort
	 * @param @param order
	 * @param @param courseId
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return Object    返回类型
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer pageSize,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "")String courseId) throws MyException {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("isDeleted", 0);
		reqMap.put("enabled", 1);
		reqMap.put("courseId", courseId);

		if(StringUtils.isEmpty(sort)){
			reqMap.put("orderByClause","tip.createdate desc");
		} else{
			reqMap.put("orderByClause",sort + " " + order);
		}

		PageBounds pageBounds = new PageBounds(page, pageSize);
		List<OnlineCourseTips> list = onlineCourseTipsService.queryByList(reqMap, pageBounds);
		
		for(OnlineCourseTips oc : list){
			oc.setTips(CommonUtils.formatStr(oc.getTips(), "● ", "\r\n"));
		}
		
		PageList<OnlineCourseTips> pageList = (PageList<OnlineCourseTips>) list;
		Paginator paginator = pageList.getPaginator();

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		indexPages.put("total", paginator.getTotalCount());	
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}

	/**
	 * 
	 * @Title: edit
	 * @Description:编辑
	 * @param @param id
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView edit(
			@RequestParam(required = false) String id,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) {
		Date startDate = new Date();
		logger.info("edit begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_SYSTEM + "role_edit");

		if (!StringUtils.isBlank(id)) {
			OnlineCourseTips onlineCourseTips = onlineCourseTipsService.selectByPrimaryKey(Integer.parseInt(id));
			mv.addObject("onlineCourseTips", onlineCourseTips);
		}

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("edit end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: save
	 * @Description:保存
	 * @param @param onlineCourseTips
	 * @param @param folPara
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping(value = "/save",method = RequestMethod.POST)
	@ResponseBody
	public boolean save(OnlineCourseTips onlineCourseTips,FolPara folPara) throws MyException {
		Date startDate = new Date();
		logger.info("save begin: " + startDate);

		boolean flag = false;

		MstUser mstUser = this.getMySession().getMstUser();

		try {
			if (onlineCourseTips.getId() != null) {
				onlineCourseTips.setUpdatedate(startDate);
				onlineCourseTips.setUpdateuser(mstUser.getId());

				onlineCourseTipsService.updateByPrimaryKeySelective(onlineCourseTips);
			} else {
				onlineCourseTips.setEnabled(true);
				onlineCourseTips.setIsdeleted(false);
				onlineCourseTips.setCreatedate(startDate);
				onlineCourseTips.setCreateuser(mstUser.getId());

				onlineCourseTipsService.insert(onlineCourseTips);
			}
			
			flag = true;
		} catch (Exception e) {
			logger.error("save error:" ,e);
		}

		logger.info("save end run(s):" + DateUtil.calLastedTime(startDate));

		return flag;
	}

	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param request
	 * @param @param response
	 * @param @param ids
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping("delList")
	@ResponseBody
	public boolean delList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) String ids) throws MyException {
		Date startDate = new Date();
		logger.info("delList begin: " + startDate);

		boolean flag = false;

		MstUser mstUser = this.getMySession().getMstUser();

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("ids", ids);
		reqMap.put("updateUser", mstUser.getId());

		try {
			onlineCourseTipsService.delList(reqMap);
			flag = true;
		} catch (Exception e) {
			logger.error("delList error: " ,e);
		}

		logger.info("delList end run(s): " + DateUtil.calLastedTime(startDate));

		return flag;
	}
}
