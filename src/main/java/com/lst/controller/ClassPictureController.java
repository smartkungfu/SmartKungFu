package com.lst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.ClassPicture;
import com.lst.model.FolPara;
import com.lst.model.MstButton;
import com.lst.model.MstUser;
import com.lst.model.Resources;
import com.lst.service.ClassPictureService;
import com.lst.service.MstButtonService;
import com.lst.service.ResourcesService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: ClassPictureController
 * @Description: 课程分类封面
 *
 */
@Controller
@RequestMapping("/classPictureController")
public class ClassPictureController extends BaseController {
	
	@Autowired
	private MstButtonService mstButtonService;
	
	@Autowired
	private ClassPictureService classPictureService;
	
	@Autowired
	private ResourcesService resourcesService;
	
	/**
	 * 
	* @Title: Main
	* @Description: 列表页跳转
	* @param @param menuId
	* @param @param page
	* @param @param rows
	* @param @param index_exp
	* @param @param index_che
	* @param @param minDate
	* @param @param maxDate
	* @param @param typeFol
	* @param @param ariTypeFol
	* @param @return
	* @param @throws MyException    设定文件
	* @return ModelAndView    返回类型
	* @throws
	 */
	@RequestMapping(value = "/Main")
	public ModelAndView Main(
			@RequestParam(required = false) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE)Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "") String title) throws MyException {
		Date startDate = new Date();
		logger.info("Main begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_APP_MANAGE + "/classPicture_list");

		// 查询用户权限按钮
		MstUser mstUser = this.getMySession().getMstUser();
		List<MstButton> btns = CommonUtils.readPermissionWithBtn(menuId, mstUser, mstButtonService);

		//写参数
		FolPara folPara = new FolPara();
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setMinDate(minDate);
		folPara.setMaxDate(maxDate);
		folPara.setMenuId(menuId);
		folPara.setTitleFol(title);

		mv.addObject("btns", btns);
		mv.addObject("folPara", folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("Main end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	* @Title: queryList
	* @Description: 查询分页list
	* @param @param page
	* @param @param rows
	* @param @param sort
	* @param @param order
	* @param @param minDate
	* @param @param maxDate
	* @param @param type
	* @param @param ariType
	* @param @return    设定文件
	* @return Object    返回类型
	* @throws
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "") String title) {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		/**
		 * 查询课程动作信息
		 */
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("isDeleted", 0);
		reqMap.put("orderByClause","md.createDate desc");
		
		if(!StringUtils.isBlank(title)){
			reqMap.put("pictureName", title);
		}

		PageBounds pageBounds = new PageBounds(page, rows);
		List<ClassPicture> list =  classPictureService.queryByList(reqMap, pageBounds);

		PageList<ClassPicture> pageList = (PageList<ClassPicture>) list;
		Paginator paginator = pageList.getPaginator();

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		indexPages.put("total", paginator.getTotalCount());
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}
	
	/**
	 * 
	 * @Title: edit
	 * @Description:编辑页面
	 * @param @param id
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @return 设定文件
	 * @return ModelAndView 返回类型
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView edit(
			@RequestParam(required = false) String id,
			@RequestParam(required = false, defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false, defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) {
		Date startDate = new Date();
		logger.info("edit begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_APP_MANAGE
				+ "classPicture_edit");

		if (!StringUtils.isBlank(id)) {
			ClassPicture classPicture = classPictureService.selectByPrimaryKey(Integer.parseInt(id));
			mv.addObject("classPicture", classPicture);
		}

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("edit end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}
	
	/**
	 * 
	 * @Title: save
	 * @Description:保存
	 * @param @param folPara
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping(value = "/save",method = RequestMethod.POST)
	public String save(ClassPicture classPicture,FolPara folPara,String [] imagename,HttpServletRequest request) throws MyException {
		Date startDate = new Date();
		logger.info("save begin: " + startDate);

		MstUser mstUser = this.getMySession().getMstUser();

	
			if (classPicture.getId() != null) {
				classPictureService.updateByKey(classPicture);
			} else {
				classPicture.setIsDelete(false);
				classPicture.setCreateDate(startDate);
				classPictureService.insert(classPicture);
			}
			
		
		//图片资源信息维护
		try {
			ClassPicture imgWenw = new ClassPicture();
			imgWenw.setId(classPicture.getId());
           
			if(imagename != null && imagename.length != 0){
				Map<String, String> resMap = new HashMap<String,String>();
				resMap.put("userId", mstUser.getId().toString());
				resMap.put("ownerId", classPicture.getId().toString());
				resMap.put("tableName", "CLASSPICTURE");
				
								
				Map<String, Object> dataMap = CommonUtils.getResourcesData(request, imagename, resMap,CommonEnum.SYSCODE_REOURCES_TYPE_IMAGE,true);
				
				@SuppressWarnings("unchecked")
				List<Resources> resources = (List<Resources>) dataMap.get("resources");
		//		String reserved = (String) dataMap.get("zipFile");
				resourcesService.saveList(resources);

				//默认获取第一次作为常用图：String imagePath = resources.get(0).getRespath() + resources.get(0).getResname();
				//将所有图片获取出来
				String imagePath = "";
				for(Resources resource : resources){
					imagePath += resource.getRespath() + resource.getResname() + ";";
				}
				imgWenw.setPictureUrl(imagePath.substring(0 , imagePath.length() - 1));
			//	imgWenw.setCoverUrl2(reserved);
				
				classPictureService.updateByKey(imgWenw);
			}

		} catch (Exception e) {
			logger.error("save error: " ,e);
		}

		logger.info("save end run(s):" + DateUtil.calLastedTime(startDate));

		StringBuffer sb = new StringBuffer("?");
		sb.append("menuId=" + folPara.getMenuId());
		sb.append("&page=" + folPara.getPage());
		sb.append("&rows=" + folPara.getRows());
		
		sb.append("&index_exp=" + folPara.getIndex_exp());
		sb.append("&index_che=" + folPara.getIndex_che());

		logger.info("save end run(s):" + DateUtil.calLastedTime(startDate));

		return "redirect:Main" + sb.toString();
	}
	
	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param request
	 * @param @param response
	 * @param @param ids
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping("delList")
	@ResponseBody
	public boolean delList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) String ids) throws MyException {
		Date startDate = new Date();
		logger.info("delList begin: " + startDate);

		boolean flag = false;

	//	MstUser mstUser = this.getMySession().getMstUser();

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("ids", ids);

		try {
			classPictureService.delList(reqMap);
			flag = true;
		} catch (Exception e) {
			logger.error("delList error: " ,e);
		}

		logger.info("delList end run(s): " + DateUtil.calLastedTime(startDate));

		return flag;
	}
}
