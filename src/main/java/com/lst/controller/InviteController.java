/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：InviteController   
 * 类描述： 邀请好友控制类  
 * 创建人：zhangl  
 * 创建时间：2017年1月12日 下午5:37:42   
 * 修改人：zhangl   
 * 修改时间：2017年1月12日 下午5:37:42   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.Error404Exception;
import com.lst.model.MstClient;
import com.lst.service.MstClientService;
import com.lst.utils.ConfigProp;
import com.lst.utils.DateUtil;

/**
 * @ClassName: InviteController
 * @Description: 邀请好友控制类
 * @author zhangl
 * @date 2017年1月12日 下午5:37:42
 *
 */
@Controller
@RequestMapping("inviteController")
public class InviteController extends BaseController {

	@Autowired
	private MstClientService mstClientService;

	/**
	 * 
	 * @Title: invite
	 * @Description:邀请好友页面跳转
	 * @param @param userid
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 * @throws Error404Exception 
	 */
	@RequestMapping("invite")
	public ModelAndView invite(@RequestParam(required = true)Integer userid) throws Error404Exception{
		Date startDate = new Date();
		logger.info("invite begin: " + startDate);

		ModelAndView mv = new ModelAndView();

		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/invite");

		MstClient mstClient = mstClientService.selectByPrimaryKey(userid);

		if(mstClient == null){
			throw new Error404Exception();
		} 

		mv.addObject("mstClient", mstClient);

		logger.info("invite end spent(s): "+ DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: sendSms
	 * @Description: 短信发送接口
	 * @param @param mobileno
	 * @param @param userid
	 * @param @param receivetype
	 * @param @param language
	 * @param @param code
	 * @param @return    设定文件
	 * @return SendSMS    返回类型
	 */
	@RequestMapping(value = "sendSms" ,method = RequestMethod.POST)
	@ResponseBody
	public SendSMS sendSms(
			@RequestParam(required = true)  String mobileno,
			@RequestParam(required = false, defaultValue = "0")String userid,
			@RequestParam(required = true)  String receivetype,
			@RequestParam(required = false, defaultValue = "captcha")String mirror){
		Date startDate = new Date();
		logger.info("sendSms begin: " + startDate);

		StringBuffer sb = new StringBuffer("?");
		sb.append("mobileno=" + mobileno);
		sb.append("&userid=" + userid);
		sb.append("&receivetype=" +receivetype);
		sb.append("&mirror=" + mirror);

		SendSMS ss = new SendSMS();

		CloseableHttpClient httpclient = HttpClients.createDefault();  
		// 创建httppost    
		//HttpGet httpget = new HttpGet(CommonEnum.SMS_URL + sb.toString());  
		HttpGet httpget = new HttpGet(ConfigProp.getSMS_URL() + sb.toString());  

		try {  

			logger.info("executing request: " + httpget.getURI());

			CloseableHttpResponse response = httpclient.execute(httpget);  

			try {  
				HttpEntity entity = response.getEntity();  
				if (entity != null) {  
					String result =  EntityUtils.toString(entity, "UTF-8");
					logger.info("--------------------------------------");  
					logger.info("Response content: " + result);  
					logger.info("--------------------------------------"); 
					Gson gson = new Gson();
					ss = gson.fromJson(result, SendSMS.class);
				}  
			} finally {  
				response.close();  
			}  
		} catch (ClientProtocolException e) {  
			logger.info("sendSms error: " ,e); 
		} catch (UnsupportedEncodingException e1) {  
			logger.info("sendSms error: " ,e1);   
		} catch (IOException e) {  
			logger.info("sendSms error: " ,e); 
		} finally {  
			//关闭连接,释放资源    
			try {  
				httpclient.close();  
			} catch (IOException e) {  
				e.printStackTrace();  
			}  
		}  

		logger.info("sendSms end run(s): " + DateUtil.calLastedTime(startDate));

		return ss;
	}

	/**
	 * 
	 * @Title: sendSms
	 * @Description: 短信发送接口
	 * @param @param mobileno
	 * @param @param userid
	 * @param @param receivetype
	 * @param @param language
	 * @param @param code
	 * @param @return    设定文件
	 * @return SendSMS    返回类型
	 */
	@RequestMapping(value = "inviteRegister" ,method = RequestMethod.POST)
	@ResponseBody
	public SendSMS inviteRegister(
			@RequestParam(required = true)  String mobileno,
			@RequestParam(required = true)String userid,
			@RequestParam(required = true)  String password){
		Date startDate = new Date();
		logger.info("sendSms begin: " + startDate);

		StringBuffer sb = new StringBuffer("?");
		sb.append("mobileno=" + mobileno);
		sb.append("&userid=" + userid);
		sb.append("&password=" +password);

		SendSMS ss = new SendSMS();

		CloseableHttpClient httpclient = HttpClients.createDefault();  
		// 创建httppost    
		//HttpGet httpget = new HttpGet(CommonEnum.REGISTER_URL + sb.toString());  
		HttpGet httpget = new HttpGet(ConfigProp.getREGISTER_URL() + sb.toString());  
		
		try {  

			logger.info("executing request: " + httpget.getURI());

			CloseableHttpResponse response = httpclient.execute(httpget);  

			try {  
				HttpEntity entity = response.getEntity();  
				if (entity != null) {  
					String result =  EntityUtils.toString(entity, "UTF-8");
					logger.info("--------------------------------------");  
					logger.info("Response content: " + result);  
					logger.info("--------------------------------------"); 
					Gson gson = new Gson();
					ss = gson.fromJson(result, SendSMS.class);
				}  
			} finally {  
				response.close();  
			}  
		} catch (ClientProtocolException e) {  
			logger.info("sendSms error: " ,e); 
		} catch (UnsupportedEncodingException e1) {  
			logger.info("sendSms error: " ,e1);   
		} catch (IOException e) {  
			logger.info("sendSms error: " ,e); 
		} finally {  
			//关闭连接,释放资源    
			try {  
				httpclient.close();  
			} catch (IOException e) {  
				e.printStackTrace();  
			}  
		}  

		logger.info("sendSms end run(s): " + DateUtil.calLastedTime(startDate));

		return ss;
	}

	/**
	 * 
	 * @ClassName: SendSMS
	 * @Description: 短信类容接收类
	 * @author zhangl
	 * @date 2016年8月24日 下午12:22:29
	 *
	 */
	private class SendSMS{
		private String code;
		private String message;
		private String captcha;
		private String usetime;

		@SuppressWarnings("unused")
		public String getCode() {
			return code;
		}
		@SuppressWarnings("unused")
		public void setCode(String code) {
			this.code = code;
		}
		@SuppressWarnings("unused")
		public String getMessage() {
			return message;
		}
		@SuppressWarnings("unused")
		public void setMessage(String message) {
			this.message = message;
		}
		@SuppressWarnings("unused")
		public String getCaptcha() {
			return captcha;
		}
		@SuppressWarnings("unused")
		public void setCaptcha(String captcha) {
			this.captcha = captcha;
		}
		@SuppressWarnings("unused")
		public String getUsetime() {
			return usetime;
		}
		@SuppressWarnings("unused")
		public void setUsetime(String usetime) {
			this.usetime = usetime;
		}

	}
}
