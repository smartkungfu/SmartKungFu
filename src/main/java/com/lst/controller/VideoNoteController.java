/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：VideoNoteController   
 * 类描述：视频节点   
 * 创建人：zhangl  
 * 创建时间：2017年1月11日 下午1:59:00   
 * 修改人：zhangl   
 * 修改时间：2017年1月11日 下午1:59:00   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.FolPara;
import com.lst.model.MstUser;
import com.lst.model.Resources;
import com.lst.model.VideoNote;
import com.lst.service.ResourcesService;
import com.lst.service.VideoNoteService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: VideoNoteController
 * @Description: 视频节点
 * @author zhangl
 * @date 2017年1月11日 下午1:59:00
 *
 */
@Controller
@RequestMapping("videoNoteController")
public class VideoNoteController extends BaseController {
	
	@Autowired
	private VideoNoteService videoNoteService;
	
	@Autowired
	private ResourcesService resourcesService;

	/**
	 * 
	 * @Title: list
	 * @Description: 页面加载
	 * @param @param request
	 * @param @param response
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @param title
	 * @param @param courseId
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE) int page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) int rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = "") String title,
			@RequestParam(required = true) Integer courseId) throws MyException {
		Date startDate = new Date();
		logger.info("list begin: " + startDate);

		FolPara folPara = new FolPara();
		folPara.setMenuId(menuId);
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setTitleFol(title);
		folPara.setCourseId(courseId);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_KUNGFU + "note_list");
		mv.addObject("folPara",folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("list end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: queryList
	 * @Description: 数据集
	 * @param @param page
	 * @param @param pageSize
	 * @param @param sort
	 * @param @param order
	 * @param @param courseId
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return Object    返回类型
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer pageSize,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "")String viid) throws MyException {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("isDeleted", 0);
		reqMap.put("enabled", 1);
		reqMap.put("viId", viid);

		if(StringUtils.isEmpty(sort)){
			reqMap.put("orderByClause","vn.reserved1 asc");
		} else{
			reqMap.put("orderByClause",sort + " " + order);
		}

		PageBounds pageBounds = new PageBounds();
		List<VideoNote> list = videoNoteService.queryByList(reqMap, pageBounds);

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		indexPages.put("total",list.size());	
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}

	/**
	 * 
	 * @Title: edit
	 * @Description:编辑
	 * @param @param id
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView edit(
			@RequestParam(required = false) String id,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) {
		Date startDate = new Date();
		logger.info("edit begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_SYSTEM + "role_edit");

		if (!StringUtils.isBlank(id)) {
			VideoNote videoNote = videoNoteService.selectByPrimaryKey(Integer.parseInt(id));
			mv.addObject("videoNote", videoNote);
		}

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("edit end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: save
	 * @Description:保存
	 * @param @param onlineCourseTips
	 * @param @param folPara
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping(value = "/save",method = RequestMethod.POST)
	@ResponseBody
	public boolean save(VideoNote videoNote,FolPara folPara,Integer minute,Integer seconds,String [] imagename,HttpServletRequest request) throws MyException {
		Date startDate = new Date();
		logger.info("save begin: " + startDate);

		boolean flag = false;

		MstUser mstUser = this.getMySession().getMstUser();

	
			if (videoNote.getId() != null) {
				videoNote.setUpdatedate(startDate);
				videoNote.setUpdateuser(mstUser.getId());
				videoNote.setWorktime(new BigDecimal(minute * 60 + seconds).add(videoNote.getReserved4().divide(new BigDecimal(1000))));
				videoNote.setReserved9(minute.toString());
				videoNote.setReserved10(seconds.toString());

				videoNoteService.updateByPrimaryKeySelective(videoNote);
			} else {
				videoNote.setEnabled(true);
				videoNote.setIsdeleted(false);
				videoNote.setCreatedate(startDate);
				videoNote.setCreateuser(mstUser.getId());
				
				videoNote.setWorktime(new BigDecimal(minute * 60 + seconds).add(videoNote.getReserved4().divide(new BigDecimal(1000))));
				
				videoNote.setReserved9(minute.toString());
				videoNote.setReserved10(seconds.toString());
				videoNoteService.insert(videoNote);
			}
			
			flag = true;
	
		
		//图片资源信息维护
		try {
			VideoNote imgWenw = new VideoNote();
			imgWenw.setUpdatedate(startDate);
			imgWenw.setUpdateuser(mstUser.getId());
			imgWenw.setId(videoNote.getId());
           
			if(imagename != null && imagename.length != 0){
				Map<String, String> resMap = new HashMap<String,String>();
				resMap.put("userId", mstUser.getId().toString());
				resMap.put("ownerId", videoNote.getId().toString());
				resMap.put("tableName", "VIDEO_NOTE");
				
								
				Map<String, Object> dataMap = CommonUtils.getResourcesData(request, imagename, resMap,CommonEnum.SYSCODE_REOURCES_TYPE_IMAGE,true);
				
				@SuppressWarnings("unchecked")
				List<Resources> resources = (List<Resources>) dataMap.get("resources");
				String reserved12 = (String) dataMap.get("zipFile");
				resourcesService.saveList(resources);

				//默认获取第一次作为常用图：String imagePath = resources.get(0).getRespath() + resources.get(0).getResname();
				//将所有图片获取出来
				String imagePath = "";
				for(Resources resource : resources){
					imagePath += resource.getRespath() + resource.getResname() + ";";
				}
				imgWenw.setReserved11(imagePath.substring(0 , imagePath.length() - 1));
				imgWenw.setReserved12(reserved12);
				
				videoNoteService.updateByPrimaryKeySelective(imgWenw);
			}

		} catch (Exception e) {
			logger.error("save error: " ,e);
		}

		logger.info("save end run(s):" + DateUtil.calLastedTime(startDate));

		return flag;
	}

	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param request
	 * @param @param response
	 * @param @param ids
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping("delList")
	@ResponseBody
	public boolean delList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) String ids) throws MyException {
		Date startDate = new Date();
		logger.info("delList begin: " + startDate);

		boolean flag = false;

		MstUser mstUser = this.getMySession().getMstUser();

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("ids", ids);
		reqMap.put("updateUser", mstUser.getId());

		try {
			videoNoteService.delList(reqMap);
			flag = true;
		} catch (Exception e) {
			logger.error("delList error: " ,e);
		}

		logger.info("delList end run(s): " + DateUtil.calLastedTime(startDate));

		return flag;
	}
}
