/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：ProjectInfoController   
 * 类描述： 活动控制类   
 * 创建人：zhangl  
 * 创建时间：2016年10月18日 下午5:03:40   
 * 修改人：zhangl   
 * 修改时间：2016年10月18日 下午5:03:40   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.FolPara;
import com.lst.model.MstButton;
import com.lst.model.MstRoll;
import com.lst.model.ProjectDetail;
import com.lst.model.ProjectInfo;
import com.lst.model.MstUser;
import com.lst.model.Resources;
import com.lst.service.MstButtonService;
import com.lst.service.MstRollService;
import com.lst.service.ProjectDetailService;
import com.lst.service.ProjectInfoService;
import com.lst.service.ResourcesService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: ProjectInfoController
 * @Description: 活动控制类
 * @author zhangl
 * @date 2016年10月18日 下午5:03:40
 *
 */
@Controller
@RequestMapping("projectInfoController")
public class ProjectInfoController extends BaseController {

	@Autowired
	private MstButtonService mstButtonService;

	@Autowired
	private ProjectInfoService projectInfoService;

	@Autowired
	private ResourcesService resourcesService;

	@Autowired
	private ProjectDetailService projectDetailService;

	@Autowired
	private MstRollService mstRollService;

	/**
	 * 
	 * @Title: list
	 * @Description: 页面加载
	 * @param @param request
	 * @param @param response
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @param minDate
	 * @param @param maxDate
	 * @param @param titleFol
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE) int page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) int rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "") String titleFol) throws MyException {
		Date startDate = new Date();
		logger.info("list begin: " + startDate);

		FolPara folPara = new FolPara();
		folPara.setMenuId(menuId);
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setMinDate(minDate);
		folPara.setMaxDate(maxDate);
		folPara.setTitleFol(titleFol);

		MstUser mstUser = this.getMySession().getMstUser();
		List<MstButton> btns = CommonUtils.readPermissionWithBtn(menuId, mstUser, mstButtonService);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_KUNGFU + "project_list");
		mv.addObject("btns", btns);
		mv.addObject("folPara",folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("list end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: queryList
	 * @Description:queryList
	 * @param @param page
	 * @param @param pageSize
	 * @param @param rolName
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return Object    返回类型
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer pageSize,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "")String minDate,
			@RequestParam(required = false,defaultValue = "")String maxDate,
			@RequestParam(required = false,defaultValue = "")String title) throws MyException {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("isDeleted", 0);
		reqMap.put("enabled", 1);

		if(StringUtils.isEmpty(sort)){
			reqMap.put("orderByClause","pi.createdate desc");
		} else{
			reqMap.put("orderByClause",sort + " " + order);
		}

		if(!StringUtils.isBlank(title)){
			reqMap.put("_title", title.trim());
		}

		if(!StringUtils.isBlank(minDate) && !StringUtils.isBlank(maxDate)){
			try {
				if(DateUtil.parse(minDate).after(DateUtil.parse(maxDate))){
					reqMap.put("minDate", maxDate + DateUtil.START_DATE_HMS);
					reqMap.put("maxDate", minDate + DateUtil.END_DATE_HMS) ;
				}else{
					reqMap.put("minDate", minDate + DateUtil.START_DATE_HMS);
					reqMap.put("maxDate", maxDate + DateUtil.END_DATE_HMS);
				}
			} catch (ParseException e) {
				logger.error("queryList error: " ,e);
			}
		}	

		PageBounds pageBounds = new PageBounds(page, pageSize);
		List<ProjectInfo> list = projectInfoService.queryByList(reqMap, pageBounds);

		List<MstRoll> rolls  = new ArrayList<>();

		for (ProjectInfo pro : list) {
			//查询优惠券
			reqMap.clear();
			reqMap.put("enabled", true);
			reqMap.put("isDeleted", false);
			reqMap.put("type", CommonEnum.SYSCODE_YHQ_TYPE_HD); //优惠券类型 ： 活动			
			reqMap.put("resourceId", pro.getId());
			reqMap.put("orderByClause","r.createdate desc");
			rolls = mstRollService.queryByList(reqMap, pageBounds);
			pro.setRolls(rolls);

			//格式化活动亮点、温馨提示
			pro.setLightspot(CommonUtils.formatStr(pro.getLightspot(), "● ", "\r\n"));
			pro.setWarning(CommonUtils.formatStr(pro.getWarning(), "● ", "\r\n"));
		}

		PageList<ProjectInfo> pageList = (PageList<ProjectInfo>) list;
		Paginator paginator = pageList.getPaginator();

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		indexPages.put("total", paginator.getTotalCount());	
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}


	/**
	 * 
	 * @Title: info
	 * @Description: 详情查询
	 * @param @param id
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/info" , method = RequestMethod.GET)
	public ModelAndView info(
			@RequestParam(required = true) Integer id,
			@RequestParam(required = false,defaultValue = "") String mobileNo,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) {
		Date startDate = new Date();
		logger.info("info begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_KUNGFU + "project_info");

		ProjectInfo projectInfo = projectInfoService.selectByPrimaryKey(id);
		mv.addObject("projectInfo", projectInfo);

		Map<String, Object> proMap = new HashMap<String, Object>();
		proMap.put("enabled",1);
		proMap.put("isDeleted", 0);
		proMap.put("proId", id);
		proMap.put("orderByClause", "pd.createdate desc");

		if(!StringUtils.isEmpty(mobileNo)){
			proMap.put("_mobileNo", "mobileNo");
		}

		PageBounds pageBounds = new PageBounds();

		List<ProjectDetail> pds = projectDetailService.queryByList(proMap, pageBounds);

		mv.addObject("pds", pds);

		logger.info("info end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: edit
	 * @Description:编辑页面
	 * @param @param id
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView edit(
			@RequestParam(required = false) String id,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) {
		Date startDate = new Date();
		logger.info("edit begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_KUNGFU + "project_edit");

		if (!StringUtils.isBlank(id)) {
			ProjectInfo projectInfo = projectInfoService.selectByPrimaryKey(Integer.parseInt(id));

			//活动详细内容、活动详细图片
			List<ProjectDetailVo> details = new ArrayList<ProjectDetailVo>();
			if(!StringUtils.isEmpty(projectInfo.getReserved10())){
				String [] contents =  projectInfo.getReserved10().split("$");
				String [] imgs =  projectInfo.getReserved11().split(";");
				for(int i = 0 ; i < imgs.length ;i ++){
					if(!StringUtils.isEmpty(imgs[i])){
						ProjectDetailVo pd = new ProjectDetailVo();
						pd.setContent(contents[i]);
						pd.setImage(imgs[i]);
						
						details.add(pd);
					}
				}
			}

			mv.addObject("projectInfo", projectInfo);
			mv.addObject("details", details);
		}

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("edit end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: save
	 * @Description:save
	 * @param @param ProjectInfo
	 * @param @param folPara
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return String    返回类型
	 */
	@RequestMapping(value = "/save",method = RequestMethod.POST)
	public String save(@Valid ProjectInfo projectInfo,BindingResult br,FolPara folPara,String [] imagename,@RequestParam(required=false) MultipartFile [] files,String [] contents,HttpServletRequest request) throws MyException {
		Date startDate = new Date();
		logger.info("save begin: " + startDate);

		MstUser mstUser = this.getMySession().getMstUser();

		//处理\t问题
		if(!StringUtils.isEmpty(projectInfo.getLightspot())){
			projectInfo.setLightspot(projectInfo.getLightspot().trim());
		}

		//拦截非法请求参数
		if (br.hasErrors()){
			List<ObjectError> ers = br.getAllErrors();
			logger.info("--------------form parameter error start-----------------------");

			for(ObjectError er : ers){
				logger.info("save faile with parameter illegal: " + er.getDefaultMessage() );
			}

			logger.info("--------------form parameter error end-----------------------");
		} else {
			if (projectInfo.getId() != null) {
				projectInfo.setUpdatedate(startDate);
				projectInfo.setUpdateuser(mstUser.getId());

				projectInfoService.updateByPrimaryKeySelective(projectInfo);
			} else {
				projectInfo.setEnabled(true);
				projectInfo.setIsdeleted(false);
				projectInfo.setCreatedate(startDate);
				projectInfo.setCreateuser(mstUser.getId());
				projectInfo.setUpdatedate(startDate);

				projectInfoService.insert(projectInfo);
			}

			//图片资源信息维护
			try {
				if(imagename != null && imagename.length != 0){
					Map<String, String> resMap = new HashMap<String,String>();
					resMap.put("userId", mstUser.getId().toString());
					resMap.put("ownerId", projectInfo.getId().toString());
					resMap.put("tableName", "PROJECT_INFO");

					Map<String, Object> dataMap = CommonUtils.getResourcesData(request, imagename, resMap,CommonEnum.SYSCODE_REOURCES_TYPE_IMAGE,true);
					@SuppressWarnings("unchecked")
					List<Resources> resources = (List<Resources>) dataMap.get("resources");

					String reserved12 = (String) dataMap.get("zipFile");
					resourcesService.saveList(resources);

					//将所有图片获取出来
					String imagePath = "";
					for(Resources resource : resources){
						imagePath += resource.getRespath() + resource.getResname() + ";";
					}

					ProjectInfo imgPro = new ProjectInfo();
					imgPro.setUpdatedate(startDate);
					imgPro.setUpdateuser(mstUser.getId());
					imgPro.setId(projectInfo.getId());
					imgPro.setProbill(imagePath);

					imgPro.setReserved12(reserved12);

					projectInfoService.updateByPrimaryKeySelective(imgPro);
				}

				if(files != null && files.length != 0){

					String [] details = new String [files.length];
					for(int i = 0 ; i < files.length ;i++ ){
						if(!StringUtils.isEmpty(files[i].getOriginalFilename() )){
							details[i] = CommonUtils.saveTempImage(files[i], request);
						} else {
							details[i] = "";
						}
					}

					Map<String, String> resMap = new HashMap<String,String>();
					resMap.put("userId", mstUser.getId().toString());
					resMap.put("ownerId", projectInfo.getId().toString());
					resMap.put("tableName", "PROJECT_INFO");

					Map<String, Object> dataMap = CommonUtils.getResourcesData(request, details, resMap,CommonEnum.SYSCODE_REOURCES_TYPE_IMAGE,false);
					@SuppressWarnings("unchecked")
					List<Resources> resources = (List<Resources>) dataMap.get("resources");

					if(!resources.isEmpty()){
						resourcesService.saveList(resources);

						String contentFiled = "";
						for(String str : contents){
							contentFiled += str + "$";
						}

						String imageFiled = "";
						for(Resources resource : resources){
							imageFiled += resource.getRespath() + resource.getResname() + ";";
						}

						List<String> imagePath = new ArrayList<String>();
						for(Resources resource : resources){
							imagePath.add(resource.getRespath() + resource.getResname());
						}

						ProjectInfo imgPro = new ProjectInfo();
						imgPro.setUpdatedate(startDate);
						imgPro.setUpdateuser(mstUser.getId());
						imgPro.setId(projectInfo.getId());
						imgPro.setReserved10(contentFiled);
						imgPro.setReserved11(imageFiled);

						projectInfoService.updateByPrimaryKeySelective(imgPro);
					}
				}
			} catch (Exception e) {
				logger.error("save error: " ,e);
			}

		}

		StringBuffer sb = new StringBuffer("?");
		sb.append("menuId=" + folPara.getMenuId());
		sb.append("&page=" + folPara.getPage());
		sb.append("&rows=" + folPara.getRows());
		sb.append("&minDate=" + folPara.getMinDate());
		sb.append("&maxDate=" + folPara.getMaxDate());
		sb.append("&titleFol=" + folPara.getTitleFol());

		sb.append("&index_exp=" + folPara.getIndex_exp());
		sb.append("&index_che=" + folPara.getIndex_che());

		logger.info("save end run(s):" + DateUtil.calLastedTime(startDate));

		return "redirect:list" + sb.toString();
	}

	/**
	 * 
	 * @Title: isUnique
	 * @Description: 验证属性唯一性
	 * @param @param property 
	 * @param @param id
	 * @param @return    设定文件
	 * @return Boolean    返回类型
	 */
	@RequestMapping(value = "/isUnique",method = RequestMethod.GET)
	@ResponseBody
	public Boolean isUnique(
			@RequestParam(required = true)String property,
			@RequestParam(required = false,defaultValue = "")String id){
		Date startDate = new Date();
		logger.info("isUnique begin: " + startDate);

		boolean flag = false;

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("enabled", 1);
		reqMap.put("isDeleted", 0);
		reqMap.put("title", property);

		List<ProjectInfo> pros= projectInfoService.queryByList(reqMap, new PageBounds());

		if(pros.isEmpty()) flag = true;

		else{
			if(!StringUtils.isBlank(id)){//当为编辑操作时，存在唯一一个相同的名称名，验证通过
				if(pros.get(0).getId().equals(Integer.valueOf(id))){
					flag = true;
				}
			}
		}
		logger.info("isUnique end run(s): " 
				+ DateUtil.calLastedTime(startDate));

		return flag;
	}


	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param request
	 * @param @param response
	 * @param @param ids
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping("delList")
	@ResponseBody
	public boolean delList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) String ids) throws MyException {
		Date startDate = new Date();
		logger.info("delList begin: " + startDate);

		boolean flag = false;

		MstUser mstUser = this.getMySession().getMstUser();

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("ids", ids);
		reqMap.put("updateUser", mstUser.getId());

		try {
			projectInfoService.delList(reqMap);
			flag = true;
		} catch (Exception e) {
			logger.error("delList error: " ,e);
		}

		logger.info("delList end run(s): " + DateUtil.calLastedTime(startDate));

		return flag;
	}

	/**
	 * 
	 * @ClassName: ProjectDetail
	 * @Description: 临时类
	 * @author zhangl
	 * @date 2017年1月10日 下午6:36:03
	 *
	 */
	public class ProjectDetailVo {
		private String content;

		private String image;

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}

	}

}
