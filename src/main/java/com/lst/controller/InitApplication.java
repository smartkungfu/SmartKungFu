/**   
 *    
 * 项目名称：MedicalTeam   
 * 类名称：InitApplication   
 * 类描述：   
 * 创建人：zhangl  
 * 创建时间：2016年9月13日 下午6:08:34   
 * 修改人：zhangl   
 * 修改时间：2016年9月13日 下午6:08:34   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.util.Date;
import java.util.Timer;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.ServletContextAware;

import com.lst.common.BaseController;
import com.lst.model.DataTimerTask;
import com.lst.service.MstRollService;

/**
 * @ClassName: InitApplication
 * @Description: 初始化应用程序
 * @author zhangl
 * @date 2016年9月13日 下午6:08:34
 *
 */
@Controller
public class InitApplication extends BaseController implements ServletContextAware {

	@Autowired
	private MstRollService mstRollService ; 
	/* (非 Javadoc)
	 * <p>Title: setServletContext</p>
	 * <p>Description: </p>
	 * @param servletContext
	 * @see org.springframework.web.context.ServletContextAware#setServletContext(javax.servlet.ServletContext)
	 */
	@Override
	public void setServletContext(ServletContext servletContext) {
		Date startDate = new Date();
		logger.info("setServletContext start: " + startDate);

		// 定时模拟数据，1天数据更新
		new Timer().schedule(new DataTimerTask(servletContext,mstRollService), 1, 1000 * 60 * 60 * 24L);
		
		logger.info("setServletContext end: " + com.lst.utils.DateUtil.calLastedTime(startDate));
	}

}
