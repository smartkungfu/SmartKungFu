/**
 * 项目名称：SmartKungFu
 * 类名称：ShareContentController
 * 类描述：分享内容
 * 创建人：WangXin
 * 创建时间：2016年12月13日 下午4:58:45
 * 修改人：WangXin
 * 修改时间：2016年12月13日 下午4:58:45
 * 修改备注：
 * @version
 */
package com.lst.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.google.gson.Gson;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.Error404Exception;
import com.lst.model.Answer;
import com.lst.model.Companion;
import com.lst.model.CoursePackage;
import com.lst.model.Dynamic;
import com.lst.model.GymnasiumDetail;
import com.lst.model.MstClient;
import com.lst.model.MstDiscuss;
import com.lst.model.MstGymnasium;
import com.lst.model.OnlineCourse;
import com.lst.model.ProjectInfo;
import com.lst.model.Question;
import com.lst.model.VideoInfo;
import com.lst.model.WenwInfo;
import com.lst.model.vo.ImgAndRemarkVo;
import com.lst.model.vo.OutcomeVo;
import com.lst.model.vo.TestVo;
import com.lst.service.AnswerService;
import com.lst.service.ClientNumService;
import com.lst.service.CompanionService;
import com.lst.service.CoursePackageService;
import com.lst.service.DynamicService;
import com.lst.service.GymnasiumDetailService;
import com.lst.service.MstClientService;
import com.lst.service.MstDiscussService;
import com.lst.service.MstGymnasiumService;
import com.lst.service.OnlineCourseService;
import com.lst.service.ProjectInfoService;
import com.lst.service.QuestionService;
import com.lst.service.VideoInfoService;
import com.lst.service.WenwInfoService;
import com.lst.utils.ConfigProp;

/**
 * @ClassName: ShareContentController
 * @Description: 分享内容
 * @author WangXin
 * @date 2016年12月13日 下午4:58:45
 */
@Controller
@RequestMapping("shareContentController")
public class ShareContentController extends BaseController {
	
	@Autowired
	private MstClientService mtsClientService;

	@Autowired
	private WenwInfoService wenwInfoService;

	@Autowired
	private VideoInfoService videoInfoService;

	@Autowired
	private MstGymnasiumService mstGymnasiumService;

	@Autowired
	private GymnasiumDetailService gymnasiumDetailService;

	@Autowired
	private CoursePackageService coursePackageService;

	@Autowired
	private ClientNumService clientNumService;
	
	@Autowired
	private MstDiscussService mstDiscussService;

	@Autowired
	private ProjectInfoService projectInfoService;
	
	@Autowired
	private OnlineCourseService onlineCourseService;
	
	@Autowired
	private AnswerService answerService;
	
	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private DynamicService dynamicService;
	
	@Autowired
	private CompanionService companionService;

	@RequestMapping(value = "/view")
	public ModelAndView view(@RequestParam(required = false) Integer id, @RequestParam(required = false) String type,@RequestParam(required = false) String op) throws Error404Exception {
		ModelAndView mv = new ModelAndView();
		
		try {
			if (id != null) {
				if ("wenw".equals(type)) {
					return shareForWenw(id, mv);
				}
				if ("wenwShare".equals(type)) {
					return shareForWenw2(id, mv);
				}

				if ("video".equals(type)) {
					shareForVideo(id, mv);
				}
				
				if ("videoShare".equals(type)) {
					shareForVideo2(id, mv);
				}
				
				if ("online_course".equals(type)) {
					shareForOnline(id, mv);
				}
				
				if ("answer".equals(type)) {
					shareForAnswer(id, mv);
				}
				
				if ("question".equals(type)) {
					shareForQuestion(id, mv);
				}
				
				if ("dynamic".equals(type)) {
					shareForDynamic(id, mv);
				}
				
				if ("companion".equals(type)) {
					shareForCompanion(id, mv);
				}

				if ("project".equals(type)) {
					return shareForPro(id, mv);
				}

				if ("gym".equals(type)) {
					return shareForGym(id, mv);
				}

				if ("course".equals(type)) {
					return shareForCourse(id, mv);
				}

				if ("help_one".equals(type)) {
					mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/help_one");

					return mv;
				}

				if ("help_two".equals(type)) {
					mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/help_two");

					return mv;
				}


				if("detail".equals(type)){
					detail(id, mv);

				}
				
				if("outcome".equals(type)){
					outcome(id, op, mv);
				}
			}
		} catch (Exception e) {
			logger.error("view error: " ,e);
			throw new Error404Exception();
		}

		return mv;
	}

	/**
	* @Title: outcome
	* @param @param id
	* @param @param op
	* @param @param mv    设定文件
	* @return void    返回类型
	* @throws
	*/
	private void outcome(Integer id, String op, ModelAndView mv) {
		//用户基本信息
		MstClient mstClient = mtsClientService.selectByPrimaryKey(Integer.valueOf(id));
		//用户测试信息
		StringBuffer sb = new StringBuffer("?");
		sb.append("userid=" + id);

		TempTest tt = new TempTest();

		CloseableHttpClient httpclient = HttpClients.createDefault();  
		
		HttpGet httpget = new HttpGet(ConfigProp.getMYTEST_URL() + sb.toString());  

		try {  

			logger.info("executing request: " + httpget.getURI());

			CloseableHttpResponse response = httpclient.execute(httpget);  

			try {  
				HttpEntity entity = response.getEntity();  
				if (entity != null) {  
					String result =  EntityUtils.toString(entity, "UTF-8");
					logger.info("--------------------------------------");  
					logger.info("Response content: " + result);  
					logger.info("--------------------------------------"); 
					Gson gson = new Gson();
					tt = gson.fromJson(result, TempTest.class);
				}  
			} finally {  
				response.close();  
			}  
		} catch (ClientProtocolException e) {  
			logger.info("outcome error: " ,e); 
		} catch (UnsupportedEncodingException e1) {  
			logger.info("outcome error: " ,e1);   
		} catch (IOException e) {  
			logger.info("outcome error: " ,e); 
		} finally {  
			//关闭连接,释放资源    
			try {  
				httpclient.close();  
			} catch (IOException e) {  
				e.printStackTrace();  
			}  
		} 
		
		List<TestVo> kungfuList = tt.getCv().getKungfulist();
		
		List<TestVo> bodyList = tt.getCv().getBodylist();
		//获取后三个元素
		List<TestVo> bodylistGoods = new ArrayList<TestVo>();
		//获取前三个元素
		List<TestVo> bodylistExpects = new ArrayList<TestVo>();
		//获取前三个元素
		List<TestVo> kungfuOks = new ArrayList<TestVo>();
		
		for(int i = 0 ; i < bodyList.size(); i ++){
			if(i < 3){
				bodylistExpects.add(bodyList.get(i));
			}
			
			if( i >= bodyList.size() - 3){
				bodylistGoods.add(bodyList.get(i));
			}
			
		}
		
		for(int i = 0 ; i < kungfuList.size(); i ++){
			
			if( i >= kungfuList.size() - 3){
				kungfuOks.add(kungfuList.get(i));
			}
		}
		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareOutcome");
		
		mv.addObject("bodylistGoods",bodylistGoods);
		mv.addObject("bodylistExpects",bodylistExpects);
		mv.addObject("kungfuOks",kungfuOks);
		mv.addObject("tt",tt);
		mv.addObject("op",op);
		mv.addObject("mstClient",mstClient);
	}

	/**
	* @Title: detail
	* @param @param id
	* @param @param mv    设定文件
	* @return void    返回类型
	* @throws
	*/
	private void detail(Integer id, ModelAndView mv) {
		ProjectInfo projectInfo = projectInfoService.selectByPrimaryKey(id);

		String [] images = projectInfo.getReserved11().split(";");
		String [] reamarks = projectInfo.getReserved10().split("\\$");

		List<ImgAndRemarkVo> irs = new ArrayList<ImgAndRemarkVo>();

		ImgAndRemarkVo ir = null;
		for(int i = 0 ; i < images.length;i++){
			if(!StringUtils.isEmpty(images[i])){
				ir = new ImgAndRemarkVo();
				ir.setImage(images[i]);

				try {
					ir.setRemark(reamarks[i]);
				} catch (Exception e) {
					logger.error("view error:" ,e);
					ir.setRemark("");
				}
				irs.add(ir);
			}
		}

		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareProjectDetail");
		mv.addObject("irs",irs);
	}

	/**
	 * @Title: shareForVideo
	 * @Description: 分享视频
	 * @param @param id
	 * @param @param mv    设定文件
	 * @return void    返回类型
	 */
	private void shareForVideo(Integer id, ModelAndView mv) {
		VideoInfo videoInfo = videoInfoService.selectByPrimaryKey(id);

		//收藏
		Map<String, Object> favMap = new HashMap<String, Object>();
		favMap.put("enabled", true);
		favMap.put("isdeleted",false);
		favMap.put("clientid",null );
		favMap.put("actid", videoInfo.getId());
		favMap.put("acttype", CommonEnum.SYSCODE_GFBK_SPBK);
		favMap.put("numtype", CommonEnum.SYSCODE_COURSETYPE_FAVORITE);

		Long favCount = clientNumService.queryCountByMap(favMap);
		videoInfo.setFavoritenum(Integer.parseInt(favCount.toString()));

		//转发
		Map<String, Object> praMap = new HashMap<String, Object>();
		praMap.put("enabled", true);
		praMap.put("isdeleted",false);
		praMap.put("clientid",null );
		praMap.put("actid", videoInfo.getId());
		praMap.put("acttype", CommonEnum.SYSCODE_GFBK_SPBK);
		praMap.put("numtype", CommonEnum.SYSCODE_COURSETYPE_FORWARD);

		Long sharCount = clientNumService.queryCountByMap(praMap);
		videoInfo.setSharenum(Integer.parseInt(sharCount.toString()));

		//浏览
		Map<String, Object> lookMap = new HashMap<String, Object>();
		lookMap.put("enabled", true);
		lookMap.put("isdeleted",false);
		lookMap.put("clientid",null );
		lookMap.put("actid", videoInfo.getId());
		lookMap.put("acttype", CommonEnum.SYSCODE_GFBK_SPBK);
		lookMap.put("numtype", CommonEnum.SYSCODE_COURSETYPE_LOOK);

		Long browsenum = clientNumService.queryCountByMap(lookMap);
		String browsenumStr = numFormat(browsenum);
		videoInfo.setBrowsenum(browsenumStr);
		//评论
		Map<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("enabled", true);
		disMap.put("isdeleted",false);
		disMap.put("tarId", videoInfo.getId());
		disMap.put("type", CommonEnum.SYSCODE_GFBK_SPBK);

		Integer disNum = mstDiscussService.queryCountByMap(disMap);
		videoInfo.setDisNum(disNum);

		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareVideo");
		mv.addObject("videoInfo",videoInfo);
	}
	
	/**
	 * @Title: shareForVideo
	 * @Description: 分享视频
	 * @param @param id
	 * @param @param mv    设定文件
	 * @return void    返回类型
	 */
	private void shareForVideo2(Integer id, ModelAndView mv) {
		VideoInfo videoInfo = videoInfoService.selectByPrimaryKey(id);

		//评论
		Map<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("enabled", true);
		disMap.put("isdeleted",false);
		disMap.put("tarId", id);
		disMap.put("type", CommonEnum.SYSCODE_GFBK_SPBK);

		Integer disNum = mstDiscussService.queryCountByMap(disMap);
		videoInfo.setDisNum(disNum);
		
		//查询一级评论
//		Map<String, Object> reqMap = new HashMap<String, Object>();		
//
//		reqMap.put("enabled", true);
//		reqMap.put("isDeleted", false);
//		reqMap.put("parentId", "main");
//		reqMap.put("tarId", id);
//		reqMap.put("type", CommonEnum.SYSCODE_GFBK_SPBK);
//		reqMap.put("orderByClause", "md.createdate desc");
//
//		PageBounds pageBounds = new PageBounds(1,10);	
//		List<MstDiscuss> mainList = mstDiscussService.queryByList2(reqMap, pageBounds);
//		
//		if(mainList!=null&mainList.size()>0){
//			for(MstDiscuss md : mainList){
//				
//					//查询上级评论
//					PageBounds noPage = new PageBounds();	
//					Map<String, Object> tempMap = new HashMap<String, Object>();
//					tempMap.put("enabled", true);
//					tempMap.put("isDeleted", false);
//					tempMap.put("tarId", id);
//					tempMap.put("type", CommonEnum.SYSCODE_GFBK_SPBK);
//					tempMap.put("id", md.getParentid());
//					tempMap.put("orderByClause", "md.createdate desc");
//
//					List<MstDiscuss> autoList = mstDiscussService.queryByList2(tempMap, noPage);
//
//					md.setMds(autoList);
//			}
//		}

		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareVideo2");
		mv.addObject("videoInfo",videoInfo);
	//	mv.addObject("mainList",mainList);
	}
	
	/**
	 * @Title: shareForOnline
	 * @Description: 分享在线课程
	 * @param @param id
	 * @param @param mv    设定文件
	 * @return void    返回类型
	 */
	private void shareForOnline(Integer id, ModelAndView mv) {
		OnlineCourse onlineCourse = onlineCourseService.selectByPrimaryKey2(id);
		
		/*VideoInfo videoInfo = new VideoInfo();
		videoInfo.setId(onlineCourse.getId());
		videoInfo.setContent(onlineCourse.getIntroduction());
		videoInfo.setAuthor(null);
		videoInfo.setCreatedate(onlineCourse.getCreatedate());
		videoInfo.setTitle(onlineCourse.getTitle());
		videoInfo.setUrl(onlineCourse.getUrl());
		videoInfo.setReserved9(onlineCourse.getCoverurl());
		videoInfo.setReserved10(onlineCourse.getReserved11());*/
		//收藏
//		Map<String, Object> favMap = new HashMap<String, Object>();
//		favMap.put("enabled", true);
//		favMap.put("isdeleted",false);
//		favMap.put("clientid",null );
//		favMap.put("actid", onlineCourse.getId());
//		favMap.put("acttype", CommonEnum.SYSCODE_GFBK_SPBK);
//		favMap.put("numtype", CommonEnum.SYSCODE_COURSETYPE_FAVORITE);
//
//		Long favCount = clientNumService.queryCountByMap(favMap);
//		onlineCourse.setFavoritenum(Integer.parseInt(favCount.toString()));

		//转发
//		Map<String, Object> praMap = new HashMap<String, Object>();
//		praMap.put("enabled", true);
//		praMap.put("isdeleted",false);
//		praMap.put("clientid",null );
//		praMap.put("actid", onlineCourse.getId());
//		praMap.put("acttype", CommonEnum.SYSCODE_GFBK_ZXKC);
//		praMap.put("numtype", CommonEnum.SYSCODE_COURSETYPE_FORWARD);
//
//		Long sharCount = clientNumService.queryCountByMap(praMap);
//		onlineCourse.setSharenum(Integer.parseInt(sharCount.toString()));

		//浏览
//		Map<String, Object> lookMap = new HashMap<String, Object>();
//		lookMap.put("enabled", true);
//		lookMap.put("isdeleted",false);
//		lookMap.put("clientid",null );
//		lookMap.put("actid", onlineCourse.getId());
//		lookMap.put("acttype", CommonEnum.SYSCODE_GFBK_ZXKC);
//		lookMap.put("numtype", CommonEnum.SYSCODE_COURSETYPE_LOOK);
//
//		Long browsenum = clientNumService.queryCountByMap(lookMap);
//		String browsenumStr = numFormat(browsenum);
//		onlineCourse.setBrowsenum(browsenumStr);

		//评论
//		Map<String, Object> disMap = new HashMap<String, Object>();
//		disMap.put("enabled", true);
//		disMap.put("isdeleted",false);
//		disMap.put("tarId", onlineCourse.getId());
//		disMap.put("type", CommonEnum.SYSCODE_GFBK_ZXKC);
//
//		Integer disNum = mstDiscussService.queryCountByMap(disMap);
//		onlineCourse.setDisNum(disNum);

		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareOnlineCourse");
		mv.addObject("onlineCourse",onlineCourse);
	}
	
	/**
	 * @Title: shareForAnswer
	 * @Description: 分享回答
	 * @param @param id
	 * @param @param mv    设定文件
	 * @return void    返回类型
	 */
	private void shareForAnswer(Integer id, ModelAndView mv) {
		Answer answer = answerService.selectByPrimaryKey(id);

		//评论
		Map<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("enabled", true);
		disMap.put("isdeleted",false);
		disMap.put("tarId", answer.getAnswerId());
		disMap.put("type", CommonEnum.SYSTEM_CARESEL_RANGE_ANSWER);

		Integer disNum = mstDiscussService.queryCountByMap(disMap);
		answer.setAnswerDiscussNum(disNum);//评论数量
		
		//查询一级评论
//		Map<String, Object> reqMap = new HashMap<String, Object>();		
//
//		reqMap.put("enabled", true);
//		reqMap.put("isDeleted", false);
//		reqMap.put("parentId", "main");
//		reqMap.put("tarId", answer.getAnswerId());
//		reqMap.put("type", CommonEnum.SYSTEM_CARESEL_RANGE_ANSWER);
//		reqMap.put("orderByClause", "md.createdate desc");
//
//		PageBounds pageBounds = new PageBounds(1,10);	
//		List<MstDiscuss> mainList = mstDiscussService.queryByList2(reqMap, pageBounds);
//		
//		if(mainList!=null&mainList.size()>0){
//			for(MstDiscuss md : mainList){
//				
//					//查询上级评论
//					PageBounds noPage = new PageBounds();	
//					Map<String, Object> tempMap = new HashMap<String, Object>();
//					tempMap.put("enabled", true);
//					tempMap.put("isDeleted", false);
//					tempMap.put("tarId", answer.getAnswerId());
//					tempMap.put("type", CommonEnum.SYSTEM_CARESEL_RANGE_ANSWER);
//					tempMap.put("id", md.getParentid());
//					tempMap.put("orderByClause", "md.createdate desc");
//
//					List<MstDiscuss> autoList = mstDiscussService.queryByList2(tempMap, noPage);
//
//					md.setMds(autoList);
//			}
//		}

		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareAnswer");
		mv.addObject("answer",answer);
	//	mv.addObject("mainList",mainList);
	}
	
	/**
	 * @Title: shareForQuestion
	 * @Description: 分享问题
	 * @param @param id
	 * @param @param mv    设定文件
	 * @return void    返回类型
	 */
	private void shareForQuestion(Integer id, ModelAndView mv) {
		Question question = questionService.selectByPrimaryKey(id);
		
		String pUrl=question.getPictureUrl();
		if(pUrl.contains(",")){
			String pictureUrl2=pUrl.split(",")[0];
			question.setPictureUrl(CommonEnum.portraitUrl+pictureUrl2);
		}	
		
		//根据时间查询回答列表
//		Map<String, Object> reqMap = new HashMap<String, Object>();		
//		
//		reqMap.put("answerSourceId",id);
//		reqMap.put("orderByClause", "md.answerCreateDate desc");
//
//		PageBounds pageBounds = new PageBounds(1,10);
//		List<Answer> answerList=answerService.queryByList(reqMap, pageBounds);
		
		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareQuestion");
		mv.addObject("question",question);
	//	mv.addObject("answerList",answerList);
	}
	
	/**
	 * @Title: shareForDynamic
	 * @Description: 分享动态
	 * @param @param id
	 * @param @param mv    设定文件
	 * @return void    返回类型
	 */
	private void shareForDynamic(Integer id, ModelAndView mv) {
		Dynamic dynamic = dynamicService.selectByPrimaryKey(id);
		//如果是手机号登陆，需要给头像图片加个绝对路径
		int loginType=dynamic.getLoginType();
		if(loginType==2&dynamic.getSendPortrait()!=null){
			dynamic.setSendPortrait(CommonEnum.portraitUrl+dynamic.getSendPortrait());
		}
		
		String pUrl=dynamic.getDynamicUrl();
		if(pUrl.contains(",")){
			String pictureUrl2=pUrl.split(",")[0];
			dynamic.setDynamicUrl(CommonEnum.portraitUrl+pictureUrl2);
		}	
		//评论
		Map<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("enabled", true);
		disMap.put("isdeleted",false);
		disMap.put("tarId", dynamic.getDynamicId());
		disMap.put("type", CommonEnum.SYSTEM_CARESEL_RANGE_DYNAMIC);

		Integer disNum = mstDiscussService.queryCountByMap(disMap);
		dynamic.setDiscussNum(disNum);//评论数量
		
		//查询一级评论
//		Map<String, Object> reqMap = new HashMap<String, Object>();		
//
//		reqMap.put("enabled", true);
//		reqMap.put("isDeleted", false);
//		reqMap.put("parentId", "main");
//		reqMap.put("tarId", dynamic.getDynamicId());
//		reqMap.put("type", CommonEnum.SYSTEM_CARESEL_RANGE_DYNAMIC);
//		reqMap.put("orderByClause", "md.createdate desc");
//
//		PageBounds pageBounds = new PageBounds(1,10);	
//		List<MstDiscuss> mainList = mstDiscussService.queryByList2(reqMap, pageBounds);
//		
//		if(mainList!=null&mainList.size()>0){
//			for(MstDiscuss md : mainList){
//					//查询上级评论
//					PageBounds noPage = new PageBounds();	
//					Map<String, Object> tempMap = new HashMap<String, Object>();
//					tempMap.put("enabled", true);
//					tempMap.put("isDeleted", false);
//					tempMap.put("tarId", dynamic.getDynamicId());
//					tempMap.put("type", CommonEnum.SYSTEM_CARESEL_RANGE_DYNAMIC);
//					tempMap.put("id", md.getParentid());
//					tempMap.put("orderByClause", "md.createdate desc");
//
//					List<MstDiscuss> autoList = mstDiscussService.queryByList2(tempMap, noPage);
//
//					md.setMds(autoList);
//			}
//		}
		
		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareDynamic");
		mv.addObject("dynamic",dynamic);
//		mv.addObject("mainList",mainList);
	}
	
	/**
	 * @Title: shareForCompanion
	 * @Description: 分享嘿哈
	 * @param @param id
	 * @param @param mv    设定文件
	 * @return void    返回类型
	 */
	private void shareForCompanion(Integer id, ModelAndView mv) {
		Companion companion = companionService.selectByPrimaryKey(id);
		//如果是手机号登陆，需要给头像图片加个绝对路径
		int loginType=companion.getLoginType();
		if(loginType==2&companion.getCompanionAddPortrait()!=null){
			companion.setCompanionAddPortrait(CommonEnum.portraitUrl+companion.getCompanionAddPortrait());
		}
			
		//评论
		Map<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("enabled", true);
		disMap.put("isdeleted",false);
		disMap.put("tarId", companion.getCompanionId());
		disMap.put("type", CommonEnum.SYSTEM_CARESEL_RANGE_COMPANION);

		Integer disNum = mstDiscussService.queryCountByMap(disMap);
		companion.setDiscussNum(disNum);//评论数量
		
		//查询一级评论
//				Map<String, Object> reqMap = new HashMap<String, Object>();		
//
//				reqMap.put("enabled", true);
//				reqMap.put("isDeleted", false);
//				reqMap.put("parentId", "main");
//				reqMap.put("tarId", companion.getCompanionId());
//				reqMap.put("type", CommonEnum.SYSTEM_CARESEL_RANGE_COMPANION);
//				reqMap.put("orderByClause", "md.createdate desc");
//
//				PageBounds pageBounds = new PageBounds(1,10);	
//				List<MstDiscuss> mainList = mstDiscussService.queryByList2(reqMap, pageBounds);
//				
//				if(mainList!=null&mainList.size()>0){
//					for(MstDiscuss md : mainList){
//							//查询上级评论
//							PageBounds noPage = new PageBounds();	
//							Map<String, Object> tempMap = new HashMap<String, Object>();
//							tempMap.put("enabled", true);
//							tempMap.put("isDeleted", false);
//							tempMap.put("tarId", companion.getCompanionId());
//							tempMap.put("type", CommonEnum.SYSTEM_CARESEL_RANGE_COMPANION);
//							tempMap.put("id", md.getParentid());
//							tempMap.put("orderByClause", "md.createdate desc");
//
//							List<MstDiscuss> autoList = mstDiscussService.queryByList2(tempMap, noPage);
//
//							md.setMds(autoList);
//					}
//				}
		
		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareCompanion");
		mv.addObject("companion",companion);
	//	mv.addObject("mainList",mainList);
	}

	/**
	* @Title: numFormat
	* @param @param browsenum
	* @param @return    设定文件
	* @return String    返回类型
	* @throws
	*/
	private String numFormat(Long browsenum) {
		BigDecimal bigBr = new BigDecimal(browsenum);
		String browsenumStr = "";
		if(browsenum >= 0 && browsenum < 1000){
			browsenumStr = browsenum.toString();
		} else if(browsenum >= 1000 && browsenum < 10000){
			bigBr = bigBr.divide(new BigDecimal(1000));
			bigBr = bigBr.setScale(1,   BigDecimal.ROUND_HALF_UP); 
			browsenumStr = bigBr.toString() + "k";
		} else if(browsenum >= 10000 && browsenum < 100000000){
			bigBr = bigBr.divide(new BigDecimal(10000));
			bigBr = bigBr.setScale(1,  BigDecimal.ROUND_HALF_UP); 
			browsenumStr = bigBr.toString() + "w";
		} else {
			browsenumStr = "999w+";
		}
		return browsenumStr;
	}

	/**
	 * @Title: shareForWenw
	 * @Description: 分享文章
	 * @param @param id
	 * @param @param mv
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	private ModelAndView shareForWenw(Integer id, ModelAndView mv) {
		WenwInfo wenwInfo = wenwInfoService.selectByPrimaryKey(id);

		//格式化内容标签
		logger.info("content format before: " + wenwInfo.getContent());
		wenwInfo.setContent(wenwInfo.getContent().trim());
		logger.info("content format alter: " + wenwInfo.getContent());
		//收藏
		Map<String, Object> favMap = new HashMap<String, Object>();
		favMap.put("enabled", true);
		favMap.put("isdeleted",false);
		favMap.put("clientid",null );
		favMap.put("actid", wenwInfo.getId());
		favMap.put("acttype", CommonEnum.SYSCODE_GFBK_WZBK);
		favMap.put("numtype", CommonEnum.SYSCODE_COURSETYPE_FAVORITE);

		Long favCount = clientNumService.queryCountByMap(favMap);
		wenwInfo.setFavoritenum(Integer.parseInt(favCount.toString()));

		//转发
		Map<String, Object> praMap = new HashMap<String, Object>();
		praMap.put("enabled", true);
		praMap.put("isdeleted",false);
		praMap.put("clientid",null );
		praMap.put("actid", wenwInfo.getId());
		praMap.put("acttype", CommonEnum.SYSCODE_GFBK_WZBK);
		praMap.put("numtype", CommonEnum.SYSCODE_COURSETYPE_FORWARD);

		Long sharCount = clientNumService.queryCountByMap(praMap);
		wenwInfo.setSharenum(Integer.parseInt(sharCount.toString()));

		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareWenw");
		mv.addObject("wenwInfo", wenwInfo);

		return mv;
	}
	
	/**
	 * @Title: shareForWenw
	 * @Description: 分享文章
	 * @param @param id
	 * @param @param mv
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	private ModelAndView shareForWenw2(Integer id, ModelAndView mv) {
		WenwInfo wenwInfo = wenwInfoService.selectByPrimaryKey(id);

		//格式化内容标签
		logger.info("content format before: " + wenwInfo.getContent());
		wenwInfo.setContent(wenwInfo.getContent().trim());
		logger.info("content format alter: " + wenwInfo.getContent());

		//评论
		Map<String, Object> disMap = new HashMap<String, Object>();
		disMap.put("enabled", true);
		disMap.put("isdeleted",false);
		disMap.put("tarId", wenwInfo.getId());
		disMap.put("type", CommonEnum.SYSCODE_GFBK_WZBK);

		Integer disNum = mstDiscussService.queryCountByMap(disMap);
		wenwInfo.setReserved1(disNum);//评论数量
		//查询一级评论
//				Map<String, Object> reqMap = new HashMap<String, Object>();		
//
//				reqMap.put("enabled", true);
//				reqMap.put("isDeleted", false);
//				reqMap.put("parentId", "main");
//				reqMap.put("tarId", wenwInfo.getId());
//				reqMap.put("type", CommonEnum.SYSCODE_GFBK_WZBK);
//				reqMap.put("orderByClause", "md.createdate desc");
//
//				PageBounds pageBounds = new PageBounds(1,10);	
//				List<MstDiscuss> mainList = mstDiscussService.queryByList2(reqMap, pageBounds);
//				
//				if(mainList!=null&mainList.size()>0){
//					for(MstDiscuss md : mainList){
//						
//							//查询上级评论
//							PageBounds noPage = new PageBounds();	
//							Map<String, Object> tempMap = new HashMap<String, Object>();
//							tempMap.put("enabled", true);
//							tempMap.put("isDeleted", false);
//							tempMap.put("tarId", wenwInfo.getId());
//							tempMap.put("type", CommonEnum.SYSCODE_GFBK_WZBK);
//							tempMap.put("id", md.getParentid());
//							tempMap.put("orderByClause", "md.createdate desc");
//
//							List<MstDiscuss> autoList = mstDiscussService.queryByList2(tempMap, noPage);
//
//							md.setMds(autoList);
//					}
//				}

		
		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareWenw2");
		mv.addObject("wenwInfo", wenwInfo);
	//	mv.addObject("mainList",mainList);
		return mv;
	}
	

	/**
	 * @Title: shareForPro
	 * @Description:分享活动
	 * @param @param id
	 * @param @param mv
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	private ModelAndView shareForPro(Integer id, ModelAndView mv) {
		ProjectInfo projectInfo = projectInfoService.selectByPrimaryKey(id);
		//活动海报
		List<String> probills = new ArrayList<String>();
		String [] probillArr = projectInfo.getProbill().split(";");
		for(String str : probillArr){
			if(!StringUtils.isEmpty(str)){
				probills.add(str);
			}
		}
		projectInfo.setProbills(probills);

		//活动亮点
		List<String> lightspots = new ArrayList<String>();
		String [] lightspotArr =  projectInfo.getLightspot().split("\r\n");
		for(String str :lightspotArr){
			if(!StringUtils.isEmpty(str)){
				lightspots.add(str);
			}
		}
		projectInfo.setLightspots(lightspots);

		//温馨提示
		List<String> warnings = new ArrayList<String>();
		String [] warningArr =  projectInfo.getWarning().split("\r\n");
		for(String str :warningArr){
			if(!StringUtils.isEmpty(str)){
				warnings.add(str);
			}
		}
		projectInfo.setWarnings(warnings);

		//更多活动
		Map<String, Object> moreMap = new HashMap<String, Object>();
		moreMap.put("isDeleted", false);
		moreMap.put("enabled", true);
		moreMap.put("filterMaster", projectInfo.getId());
		moreMap.put("orderByClause", "pi.createdate desc");

		PageBounds pageBounds = new PageBounds(1,5);

		List<ProjectInfo> morePro = projectInfoService.queryByList(moreMap, pageBounds);
		projectInfo.setMorePro(morePro);

		for(ProjectInfo pro:morePro){
			if(!StringUtils.isEmpty(pro.getProbill())){
				pro.setProbill(pro.getProbill().split(";")[0]);
			}
		}

		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareProject");
		mv.addObject("projectInfo",projectInfo);

		return mv;
	}

	/**
	 * @Title: shareForCourse
	 * @Description: 分享课程
	 * @param @param id
	 * @param @param mv
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	private ModelAndView shareForCourse(Integer id, ModelAndView mv) {
		PageBounds pageBounds = new PageBounds(1,5);

		GymnasiumDetail gd = gymnasiumDetailService.selectByPrimaryKey(id);

		//收藏
		Map<String, Object> numMap = new HashMap<String, Object>();
		numMap.put("enabled", true);
		numMap.put("isdeleted",false);
		numMap.put("clientid",null );
		numMap.put("actid", gd.getId());
		numMap.put("acttype", CommonEnum.SYSCODE_GFBK_KCBK);
		numMap.put("numtype", CommonEnum.SYSCODE_COURSETYPE_FAVORITE);

		Long count = clientNumService.queryCountByMap(numMap);
		gd.setFavoritenum(Integer.parseInt(count.toString()));
		//套餐
		Map<String, Object> packMap = new HashMap<String,Object>();
		packMap.put("enabled", true);
		packMap.put("isDeleted",false);
		packMap.put("courseId",gd.getId());

		List<CoursePackage> cps = coursePackageService.queryByList(packMap, pageBounds);
		String cpStr = "";
		for(CoursePackage cp : cps){
			cpStr += cp.getPackagetypeName() + " /";
		}

		cpStr = cpStr.substring(0,cpStr.length() - 1);
		gd.setCpStr(cpStr);
		//更多课程
		Map<String, Object> moreMap = new HashMap<String, Object>();
		moreMap.put("isDeleted", false);
		moreMap.put("enabled", true);
		moreMap.put("gymId", gd.getGymid());
		moreMap.put("orderByClause", "gd.createdate desc");

		List<GymnasiumDetail> moreGds = gymnasiumDetailService.queryListByMap(moreMap, pageBounds);
		for(GymnasiumDetail moreGd : moreGds){
			if(!StringUtils.isEmpty(moreGd.getCoursebill())){
				moreGd.setCoursebill(moreGd.getCoursebill().split(";")[0]);
			}
		}

		gd.setMoreGds(moreGds);

		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareGymnasiumDetail");
		mv.addObject("gd",gd);

		return mv;
	}

	/**
	 * @Title: shareForGym
	 * @Description: 分享场馆
	 * @param @param id
	 * @param @param mv
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	private ModelAndView shareForGym(Integer id, ModelAndView mv) {
		//获取场馆信息
		MstGymnasium mstGymnasium = mstGymnasiumService.selectByPrimaryKey(id);
		//多张海报处理
		List<String> gymBills = new ArrayList<String>();
		if(!StringUtils.isEmpty(mstGymnasium.getGymbill())){
			String [] imgs = mstGymnasium.getGymbill().split(";");
			for(String img : imgs){
				if(!StringUtils.isEmpty(img)){
					gymBills.add(img);
				}
			}
		}

		mstGymnasium.setGymBills(gymBills);

		PageBounds pageBounds = new PageBounds();

		//获取特色课程
		Map<String, Object> speMap = new HashMap<String, Object>();
		speMap.put("isDeleted", false);
		speMap.put("enabled", true);
		speMap.put("gymId", mstGymnasium.getId());
		speMap.put("courseType", CommonEnum.SYSCODE_COURSETYPE_SPE);
		speMap.put("orderByClause", "gd.createdate desc");

		List<GymnasiumDetail> speGds = gymnasiumDetailService.queryListByMap(speMap, pageBounds);

		mstGymnasium.setSpeGds(speGds);

		//获取其他课程
		Map<String, Object> otherMap = new HashMap<String, Object>();
		otherMap.put("isDeleted", false);
		otherMap.put("enabled", true);
		otherMap.put("gymId", mstGymnasium.getId());
		otherMap.put("courseType", CommonEnum.SYSCODE_COURSETYPE_OTHERS);
		otherMap.put("orderByClause", "gd.createdate desc");

		List<GymnasiumDetail> otherGds = gymnasiumDetailService.queryListByMap(otherMap, pageBounds);

		mstGymnasium.setOtherGds(otherGds);

		mv.setViewName(CommonEnum.FORWARD_VIEW_OTHERS + "/ShareGymnasium");
		mv.addObject("mstGymnasium",mstGymnasium);

		return mv;
	}
	
	
	/**
	 ***********临时类********************** 
	 */
	
	public class TempTest{
		
		private OutcomeVo cv;

		/**
		 * @return the cv
		 */
		public OutcomeVo getCv() {
			return cv;
		}

		/**
		 * @param cv the cv to set
		 */
		public void setCv(OutcomeVo cv) {
			this.cv = cv;
		}
		
	}
	
	public static void main(String[] args) {
		Long l = (long)1000;
		System.out.println(Integer.parseInt(l.toString()));;
		
	}

}
