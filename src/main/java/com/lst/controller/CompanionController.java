package com.lst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.Companion;
import com.lst.model.CompanionLog;
import com.lst.model.FolPara;
import com.lst.model.MstButton;
import com.lst.model.MstUser;
import com.lst.service.CompanionLogService;
import com.lst.service.CompanionService;
import com.lst.service.MstButtonService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: CompanionController
 * @Description: 嘿哈管理
 *
 */
@Controller
@RequestMapping("/companionController")
public class CompanionController extends BaseController {
	
	@Autowired
	private MstButtonService mstButtonService;
	
	@Autowired
	private CompanionService companionService;
	
	@Autowired
	private CompanionLogService companionLogService;
	
	
	/**
	 * 
	* @Title: Main
	* @Description: 列表页跳转
	* @param @param menuId
	* @param @param page
	* @param @param rows
	* @param @param index_exp
	* @param @param index_che
	* @param @param minDate
	* @param @param maxDate
	* @param @param typeFol
	* @param @param ariTypeFol
	* @param @return
	* @param @throws MyException    设定文件
	* @return ModelAndView    返回类型
	* @throws
	 */
	@RequestMapping(value = "/Main")
	public ModelAndView Main(
			@RequestParam(required = false) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE)Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "") String title) throws MyException {
		Date startDate = new Date();
		logger.info("Main begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_APP_MANAGE + "/companion_list");

		// 查询用户权限按钮
		MstUser mstUser = this.getMySession().getMstUser();
		List<MstButton> btns = CommonUtils.readPermissionWithBtn(menuId, mstUser, mstButtonService);

		//写参数
		FolPara folPara = new FolPara();
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setMinDate(minDate);
		folPara.setMaxDate(maxDate);
		folPara.setMenuId(menuId);
		folPara.setTitleFol(title);

		mv.addObject("btns", btns);
		mv.addObject("folPara", folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("Main end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	* @Title: queryList
	* @Description: 查询分页list
	* @param @param page
	* @param @param rows
	* @param @param sort
	* @param @param order
	* @param @param minDate
	* @param @param maxDate
	* @param @param type
	* @param @param ariType
	* @param @return    设定文件
	* @return Object    返回类型
	* @throws
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "") String title) {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("orderByClause","md.companionCreateDate desc");
	
		if(!StringUtils.isBlank(title)){
			reqMap.put("searchwork", title);
		}

		PageBounds pageBounds = new PageBounds(page, rows);
		List<Companion> list =  companionService.queryByList(reqMap, pageBounds);

		PageList<Companion> pageList = (PageList<Companion>) list;
		Paginator paginator = pageList.getPaginator();

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		indexPages.put("total", paginator.getTotalCount());
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}
	
	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param request
	 * @param @param response
	 * @param @param ids
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping("delList")
	@ResponseBody
	public boolean delList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) String ids) throws MyException {
		Date startDate = new Date();
		logger.info("delList begin: " + startDate);

		boolean flag = false;

	//	MstUser mstUser = this.getMySession().getMstUser();
  
		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("ids", ids);
		try {
			companionService.delList(reqMap);
			flag = true;
		} catch (Exception e) {
			logger.error("delList error: " ,e);
		}

		logger.info("delList end run(s): " + DateUtil.calLastedTime(startDate));

		return flag;
	}
	
	
	/**
	 * 
	 * @Title: queryLogList
	 * @Description: 查看对嘿哈感兴趣的人列表
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/queryLogList")
	public ModelAndView queryDiscuss(
			@RequestParam(required = false) int id,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) throws MyException {
		Date startDate = new Date();
		logger.info("queryDiscuss begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_APP_MANAGE + "/companionLog_list");

		mv.addObject("addId", id);
		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);
		
		logger.info("queryDiscuss end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	* @Title: queryCompanionLogList
	* @Description: 查询感兴趣list
	* @param @param page
	* @param @param rows
	* @param @param minDate
	* @param @param maxDate
	* @param @param type
	* @param @param tarid
	* @param @return    设定文件
	* @return Object    返回类型
	* @throws
	 */
	@RequestMapping(value = "/queryCompanionLogList")
	@ResponseBody
	public Object queryDiscussList(@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "") String addId) {
		Date startDate = new Date();
		logger.info("queryDiscussList begin: " + startDate);

		/**
		 * 查询评论信息
		 */
		Map<String, Object> reqMap = new HashMap<String, Object>();
		
		if(!StringUtils.isBlank(addId)){
			reqMap.put("sourceId", addId);
		}
		
		if(StringUtils.isEmpty(sort)){
			reqMap.put("orderByClause","md.createDate desc");
		} else{
			reqMap.put("orderByClause",sort + " " + order);
		}
		
		
		if(!StringUtils.isBlank(minDate) && !StringUtils.isBlank(maxDate)){
			reqMap.put("minDate", minDate + DateUtil.START_DATE_HMS);
			reqMap.put("maxDate", maxDate + DateUtil.END_DATE_HMS);
		}

		PageBounds pageBounds = new PageBounds(page, rows);

		HashMap<String, Object> indexPages = new HashMap<String, Object>();

		List<CompanionLog> list = companionLogService.queryByList(reqMap, pageBounds);
		PageList<CompanionLog> pageList = (PageList<CompanionLog>) list;
		Paginator paginator = pageList.getPaginator();

		indexPages.put("total", paginator.getTotalCount());
		indexPages.put("rows", list);

		logger.info("queryDiscussList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}
	
}
