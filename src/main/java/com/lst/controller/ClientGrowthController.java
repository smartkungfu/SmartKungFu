/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：ClientGrowthController   
 * 类描述：   
 * 创建人：Wdd   
 * 创建时间：2016年10月26日 下午5:40:05   
 * 修改人：Wdd  
 * 修改时间：2016年10月26日 下午5:40:05   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.ClientGrowth;
import com.lst.model.FolPara;
import com.lst.service.ClientGrowthService;
import com.lst.utils.DateUtil;

/**
 * @ClassName: ClientGrowthController
 * @Description: 用户成就
 * @author Wdd
 * @date 2016年10月26日 下午5:40:05
 *
 */
@Controller
@RequestMapping("/clientGrowthController")
public class ClientGrowthController extends BaseController {

	@Autowired
	private ClientGrowthService clientGrowthService;

	/**
	 * 
	 * @Title: Main
	 * @Description: 页面加载
	 * @param @param menuId
	 * @param @param page
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @param rows
	 * @param @param minDate
	 * @param @param maxDate
	 * @param @param nickName
	 * @param @param mobileNo
	 * @param @param nameCn
	 * @param @param clientid
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/Main")
	public ModelAndView Main(
			@RequestParam(required = false) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE)Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "") String nickName,
			@RequestParam(required = false,defaultValue = "") String mobileNo,
			@RequestParam(required = false,defaultValue = "") String nameCn,
			@RequestParam(required = false) int clientid) throws MyException {
		Date startDate = new Date();
		logger.info("Main begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_APP_MANAGE + "/client_growth_list");

		//写参数
		FolPara folPara = new FolPara();
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setNickName(nickName);;
		folPara.setMinDate(minDate);
		folPara.setMaxDate(maxDate);
		folPara.setNameCn(nameCn);
		folPara.setMobileNo(mobileNo);
		folPara.setMenuId(menuId);

		mv.addObject("folPara", folPara);
		mv.addObject("clientid", clientid);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("Main end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: queryList
	 * @Description: 查询分页数据
	 * @param @param page
	 * @param @param rows
	 * @param @param clientid
	 * @param @return    设定文件
	 * @return Object    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false) int clientid,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "") String growthtype) {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		/**
		 * 查询用户成就信息
		 */
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("isDeleted", 0);
		reqMap.put("enabled", 1);
		reqMap.put("clientid", clientid);

		if(StringUtils.isEmpty(sort)){
			reqMap.put("orderByClause","g.createdate desc");
		} else{
			reqMap.put("orderByClause",sort + " " + order);
		}
		if(!StringUtils.isBlank(growthtype)){
			reqMap.put("growthtype", Integer.parseInt(growthtype));
		}

		if(!StringUtils.isBlank(minDate) && !StringUtils.isBlank(maxDate)){
			reqMap.put("minDate", minDate + DateUtil.START_DATE_HMS);
			reqMap.put("maxDate", maxDate + DateUtil.END_DATE_HMS);
		}

		PageBounds pageBounds = new PageBounds(page, rows);
		List<ClientGrowth> list = clientGrowthService.queryByList(reqMap, pageBounds);

		PageList<ClientGrowth> pageList = (PageList<ClientGrowth>) list;
		Paginator paginator = pageList.getPaginator();

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		// indexPages.put("paginator", paginator);
		// indexPages.put("totalRows", paginator.getTotalCount());
		// indexPages.put("pageSize", paginator.getLimit());
		// indexPages.put("pageNumber", paginator.getPage());
		indexPages.put("total", paginator.getTotalCount());
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}

	/**
	 * 
	 * @Title: clientGrowth
	 * @Description: 功力值增值
	 * @param @param userid
	 * @param @param growthid
	 * @param @param growthvalue
	 * @param @param growthtype
	 * @param @return    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping("clientGrowth")
	@ResponseBody
	public boolean clientGrowth(
			@RequestParam(required = true)Integer userid,
			@RequestParam(required = true)Integer growthid,
			@RequestParam(required = true)Integer growthvalue,
			@RequestParam(required = true)Integer growthtype){
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("clientId", userid);
		map.put("growthId", growthid);
		map.put("growthType", growthtype);

		List<ClientGrowth> list = clientGrowthService.queryByList(map,new PageBounds());
		if (list == null || list.isEmpty()) {

			ClientGrowth cg = new ClientGrowth();

			cg.setClientid(userid);
			cg.setGrowthid(growthid);
			cg.setGrowthtype(growthtype);
			cg.setGrowthvalue(growthvalue);
			cg.setIsdeleted(false);
			cg.setEnabled(true);

			cg.setCreatedate(startDate);
			cg.setCreateuser(userid);
			clientGrowthService.insert(cg);
			
			return true;
		}
		
		logger.info("queryList end spent(s): " + DateUtil.calLastedTime(startDate));
		
		return false;
	}
}
