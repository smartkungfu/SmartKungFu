/**   
*    
* 项目名称：SmartKungFu   
* 类名称：RefundResultController   
* 类描述：   
* 创建人：zhangl  
* 创建时间：2016年12月6日 上午10:48:28   
* 修改人：zhangl   
* 修改时间：2016年12月6日 上午10:48:28   
* 修改备注：   
* @version    
*    
*/
package com.lst.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.ClientRefund;
import com.lst.model.FolPara;
import com.lst.model.MstUser;
import com.lst.service.RefundResultService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: RefundResultController
 * @Description: 退款处理控制类
 * @author zhangl
 * @date 2016年12月6日 上午10:48:28
 *
 */
@Controller
@RequestMapping("refundResultController")
public class RefundResultController extends BaseController {
	
	@Autowired
	private RefundResultService refundResultService;
	
	
	/**
	 * 
	 * @Title: save
	 * @Description:save
	 * @param @param mstrole
	 * @param @param folPara
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return String    返回类型
	 */
	@RequestMapping(value = "/save",method = RequestMethod.POST)
	public String save(ClientRefund cr,boolean isTicket,boolean isGrade,boolean isGradeValue,BigDecimal refPrice,FolPara folPara) throws MyException {
		Date startDate = new Date();
		logger.info("save begin: " + startDate);

		MstUser mstUser = this.getMySession().getMstUser();

		if (cr.getId() != null && cr.getOd() != null) {
			
			//更新退款申请状态
			cr.setId(cr.getId());
			cr.setUpdatedate(startDate);
			cr.setUpdateuser(mstUser.getId());
			cr.setRefstatus(CommonEnum.SYSCODE_REFUNDSTATUS_DEALED);
			
			//更新订单详情状态
			cr.getOd().setId(cr.getOd().getId());
			cr.getOd().setUpdatedate(startDate);
			cr.getOd().setUpdateuser(mstUser.getId());
			cr.getOd().setOrderstatus(CommonEnum.SYSCODE_ORDERSTATUS_REFUNDED);
			
			boolean flag = refundResultService.dealRefund(cr, isTicket, isGrade, isGradeValue, refPrice, mstUser);
			if(flag){
				//短息发送
				List<NameValuePair> formparams = new ArrayList<NameValuePair>();
				
				String content = String.format("%s|%s", cr.getMstOrder().getOrderno(),refPrice.setScale(2, BigDecimal.ROUND_HALF_UP));
				
				formparams.add(new BasicNameValuePair("userid", cr.getClientid().toString()));  
				formparams.add(new BasicNameValuePair("receivetype",(CommonEnum.SYSCODE_MSG_RECEIVE_TYPE_CLIENT).toString()));  
				formparams.add(new BasicNameValuePair("mobileno", cr.getMstOrder().getPhoneno()));  
				formparams.add(new BasicNameValuePair("mirror", "refund")); 
				formparams.add(new BasicNameValuePair("values",content)); 
				
				String result = CommonUtils.sendSMS(formparams);
				
				logger.info("decPara result: " + result);
			}
		} else {
			//code null
		}

		StringBuffer sb = new StringBuffer("?");
		sb.append("menuId=" + folPara.getMenuId());
		sb.append("&page=" + folPara.getPage());
		sb.append("&rows=" + folPara.getRows());
		sb.append("&orderNo=" + folPara.getOrderNo());

		logger.info("save end run(s):" + DateUtil.calLastedTime(startDate));

		return "redirect:../clientRefundController/list" + sb.toString();
	} 
}
