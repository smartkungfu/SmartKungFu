package com.lst.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.FolPara;
import com.lst.model.MstButton;
import com.lst.model.MstUser;
import com.lst.service.AnswerService;
import com.lst.service.CompanionService;
import com.lst.service.DynamicService;
import com.lst.service.MstButtonService;
import com.lst.service.MstClientService;
import com.lst.service.QuestionService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: DataController
 * @Description: 嘿哈管理
 *
 */
@Controller
@RequestMapping("/dataController")
public class DataController extends BaseController {
	
	@Autowired
	private MstButtonService mstButtonService;
	
	@Autowired
	private MstClientService mstClientService;
	
	@Autowired
	private DynamicService dynamicService;
	
	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private AnswerService answerService;
	
	@Autowired
	private CompanionService companionService;
	
	/**
	 * 
	* @Title: Main
	* @Description: 列表页跳转
	* @param @param menuId
	* @param @param page
	* @param @param rows
	* @param @param index_exp
	* @param @param index_che
	* @param @param minDate
	* @param @param maxDate
	* @param @param typeFol
	* @param @param ariTypeFol
	* @param @return
	* @param @throws MyException    设定文件
	* @return ModelAndView    返回类型
	* @throws
	 */
	@RequestMapping(value = "/Main")
	public ModelAndView Main(
			@RequestParam(required = false) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE)Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) throws MyException {
		Date startDate = new Date();
		logger.info("Main begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_STATISTICS + "/data");

		// 查询用户权限按钮
		MstUser mstUser = this.getMySession().getMstUser();
		List<MstButton> btns = CommonUtils.readPermissionWithBtn(menuId, mstUser, mstButtonService);

		//写参数
		FolPara folPara = new FolPara();
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setMenuId(menuId);
	

		mv.addObject("btns", btns);
		mv.addObject("folPara", folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("Main end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}
	
	
	/**
	 * 
	* @Title: Main
	* @Description: 列表页跳转
	* @param @param menuId
	* @param @param page
	* @param @param rows
	* @param @param index_exp
	* @param @param index_che
	* @param @param minDate
	* @param @param maxDate
	* @param @param typeFol
	* @param @param ariTypeFol
	* @param @return
	* @param @throws MyException    设定文件
	* @return ModelAndView    返回类型
	* @throws
	 */
	@RequestMapping(value = "/Main2")
	public ModelAndView Main2(
			@RequestParam(required = false) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE)Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) throws MyException {
		Date startDate = new Date();
		logger.info("Main begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_STATISTICS + "/dataweek");

		// 查询用户权限按钮
		MstUser mstUser = this.getMySession().getMstUser();
		List<MstButton> btns = CommonUtils.readPermissionWithBtn(menuId, mstUser, mstButtonService);

		//写参数
		FolPara folPara = new FolPara();
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setMenuId(menuId);
	

		mv.addObject("btns", btns);
		mv.addObject("folPara", folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("Main end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}
	
	
	
	/**
	 * 
	* @Title: Main
	* @Description: 列表页跳转
	* @param @param menuId
	* @param @param page
	* @param @param rows
	* @param @param index_exp
	* @param @param index_che
	* @param @param minDate
	* @param @param maxDate
	* @param @param typeFol
	* @param @param ariTypeFol
	* @param @return
	* @param @throws MyException    设定文件
	* @return ModelAndView    返回类型
	* @throws
	 */
	@RequestMapping(value = "/Main3")
	public ModelAndView Main3(
			@RequestParam(required = false) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE)Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) throws MyException {
		Date startDate = new Date();
		logger.info("Main begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_STATISTICS + "/datamonth");

		// 查询用户权限按钮
		MstUser mstUser = this.getMySession().getMstUser();
		List<MstButton> btns = CommonUtils.readPermissionWithBtn(menuId, mstUser, mstButtonService);

		//写参数
		FolPara folPara = new FolPara();
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setMenuId(menuId);
	

		mv.addObject("btns", btns);
		mv.addObject("folPara", folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("Main end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}
	
	
	

/**
 * 今天数据量查询
 * @param page
 * @param rows
 * @param sort
 * @param order
 * @return
 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate) {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		List<Integer> dataList=new ArrayList<Integer>();
		
		System.out.println("开始时间"+minDate);
		System.out.println("结束时间"+maxDate);
		try {
			if(!StringUtils.isBlank(minDate)&& !StringUtils.isBlank(maxDate)){
				Date startTime=sdf.parse(minDate);
				Date endTime = sdf.parse(maxDate);
				Map<String, Object> reqMap = new HashMap<String, Object>();
				reqMap.put("startTime", startTime);
				reqMap.put("endTime", endTime);
				
				int clientNum =  mstClientService.countByMap(reqMap);//今日新增用户量
				int dyNum = dynamicService.countByMap(reqMap);//今日新增动态量
				int quNum = questionService.countByMap(reqMap);//今日新增问题量
				int anNum = answerService.countByMap(reqMap);//今日新增回答量
				int comNum = companionService.countByMap(reqMap);//今日新增嘿哈量
				
		        dataList.add(clientNum);
		        dataList.add(dyNum);
		        dataList.add(quNum);
		        dataList.add(anNum);
		        dataList.add(comNum);
			}else{
				Date startTime = sdf.parse(sdf.format(startDate));
				Calendar ca = Calendar.getInstance();
				ca.add(Calendar.DATE, 1);
				Date endTime2 = ca.getTime();
				Date endTime = sdf.parse(sdf.format(endTime2));
				Map<String, Object> reqMap = new HashMap<String, Object>();
				reqMap.put("startTime", startTime);
				reqMap.put("endTime", endTime);
				
				int clientNum =  mstClientService.countByMap(reqMap);//今日新增用户量
				int dyNum = dynamicService.countByMap(reqMap);//今日新增动态量
				int quNum = questionService.countByMap(reqMap);//今日新增问题量
				int anNum = answerService.countByMap(reqMap);//今日新增回答量
				int comNum = companionService.countByMap(reqMap);//今日新增嘿哈量
				
		        dataList.add(clientNum);
		        dataList.add(dyNum);
		        dataList.add(quNum);
		        dataList.add(anNum);
		        dataList.add(comNum);
			}
			
		} catch (ParseException e) {
			
			e.printStackTrace();
		}

		 return dataList;
		
	}
	
	
	/**
	 * 一周数量查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/queryList2")
	@ResponseBody
	public Object queryList2(@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order) throws Exception {
	
			List<Integer> dataList = new ArrayList<Integer>();
	
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date timeWeek1 = getNowWeekBegin();//星期一日期
			Calendar ca = Calendar.getInstance();
			ca.setTime(timeWeek1);
			ca.add(Calendar.DATE, 1);
			Date time1 = ca.getTime();
			Date timeWeek2 = sdf.parse(sdf.format(time1));//星期二日期
			
			ca.add(Calendar.DATE, 1);
			Date time2 = ca.getTime();
			Date timeWeek3 = sdf.parse(sdf.format(time2));//星期三日期
			
			ca.add(Calendar.DATE, 1);
			Date time3 = ca.getTime();
			Date timeWeek4 = sdf.parse(sdf.format(time3));//星期四日期
			
			ca.add(Calendar.DATE, 1);
			Date time4 = ca.getTime();
			Date timeWeek5 = sdf.parse(sdf.format(time4));//星期五日期
			
			ca.add(Calendar.DATE, 1);
			Date time5 = ca.getTime();
			Date timeWeek6 = sdf.parse(sdf.format(time5));//星期六日期
			
			ca.add(Calendar.DATE, 1);
			Date time6 = ca.getTime();
			Date timeWeek7 = sdf.parse(sdf.format(time6));//星期天日期
			
			ca.add(Calendar.DATE, 1);
			Date time7 = ca.getTime();
			Date timeWeek8 = sdf.parse(sdf.format(time7));//下周一日期
	
			Map<String, Object> reqMap1 = new HashMap<String, Object>();
			reqMap1.put("startTime", timeWeek1);
			reqMap1.put("endTime", timeWeek2);
	
			int clientNum1 = mstClientService.countByMap(reqMap1);// 星期一新增用户量
			int dyNum1 = dynamicService.countByMap(reqMap1);// 星期一新增动态量
			int quNum1 = questionService.countByMap(reqMap1);// 星期一新增问题量
			int anNum1 = answerService.countByMap(reqMap1);// 星期一新增回答量
			int comNum1 = companionService.countByMap(reqMap1);// 星期一新增嘿哈量
			
			Map<String, Object> reqMap2 = new HashMap<String, Object>();
			reqMap2.put("startTime", timeWeek2);
			reqMap2.put("endTime", timeWeek3);
	
			int clientNum2 = mstClientService.countByMap(reqMap2);// 星期二新增用户量
			int dyNum2 = dynamicService.countByMap(reqMap2);// 星期二新增动态量
			int quNum2 = questionService.countByMap(reqMap2);// 星期二新增问题量
			int anNum2 = answerService.countByMap(reqMap2);// 星期二新增回答量
			int comNum2 = companionService.countByMap(reqMap2);// 星期二新增嘿哈量
			
			Map<String, Object> reqMap3 = new HashMap<String, Object>();
			reqMap3.put("startTime", timeWeek3);
			reqMap3.put("endTime", timeWeek4);
	
			int clientNum3 = mstClientService.countByMap(reqMap3);// 星期三新增用户量
			int dyNum3 = dynamicService.countByMap(reqMap3);// 星期三新增动态量
			int quNum3 = questionService.countByMap(reqMap3);// 星期三新增问题量
			int anNum3 = answerService.countByMap(reqMap3);// 星期三新增回答量
			int comNum3 = companionService.countByMap(reqMap3);// 星期三新增嘿哈量
			
			Map<String, Object> reqMap4 = new HashMap<String, Object>();
			reqMap4.put("startTime", timeWeek4);
			reqMap4.put("endTime", timeWeek5);
	
			int clientNum4 = mstClientService.countByMap(reqMap4);// 星期四新增用户量
			int dyNum4 = dynamicService.countByMap(reqMap4);// 星期四新增动态量
			int quNum4 = questionService.countByMap(reqMap4);// 星期四新增问题量
			int anNum4 = answerService.countByMap(reqMap4);// 星期四新增回答量
			int comNum4 = companionService.countByMap(reqMap4);// 星期四新增嘿哈量
			
			Map<String, Object> reqMap5 = new HashMap<String, Object>();
			reqMap5.put("startTime", timeWeek5);
			reqMap5.put("endTime", timeWeek6);
	
			int clientNum5 = mstClientService.countByMap(reqMap5);// 星期五新增用户量
			int dyNum5 = dynamicService.countByMap(reqMap5);// 星期五新增动态量
			int quNum5 = questionService.countByMap(reqMap5);// 星期五新增问题量
			int anNum5 = answerService.countByMap(reqMap5);// 星期五新增回答量
			int comNum5 = companionService.countByMap(reqMap5);// 星期五新增嘿哈量
			
			Map<String, Object> reqMap6 = new HashMap<String, Object>();
			reqMap6.put("startTime", timeWeek6);
			reqMap6.put("endTime", timeWeek7);
	
			int clientNum6 = mstClientService.countByMap(reqMap6);// 星期六新增用户量
			int dyNum6 = dynamicService.countByMap(reqMap6);// 星期六新增动态量
			int quNum6 = questionService.countByMap(reqMap6);// 星期六新增问题量
			int anNum6 = answerService.countByMap(reqMap6);// 星期六新增回答量
			int comNum6 = companionService.countByMap(reqMap6);// 星期六新增嘿哈量
			
			Map<String, Object> reqMap7 = new HashMap<String, Object>();
			reqMap7.put("startTime", timeWeek7);
			reqMap7.put("endTime", timeWeek8);
	
			int clientNum7 = mstClientService.countByMap(reqMap7);// 星期天新增用户量
			int dyNum7 = dynamicService.countByMap(reqMap7);// 星期天新增动态量
			int quNum7 = questionService.countByMap(reqMap7);// 星期天新增问题量
			int anNum7 = answerService.countByMap(reqMap7);// 星期天新增回答量
			int comNum7 = companionService.countByMap(reqMap7);// 星期天新增嘿哈量
	
			dataList.add(clientNum1);
			dataList.add(clientNum2);
			dataList.add(clientNum3);
			dataList.add(clientNum4);
			dataList.add(clientNum5);
			dataList.add(clientNum6);
			dataList.add(clientNum7);
			
			dataList.add(dyNum1);
			dataList.add(dyNum2);
			dataList.add(dyNum3);
			dataList.add(dyNum4);
			dataList.add(dyNum5);
			dataList.add(dyNum6);
			dataList.add(dyNum7);
			
			dataList.add(quNum1);
			dataList.add(quNum2);
			dataList.add(quNum3);
			dataList.add(quNum4);
			dataList.add(quNum5);
			dataList.add(quNum6);
			dataList.add(quNum7);
			
			dataList.add(anNum1);
			dataList.add(anNum2);
			dataList.add(anNum3);
			dataList.add(anNum4);
			dataList.add(anNum5);
			dataList.add(anNum6);
			dataList.add(anNum7);
			
			dataList.add(comNum1);
			dataList.add(comNum2);
			dataList.add(comNum3);
			dataList.add(comNum4);
			dataList.add(comNum5);
			dataList.add(comNum6);
			dataList.add(comNum7);
			return dataList;
		
	}
	
	/**
	 * 一年数量查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/queryList3")
	@ResponseBody
	public Object queryList3(@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order) throws Exception {
	
			List<Integer> dataList = new ArrayList<Integer>();
	
			Date timeMonth1 = getNowMothBegin();//今年1月1日
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar ca = Calendar.getInstance();
			ca.setTime(timeMonth1);
			ca.add(Calendar.MONTH, 1);
			Date time1 = ca.getTime();
			Date timeMonth2 = sdf.parse(sdf.format(time1));//今年2月1日
			
			ca.add(Calendar.MONTH, 1);
			Date time2 = ca.getTime();
			Date timeMonth3 = sdf.parse(sdf.format(time2));//今年3月1日
			
			ca.add(Calendar.MONTH, 1);
			Date time3 = ca.getTime();
			Date timeMonth4 = sdf.parse(sdf.format(time3));//今年4月1日
			
			ca.add(Calendar.MONTH, 1);
			Date time4 = ca.getTime();
			Date timeMonth5 = sdf.parse(sdf.format(time4));//今年5月1日
			
			ca.add(Calendar.MONTH, 1);
			Date time5 = ca.getTime();
			Date timeMonth6 = sdf.parse(sdf.format(time5));//今年6月1日
			
			ca.add(Calendar.MONTH, 1);
			Date time6 = ca.getTime();
			Date timeMonth7 = sdf.parse(sdf.format(time6));//今年7月1日
			
			ca.add(Calendar.MONTH, 1);
			Date time7 = ca.getTime();
			Date timeMonth8 = sdf.parse(sdf.format(time7));//今年8月1日
			
			ca.add(Calendar.MONTH, 1);
			Date time8 = ca.getTime();
			Date timeMonth9 = sdf.parse(sdf.format(time8));//今年9月1日
			
			ca.add(Calendar.MONTH, 1);
			Date time9 = ca.getTime();
			Date timeMonth10 = sdf.parse(sdf.format(time9));//今年10月1日
			
			ca.add(Calendar.MONTH, 1);
			Date time10 = ca.getTime();
			Date timeMonth11 = sdf.parse(sdf.format(time10));//今年11月1日
			
			ca.add(Calendar.MONTH, 1);
			Date time11 = ca.getTime();
			Date timeMonth12 = sdf.parse(sdf.format(time11));//今年12月1日
			
			ca.add(Calendar.MONTH, 1);
			Date time12 = ca.getTime();
			Date timeMonth13 = sdf.parse(sdf.format(time12));//下年1月1日
	
			Map<String, Object> reqMap1 = new HashMap<String, Object>();
			reqMap1.put("startTime", timeMonth1);
			reqMap1.put("endTime", timeMonth2);
	
			int clientNum1 = mstClientService.countByMap(reqMap1);// 一月新增用户量
			int dyNum1 = dynamicService.countByMap(reqMap1);// 一月新增动态量
			int quNum1 = questionService.countByMap(reqMap1);// 一月新增问题量
			int anNum1 = answerService.countByMap(reqMap1);// 一月新增回答量
			int comNum1 = companionService.countByMap(reqMap1);// 一月新增嘿哈量
			
			Map<String, Object> reqMap2 = new HashMap<String, Object>();
			reqMap2.put("startTime", timeMonth2);
			reqMap2.put("endTime", timeMonth3);
	
			int clientNum2 = mstClientService.countByMap(reqMap2);// 二月新增用户量
			int dyNum2 = dynamicService.countByMap(reqMap2);// 二月新增动态量
			int quNum2 = questionService.countByMap(reqMap2);// 二月新增问题量
			int anNum2 = answerService.countByMap(reqMap2);// 二月新增回答量
			int comNum2 = companionService.countByMap(reqMap2);// 二月新增嘿哈量
			
			Map<String, Object> reqMap3 = new HashMap<String, Object>();
			reqMap3.put("startTime", timeMonth3);
			reqMap3.put("endTime", timeMonth4);
	
			int clientNum3 = mstClientService.countByMap(reqMap3);// 三月新增用户量
			int dyNum3 = dynamicService.countByMap(reqMap3);// 三月新增动态量
			int quNum3 = questionService.countByMap(reqMap3);// 三月新增问题量
			int anNum3 = answerService.countByMap(reqMap3);// 三月新增回答量
			int comNum3 = companionService.countByMap(reqMap3);// 三月新增嘿哈量
			
			Map<String, Object> reqMap4 = new HashMap<String, Object>();
			reqMap4.put("startTime", timeMonth4);
			reqMap4.put("endTime", timeMonth5);
	
			int clientNum4 = mstClientService.countByMap(reqMap4);// 四月新增用户量
			int dyNum4 = dynamicService.countByMap(reqMap4);// 四月新增动态量
			int quNum4 = questionService.countByMap(reqMap4);// 四月新增问题量
			int anNum4 = answerService.countByMap(reqMap4);// 四月新增回答量
			int comNum4 = companionService.countByMap(reqMap4);//四月新增嘿哈量
			
			Map<String, Object> reqMap5 = new HashMap<String, Object>();
			reqMap5.put("startTime", timeMonth5);
			reqMap5.put("endTime", timeMonth6);
	
			int clientNum5 = mstClientService.countByMap(reqMap5);// 五月新增用户量
			int dyNum5 = dynamicService.countByMap(reqMap5);// 五月新增动态量
			int quNum5 = questionService.countByMap(reqMap5);// 五月新增问题量
			int anNum5 = answerService.countByMap(reqMap5);// 五月新增回答量
			int comNum5 = companionService.countByMap(reqMap5);// 五月新增嘿哈量
			
			Map<String, Object> reqMap6 = new HashMap<String, Object>();
			reqMap6.put("startTime", timeMonth6);
			reqMap6.put("endTime", timeMonth7);
	
			int clientNum6 = mstClientService.countByMap(reqMap6);// 六月新增用户量
			int dyNum6 = dynamicService.countByMap(reqMap6);// 六月新增动态量
			int quNum6 = questionService.countByMap(reqMap6);// 六月新增问题量
			int anNum6 = answerService.countByMap(reqMap6);// 六月新增回答量
			int comNum6 = companionService.countByMap(reqMap6);// 六月新增嘿哈量
			
			Map<String, Object> reqMap7 = new HashMap<String, Object>();
			reqMap7.put("startTime", timeMonth7);
			reqMap7.put("endTime", timeMonth8);
	
			int clientNum7 = mstClientService.countByMap(reqMap7);// 七月新增用户量
			int dyNum7 = dynamicService.countByMap(reqMap7);// 七月新增动态量
			int quNum7 = questionService.countByMap(reqMap7);// 七月新增问题量
			int anNum7 = answerService.countByMap(reqMap7);// 七月新增回答量
			int comNum7 = companionService.countByMap(reqMap7);// 七月新增嘿哈量
			
			Map<String, Object> reqMap8 = new HashMap<String, Object>();
			reqMap8.put("startTime", timeMonth8);
			reqMap8.put("endTime", timeMonth9);
	
			int clientNum8 = mstClientService.countByMap(reqMap8);// 八月新增用户量
			int dyNum8 = dynamicService.countByMap(reqMap8);// 八月新增动态量
			int quNum8 = questionService.countByMap(reqMap8);// 八月新增问题量
			int anNum8 = answerService.countByMap(reqMap8);// 八月新增回答量
			int comNum8 = companionService.countByMap(reqMap8);// 八月新增嘿哈量
			
			Map<String, Object> reqMap9 = new HashMap<String, Object>();
			reqMap9.put("startTime", timeMonth9);
			reqMap9.put("endTime", timeMonth10);
	
			int clientNum9 = mstClientService.countByMap(reqMap9);// 九月新增用户量
			int dyNum9 = dynamicService.countByMap(reqMap9);// 九月新增动态量
			int quNum9 = questionService.countByMap(reqMap9);// 九月新增问题量
			int anNum9 = answerService.countByMap(reqMap9);// 九月新增回答量
			int comNum9 = companionService.countByMap(reqMap9);// 九月新增嘿哈量
			
			Map<String, Object> reqMap10 = new HashMap<String, Object>();
			reqMap10.put("startTime", timeMonth10);
			reqMap10.put("endTime", timeMonth11);
	
			int clientNum10 = mstClientService.countByMap(reqMap10);// 十月新增用户量
			int dyNum10 = dynamicService.countByMap(reqMap10);// 十月新增动态量
			int quNum10 = questionService.countByMap(reqMap10);// 十月新增问题量
			int anNum10 = answerService.countByMap(reqMap10);// 十月新增回答量
			int comNum10 = companionService.countByMap(reqMap10);// 十月新增嘿哈量
			
			Map<String, Object> reqMap11 = new HashMap<String, Object>();
			reqMap11.put("startTime", timeMonth11);
			reqMap11.put("endTime", timeMonth12);
	
			int clientNum11 = mstClientService.countByMap(reqMap11);// 十一月新增用户量
			int dyNum11 = dynamicService.countByMap(reqMap11);// 十一月新增动态量
			int quNum11 = questionService.countByMap(reqMap11);// 十一月新增问题量
			int anNum11 = answerService.countByMap(reqMap11);// 十一月新增回答量
			int comNum11 = companionService.countByMap(reqMap11);// 十一月新增嘿哈量
			
			Map<String, Object> reqMap12 = new HashMap<String, Object>();
			reqMap12.put("startTime", timeMonth12);
			reqMap12.put("endTime", timeMonth13);
	
			int clientNum12 = mstClientService.countByMap(reqMap12);// 十二月新增用户量
			int dyNum12 = dynamicService.countByMap(reqMap12);// 十二月新增动态量
			int quNum12 = questionService.countByMap(reqMap12);// 十二月新增问题量
			int anNum12 = answerService.countByMap(reqMap12);// 十二月新增回答量
			int comNum12 = companionService.countByMap(reqMap12);// 十二月新增嘿哈量
	
			dataList.add(clientNum1);
			dataList.add(clientNum2);
			dataList.add(clientNum3);
			dataList.add(clientNum4);
			dataList.add(clientNum5);
			dataList.add(clientNum6);
			dataList.add(clientNum7);
			dataList.add(clientNum8);
			dataList.add(clientNum9);
			dataList.add(clientNum10);
			dataList.add(clientNum11);
			dataList.add(clientNum12);
			
			dataList.add(dyNum1);
			dataList.add(dyNum2);
			dataList.add(dyNum3);
			dataList.add(dyNum4);
			dataList.add(dyNum5);
			dataList.add(dyNum6);
			dataList.add(dyNum7);
			dataList.add(dyNum8);
			dataList.add(dyNum9);
			dataList.add(dyNum10);
			dataList.add(dyNum11);
			dataList.add(dyNum12);
			
			dataList.add(quNum1);
			dataList.add(quNum2);
			dataList.add(quNum3);
			dataList.add(quNum4);
			dataList.add(quNum5);
			dataList.add(quNum6);
			dataList.add(quNum7);
			dataList.add(quNum8);
			dataList.add(quNum9);
			dataList.add(quNum10);
			dataList.add(quNum11);
			dataList.add(quNum12);
			
			dataList.add(anNum1);
			dataList.add(anNum2);
			dataList.add(anNum3);
			dataList.add(anNum4);
			dataList.add(anNum5);
			dataList.add(anNum6);
			dataList.add(anNum7);
			dataList.add(anNum8);
			dataList.add(anNum9);
			dataList.add(anNum10);
			dataList.add(anNum11);
			dataList.add(anNum12);
			
			dataList.add(comNum1);
			dataList.add(comNum2);
			dataList.add(comNum3);
			dataList.add(comNum4);
			dataList.add(comNum5);
			dataList.add(comNum6);
			dataList.add(comNum7);
			dataList.add(comNum8);
			dataList.add(comNum9);
			dataList.add(comNum10);
			dataList.add(comNum11);
			dataList.add(comNum12);
			
			return dataList;
		
	}	
	
	
	/**
	 * 获得本周的第一天
	 * @return
	 * @throws Exception
	 */
	public static Date getNowWeekBegin() throws Exception {
		
		int mondayPlus;
		Calendar cd = Calendar.getInstance();
		// 获得今天是一周的第几天，星期日是第一天，星期二是第二天......
		int dayOfWeek = cd.get(Calendar.DAY_OF_WEEK) - 1; // 因为按中国礼拜一作为第一天所以这里减1
		if (dayOfWeek == 1) {
			mondayPlus = 0;
		} else {
			mondayPlus = 1 - dayOfWeek;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar ca = Calendar.getInstance();
		ca.add(Calendar.DATE, mondayPlus);
		Date startDate = ca.getTime();

		Date startTime = sdf.parse(sdf.format(startDate));
		return startTime;

		}
	
	/**
	 * 获得本年的第一天
	 * @return
	 * @throws Exception
	 */
	public static Date getNowMothBegin() throws Exception {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		Date date=new Date();
		String dateStr=sdf.format(date)+"-01-01";
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		Date startTime=sdf2.parse(dateStr);
		return startTime;

		}
	
}
