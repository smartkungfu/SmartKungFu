/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：ResourcesController   
 * 类描述：资源控制类   
 * 创建人：zhangl  
 * 创建时间：2016年10月18日 下午1:59:53   
 * 修改人：zhangl   
 * 修改时间：2016年10月18日 下午1:59:53   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.lst.common.BaseController;
import com.lst.service.ResourcesService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: ResourcesController
 * @Description: 资源控制类
 * @author zhangl
 * @date 2016年10月18日 下午1:59:53
 *
 */
@Controller
@RequestMapping("resourcesController")
public class ResourcesController extends BaseController {

	@Autowired
	private ResourcesService resourcesService;

	/**
	 * 
	 * @Title: saveTempImage
	 * @Description: 临时保存文件，预览用
	 * @param @param request
	 * @param @param response
	 * @param @return 文件路径
	 * @return Object 返回类型
	 * @throws
	 * 
	 */
	@RequestMapping("/saveTempImage")
	@ResponseBody
	public ResObj saveTempImage(HttpServletRequest request, HttpServletResponse response, MultipartFile myfiles,MultipartFile cover,MultipartFile cover1,MultipartFile cover2) {
		Date startDate = new Date();
		logger.info("saveTempIamge begin: " + startDate);

		String str = null;

		try {
			if(myfiles != null){
				str =  CommonUtils.saveTempImage(myfiles, request);
			}
			
			if(cover != null){
				str =  CommonUtils.saveTempImage(cover, request);
			}
			
			if(cover1 != null){
				str =  CommonUtils.saveTempImage(cover1, request);
			}
			
			if(cover2 != null){
				str =  CommonUtils.saveTempImage(cover2, request);
			}
			
		} catch (Exception e) {
			logger.error("saveTempImage error: " ,e);
		}

		logger.info("decPara str: " + str );

		logger.info("saveTempIamge end run(s): " + DateUtil.calLastedTime(startDate));

		ResObj resObj = new ResObj();
		resObj.setResName(str);

		return  resObj;
	}

	/**
	 * 
	 * @ClassName: ResObj
	 * @Description:为处理bootstrap fileinput插件返回值不能带后缀问题，声明内部内用对象的形式包裹参数
	 * @author zhangl
	 * @date 2016年10月19日 上午9:53:49
	 *
	 */
	private class ResObj{

		private String resName;

		@SuppressWarnings("unused")
		public String getResName() {
			return resName;
		}

		public void setResName(String resName) {
			this.resName = resName;
		}
	}
}
