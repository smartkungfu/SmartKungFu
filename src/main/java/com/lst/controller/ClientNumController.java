/**   
*    
* 项目名称：SmartKungFu   
* 类名称：ClientNumController   
* 类描述：   
* 创建人：Wdd   
* 创建时间：2016年11月4日 下午5:15:42   
* 修改人：Wdd  
* 修改时间：2016年11月4日 下午5:15:42   
* 修改备注：   
* @version    
*    
*/
package com.lst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.ClientNum;
import com.lst.model.FolPara;
import com.lst.model.MstGymnasium;
import com.lst.model.ProjectInfo;
import com.lst.model.StarInfo;
import com.lst.model.VideoInfo;
import com.lst.model.WenwInfo;
import com.lst.service.ClientNumService;
import com.lst.service.MstGymnasiumService;
import com.lst.service.ProjectInfoService;
import com.lst.service.StarInfoService;
import com.lst.service.VideoInfoService;
import com.lst.service.WenwInfoService;
import com.lst.utils.DateUtil;

/**
 * @ClassName: ClientNumController
 * @Description: 用户收藏
 * @author Wdd
 * @date 2016年11月4日 下午5:15:42
 *
 */
@Controller
@RequestMapping("/clientNumController")
public class ClientNumController extends BaseController {
	
	@Autowired
	private ClientNumService clientNumService;
	
	@Autowired
	private WenwInfoService wenwInfoService;

	@Autowired
	private VideoInfoService videoInfoService;

	@Autowired
	private ProjectInfoService projectInfoService;

	@Autowired
	private StarInfoService starInfoService;

	@Autowired
	private MstGymnasiumService mstGymnasiumService;

	/**
	 * 
	 * @Title: Main
	 * @Description:页面加载
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @param minDate
	 * @param @param maxDate
	 * @param @param nickName
	 * @param @param mobileNo
	 * @param @param nameCn
	 * @param @param clientid
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/Main")
	public ModelAndView Main(
			@RequestParam(required = false) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE)Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "") String nickName,
			@RequestParam(required = false,defaultValue = "") String mobileNo,
			@RequestParam(required = false,defaultValue = "") String nameCn,
			@RequestParam(required = false) int clientid) throws MyException {
		Date startDate = new Date();
		logger.info("Main begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_APP_MANAGE + "/client_num_list");

		//写参数
		FolPara folPara = new FolPara();
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setNickName(nickName);
		folPara.setMinDate(minDate);
		folPara.setMaxDate(maxDate);
		folPara.setNameCn(nameCn);
		folPara.setMobileNo(mobileNo);
		folPara.setMenuId(menuId);

		mv.addObject("folPara", folPara);
		mv.addObject("clientid", clientid);
		
		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("Main end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: queryList
	 * @Description: 查询分页数据
	 * @param @param page
	 * @param @param rows
	 * @param @param clientid
	 * @param @return    设定文件
	 * @return Object    返回类型
	 * @throws
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page, 
			@RequestParam(required = false, defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer rows,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false) int clientid,
			@RequestParam(required = false,defaultValue = "") String minDate,
			@RequestParam(required = false,defaultValue = "") String maxDate,
			@RequestParam(required = false,defaultValue = "") String numtype,
			@RequestParam(required = false,defaultValue = "") String acttype) {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		/**
		 * 查询用户收藏信息
		 */
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("isDeleted", 0);
		reqMap.put("enabled", 1);
		reqMap.put("clientid", clientid);
		
		if(StringUtils.isEmpty(sort)){
			reqMap.put("orderByClause","n.createdate desc");
		} else{
			reqMap.put("orderByClause",sort + " " + order);
		}
		if(!StringUtils.isBlank(numtype)){
			reqMap.put("numtype",Integer.parseInt(numtype));
		}
		if(!StringUtils.isBlank(acttype)){
			reqMap.put("acttype",Integer.parseInt(acttype));
		}

		if(!StringUtils.isBlank(minDate) && !StringUtils.isBlank(maxDate)){
			reqMap.put("minDate", minDate + DateUtil.START_DATE_HMS);
			reqMap.put("maxDate", maxDate + DateUtil.END_DATE_HMS);
		}

		PageBounds pageBounds = new PageBounds(page, rows);
		List<ClientNum> list = clientNumService.queryByList(reqMap, pageBounds);

		//根据内容类型查询相应的表，取出标题
		for (ClientNum clientNum : list) {
			switch (clientNum.getActtype()) {
			//文章
			case CommonEnum.SYSCODE_GFBK_WZBK:
				WenwInfo wenwInfo = wenwInfoService.selectByPrimaryKey(clientNum.getActid());
				clientNum.setTitle(wenwInfo.getTitle());
				break;
				//视频
			case CommonEnum.SYSCODE_GFBK_SPBK:
				VideoInfo videoInfo = videoInfoService.selectByPrimaryKey(clientNum.getActid());
				clientNum.setTitle(videoInfo.getTitle());
				break;
				//活动
			case CommonEnum.SYSCODE_GFBK_HDBK:
				ProjectInfo projectInfo = projectInfoService.selectByPrimaryKey(clientNum.getActid());
				clientNum.setTitle(projectInfo.getTitle());
				break;
				//场馆
			case CommonEnum.SYSCODE_GFBK_CGBK:
				MstGymnasium mstGymnasium = mstGymnasiumService.selectByPrimaryKey(clientNum.getActid());
				clientNum.setTitle(mstGymnasium.getGymname());
				break;
				//达人
			case CommonEnum.SYSCODE_GFBK_DRBK:
				StarInfo starInfo = starInfoService.selectByPrimaryKey(clientNum.getActid());
				clientNum.setTitle(starInfo.getTitle());
				break;
			default:
				break;
			}
		}

		PageList<ClientNum> pageList = (PageList<ClientNum>) list;
		Paginator paginator = pageList.getPaginator();

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		// indexPages.put("paginator", paginator);
		// indexPages.put("totalRows", paginator.getTotalCount());
		// indexPages.put("pageSize", paginator.getLimit());
		// indexPages.put("pageNumber", paginator.getPage());
		indexPages.put("total", paginator.getTotalCount());
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}

}
