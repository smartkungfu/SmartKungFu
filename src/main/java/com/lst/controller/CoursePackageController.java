/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：CoursePackageController   
 * 类描述：    课程套餐控制类
 * 创建人：zhangl  
 * 创建时间：2016年11月1日 下午5:47:45   
 * 修改人：zhangl   
 * 修改时间：2016年11月1日 下午5:47:45   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.CoursePackage;
import com.lst.model.GymnasiumDetail;
import com.lst.model.MstUser;
import com.lst.service.CoursePackageService;
import com.lst.service.GymnasiumDetailService;
import com.lst.utils.DateUtil;

/**
 * @ClassName: CoursePackageController
 * @Description: 课程套餐控制类
 * @author zhangl
 * @date 2016年11月1日 下午5:47:45
 *
 */
@Controller
@RequestMapping("coursePackageController")
public class CoursePackageController extends BaseController {

	@Autowired
	private CoursePackageService coursePackageService;
	
	@Autowired
	private GymnasiumDetailService gymnasiumDetailService;

	/**
	 * 
	 * @Title: edit
	 * @Description: 编辑页面加载
	 * @param @param id
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping("edit")
	public ModelAndView edit(
			@RequestParam(required = true)Integer id,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che){
		Date startDate = new Date();
		logger.info("edit begin: " + startDate);

		ModelAndView mv = new ModelAndView();
		mv.setViewName(CommonEnum.FORWARD_VIEW_MAIN_KUNGFU + "cp_edit");
		
		GymnasiumDetail gd = gymnasiumDetailService.selectByPrimaryKey(id);
		mv.addObject("gd", gd);
		
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("enabled", true);
		reqMap.put("isDeleted", false);
		reqMap.put("courseId",id);
		reqMap.put("orderByClause", "cp.createdate");

		PageBounds pageBounds = new PageBounds();

		List<CoursePackage> cps = coursePackageService.queryByList(reqMap, pageBounds);

		mv.addObject("cps", cps);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("edit end run(s): " + DateUtil.calLastedTime(startDate));

		return mv; 
	}
	
	/**
	 * 
	 * @Title: saveWithList
	 * @Description:数据更新
	 * @param @param mstCodes
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return String    返回类型
	 */
	@RequestMapping(value = "/saveWithList")
	@ResponseBody
	public boolean saveWithList(@RequestBody List<CoursePackage> cps) throws MyException {
		Date startDate = new Date();
		logger.info("saveWithList begin: " + startDate);

		MstUser mstUser = this.getMySession().getMstUser();
		
		boolean flag = false;

		try {

			if(cps!= null && !cps.isEmpty()){

				Map<String, Object> delMap = new HashMap<String,Object>();
				delMap.put("updateUser", mstUser.getId());
				delMap.put("courseId", cps.get(0).getCourseid());
				
				for(CoursePackage cp : cps){
					cp.setCreatedate(startDate);
					cp.setIsdeleted(false);
					cp.setCreateuser(mstUser.getId());
				}

				coursePackageService.saveWithList(delMap, cps);
				
				flag = true;
			}

		} catch (Exception e) {
			logger.error("saveWithList error: " ,e);
		}

		logger.info("save end run(s):" + DateUtil.calLastedTime(startDate));

		return flag;
	}
}
