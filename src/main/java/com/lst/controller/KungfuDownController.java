/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：KungfuDownController   
 * 类描述：功夫资源下载类   
 * 创建人：zhangl  
 * 创建时间：2016年12月22日 上午11:18:22   
 * 修改人：zhangl   
 * 修改时间：2016年12月22日 上午11:18:22   
 * 修改备注：   功夫资源下载类
 * @version    
 *    
 */
package com.lst.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lst.common.BaseController;
import com.lst.utils.DateUtil;

/**
 * @ClassName: KungfuDownController
 * @Description:资源下载
 * @author zhangl
 * @date 2016年12月22日 上午11:18:22
 *
 */
@Controller
@RequestMapping("kungfuDownController")
public class KungfuDownController extends BaseController {
	
	/**
	 * 
	 * @Title: downloadFile
	 * @Description:(文件下载)
	 * @param @param request
	 * @param @param response
	 * @param @param fileName
	 * @param @throws ServletException
	 * @param @throws IOException    设定文件
	 * @return void    返回类型
	 * @throws
	 */
	@RequestMapping("downloadFile")
	public void downloadFile(HttpServletRequest request, HttpServletResponse response,String fileName) throws
		ServletException, IOException {
		Date startDate = new Date();
		logger.info("downloadFile begin: " + startDate);
		
		if(!StringUtils.isEmpty(fileName)){
			response.setContentType("text/html; charset=GBK");
			String savePath = request.getSession().getServletContext().getRealPath("/download");
			
			File file=new File( savePath + "/" + fileName);
			
			response.setContentType("application/x-msdownload");
			response.setContentLength((int)file.length());
			
			String newFileName = DateUtil.date2Str(startDate, DateUtil.ALL_DATETIME_STRING_QUEUE_MILL);
			response.setHeader("Content-Disposition","attachment;filename=" + newFileName + ".ipa");       
			
			//读出文件到i/o流
			FileInputStream fis = new FileInputStream(file);
			BufferedInputStream buff = new BufferedInputStream(fis);
			byte [] b = new byte[1024];
			long k = 0;
			OutputStream myout = response.getOutputStream();
			while(k < file.length()){
				int j = buff.read(b,0,1024);
				k += j;
				myout.write(b,0,j);  
			}
			
			myout.close();
			fis.close();
		}
		
		logger.info("downloadFile end spent(s): " + DateUtil.calLastedTime(startDate));
		
	}
}
