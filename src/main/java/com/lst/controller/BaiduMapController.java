package com.lst.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.FolPara;
import com.lst.model.MstGymnasium;
import com.lst.model.MstUser;
import com.lst.model.ProjectInfo;
import com.lst.model.vo.MapVo;
import com.lst.service.MstGymnasiumService;
import com.lst.service.ProjectInfoService;
import com.lst.utils.CommonUtils;
import com.lst.utils.DateUtil;

/**
 * @ClassName: BaiduMapController
 * @Description: 百度地图地理分配
 * @author zhangl
 * @date 2016年12月8日 上午10:14:39
 *
 */
@Controller
@RequestMapping("baiduMapController")
public class BaiduMapController extends BaseController {

	@Autowired
	private MstGymnasiumService mstGymnasiumService;

	@Autowired
	private ProjectInfoService projectInfoService;

	/**
	 * 
	 * @Title: list
	 * @Description:页面跳转
	 * @param @param menuId
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/list")
	public ModelAndView Main(
			@RequestParam(required = false) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) throws MyException {
		Date startDate = new Date();
		logger.info("list begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_KUNGFU + "/map_list");

		//写参数
		FolPara folPara = new FolPara();
		folPara.setMenuId(menuId);

		mv.addObject("folPara", folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("list end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: addMap
	 * @Description:添加地图
	 * @param @param mapVo
	 * @param @param br
	 * @param @return    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping(value = "/addMap" , method = RequestMethod.POST)
	@ResponseBody
	public boolean addMap(@Valid MapVo mapVo,BindingResult br){
		Date startDate = new Date();
		logger.info("addMap begin: " + startDate);

		boolean flag = false;

		try {
			if (!br.hasErrors()){

				MstUser mstUser = this.getMySession().getMstUser();
				if(new Integer(CommonEnum.SYSCODE_GFBK_HDBK).equals(mapVo.getActType())){//活动
					ProjectInfo projectInfo  = new ProjectInfo();

					projectInfo.setUpdatedate(startDate);
					projectInfo.setUpdateuser(mstUser.getId());
					projectInfo.setLonggitude(mapVo.getLongitude());
					projectInfo.setLatitude(mapVo.getLatitude());
					projectInfo.setId(mapVo.getActId());

					projectInfoService.updateByPrimaryKeySelective(projectInfo);

					flag = true;
				} else {//场馆
					MstGymnasium mstGymnasiym = new MstGymnasium();
					mstGymnasiym.setUpdatedate(startDate);
					mstGymnasiym.setUpdateuser(mstUser.getId());
					mstGymnasiym.setLongitude(mapVo.getLongitude());
					mstGymnasiym.setLatitude(mapVo.getLatitude());
					mstGymnasiym.setId(mapVo.getActId());

					mstGymnasiumService.updateByPrimaryKeySelective(mstGymnasiym);

					flag = true;
				}
			} else CommonUtils.faramsLogForForm(br);
		} catch (Exception e) {
			logger.error("addMap error:" ,e);
		}

		logger.info("addMap end run(s): " + DateUtil.calLastedTime(startDate));

		return flag;
	} 

	/**
	 * 
	 * @Title: delMap
	 * @Description:删除地图
	 * @param @param mapVo
	 * @param @param br
	 * @param @return    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping(value = "/delMap" , method = RequestMethod.POST)
	@ResponseBody
	public boolean delMap(@Valid MapVo mapVo,BindingResult br){
		Date startDate = new Date();
		logger.info("delMap begin: " + startDate);

		boolean flag = false;

		try {
			if (!br.hasErrors()){

				MstUser mstUser = this.getMySession().getMstUser();
				if(new Integer(CommonEnum.SYSCODE_GFBK_HDBK).equals(mapVo.getActType())){//活动
					ProjectInfo projectInfo  = new ProjectInfo();

					projectInfo.setUpdatedate(startDate);
					projectInfo.setUpdateuser(mstUser.getId());
					projectInfo.setLonggitude("");
					projectInfo.setLatitude("");
					projectInfo.setId(mapVo.getActId());

					projectInfoService.updateByPrimaryKeySelective(projectInfo);

					flag = true;
				} else {//场馆
					MstGymnasium mstGymnasiym = new MstGymnasium();
					mstGymnasiym.setUpdatedate(startDate);
					mstGymnasiym.setUpdateuser(mstUser.getId());
					mstGymnasiym.setLongitude("");
					mstGymnasiym.setLatitude("");
					mstGymnasiym.setId(mapVo.getActId());

					mstGymnasiumService.updateByPrimaryKeySelective(mstGymnasiym);

					flag = true;
				}
			} else CommonUtils.faramsLogForForm(br);
		} catch (Exception e) {
			logger.error("delMap error:" ,e);
		}

		logger.info("delMap end run(s): " + DateUtil.calLastedTime(startDate));

		return flag;
	} 

	/**
	 * 
	 * @Title: initDataForPro
	 * @Description: 初始化活动信息
	 * @param @return    设定文件
	 * @return List<MapVo>  返回类型
	 */
	@RequestMapping(value = "/initDataForPro" ,method = RequestMethod.GET)
	@ResponseBody
	public List<MapVo> initDataForPro(){
		Date startDate = new Date();
		logger.info("initDataForPro begin: " + startDate);

		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("enabled", true);
		reqMap.put("isDeleted",false);
		//reqMap.put("flag","addMap");

		List<ProjectInfo> list = projectInfoService.queryByList(reqMap, new PageBounds());

		List<MapVo> maps = new ArrayList<MapVo>();
		MapVo map = null;
		for(ProjectInfo pro : list){
			map = new MapVo();
			map.setActId(pro.getId());
			map.setTitle(pro.getTitle());
			map.setLongitude(pro.getLonggitude());
			map.setLatitude(pro.getLatitude());
			map.setTitle(pro.getTitle());
			map.setActType(CommonEnum.SYSCODE_GFBK_HDBK);
			map.setAddress(pro.getAddress());
			if(!StringUtils.isEmpty(pro.getProbill())){
				map.setUrl(pro.getProbill().split(";")[0]);
			}

			maps.add(map);
		}

		logger.info("initDataForPro end run(s): " + DateUtil.calLastedTime(startDate));

		return maps;
	}

	/**
	 * 
	 * @Title: initDataFoGym
	 * @Description: 初始化活动信息
	 * @param @return    设定文件
	 * @return List<MapVo>    返回类型
	 */
	@RequestMapping(value = "/initDataFoGym" ,method = RequestMethod.GET)
	@ResponseBody
	public List<MapVo> initDataFoGym(){
		Date startDate = new Date();
		logger.info("initDataFoGym" + startDate);

		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("enabled", true);
		reqMap.put("isDeleted",false);
		//reqMap.put("flag","addMap");

		List<MstGymnasium> list = mstGymnasiumService.queryByList(reqMap, new PageBounds());

		List<MapVo> maps = new ArrayList<MapVo>();
		MapVo map = null;
		for(MstGymnasium gym : list){
			map = new MapVo();
			map.setActId(gym.getId());
			map.setTitle(gym.getGymname());
			map.setLatitude(gym.getLatitude());
			map.setLongitude(gym.getLongitude());
			map.setTitle(gym.getGymname());
			map.setActType(CommonEnum.SYSCODE_GFBK_CGBK);
			map.setAddress(gym.getAddress());
			if(!StringUtils.isEmpty(gym.getGymbill())){
				map.setUrl(gym.getGymbill().split(";")[0]);
			}

			maps.add(map);
		}



		logger.info("initDataFoGym end run(s): " + DateUtil.calLastedTime(startDate));

		return maps;
	}

	/**
	 * 
	 * @Title: initDataFoAll
	 * @Description: 初始化所有数据
	 * @param @return    设定文件
	 * @return List<MapVo>    返回类型
	 */
	@RequestMapping(value = "/initDataForAll" ,method = RequestMethod.GET)
	@ResponseBody
	public List<MapVo> initDataFoAll(){
		Date startDate = new Date();
		logger.info("initDataForAll" + startDate);

		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("enabled", true);
		reqMap.put("isDeleted",false);

		List<MstGymnasium> gymList = mstGymnasiumService.queryByList(reqMap, new PageBounds());

		List<ProjectInfo> proList = projectInfoService.queryByList(reqMap, new PageBounds());

		List<MapVo> maps = new ArrayList<MapVo>();
		MapVo map = null;
		for(MstGymnasium gym : gymList){
			if(!StringUtils.isEmpty(gym.getLongitude()) && !StringUtils.isEmpty(gym.getLatitude())){
				map = new MapVo();
				map.setActId(gym.getId());
				map.setTitle(gym.getGymname());
				map.setLatitude(gym.getLatitude());
				map.setLongitude(gym.getLongitude());
				map.setTitle(gym.getGymname());
				map.setActType(CommonEnum.SYSCODE_GFBK_CGBK);
				map.setAddress(gym.getAddress());
				map.setUrl(gym.getGymbill());
				if(!StringUtils.isEmpty(gym.getGymbill())){
					map.setUrl(gym.getGymbill().split(";")[0]);
				}
				maps.add(map);
			} 

		}

		for(ProjectInfo pro : proList){

			if(!StringUtils.isEmpty(pro.getLonggitude()) 
					&&
			   !StringUtils.isEmpty(pro.getLatitude())){
				map = new MapVo();
				map.setActId(pro.getId());
				map.setTitle(pro.getTitle());
				map.setLongitude(pro.getLonggitude());
				map.setLatitude(pro.getLatitude());
				map.setTitle(pro.getTitle());
				map.setActType(CommonEnum.SYSCODE_GFBK_HDBK);
				map.setAddress(pro.getAddress());
				if(!StringUtils.isEmpty(pro.getProbill())){
					map.setUrl(pro.getProbill().split(";")[0]);
				}
				
				
				maps.add(map);
			}
		}

		logger.info("initDataForAll end run(s): " + DateUtil.calLastedTime(startDate));

		return maps;
	}
}
