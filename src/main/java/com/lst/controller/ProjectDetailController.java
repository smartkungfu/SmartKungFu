/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：ProjectDetailController   
 * 类描述：   活动详情控制类
 * 创建人：zhangl  
 * 创建时间：2016年10月20日 下午6:49:11   
 * 修改人：zhangl   
 * 修改时间：2016年10月20日 下午6:49:11   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.controller;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

import com.lst.common.BaseController;

/**
 * @ClassName: ProjectDetailController
 * @Description: 活动详情控制类
 * @author zhangl
 * @date 2016年10月20日 下午6:49:11
 *
 */
@Controller
@RequestMapping("projectDetailController")
public class ProjectDetailController extends BaseController {
	//code
}
