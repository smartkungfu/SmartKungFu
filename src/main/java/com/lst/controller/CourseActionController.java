package com.lst.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.common.BaseController;
import com.lst.common.CommonEnum;
import com.lst.exception.MyException;
import com.lst.model.CourseAction;
import com.lst.model.FolPara;
import com.lst.model.MstUser;
import com.lst.service.CourseActionService;
import com.lst.utils.DateUtil;

/**
 * @ClassName: CourseActionController
 * @Description: 课程与动作关联
 * @author zmm
 * @date 2017年09月27日 
 *
 */
@Controller
@RequestMapping("courseActionController")
public class CourseActionController extends BaseController {
	
	@Autowired
	private CourseActionService courseActionService;

	/**
	 * 
	 * @Title: list
	 * @Description: 页面加载
	 * @param @param request
	 * @param @param response
	 * @param @param menuId
	 * @param @param page
	 * @param @param rows
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @param title
	 * @param @param courseId
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/list")
	public ModelAndView list(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) int menuId,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE) int page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) int rows,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che,
			@RequestParam(required = false,defaultValue = "") String title,
			@RequestParam(required = true) Integer courseId) throws MyException {
		Date startDate = new Date();
		logger.info("list begin: " + startDate);

		FolPara folPara = new FolPara();
		folPara.setMenuId(menuId);
		folPara.setPage(page);
		folPara.setRows(rows);
		folPara.setTitleFol(title);
		folPara.setCourseId(courseId);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_KUNGFU + "courseAction_list");
		mv.addObject("folPara",folPara);

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("list end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: queryList
	 * @Description: 数据集
	 * @param @param page
	 * @param @param pageSize
	 * @param @param sort
	 * @param @param order
	 * @param @param courseId
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return Object    返回类型
	 */
	@RequestMapping(value = "/queryList")
	@ResponseBody
	public Object queryList(
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGE) Integer page,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEMPAGE_PAGESIZE) Integer pageSize,
			@RequestParam(required = false,defaultValue = "")String sort,
			@RequestParam(required = false,defaultValue = CommonEnum.SYSTEM_ORDER_DESC)String order,
			@RequestParam(required = false,defaultValue = "")String courseId) throws MyException {
		Date startDate = new Date();
		logger.info("queryList begin: " + startDate);

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("isDelete", 0);
		reqMap.put("courseId", courseId);

		if(StringUtils.isEmpty(sort)){
			reqMap.put("orderByClause","md.rank asc");
		} else{
			reqMap.put("orderByClause",sort + " " + order);
		}

		PageBounds pageBounds = new PageBounds();
		List<CourseAction> list = courseActionService.queryByList(reqMap, pageBounds);

		HashMap<String, Object> indexPages = new HashMap<String, Object>();
		indexPages.put("total",list.size());	
		indexPages.put("rows", list);

		logger.info("queryList end run(s): " + DateUtil.calLastedTime(startDate));

		return indexPages;
	}

	/**
	 * 
	 * @Title: edit
	 * @Description:编辑
	 * @param @param id
	 * @param @param index_exp
	 * @param @param index_che
	 * @param @return    设定文件
	 * @return ModelAndView    返回类型
	 */
	@RequestMapping(value = "/edit")
	public ModelAndView edit(
			@RequestParam(required = false) String id,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_EXP) String index_exp,
			@RequestParam(required = false,defaultValue = CommonEnum.MENU_STYLE_INDEX_CHE) String index_che) {
		Date startDate = new Date();
		logger.info("edit begin: " + startDate);

		ModelAndView mv = new ModelAndView(CommonEnum.FORWARD_VIEW_MAIN_SYSTEM + "role_edit");

		if (!StringUtils.isBlank(id)) {
			CourseAction courseAction = courseActionService.selectByPrimaryKey(Integer.parseInt(id));
			mv.addObject("courseAction", courseAction);
		}

		mv.addObject("MENU_STYLE_INDEX_EXP", index_exp);
		mv.addObject("MENU_STYLE_INDEX_CHE", index_che);

		logger.info("edit end run(s): " + DateUtil.calLastedTime(startDate));

		return mv;
	}

	/**
	 * 
	 * @Title: save
	 * @Description:保存
	 * @param @param onlineCourseTips
	 * @param @param folPara
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping(value = "/save",method = RequestMethod.POST)
	@ResponseBody
	public boolean save(CourseAction courseAction,FolPara folPara,HttpServletRequest request) throws MyException {
		Date startDate = new Date();
		logger.info("save begin: " + startDate);

		boolean flag = false;

		MstUser mstUser = this.getMySession().getMstUser();

	
			if (courseAction.getId() != null) {
				courseActionService.update(courseAction);
			} else {
				courseAction.setIsDelete(false);
				courseAction.setCreateDate(startDate);
				courseAction.setCreateUser(mstUser.getId());
				
				courseActionService.insert(courseAction);
			}
			
			flag = true;
	
		
		logger.info("save end run(s):" + DateUtil.calLastedTime(startDate));

		return flag;
	}

	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param request
	 * @param @param response
	 * @param @param ids
	 * @param @return
	 * @param @throws MyException    设定文件
	 * @return boolean    返回类型
	 */
	@RequestMapping("delList")
	@ResponseBody
	public boolean delList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = true) String ids) throws MyException {
		Date startDate = new Date();
		logger.info("delList begin: " + startDate);

		boolean flag = false;

		Map<String, Object> reqMap = new HashMap<String,Object>();
		reqMap.put("ids", ids);

		try {
			courseActionService.delList(reqMap);
			flag = true;
		} catch (Exception e) {
			logger.error("delList error: " ,e);
		}

		logger.info("delList end run(s): " + DateUtil.calLastedTime(startDate));

		return flag;
	}
}
