/**   
*    
* 项目名称：SmartKungFu   
* 类名称：BaseModel   
* 类描述：   
* 创建人：zhangl  
* 创建时间：2016年9月21日 下午6:10:07   
* 修改人：zhangl   
* 修改时间：2016年9月21日 下午6:10:07   
* 修改备注：   
* @version    
*    
*/
package com.lst.model;

/**
 * @ClassName: BaseModel
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangl
 * @date 2016年9月21日 下午6:10:07
 *
 */
public class BaseModel {
	private String cUName;

	public String getcUName() {
		return cUName;
	}

	public void setcUName(String cUName) {
		this.cUName = cUName;
	}
	
}
