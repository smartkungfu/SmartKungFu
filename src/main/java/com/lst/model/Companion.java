package com.lst.model;

import java.util.Date;


public class Companion {
	
    private Integer companionId;

    private String companionTitle;
	
    private String pictureUrl;

    private String companionArea;

    private String companionDescribe;
	
    private String companionPictureUrl;

	private String companionStartTime;

    private Integer companionTotal=0; 
    
	private String companionPrice;
    
    private Integer companionCreateUser;
    
    private Date companionCreateDate;
    
  	private String longitude;
    
  	private String latitude;
    
  	private String companionAddName;
    
  	private String companionAddPortrait;
    
    private Boolean isDelete;
    
  	private Integer interestNum;
    
    private Boolean isInterested=false;
    
    private Boolean isFavorite=false;
    
   	private Integer discussNum=0;
   	
	private Integer loginType;
	

	public Integer getLoginType() {
		return loginType;
	}

	public void setLoginType(Integer loginType) {
		this.loginType = loginType;
	}

	public Integer getCompanionId() {
		return companionId;
	}

	public void setCompanionId(Integer companionId) {
		this.companionId = companionId;
	}

	public String getCompanionTitle() {
		return companionTitle;
	}

	public void setCompanionTitle(String companionTitle) {
		this.companionTitle = companionTitle;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public String getCompanionArea() {
		return companionArea;
	}

	public void setCompanionArea(String companionArea) {
		this.companionArea = companionArea;
	}

	public String getCompanionDescribe() {
		return companionDescribe;
	}

	public void setCompanionDescribe(String companionDescribe) {
		this.companionDescribe = companionDescribe;
	}

	public String getCompanionPictureUrl() {
		return companionPictureUrl;
	}

	public void setCompanionPictureUrl(String companionPictureUrl) {
		this.companionPictureUrl = companionPictureUrl;
	}

	public String getCompanionStartTime() {
		return companionStartTime;
	}

	public void setCompanionStartTime(String companionStartTime) {
		this.companionStartTime = companionStartTime;
	}

	public Integer getCompanionTotal() {
		return companionTotal;
	}

	public void setCompanionTotal(Integer companionTotal) {
		this.companionTotal = companionTotal;
	}

	public Integer getCompanionCreateUser() {
		return companionCreateUser;
	}

	public void setCompanionCreateUser(Integer companionCreateUser) {
		this.companionCreateUser = companionCreateUser;
	}

	public Date getCompanionCreateDate() {
		return companionCreateDate;
	}

	public void setCompanionCreateDate(Date companionCreateDate) {
		this.companionCreateDate = companionCreateDate;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getCompanionAddName() {
		return companionAddName;
	}

	public void setCompanionAddName(String companionAddName) {
		this.companionAddName = companionAddName;
	}

	public String getCompanionAddPortrait() {
		return companionAddPortrait;
	}

	public void setCompanionAddPortrait(String companionAddPortrait) {
		this.companionAddPortrait = companionAddPortrait;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getCompanionPrice() {
		return companionPrice;
	}

	public void setCompanionPrice(String companionPrice) {
		this.companionPrice = companionPrice;
	}

	public Boolean getIsInterested() {
		return isInterested;
	}

	public void setIsInterested(Boolean isInterested) {
		this.isInterested = isInterested;
	}

	public Integer getInterestNum() {
		return interestNum;
	}

	public void setInterestNum(Integer interestNum) {
		this.interestNum = interestNum;
	}

	public Boolean getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(Boolean isFavorite) {
		this.isFavorite = isFavorite;
	}

	public Integer getDiscussNum() {
		return discussNum;
	}

	public void setDiscussNum(Integer discussNum) {
		this.discussNum = discussNum;
	}
    
}