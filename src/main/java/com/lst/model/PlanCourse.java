package com.lst.model;

import java.util.Date;

public class PlanCourse {
	
    private Integer id;             //id
	
    private Integer planId;         //计划id
	
    private Boolean isDelete;       //标识是否删除 0：未删除   1：删除
	
    private Date trainDate;         //训练日期
	
    private Integer courseId;      //训练课程id
	
    private Integer createUser;      //创建人id
	
    private Date createDate;        //创建时间
	
    private Integer planStatus;   //训练状态，1：未开始 2：已结束  3:作废重新制定
	
    private String title;      //课程标题
	
    private String viewTime;      //课程时长
	
    private Long trainDate2;         //训练时间戳

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Date getTrainDate() {
		return trainDate;
	}

	public void setTrainDate(Date trainDate) {
		this.trainDate = trainDate;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public Integer getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Integer createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getPlanStatus() {
		return planStatus;
	}

	public void setPlanStatus(Integer planStatus) {
		this.planStatus = planStatus;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public Long getTrainDate2() {
		return trainDate2;
	}

	public void setTrainDate2(Long trainDate2) {
		this.trainDate2 = trainDate2;
	}

	public String getViewTime() {
		return viewTime;
	}

	public void setViewTime(String viewTime) {
		this.viewTime = viewTime;
	}

}