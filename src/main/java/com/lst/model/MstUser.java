package com.lst.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import com.lst.common.CommonEnum;

public class MstUser extends BaseModel {
	
	private Integer id;

	@NotBlank(message = "用户账号不能为空")
	@Pattern(regexp = CommonEnum.REGEX_ACCOUNT,message = "用户账号格式错误")
	private String account;

	private String password;

	@NotNull(message = "用户昵称不能为空")
	private String namecn;

	private Boolean gender;

	@NotNull(message = "出生日期不能为空")
	private Date birthday;

	@NotBlank(message = "电话号码不能为空")
	@Pattern(regexp = CommonEnum.REGEX_MOBILE , message = "手机号格式错误")
	private String mobileno;

	private Integer rolid;

	private Integer gymid;

	private Integer province;

	private Integer city;

	private Integer area;

	private Integer street;

	private String address;

	//@NotBlank(message = "身份证号不能为空")
	//@Pattern(regexp = CommonEnum.REGEX_IDCARD , message = "身份证号格式错误")
	private String idcard;

	private String portrait;

	private Boolean enabled;

	private Boolean isdeleted;

	private Integer reserved1;

	private Integer reserved2;

	private BigDecimal reserved3;

	private BigDecimal reserved4;

	private Date reserved5;

	private Date reserved6;

	private Boolean reserved7;

	private Boolean reserved8;

	private String reserved9;

	private String reserved10;

	private String reserved11;

	private String reserved12;

	private String remark;

	private Date createdate;

	private Integer createuser;

	private Date updatedate;

	private Integer updateuser;

	/**
	 *********附加属性************************ 
	 */

	private List<UserRole> urs;
	
	//角色名称集
	private String rolnames;
	

	public List<UserRole> getUrs() {
		return urs;
	}

	public void setUrs(List<UserRole> urs) {
		this.urs = urs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account == null ? null : account.trim();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password == null ? null : password.trim();
	}

	public String getNamecn() {
		return namecn;
	}

	public void setNamecn(String namecn) {
		this.namecn = namecn == null ? null : namecn.trim();
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno == null ? null : mobileno.trim();
	}

	public Integer getRolid() {
		return rolid;
	}

	public void setRolid(Integer rolid) {
		this.rolid = rolid;
	}

	public Integer getGymid() {
		return gymid;
	}

	public void setGymid(Integer gymid) {
		this.gymid = gymid;
	}

	public Integer getProvince() {
		return province;
	}

	public void setProvince(Integer province) {
		this.province = province;
	}

	public Integer getCity() {
		return city;
	}

	public void setCity(Integer city) {
		this.city = city;
	}

	public Integer getArea() {
		return area;
	}

	public void setArea(Integer area) {
		this.area = area;
	}

	public Integer getStreet() {
		return street;
	}

	public void setStreet(Integer street) {
		this.street = street;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address == null ? null : address.trim();
	}

	public String getIdcard() {
		return idcard;
	}

	public void setIdcard(String idcard) {
		this.idcard = idcard == null ? null : idcard.trim();
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait == null ? null : portrait.trim();
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getIsdeleted() {
		return isdeleted;
	}

	public void setIsdeleted(Boolean isdeleted) {
		this.isdeleted = isdeleted;
	}

	public Integer getReserved1() {
		return reserved1;
	}

	public void setReserved1(Integer reserved1) {
		this.reserved1 = reserved1;
	}

	public Integer getReserved2() {
		return reserved2;
	}

	public void setReserved2(Integer reserved2) {
		this.reserved2 = reserved2;
	}

	public BigDecimal getReserved3() {
		return reserved3;
	}

	public void setReserved3(BigDecimal reserved3) {
		this.reserved3 = reserved3;
	}

	public BigDecimal getReserved4() {
		return reserved4;
	}

	public void setReserved4(BigDecimal reserved4) {
		this.reserved4 = reserved4;
	}

	public Date getReserved5() {
		return reserved5;
	}

	public void setReserved5(Date reserved5) {
		this.reserved5 = reserved5;
	}

	public Date getReserved6() {
		return reserved6;
	}

	public void setReserved6(Date reserved6) {
		this.reserved6 = reserved6;
	}

	public Boolean getReserved7() {
		return reserved7;
	}

	public void setReserved7(Boolean reserved7) {
		this.reserved7 = reserved7;
	}

	public Boolean getReserved8() {
		return reserved8;
	}

	public void setReserved8(Boolean reserved8) {
		this.reserved8 = reserved8;
	}

	public String getReserved9() {
		return reserved9;
	}

	public void setReserved9(String reserved9) {
		this.reserved9 = reserved9 == null ? null : reserved9.trim();
	}

	public String getReserved10() {
		return reserved10;
	}

	public void setReserved10(String reserved10) {
		this.reserved10 = reserved10 == null ? null : reserved10.trim();
	}

	public String getReserved11() {
		return reserved11;
	}

	public void setReserved11(String reserved11) {
		this.reserved11 = reserved11 == null ? null : reserved11.trim();
	}

	public String getReserved12() {
		return reserved12;
	}

	public void setReserved12(String reserved12) {
		this.reserved12 = reserved12 == null ? null : reserved12.trim();
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public Integer getCreateuser() {
		return createuser;
	}

	public void setCreateuser(Integer createuser) {
		this.createuser = createuser;
	}

	public Date getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public Integer getUpdateuser() {
		return updateuser;
	}

	public void setUpdateuser(Integer updateuser) {
		this.updateuser = updateuser;
	}

	public String getRolnames() {
		return rolnames;
	}

	public void setRolnames(String rolnames) {
		this.rolnames = rolnames;
	}
	
	
}