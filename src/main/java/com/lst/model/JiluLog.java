package com.lst.model;

import java.util.Date;

public class JiluLog {
	
	
    private Integer jiluLogId;

    private Integer jiluType;
	
    private Integer sourceId;
	
    private Integer createUser;
	
    private Date createDate;
	
    private Boolean isLove;

    private Boolean isDelete;
	
    private String loveName;
	
    private String lovePortrait;
	
	public Integer getJiluLogId() {
		return jiluLogId;
	}

	public void setJiluLogId(Integer jiluLogId) {
		this.jiluLogId = jiluLogId;
	}

	public Integer getJiluType() {
		return jiluType;
	}

	public void setJiluType(Integer jiluType) {
		this.jiluType = jiluType;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Integer createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Boolean getIsLove() {
		return isLove;
	}

	public void setIsLove(Boolean isLove) {
		this.isLove = isLove;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getLoveName() {
		return loveName;
	}

	public void setLoveName(String loveName) {
		this.loveName = loveName;
	}

	public String getLovePortrait() {
		return lovePortrait;
	}

	public void setLovePortrait(String lovePortrait) {
		this.lovePortrait = lovePortrait;
	}
	
}