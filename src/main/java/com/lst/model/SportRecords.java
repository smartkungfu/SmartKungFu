package com.lst.model;

import java.util.Date;

public class SportRecords {
    private Integer id;

    private Boolean isDelete;
    
    private String title;
    
    private String content;
    
    private Integer courseId;
    
    private Integer enterType;
    
    private Integer trainTime;
    
    private Integer calories;
    
    private Integer feelStatus;
    
    private String actionNum;
    
    private Date createDate;
    
    private Integer createUser;
    
    private Date createDate2;
    
    private Integer totalNum;
    
    private Integer planId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public Integer getEnterType() {
		return enterType;
	}

	public void setEnterType(Integer enterType) {
		this.enterType = enterType;
	}

	public Integer getCalories() {
		return calories;
	}

	public void setCalories(Integer calories) {
		this.calories = calories;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Integer createUser) {
		this.createUser = createUser;
	}

	public Integer getTrainTime() {
		return trainTime;
	}

	public void setTrainTime(Integer trainTime) {
		this.trainTime = trainTime;
	}

	public Integer getFeelStatus() {
		return feelStatus;
	}

	public void setFeelStatus(Integer feelStatus) {
		this.feelStatus = feelStatus;
	}

	public String getActionNum() {
		return actionNum;
	}

	public void setActionNum(String actionNum) {
		this.actionNum = actionNum;
	}

	public Date getCreateDate2() {
		return createDate2;
	}

	public void setCreateDate2(Date createDate2) {
		this.createDate2 = createDate2;
	}

	public Integer getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
    
}