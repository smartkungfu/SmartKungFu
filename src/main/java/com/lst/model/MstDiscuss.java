package com.lst.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class MstDiscuss {
    private Integer id;

    private Integer type;

    private Integer tarid;

    private Integer parentid;

    private String content;

    private Integer praisenum;

    private Integer opposenum;

    private Boolean enabled;

    private Boolean isdeleted;

    private Integer reserved1;

    private Integer reserved2;

    private BigDecimal reserved3;

    private BigDecimal reserved4;

    private Date reserved5;

    private Date reserved6;

    private Boolean reserved7;

    private Boolean reserved8;

    private String reserved9;

    private String reserved10;

    private String reserved11;

    private String reserved12;

    private String remark;

    private Date createdate;

    private Integer createuser;

    private Date updatedate;

    private Integer updateuser;
    
    //新加字段
    private String 	nickName;
    
    private String sendName;//发送人名字
    
    private String sendPortrait;//发送人头像
    
    private String sendTel;//发送人手机号
    
    private String replyName;//回复人名字
    
    private String replyPortrait;//回复人头像
    
    private String replyTel;//回复人手机号
    
    private String vititle;//视频标题
    
    private String octitle;//在线课程标题
    
    private List<MstDiscuss>  mds;
    
    public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getTarid() {
        return tarid;
    }

    public void setTarid(Integer tarid) {
        this.tarid = tarid;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getPraisenum() {
        return praisenum;
    }

    public void setPraisenum(Integer praisenum) {
        this.praisenum = praisenum;
    }

    public Integer getOpposenum() {
        return opposenum;
    }

    public void setOpposenum(Integer opposenum) {
        this.opposenum = opposenum;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public Integer getReserved1() {
        return reserved1;
    }

    public void setReserved1(Integer reserved1) {
        this.reserved1 = reserved1;
    }

    public Integer getReserved2() {
        return reserved2;
    }

    public void setReserved2(Integer reserved2) {
        this.reserved2 = reserved2;
    }

    public BigDecimal getReserved3() {
        return reserved3;
    }

    public void setReserved3(BigDecimal reserved3) {
        this.reserved3 = reserved3;
    }

    public BigDecimal getReserved4() {
        return reserved4;
    }

    public void setReserved4(BigDecimal reserved4) {
        this.reserved4 = reserved4;
    }

    public Date getReserved5() {
        return reserved5;
    }

    public void setReserved5(Date reserved5) {
        this.reserved5 = reserved5;
    }

    public Date getReserved6() {
        return reserved6;
    }

    public void setReserved6(Date reserved6) {
        this.reserved6 = reserved6;
    }

    public Boolean getReserved7() {
        return reserved7;
    }

    public void setReserved7(Boolean reserved7) {
        this.reserved7 = reserved7;
    }

    public Boolean getReserved8() {
        return reserved8;
    }

    public void setReserved8(Boolean reserved8) {
        this.reserved8 = reserved8;
    }

    public String getReserved9() {
        return reserved9;
    }

    public void setReserved9(String reserved9) {
        this.reserved9 = reserved9 == null ? null : reserved9.trim();
    }

    public String getReserved10() {
        return reserved10;
    }

    public void setReserved10(String reserved10) {
        this.reserved10 = reserved10 == null ? null : reserved10.trim();
    }

    public String getReserved11() {
        return reserved11;
    }

    public void setReserved11(String reserved11) {
        this.reserved11 = reserved11 == null ? null : reserved11.trim();
    }

    public String getReserved12() {
        return reserved12;
    }

    public void setReserved12(String reserved12) {
        this.reserved12 = reserved12 == null ? null : reserved12.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Integer getCreateuser() {
        return createuser;
    }

    public void setCreateuser(Integer createuser) {
        this.createuser = createuser;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public Integer getUpdateuser() {
        return updateuser;
    }

    public void setUpdateuser(Integer updateuser) {
        this.updateuser = updateuser;
    }

	public String getSendName() {
		return sendName;
	}

	public void setSendName(String sendName) {
		this.sendName = sendName;
	}

	public String getSendPortrait() {
		return sendPortrait;
	}

	public void setSendPortrait(String sendPortrait) {
		this.sendPortrait = sendPortrait;
	}

	public String getSendTel() {
		return sendTel;
	}

	public void setSendTel(String sendTel) {
		this.sendTel = sendTel;
	}

	public String getReplyName() {
		return replyName;
	}

	public void setReplyName(String replyName) {
		this.replyName = replyName;
	}

	public String getReplyPortrait() {
		return replyPortrait;
	}

	public void setReplyPortrait(String replyPortrait) {
		this.replyPortrait = replyPortrait;
	}

	public String getReplyTel() {
		return replyTel;
	}

	public void setReplyTel(String replyTel) {
		this.replyTel = replyTel;
	}

	public String getVititle() {
		return vititle;
	}

	public void setVititle(String vititle) {
		this.vititle = vititle;
	}

	public String getOctitle() {
		return octitle;
	}

	public void setOctitle(String octitle) {
		this.octitle = octitle;
	}

	public List<MstDiscuss> getMds() {
		return mds;
	}

	public void setMds(List<MstDiscuss> mds) {
		this.mds = mds;
	}
    
}