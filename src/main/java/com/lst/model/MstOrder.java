package com.lst.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import com.lst.common.CommonEnum;

public class MstOrder extends BaseModel {
    private Integer id;

    @NotBlank(message = "订单号为空")
    private String orderno;

    @NotNull(message = "用户ID为空")
    private Integer clientid;

    @NotNull(message = "动态ID为空")
    private Integer actid;

    @NotNull(message = "动态类型为空")
    private Integer acttype;

    @NotBlank(message = "动态名称为空")
    private String actname;

    private String actbill;

    private Integer packageid;

    private Integer scheid;

    private Integer num;

    private BigDecimal price = new BigDecimal(0.00);

    private BigDecimal preprice = new BigDecimal(0.00);

    private BigDecimal actprice = new BigDecimal(0.00);

    @NotBlank(message = "联系人为空")
    private String contact;

    @NotBlank(message = "手机号为空")
    @Pattern(regexp = CommonEnum.REGEX_MOBILE, message = "手机号格式错误")
    private String phoneno;

    private Integer paytype;

    private Date paydate;

    private String tradeno;
    
    private String alipaytradeno;

    private Boolean enabled;

    private Boolean isdeleted;

    private Integer reserved1;

    private Integer reserved2;

    private BigDecimal reserved3;

    private BigDecimal reserved4;

    private Date reserved5;

    private Date reserved6;

    private Boolean reserved7;

    private Boolean reserved8;

    private String reserved9;

    private String reserved10;

    private String reserved11;

    private String reserved12;

    private String remark;

    private Date createdate;

    private Integer createuser;

    private Date updatedate;

    private Integer updateuser;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getOrderno() {
	return orderno;
    }

    public void setOrderno(String orderno) {
	this.orderno = orderno == null ? null : orderno.trim();
    }

    public Integer getClientid() {
	return clientid;
    }

    public void setClientid(Integer clientid) {
	this.clientid = clientid;
    }

    public Integer getActid() {
	return actid;
    }

    public void setActid(Integer actid) {
	this.actid = actid;
    }

    public Integer getActtype() {
	return acttype;
    }

    public void setActtype(Integer acttype) {
	this.acttype = acttype;
    }

    public String getActname() {
	return actname;
    }

    public void setActname(String actname) {
	this.actname = actname == null ? null : actname.trim();
    }

    public String getActbill() {
	return actbill;
    }

    public void setActbill(String actbill) {
	this.actbill = actbill == null ? null : actbill.trim();
    }

    public Integer getPackageid() {
	return packageid;
    }

    public void setPackageid(Integer packageid) {
	this.packageid = packageid;
    }

    public Integer getScheid() {
	return scheid;
    }

    public void setScheid(Integer scheid) {
	this.scheid = scheid;
    }

    public Integer getNum() {
	return num;
    }

    public void setNum(Integer num) {
	this.num = num;
    }

    public BigDecimal getPreprice() {
	return preprice;
    }

    public void setPreprice(BigDecimal preprice) {
	this.preprice = preprice;
    }

    public BigDecimal getActprice() {
	return actprice;
    }

    public void setActprice(BigDecimal actprice) {
	this.actprice = actprice;
    }

    public String getContact() {
	return contact;
    }

    public void setContact(String contact) {
	this.contact = contact == null ? null : contact.trim();
    }

    public String getPhoneno() {
	return phoneno;
    }

    public void setPhoneno(String phoneno) {
	this.phoneno = phoneno == null ? null : phoneno.trim();
    }

    public Integer getPaytype() {
	return paytype;
    }

    public void setPaytype(Integer paytype) {
	this.paytype = paytype;
    }

    public Date getPaydate() {
	return paydate;
    }

    public void setPaydate(Date paydate) {
	this.paydate = paydate;
    }

    public String getTradeno() {
	return tradeno;
    }

    public void setTradeno(String tradeno) {
	this.tradeno = tradeno;
    }

    public String getAlipaytradeno() {
	return alipaytradeno;
    }

    public void setAlipaytradeno(String alipaytradeno) {
	this.alipaytradeno = alipaytradeno;
    }

    public Boolean getEnabled() {
	return enabled;
    }

    public void setEnabled(Boolean enabled) {
	this.enabled = enabled;
    }

    public Boolean getIsdeleted() {
	return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
	this.isdeleted = isdeleted;
    }

    public Integer getReserved1() {
	return reserved1;
    }

    public void setReserved1(Integer reserved1) {
	this.reserved1 = reserved1;
    }

    public Integer getReserved2() {
	return reserved2;
    }

    public void setReserved2(Integer reserved2) {
	this.reserved2 = reserved2;
    }

    public BigDecimal getReserved3() {
	return reserved3;
    }

    public void setReserved3(BigDecimal reserved3) {
	this.reserved3 = reserved3;
    }

    public BigDecimal getReserved4() {
	return reserved4;
    }

    public void setReserved4(BigDecimal reserved4) {
	this.reserved4 = reserved4;
    }

    public Date getReserved5() {
	return reserved5;
    }

    public void setReserved5(Date reserved5) {
	this.reserved5 = reserved5;
    }

    public Date getReserved6() {
	return reserved6;
    }

    public void setReserved6(Date reserved6) {
	this.reserved6 = reserved6;
    }

    public Boolean getReserved7() {
	return reserved7;
    }

    public void setReserved7(Boolean reserved7) {
	this.reserved7 = reserved7;
    }

    public Boolean getReserved8() {
	return reserved8;
    }

    public void setReserved8(Boolean reserved8) {
	this.reserved8 = reserved8;
    }

    public String getReserved9() {
	return reserved9;
    }

    public void setReserved9(String reserved9) {
	this.reserved9 = reserved9 == null ? null : reserved9.trim();
    }

    public String getReserved10() {
	return reserved10;
    }

    public void setReserved10(String reserved10) {
	this.reserved10 = reserved10 == null ? null : reserved10.trim();
    }

    public String getReserved11() {
	return reserved11;
    }

    public void setReserved11(String reserved11) {
	this.reserved11 = reserved11 == null ? null : reserved11.trim();
    }

    public String getReserved12() {
	return reserved12;
    }

    public void setReserved12(String reserved12) {
	this.reserved12 = reserved12 == null ? null : reserved12.trim();
    }

    public String getRemark() {
	return remark;
    }

    public void setRemark(String remark) {
	this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreatedate() {
	return createdate;
    }

    public void setCreatedate(Date createdate) {
	this.createdate = createdate;
    }

    public Integer getCreateuser() {
	return createuser;
    }

    public void setCreateuser(Integer createuser) {
	this.createuser = createuser;
    }

    public Date getUpdatedate() {
	return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
	this.updatedate = updatedate;
    }

    public Integer getUpdateuser() {
	return updateuser;
    }

    public void setUpdateuser(Integer updateuser) {
	this.updateuser = updateuser;
    }

    public BigDecimal getPrice() {
	return price;
    }

    public void setPrice(BigDecimal price) {
	this.price = price;
    }

}