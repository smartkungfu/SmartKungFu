package com.lst.model;

import java.util.Date;

public class Answer {
	
    private Integer answerId;             //回答id

    private Integer answerSourceId;       //回答目标id
	
    private String answerTitle;           //回答的问题标题
	
    private String answerDetail;          //回答具体内容
	
    private Integer answerLoveNum=0;        //对回答的喜欢数量
	
    private Integer answerDiscussNum=0;      //回答的评论数量
	
    private Integer answerShareNum=0;        //回答的分享数量
	
    private Integer answerFavoriteNum=0;     //回答的收藏数量
	
    private Integer answerCreateUser;      //回答的人id
	
    private Date answerCreateDate;         //回答的时间
	
    private String answerCreateDate2;         //临时存储转换回答的时间格式
	
    private String answerName;//回答问题人名字
	    
	private String answerPortrait;//回答问题人头像
	
	private Boolean isDelete;

	public Integer getAnswerId() {
		return answerId;
	}

	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}

	public Integer getAnswerSourceId() {
		return answerSourceId;
	}

	public void setAnswerSourceId(Integer answerSourceId) {
		this.answerSourceId = answerSourceId;
	}

	public String getAnswerTitle() {
		return answerTitle;
	}

	public void setAnswerTitle(String answerTitle) {
		this.answerTitle = answerTitle;
	}

	public String getAnswerDetail() {
		return answerDetail;
	}

	public void setAnswerDetail(String answerDetail) {
		this.answerDetail = answerDetail;
	}

	public Integer getAnswerLoveNum() {
		return answerLoveNum;
	}

	public void setAnswerLoveNum(Integer answerLoveNum) {
		this.answerLoveNum = answerLoveNum;
	}

	public Integer getAnswerDiscussNum() {
		return answerDiscussNum;
	}

	public void setAnswerDiscussNum(Integer answerDiscussNum) {
		this.answerDiscussNum = answerDiscussNum;
	}

	public Integer getAnswerShareNum() {
		return answerShareNum;
	}

	public void setAnswerShareNum(Integer answerShareNum) {
		this.answerShareNum = answerShareNum;
	}

	public Integer getAnswerFavoriteNum() {
		return answerFavoriteNum;
	}

	public void setAnswerFavoriteNum(Integer answerFavoriteNum) {
		this.answerFavoriteNum = answerFavoriteNum;
	}

	public Integer getAnswerCreateUser() {
		return answerCreateUser;
	}

	public void setAnswerCreateUser(Integer answerCreateUser) {
		this.answerCreateUser = answerCreateUser;
	}

	public Date getAnswerCreateDate() {
		return answerCreateDate;
	}

	public void setAnswerCreateDate(Date answerCreateDate) {
		this.answerCreateDate = answerCreateDate;
	}

	public String getAnswerCreateDate2() {
		return answerCreateDate2;
	}

	public void setAnswerCreateDate2(String answerCreateDate2) {
		this.answerCreateDate2 = answerCreateDate2;
	}

	public String getAnswerName() {
		return answerName;
	}

	public void setAnswerName(String answerName) {
		this.answerName = answerName;
	}

	public String getAnswerPortrait() {
		return answerPortrait;
	}

	public void setAnswerPortrait(String answerPortrait) {
		this.answerPortrait = answerPortrait;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
}