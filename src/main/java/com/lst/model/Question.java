package com.lst.model;

import java.util.Date;

public class Question {
	
    private Integer questionId;

    private String questionTitle;
	
    private String questionContent;

    private Integer answerNum;
	
    private String pictureUrl;
	
    private Integer questionLoveNum;
	
	private Boolean isDelete=false;
	
    private Integer questionShareNum;
	
    private Integer questionFavoriteNum;
	
    private Integer createUser;
	
    private Date createDate;
	
    private String sendName;//提出问题人名字
	    
	private String sendPortrait;//提出问题人头像
	
	private Integer questionType;
	
	private Integer recordId;

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public String getQuestionTitle() {
		return questionTitle;
	}

	public void setQuestionTitle(String questionTitle) {
		this.questionTitle = questionTitle;
	}

	public String getQuestionContent() {
		return questionContent;
	}

	public void setQuestionContent(String questionContent) {
		this.questionContent = questionContent;
	}

	public Integer getAnswerNum() {
		return answerNum;
	}

	public void setAnswerNum(Integer answerNum) {
		this.answerNum = answerNum;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public Integer getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Integer createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getQuestionShareNum() {
		return questionShareNum;
	}

	public void setQuestionShareNum(Integer questionShareNum) {
		this.questionShareNum = questionShareNum;
	}

	public Integer getQuestionFavoriteNum() {
		return questionFavoriteNum;
	}

	public void setQuestionFavoriteNum(Integer questionFavoriteNum) {
		this.questionFavoriteNum = questionFavoriteNum;
	}

	public Integer getQuestionLoveNum() {
		return questionLoveNum;
	}

	public void setQuestionLoveNum(Integer questionLoveNum) {
		this.questionLoveNum = questionLoveNum;
	}

	public String getSendName() {
		return sendName;
	}

	public void setSendName(String sendName) {
		this.sendName = sendName;
	}

	public String getSendPortrait() {
		return sendPortrait;
	}

	public void setSendPortrait(String sendPortrait) {
		this.sendPortrait = sendPortrait;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Integer getQuestionType() {
		return questionType;
	}

	public void setQuestionType(Integer questionType) {
		this.questionType = questionType;
	}

	public Integer getRecordId() {
		return recordId;
	}

	public void setRecordId(Integer recordId) {
		this.recordId = recordId;
	}

}