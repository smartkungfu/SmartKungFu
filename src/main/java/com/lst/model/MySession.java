/**   
*    
* 项目名称：SmartKungFu   
* 类名称：MySession   
* 类描述：   
* 创建人：zhangl  
* 创建时间：2016年9月21日 下午6:08:38   
* 修改人：zhangl   
* 修改时间：2016年9月21日 下午6:08:38   
* 修改备注：   
* @version    
*    
*/
package com.lst.model;

import java.util.List;

/**
 * @ClassName: MySession
 * @Description: Session
 * @author zhangl
 * @date 2016年9月21日 下午6:08:38
 *
 */
public class MySession {
	
	private MstUser mstUser;
	
	private List<MstMenu> menus;

	public MstUser getMstUser() {
		return mstUser;
	}

	public void setMstUser(MstUser mstUser) {
		this.mstUser = mstUser;
	}

	public List<MstMenu> getMenus() {
		return menus;
	}

	public void setMenus(List<MstMenu> menus) {
		this.menus = menus;
	}
	
}
