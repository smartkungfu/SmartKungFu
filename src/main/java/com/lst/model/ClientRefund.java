package com.lst.model;

import java.math.BigDecimal;
import java.util.Date;

public class ClientRefund {
    private Integer id;

    private Integer clientid;

    private Integer orderid;
    
    private Integer orderdetailid;

    private Integer causeid;

    private Integer refstatus;

    private BigDecimal refprice;

    private Integer refnum;

    private String content;

    private Boolean enabled;

    private Boolean isdeleted;

    private Integer reserved1;

    private Integer reserved2;

    private BigDecimal reserved3;

    private BigDecimal reserved4;

    private Date reserved5;

    private Date reserved6;

    private Boolean reserved7;

    private Boolean reserved8;

    private String reserved9;

    private String reserved10;

    private String reserved11;

    private String reserved12;

    private String remark;

    private Date createdate;

    private Integer createuser;

    private Date updatedate;

    private Integer updateuser;
    
    
    /*******附加属性************/
    
    private MstClient mstClient;
    
    private MstOrder mstOrder;
    
    private String causeName;
    
    private String refStatusName;
    
    private OrderDetail od;
    
    private ClientGrowth cg;
    
    private RefundResult rr;

	public RefundResult getRr() {
		return rr;
	}

	public void setRr(RefundResult rr) {
		this.rr = rr;
	}

	public ClientGrowth getCg() {
		return cg;
	}

	public void setCg(ClientGrowth cg) {
		this.cg = cg;
	}

	public OrderDetail getOd() {
		return od;
	}

	public void setOd(OrderDetail od) {
		this.od = od;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClientid() {
        return clientid;
    }

    public void setClientid(Integer clientid) {
        this.clientid = clientid;
    }

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    public Integer getCauseid() {
        return causeid;
    }

    public void setCauseid(Integer causeid) {
        this.causeid = causeid;
    }

    public Integer getRefstatus() {
        return refstatus;
    }

    public void setRefstatus(Integer refstatus) {
        this.refstatus = refstatus;
    }

    public BigDecimal getRefprice() {
        return refprice;
    }

    public void setRefprice(BigDecimal refprice) {
        this.refprice = refprice;
    }

    public Integer getRefnum() {
        return refnum;
    }

    public void setRefnum(Integer refnum) {
        this.refnum = refnum;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public Integer getReserved1() {
        return reserved1;
    }

    public void setReserved1(Integer reserved1) {
        this.reserved1 = reserved1;
    }

    public Integer getReserved2() {
        return reserved2;
    }

    public void setReserved2(Integer reserved2) {
        this.reserved2 = reserved2;
    }

    public BigDecimal getReserved3() {
        return reserved3;
    }

    public void setReserved3(BigDecimal reserved3) {
        this.reserved3 = reserved3;
    }

    public BigDecimal getReserved4() {
        return reserved4;
    }

    public void setReserved4(BigDecimal reserved4) {
        this.reserved4 = reserved4;
    }

    public Date getReserved5() {
        return reserved5;
    }

    public void setReserved5(Date reserved5) {
        this.reserved5 = reserved5;
    }

    public Date getReserved6() {
        return reserved6;
    }

    public void setReserved6(Date reserved6) {
        this.reserved6 = reserved6;
    }

    public Boolean getReserved7() {
        return reserved7;
    }

    public void setReserved7(Boolean reserved7) {
        this.reserved7 = reserved7;
    }

    public Boolean getReserved8() {
        return reserved8;
    }

    public void setReserved8(Boolean reserved8) {
        this.reserved8 = reserved8;
    }

    public String getReserved9() {
        return reserved9;
    }

    public void setReserved9(String reserved9) {
        this.reserved9 = reserved9 == null ? null : reserved9.trim();
    }

    public String getReserved10() {
        return reserved10;
    }

    public void setReserved10(String reserved10) {
        this.reserved10 = reserved10 == null ? null : reserved10.trim();
    }

    public String getReserved11() {
        return reserved11;
    }

    public void setReserved11(String reserved11) {
        this.reserved11 = reserved11 == null ? null : reserved11.trim();
    }

    public String getReserved12() {
        return reserved12;
    }

    public void setReserved12(String reserved12) {
        this.reserved12 = reserved12 == null ? null : reserved12.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Integer getCreateuser() {
        return createuser;
    }

    public void setCreateuser(Integer createuser) {
        this.createuser = createuser;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public Integer getUpdateuser() {
        return updateuser;
    }

    public void setUpdateuser(Integer updateuser) {
        this.updateuser = updateuser;
    }

	public MstClient getMstClient() {
		return mstClient;
	}

	public void setMstClient(MstClient mstClient) {
		this.mstClient = mstClient;
	}

	public MstOrder getMstOrder() {
		return mstOrder;
	}

	public void setMstOrder(MstOrder mstOrder) {
		this.mstOrder = mstOrder;
	}

	public String getCauseName() {
		return causeName;
	}

	public void setCauseName(String causeName) {
		this.causeName = causeName;
	}

	public String getRefStatusName() {
		return refStatusName;
	}

	public void setRefStatusName(String refStatusName) {
		this.refStatusName = refStatusName;
	}

	public Integer getOrderdetailid() {
		return orderdetailid;
	}

	public void setOrderdetailid(Integer orderdetailid) {
		this.orderdetailid = orderdetailid;
	}
	
	
    
}