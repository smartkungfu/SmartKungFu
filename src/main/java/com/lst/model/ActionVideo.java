package com.lst.model;

import java.util.Date;

public class ActionVideo {
    private Integer id;

    private Boolean isDelete;
    
    private Integer type;
    
    private String actionTitle;

    private String videoName;
    
    private Integer restTime;
    
    private String coverUrl;
    
    private String coverUrl2;
    
    private Integer createUser;
    
    private Date createDate;
    
    private Integer frequency; //视频播放次数
    
    private Integer totalTime; //播放总时间
    
    private Integer videoType;  //标识按组播放还是按时间播放，1：按组播放    2：按时间播放
    
    private Integer groupNumber;//播放组数
    
    private String typeName;//分类名称
    
    private String actionPoints;//动作要点

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getActionTitle() {
		return actionTitle;
	}

	public void setActionTitle(String actionTitle) {
		this.actionTitle = actionTitle;
	}

	public String getVideoName() {
		return videoName;
	}

	public void setVideoName(String videoName) {
		this.videoName = videoName;
	}

	public Integer getRestTime() {
		return restTime;
	}

	public void setRestTime(Integer restTime) {
		this.restTime = restTime;
	}

	public String getCoverUrl() {
		return coverUrl;
	}

	public void setCoverUrl(String coverUrl) {
		this.coverUrl = coverUrl;
	}

	public Integer getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Integer createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getCoverUrl2() {
		return coverUrl2;
	}

	public void setCoverUrl2(String coverUrl2) {
		this.coverUrl2 = coverUrl2;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public Integer getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(Integer totalTime) {
		this.totalTime = totalTime;
	}

	public Integer getVideoType() {
		return videoType;
	}

	public void setVideoType(Integer videoType) {
		this.videoType = videoType;
	}

	public Integer getGroupNumber() {
		return groupNumber;
	}

	public void setGroupNumber(Integer groupNumber) {
		this.groupNumber = groupNumber;
	}

	public String getActionPoints() {
		return actionPoints;
	}

	public void setActionPoints(String actionPoints) {
		this.actionPoints = actionPoints;
	}
	
}