package com.lst.model;

import java.util.Date;

public class Plan {
	
    private Integer id;             //id
	
    private Boolean isDelete;       //标识是否删除 0：未删除   1：删除
	
    private Integer status;         //标识状态   1：进行中   2：已完成     3：过期
	
    private Integer identityType;   //身份类型
	
    private Integer trainType;      //训练能力类型
	
    private Integer kfClassType;    //倾向类型
	
    private String instrumentType; //器械类型
	
    private Integer runType; //标识是否包含训练跑
	
    private Integer frequency;      //训练频率
	
    private Date startTime;         //开始时间
	
    private Integer height;         //身高
	
    private Integer weight;        //体重
	
    private Integer createUser;     //创建人id
	
    private Date createDate;      //创建时间
	
    private Boolean isNotice=false;     //标识是否提醒
	
    private String noticeTime="00:00";     //提醒时间
    
    private String createName;     //创建人昵称
    
    private String createPortrait;     //创建人头像
   
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getIdentityType() {
		return identityType;
	}

	public void setIdentityType(Integer identityType) {
		this.identityType = identityType;
	}

	public Integer getTrainType() {
		return trainType;
	}

	public void setTrainType(Integer trainType) {
		this.trainType = trainType;
	}

	public Integer getKfClassType() {
		return kfClassType;
	}

	public void setKfClassType(Integer kfClassType) {
		this.kfClassType = kfClassType;
	}

	public String getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(String instrumentType) {
		this.instrumentType = instrumentType;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Integer getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Integer createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getRunType() {
		return runType;
	}

	public void setRunType(Integer runType) {
		this.runType = runType;
	}

	public Boolean getIsNotice() {
		return isNotice;
	}

	public void setIsNotice(Boolean isNotice) {
		this.isNotice = isNotice;
	}

	public String getNoticeTime() {
		return noticeTime;
	}

	public void setNoticeTime(String noticeTime) {
		this.noticeTime = noticeTime;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getCreatePortrait() {
		return createPortrait;
	}

	public void setCreatePortrait(String createPortrait) {
		this.createPortrait = createPortrait;
	}

}