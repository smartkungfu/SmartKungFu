package com.lst.model;

import java.math.BigDecimal;
import java.util.Date;

public class OnlineCourse extends BaseModel {
    private Integer id;

    private String title;
    
    private String url;
    
    private String coverurl;

    private String type;

    private String courseno;

    private BigDecimal calorie;

    private String introduction;

    private String people;

    private String descript;

    private String actionname;

    private String cycle;

    private String notice;

    private Boolean enabled;

    private Boolean isdeleted;

    private Integer reserved1;

    private String reserved2;

    private BigDecimal reserved3;

    private BigDecimal reserved4;

    private Date reserved5;

    private Date reserved6;

    private Boolean reserved7;

    private Boolean reserved8;

    private String reserved9;

    private String reserved10;

    private String reserved11;

    private String reserved12;

    private String remark;

    private Date createdate;

    private Integer createuser;

    private Date updatedate;

    private Integer updateuser;
    
    /************新添字段**************/
    
    
    private Integer trainType;
    
    private Integer kfClassType;
    
    private Integer instrumentType;
    
    private Integer runType;
    
    private Integer viewTime;
    
    private Integer articleNum;
    
    
    /************附加字段**************/
    
    private String  typeName;
    
    private Integer praisenum;

	private String browsenum;

	private Integer favoritenum;

	private Integer sharenum;
	
	private Integer disNum;

    public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title == null ? null : title.trim();
    }

    public String getUrl() {
	return url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public String getCoverurl() {
	return coverurl;
    }

    public void setCoverurl(String coverurl) {
	this.coverurl = coverurl;
    }

    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCourseno() {
	return courseno;
    }

    public void setCourseno(String courseno) {
	this.courseno = courseno == null ? null : courseno.trim();
    }

    public BigDecimal getCalorie() {
	return calorie;
    }

    public void setCalorie(BigDecimal calorie) {
	this.calorie = calorie;
    }

    public String getIntroduction() {
	return introduction;
    }

    public void setIntroduction(String introduction) {
	this.introduction = introduction == null ? null : introduction.trim();
    }

    public String getPeople() {
	return people;
    }

    public void setPeople(String people) {
	this.people = people == null ? null : people.trim();
    }

    public String getDescript() {
	return descript;
    }

    public void setDescript(String descript) {
	this.descript = descript == null ? null : descript.trim();
    }

    public String getActionname() {
	return actionname;
    }

    public void setActionname(String actionname) {
	this.actionname = actionname == null ? null : actionname.trim();
    }

    public String getCycle() {
	return cycle;
    }

    public void setCycle(String cycle) {
	this.cycle = cycle == null ? null : cycle.trim();
    }

    public String getNotice() {
	return notice;
    }

    public void setNotice(String notice) {
	this.notice = notice == null ? null : notice.trim();
    }

    public Boolean getEnabled() {
	return enabled;
    }

    public void setEnabled(Boolean enabled) {
	this.enabled = enabled;
    }

    public Boolean getIsdeleted() {
	return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
	this.isdeleted = isdeleted;
    }

    public Integer getReserved1() {
	return reserved1;
    }

    public void setReserved1(Integer reserved1) {
	this.reserved1 = reserved1;
    }

    public String getReserved2() {
		return reserved2;
	}

	public void setReserved2(String reserved2) {
		this.reserved2 = reserved2;
	}

	public BigDecimal getReserved3() {
	return reserved3;
    }

    public void setReserved3(BigDecimal reserved3) {
	this.reserved3 = reserved3;
    }

    public BigDecimal getReserved4() {
	return reserved4;
    }

    public void setReserved4(BigDecimal reserved4) {
	this.reserved4 = reserved4;
    }

    public Date getReserved5() {
	return reserved5;
    }

    public void setReserved5(Date reserved5) {
	this.reserved5 = reserved5;
    }

    public Date getReserved6() {
	return reserved6;
    }

    public void setReserved6(Date reserved6) {
	this.reserved6 = reserved6;
    }

    public Boolean getReserved7() {
	return reserved7;
    }

    public void setReserved7(Boolean reserved7) {
	this.reserved7 = reserved7;
    }

    public Boolean getReserved8() {
	return reserved8;
    }

    public void setReserved8(Boolean reserved8) {
	this.reserved8 = reserved8;
    }

    public String getReserved9() {
	return reserved9;
    }

    public void setReserved9(String reserved9) {
	this.reserved9 = reserved9 == null ? null : reserved9.trim();
    }

    public String getReserved10() {
	return reserved10;
    }

    public void setReserved10(String reserved10) {
	this.reserved10 = reserved10 == null ? null : reserved10.trim();
    }

    public String getReserved11() {
	return reserved11;
    }

    public void setReserved11(String reserved11) {
	this.reserved11 = reserved11 == null ? null : reserved11.trim();
    }

    public String getReserved12() {
	return reserved12;
    }

    public void setReserved12(String reserved12) {
	this.reserved12 = reserved12 == null ? null : reserved12.trim();
    }

    public String getRemark() {
	return remark;
    }

    public void setRemark(String remark) {
	this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreatedate() {
	return createdate;
    }

    public void setCreatedate(Date createdate) {
	this.createdate = createdate;
    }

    public Integer getCreateuser() {
	return createuser;
    }

    public void setCreateuser(Integer createuser) {
	this.createuser = createuser;
    }

    public Date getUpdatedate() {
	return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
	this.updatedate = updatedate;
    }

    public Integer getUpdateuser() {
	return updateuser;
    }

    public void setUpdateuser(Integer updateuser) {
	this.updateuser = updateuser;
    }

	public Integer getPraisenum() {
		return praisenum;
	}

	public void setPraisenum(Integer praisenum) {
		this.praisenum = praisenum;
	}

	/**
	 * @return the browsenum
	 */
	public String getBrowsenum() {
		return browsenum;
	}

	/**
	 * @param browsenum the browsenum to set
	 */
	public void setBrowsenum(String browsenum) {
		this.browsenum = browsenum;
	}

	public Integer getFavoritenum() {
		return favoritenum;
	}

	public void setFavoritenum(Integer favoritenum) {
		this.favoritenum = favoritenum;
	}

	public Integer getSharenum() {
		return sharenum;
	}

	public void setSharenum(Integer sharenum) {
		this.sharenum = sharenum;
	}

	public Integer getDisNum() {
		return disNum;
	}

	public void setDisNum(Integer disNum) {
		this.disNum = disNum;
	}

	public Integer getTrainType() {
		return trainType;
	}

	public void setTrainType(Integer trainType) {
		this.trainType = trainType;
	}

	public Integer getKfClassType() {
		return kfClassType;
	}

	public void setKfClassType(Integer kfClassType) {
		this.kfClassType = kfClassType;
	}

	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	public Integer getRunType() {
		return runType;
	}

	public void setRunType(Integer runType) {
		this.runType = runType;
	}

	public Integer getViewTime() {
		return viewTime;
	}

	public void setViewTime(Integer viewTime) {
		this.viewTime = viewTime;
	}

	public Integer getArticleNum() {
		return articleNum;
	}

	public void setArticleNum(Integer articleNum) {
		this.articleNum = articleNum;
	}

}