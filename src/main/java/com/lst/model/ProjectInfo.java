package com.lst.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.lst.common.CommonEnum;

public class ProjectInfo extends BaseModel {
	private Integer id;

	@NotBlank(message  = "活动标题为空")
	private String title;

	@NotNull(message = "活动类型为空")
	private Integer protype;

	private String probill;

	@NotNull(message = "报名费为空")
	private BigDecimal profee;
	
	@NotNull(message = "单位为空")
	private Integer unit;
	
	@NotBlank(message = "活动亮点为空")
	private String lightspot;
	
	//@NotBlank(message = "温馨提示为空")
	private String warning;

	@NotBlank(message = "联系人为空")
	private String procontact;

	@NotBlank(message = "联系电话为空")
	@Pattern(regexp = CommonEnum.REGEX_MOBILE , message = "手机号格式错误")
	private String prophoneno;

	private Integer province;

	private Integer city;

	private Integer area;

	private String address;

	@NotNull(message = "起始时间为空")
	private Date startdate;

	@NotNull(message = "结束时间为空")
	private Date enddate;
	
	@NotEmpty(message = "活动每天进行时间为空")
	private String perdate;

	private Boolean istop;

	private Integer praisenum = 0;

	private Integer browsenum = 0;

	private Integer favoritenum = 0;

	private Integer sharenum = 0;
	
	private String longgitude;

	private String latitude;

	private Boolean enabled;

	private Boolean isdeleted;

	private Integer reserved1;

	private Integer reserved2;

	private BigDecimal reserved3;

	private BigDecimal reserved4;

	private Date reserved5;

	private Date reserved6;

	private Boolean reserved7;

	private Boolean reserved8;

	private String reserved9;

	private String reserved10;

	private String reserved11;

	private String reserved12;

	private String remark;

	private Date createdate;

	private Integer createuser;

	private Date updatedate;

	private Integer updateuser;

	/**
	 ***********附加属性*************** 
	 */
	
	private String unitName;
	
	private String provinceName;
	
	private String cityName;
	
	private String areaName;
	
	private String proTypeName;
	
	private List<MstRoll> rolls;
	
	private List<ProjectInfo> morePro;
	
	private List<String> probills;
	
	private List<String> lightspots;
	
	private List<String> warnings;
	
	private List<String> contentDetails;
	
	public List<String> getContentDetails() {
		return contentDetails;
	}

	public void setContentDetails(List<String> contentDetails) {
		this.contentDetails = contentDetails;
	}

	public List<String> getProbills() {
		return probills;
	}

	public void setProbills(List<String> probills) {
		this.probills = probills;
	}

	public List<String> getWarnings() {
		return warnings;
	}

	public void setWarnings(List<String> warnings) {
		this.warnings = warnings;
	}

	public List<String> getLightspots() {
		return lightspots;
	}

	public void setLightspots(List<String> lightspots) {
		this.lightspots = lightspots;
	}

	public String getProTypeName() {
		return proTypeName;
	}

	public void setProTypeName(String proTypeName) {
		this.proTypeName = proTypeName;
	}

	public List<MstRoll> getRolls() {
		return rolls;
	}

	public void setRolls(List<MstRoll> rolls) {
		this.rolls = rolls;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title == null ? null : title.trim();
	}

	public Integer getProtype() {
		return protype;
	}

	public void setProtype(Integer protype) {
		this.protype = protype;
	}

	public String getProbill() {
		return probill;
	}

	public void setProbill(String probill) {
		this.probill = probill == null ? null : probill.trim();
	}

	public BigDecimal getProfee() {
		if(profee != null){
			return profee.setScale(2, BigDecimal.ROUND_HALF_UP);
		} else {
			//return new BigDecimal(0.00);
			return null;
		}
	}

	public void setProfee(BigDecimal profee) {
		this.profee = profee;
	}

	public Integer getUnit() {
		return unit;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public String getProcontact() {
		return procontact;
	}

	public void setProcontact(String procontact) {
		this.procontact = procontact == null ? null : procontact.trim();
	}

	public String getProphoneno() {
		return prophoneno;
	}

	public void setProphoneno(String prophoneno) {
		this.prophoneno = prophoneno == null ? null : prophoneno.trim();
	}

	public Integer getProvince() {
		return province;
	}

	public void setProvince(Integer province) {
		this.province = province;
	}

	public Integer getCity() {
		return city;
	}

	public void setCity(Integer city) {
		this.city = city;
	}

	public Integer getArea() {
		return area;
	}

	public void setArea(Integer area) {
		this.area = area;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address == null ? null : address.trim();
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public Boolean getIstop() {
		return istop;
	}

	public void setIstop(Boolean istop) {
		this.istop = istop;
	}

	public Integer getPraisenum() {
		return praisenum;
	}

	public void setPraisenum(Integer praisenum) {
		this.praisenum = praisenum;
	}

	public Integer getBrowsenum() {
		return browsenum;
	}

	public void setBrowsenum(Integer browsenum) {
		this.browsenum = browsenum;
	}

	public Integer getFavoritenum() {
		return favoritenum;
	}

	public void setFavoritenum(Integer favoritenum) {
		this.favoritenum = favoritenum;
	}

	public Integer getSharenum() {
		return sharenum;
	}

	public void setSharenum(Integer sharenum) {
		this.sharenum = sharenum;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getIsdeleted() {
		return isdeleted;
	}

	public void setIsdeleted(Boolean isdeleted) {
		this.isdeleted = isdeleted;
	}

	public Integer getReserved1() {
		return reserved1;
	}

	public void setReserved1(Integer reserved1) {
		this.reserved1 = reserved1;
	}

	public Integer getReserved2() {
		return reserved2;
	}

	public void setReserved2(Integer reserved2) {
		this.reserved2 = reserved2;
	}

	public BigDecimal getReserved3() {
		return reserved3;
	}

	public void setReserved3(BigDecimal reserved3) {
		this.reserved3 = reserved3;
	}

	public BigDecimal getReserved4() {
		return reserved4;
	}

	public void setReserved4(BigDecimal reserved4) {
		this.reserved4 = reserved4;
	}

	public Date getReserved5() {
		return reserved5;
	}

	public void setReserved5(Date reserved5) {
		this.reserved5 = reserved5;
	}

	public Date getReserved6() {
		return reserved6;
	}

	public void setReserved6(Date reserved6) {
		this.reserved6 = reserved6;
	}

	public Boolean getReserved7() {
		return reserved7;
	}

	public void setReserved7(Boolean reserved7) {
		this.reserved7 = reserved7;
	}

	public Boolean getReserved8() {
		return reserved8;
	}

	public void setReserved8(Boolean reserved8) {
		this.reserved8 = reserved8;
	}

	public String getReserved9() {
		return reserved9;
	}

	public void setReserved9(String reserved9) {
		this.reserved9 = reserved9 == null ? null : reserved9.trim();
	}

	public String getReserved10() {
		return reserved10;
	}

	public void setReserved10(String reserved10) {
		this.reserved10 = reserved10 == null ? null : reserved10.trim();
	}

	public String getReserved11() {
		return reserved11;
	}

	public void setReserved11(String reserved11) {
		this.reserved11 = reserved11 == null ? null : reserved11.trim();
	}

	public String getReserved12() {
		return reserved12;
	}

	public void setReserved12(String reserved12) {
		this.reserved12 = reserved12 == null ? null : reserved12.trim();
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public Integer getCreateuser() {
		return createuser;
	}

	public void setCreateuser(Integer createuser) {
		this.createuser = createuser;
	}

	public Date getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public Integer getUpdateuser() {
		return updateuser;
	}

	public void setUpdateuser(Integer updateuser) {
		this.updateuser = updateuser;
	}

	public String getLightspot() {
		return lightspot;
	}

	public void setLightspot(String lightspot) {
		this.lightspot = lightspot;
	}

	public String getWarning() {
		return warning;
	}

	public void setWarning(String warning) {
		this.warning = warning;
	}

	public String getPerdate() {
		return perdate;
	}

	public void setPerdate(String perdate) {
		this.perdate = perdate;
	}

	public String getLonggitude() {
		return longgitude;
	}

	public void setLonggitude(String longgitude) {
		this.longgitude = longgitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public List<ProjectInfo> getMorePro() {
		return morePro;
	}

	public void setMorePro(List<ProjectInfo> morePro) {
		this.morePro = morePro;
	}

	/**
	* <p>Title: </p>
	* <p>Description: </p>
	*/
	public ProjectInfo() {
		super();
	}
	

}