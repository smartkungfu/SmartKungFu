package com.lst.model;

import java.math.BigDecimal;
import java.util.Date;

public class OrderDetail {
	private Integer id;

	private Integer orderid;

	private Integer clientid;

	private Integer num;
	
	private BigDecimal price = new BigDecimal(0.00);

	private BigDecimal preprice = new BigDecimal(0.00);

	private Integer ticket;

	private BigDecimal ticprice =  new BigDecimal(0.00);

	private Integer grade;

	private BigDecimal graprice =  new BigDecimal(0.00);

	private BigDecimal actprice =  new BigDecimal(0.00);

	private Integer orderstatus;

	private Boolean enabled;

	private Boolean isdeleted;

	private Integer reserved1;

	private Integer reserved2;

	private BigDecimal reserved3;

	private BigDecimal reserved4;

	private Date reserved5;

	private Date reserved6;

	private Boolean reserved7;

	private Boolean reserved8;

	private String reserved9;

	private String reserved10;

	private String reserved11;

	private String reserved12;

	private String remark;

	private Date createdate;

	private Integer createuser;

	private Date updatedate;

	private Integer updateuser;

	/***********附加属性********************/
	
	//订单状态名称
	private String orderStatusName;
	
	//成长值名称
	private String graName = "-"; 
	
	//用户优惠卷
	private ClientRoll cr;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrderid() {
		return orderid;
	}

	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}

	public Integer getClientid() {
		return clientid;
	}

	public void setClientid(Integer clientid) {
		this.clientid = clientid;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public BigDecimal getPreprice() {
		return preprice;
	}

	public void setPreprice(BigDecimal preprice) {
		this.preprice = preprice;
	}

	public Integer getTicket() {
		return ticket;
	}

	public void setTicket(Integer ticket) {
		this.ticket = ticket;
	}

	public BigDecimal getTicprice() {
		return ticprice;
	}

	public void setTicprice(BigDecimal ticprice) {
		this.ticprice = ticprice;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public BigDecimal getGraprice() {
		return graprice;
	}

	public void setGraprice(BigDecimal graprice) {
		this.graprice = graprice;
	}

	public BigDecimal getActprice() {
		return actprice;
	}

	public void setActprice(BigDecimal actprice) {
		this.actprice = actprice;
	}

	public Integer getOrderstatus() {
		return orderstatus;
	}

	public void setOrderstatus(Integer orderstatus) {
		this.orderstatus = orderstatus;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getIsdeleted() {
		return isdeleted;
	}

	public void setIsdeleted(Boolean isdeleted) {
		this.isdeleted = isdeleted;
	}

	public Integer getReserved1() {
		return reserved1;
	}

	public void setReserved1(Integer reserved1) {
		this.reserved1 = reserved1;
	}

	public Integer getReserved2() {
		return reserved2;
	}

	public void setReserved2(Integer reserved2) {
		this.reserved2 = reserved2;
	}

	public BigDecimal getReserved3() {
		return reserved3;
	}

	public void setReserved3(BigDecimal reserved3) {
		this.reserved3 = reserved3;
	}

	public BigDecimal getReserved4() {
		return reserved4;
	}

	public void setReserved4(BigDecimal reserved4) {
		this.reserved4 = reserved4;
	}

	public Date getReserved5() {
		return reserved5;
	}

	public void setReserved5(Date reserved5) {
		this.reserved5 = reserved5;
	}

	public Date getReserved6() {
		return reserved6;
	}

	public void setReserved6(Date reserved6) {
		this.reserved6 = reserved6;
	}

	public Boolean getReserved7() {
		return reserved7;
	}

	public void setReserved7(Boolean reserved7) {
		this.reserved7 = reserved7;
	}

	public Boolean getReserved8() {
		return reserved8;
	}

	public void setReserved8(Boolean reserved8) {
		this.reserved8 = reserved8;
	}

	public String getReserved9() {
		return reserved9;
	}

	public void setReserved9(String reserved9) {
		this.reserved9 = reserved9 == null ? null : reserved9.trim();
	}

	public String getReserved10() {
		return reserved10;
	}

	public void setReserved10(String reserved10) {
		this.reserved10 = reserved10 == null ? null : reserved10.trim();
	}

	public String getReserved11() {
		return reserved11;
	}

	public void setReserved11(String reserved11) {
		this.reserved11 = reserved11 == null ? null : reserved11.trim();
	}

	public String getReserved12() {
		return reserved12;
	}

	public void setReserved12(String reserved12) {
		this.reserved12 = reserved12 == null ? null : reserved12.trim();
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public Integer getCreateuser() {
		return createuser;
	}

	public void setCreateuser(Integer createuser) {
		this.createuser = createuser;
	}

	public Date getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public Integer getUpdateuser() {
		return updateuser;
	}

	public void setUpdateuser(Integer updateuser) {
		this.updateuser = updateuser;
	}

	public String getOrderStatusName() {
		return orderStatusName;
	}

	public void setOrderStatusName(String orderStatusName) {
		this.orderStatusName = orderStatusName;
	}

	public String getGraName() {
		return graName;
	}

	public void setGraName(String graName) {
		this.graName = graName;
	}

	public ClientRoll getCr() {
		return cr;
	}

	public void setCr(ClientRoll cr) {
		this.cr = cr;
	}

	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	
	
}