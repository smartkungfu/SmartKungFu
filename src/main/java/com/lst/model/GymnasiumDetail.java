package com.lst.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class GymnasiumDetail extends BaseModel {
	private Integer id;

	private Integer gymid;

	private String coursename;

	private Integer coursetype;

	private String coursebill;

	private String pricerang;

	private String crowd;

	private String courseresult;

	private String coach;

	private String coachcontent;

	private BigDecimal workage;

	private Integer limitnum;

	private String learningtime;

	private String phoneno;

	private Integer praisenum;

	private Integer browsenum;

	private Integer favoritenum;

	private Integer sharenum;

	private Boolean enabled;

	private Boolean isdeleted;

	private Integer reserved1;

	private Integer reserved2;

	private BigDecimal reserved3;

	private BigDecimal reserved4;

	private Date reserved5;

	private Date reserved6;

	private Boolean reserved7;

	private Boolean reserved8;

	private String reserved9;

	private String reserved10;

	private String reserved11;

	private String reserved12;

	private String remark;

	private Date createdate;

	private Integer createuser;

	private Date updatedate;

	private Integer updateuser;


	/**
	 *************附加属性*************
	 */
	private String coursetypeName;
	
	private MstGymnasium mstGymnasium;
	
	private String cUName;
	
	private String gymName;
	
	private List<CoursePackage> cps;
	
	//格式化套餐字符集
	private String cpStr;
	
	private List<MstRoll> rolls;
	
	//更多课程
	private List<GymnasiumDetail> moreGds;
	
	public String getcUName() {
		return cUName;
	}

	public void setcUName(String cUName) {
		this.cUName = cUName;
	}

	public String getGymName() {
		return gymName;
	}

	public void setGymName(String gymName) {
		this.gymName = gymName;
	}

	public List<MstRoll> getRolls() {
		return rolls;
	}

	public void setRolls(List<MstRoll> rolls) {
		this.rolls = rolls;
	}

	public String getCoursetypeName() {
		return coursetypeName;
	}

	public void setCoursetypeName(String coursetypeName) {
		this.coursetypeName = coursetypeName;
	}

	public MstGymnasium getMstGymnasium() {
		return mstGymnasium;
	}

	public void setMstGymnasium(MstGymnasium mstGymnasium) {
		this.mstGymnasium = mstGymnasium;
	}

	public List<CoursePackage> getCps() {
		return cps;
	}

	public void setCps(List<CoursePackage> cps) {
		this.cps = cps;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGymid() {
		return gymid;
	}

	public void setGymid(Integer gymid) {
		this.gymid = gymid;
	}

	public String getCoursename() {
		return coursename;
	}

	public void setCoursename(String coursename) {
		this.coursename = coursename == null ? null : coursename.trim();
	}

	public Integer getCoursetype() {
		return coursetype;
	}

	public void setCoursetype(Integer coursetype) {
		this.coursetype = coursetype;
	}

	public String getCoursebill() {
		return coursebill;
	}

	public void setCoursebill(String coursebill) {
		this.coursebill = coursebill == null ? null : coursebill.trim();
	}

	public String getPricerang() {
		return pricerang;
	}

	public void setPricerang(String pricerang) {
		this.pricerang = pricerang == null ? null : pricerang.trim();
	}

	public String getCrowd() {
		return crowd;
	}

	public void setCrowd(String crowd) {
		this.crowd = crowd == null ? null : crowd.trim();
	}

	public String getCourseresult() {
		return courseresult;
	}

	public void setCourseresult(String courseresult) {
		this.courseresult = courseresult == null ? null : courseresult.trim();
	}

	public String getCoach() {
		return coach;
	}

	public void setCoach(String coach) {
		this.coach = coach == null ? null : coach.trim();
	}

	public String getCoachcontent() {
		return coachcontent;
	}

	public void setCoachcontent(String coachcontent) {
		this.coachcontent = coachcontent == null ? null : coachcontent.trim();
	}

	public BigDecimal getWorkage() {
		if(workage != null){
			return workage.setScale(1,BigDecimal.ROUND_HALF_DOWN);
		} else {
			//return new BigDecimal(1.0);
			return null;
		}
		
	}

	public void setWorkage(BigDecimal workage) {
		this.workage = workage;
	}

	public Integer getLimitnum() {
		return limitnum;
	}

	public void setLimitnum(Integer limitnum) {
		this.limitnum = limitnum;
	}

	public String getLearningtime() {
		return learningtime;
	}

	public void setLearningtime(String learningtime) {
		this.learningtime = learningtime == null ? null : learningtime.trim();
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno == null ? null : phoneno.trim();
	}

	public Integer getPraisenum() {
		return praisenum;
	}

	public void setPraisenum(Integer praisenum) {
		this.praisenum = praisenum;
	}

	public Integer getBrowsenum() {
		return browsenum;
	}

	public void setBrowsenum(Integer browsenum) {
		this.browsenum = browsenum;
	}

	public Integer getFavoritenum() {
		return favoritenum;
	}

	public void setFavoritenum(Integer favoritenum) {
		this.favoritenum = favoritenum;
	}

	public Integer getSharenum() {
		return sharenum;
	}

	public void setSharenum(Integer sharenum) {
		this.sharenum = sharenum;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getIsdeleted() {
		return isdeleted;
	}

	public void setIsdeleted(Boolean isdeleted) {
		this.isdeleted = isdeleted;
	}

	public Integer getReserved1() {
		return reserved1;
	}

	public void setReserved1(Integer reserved1) {
		this.reserved1 = reserved1;
	}

	public Integer getReserved2() {
		return reserved2;
	}

	public void setReserved2(Integer reserved2) {
		this.reserved2 = reserved2;
	}

	public BigDecimal getReserved3() {
		return reserved3;
	}

	public void setReserved3(BigDecimal reserved3) {
		this.reserved3 = reserved3;
	}

	public BigDecimal getReserved4() {
		return reserved4;
	}

	public void setReserved4(BigDecimal reserved4) {
		this.reserved4 = reserved4;
	}

	public Date getReserved5() {
		return reserved5;
	}

	public void setReserved5(Date reserved5) {
		this.reserved5 = reserved5;
	}

	public Date getReserved6() {
		return reserved6;
	}

	public void setReserved6(Date reserved6) {
		this.reserved6 = reserved6;
	}

	public Boolean getReserved7() {
		return reserved7;
	}

	public void setReserved7(Boolean reserved7) {
		this.reserved7 = reserved7;
	}

	public Boolean getReserved8() {
		return reserved8;
	}

	public void setReserved8(Boolean reserved8) {
		this.reserved8 = reserved8;
	}

	public String getReserved9() {
		return reserved9;
	}

	public void setReserved9(String reserved9) {
		this.reserved9 = reserved9 == null ? null : reserved9.trim();
	}

	public String getReserved10() {
		return reserved10;
	}

	public void setReserved10(String reserved10) {
		this.reserved10 = reserved10 == null ? null : reserved10.trim();
	}

	public String getReserved11() {
		return reserved11;
	}

	public void setReserved11(String reserved11) {
		this.reserved11 = reserved11 == null ? null : reserved11.trim();
	}

	public String getReserved12() {
		return reserved12;
	}

	public void setReserved12(String reserved12) {
		this.reserved12 = reserved12 == null ? null : reserved12.trim();
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public Integer getCreateuser() {
		return createuser;
	}

	public void setCreateuser(Integer createuser) {
		this.createuser = createuser;
	}

	public Date getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public Integer getUpdateuser() {
		return updateuser;
	}

	public void setUpdateuser(Integer updateuser) {
		this.updateuser = updateuser;
	}

	public String getCpStr() {
		return cpStr;
	}

	public void setCpStr(String cpStr) {
		this.cpStr = cpStr;
	}

	public List<GymnasiumDetail> getMoreGds() {
		return moreGds;
	}

	public void setMoreGds(List<GymnasiumDetail> moreGds) {
		this.moreGds = moreGds;
	}
	
}