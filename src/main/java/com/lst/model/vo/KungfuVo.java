/**   
*    
* 项目名称：SmartKungFu   
* 类名称：KungfuVo   
* 类描述：   
* 创建人：zhangl 
* 创建时间：2017年3月10日 上午10:07:04   
* 修改人：zhangl
* 修改时间：2017年3月10日 上午10:07:04   
* 修改备注：   
* @version    
*    
*/
package com.lst.model.vo;

/**
 * @ClassName: KungfuVo
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangl
 * @date 2017年3月10日 上午10:07:04
 *
 */
public class KungfuVo {
	
	private Integer id;
	
	private String title;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
