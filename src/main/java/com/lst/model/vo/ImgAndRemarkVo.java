/**   
*    
* 项目名称：SmartKungFu   
* 类名称：ImgAndRemarkVo   
* 类描述：   
* 创建人：zhangl  
* 创建时间：2016年12月18日 下午1:33:04   
* 修改人：zhangl   
* 修改时间：2016年12月18日 下午1:33:04   
* 修改备注：   
* @version    
*    
*/
package com.lst.model.vo;

/**
 * @ClassName: ImgAndRemarkVo
 * @Description: 图文并茂视图
 * @author zhangl
 * @date 2016年12月18日 下午1:33:04
 *
 */
public class ImgAndRemarkVo {
	
	private String image;
	
	private String remark;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
