/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：OutcomeVo   
 * 类描述：  测试结果视图类
 * 创建人：zhangl 
 * 创建时间：2017年2月7日 下午6:54:46   
 * 修改人：zhangl
 * 修改时间：2017年2月7日 下午6:54:46   
 * 修改备注：  测试结果视图类
 * @version    
 *    
 */
package com.lst.model.vo;

import java.util.List;

/**
 * @ClassName: OutcomeVo
 * @Description: 测试结果视图类
 * @author zhangl
 * @date 2017年2月7日 下午6:54:46
 *
 */
public class OutcomeVo {
	
	private List<TestVo> bodylist;
	
	private List<TestVo> kungfulist;
	
	private Integer bodyStatus;
	
	private String testDate;

	/**
	 * @return the bodyStatus
	 */
	public Integer getBodyStatus() {
		return bodyStatus;
	}

	/**
	 * @param bodyStatus the bodyStatus to set
	 */
	public void setBodyStatus(Integer bodyStatus) {
		this.bodyStatus = bodyStatus;
	}

	/**
	 * @return the bodylist
	 */
	public List<TestVo> getBodylist() {
		return bodylist;
	}

	/**
	 * @param bodylist the bodylist to set
	 */
	public void setBodylist(List<TestVo> bodylist) {
		this.bodylist = bodylist;
	}

	/**
	 * @return the kungfulist
	 */
	public List<TestVo> getKungfulist() {
		return kungfulist;
	}

	/**
	 * @param kungfulist the kungfulist to set
	 */
	public void setKungfulist(List<TestVo> kungfulist) {
		this.kungfulist = kungfulist;
	}

	/**
	 * @return the testDate
	 */
	public String getTestDate() {
		return testDate;
	}

	/**
	 * @param testDate the testDate to set
	 */
	public void setTestDate(String testDate) {
		this.testDate = testDate;
	}
	
}
