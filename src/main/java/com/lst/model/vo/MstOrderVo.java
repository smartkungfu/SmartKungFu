/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstOrderVo   
 * 类描述：订单VO层   
 * 创建人：zhangl  
 * 创建时间：2016年10月17日 上午10:38:51   
 * 修改人：zhangl   
 * 修改时间：2016年10月17日 上午10:38:51   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.model.vo;

import java.util.List;

import com.lst.model.MstClient;
import com.lst.model.MstOrder;
import com.lst.model.OrderDetail;

/**
 * @ClassName: MstOrderVo
 * @Description: 订单VO层
 * @author zhangl
 * @date 2016年10月17日 上午10:38:51
 *
 */
public class MstOrderVo extends MstOrder {
	
	//用户信息
	private MstClient mstClient;
	
	//订单详情
	private List<OrderDetail> ods;
	
	//支付方式
	private String paytypeName;
	
	//用户购买套餐
	private String packageTypeName;
	
	//有点功夫类型
	private String acttypeName;

	public MstClient getMstClient() {
		return mstClient;
	}

	public void setMstClient(MstClient mstClient) {
		this.mstClient = mstClient;
	}

	public String getPaytypeName() {
		return paytypeName;
	}

	public void setPaytypeName(String paytypeName) {
		this.paytypeName = paytypeName;
	}

	
	public String getActtypeName() {
		return acttypeName;
	}

	public void setActtypeName(String acttypeName) {
		this.acttypeName = acttypeName;
	}

	public List<OrderDetail> getOds() {
		return ods;
	}

	public void setOds(List<OrderDetail> ods) {
		this.ods = ods;
	}

	public String getPackageTypeName() {
		return packageTypeName;
	}

	public void setPackageTypeName(String packageTypeName) {
		this.packageTypeName = packageTypeName;
	}
	
}
