/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：TestVo   
 * 类描述：   测试视图类
 * 创建人：zhangl 
 * 创建时间：2017年2月7日 下午7:04:27   
 * 修改人：zhangl
 * 修改时间：2017年2月7日 下午7:04:27   
 * 修改备注：   测试视图类
 * @version    
 *    
 */
package com.lst.model.vo;

import java.math.BigDecimal;

/**
 * @ClassName: TestVo
 * @Description: 测试视图类
 * @author zhangl
 * @date 2017年2月7日 下午7:04:27
 *
 */
public class TestVo {
	
	private Integer id;
	
	private String name;
	
	private BigDecimal value;
	
	private String note;
	
	private String css;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the css
	 */
	public String getCss() {
		return css;
	}

	/**
	 * @param css the css to set
	 */
	public void setCss(String css) {
		this.css = css;
	}
	
}
