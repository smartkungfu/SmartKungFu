/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MapVo   
 * 类描述：   地图VO类
 * 创建人：zhangl  
 * 创建时间：2016年12月8日 下午12:49:47   
 * 修改人：zhangl   
 * 修改时间：2016年12月8日 下午12:49:47   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.model.vo;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @ClassName: MapVo
 * @Description:地图VO类
 * @author zhangl
 * @date 2016年12月8日 下午12:49:47
 *
 */
public class MapVo {

	@NotNull(message = "ID为空")
	private Integer actId;

	@NotNull(message = "类型为空")
	private Integer actType;

	@NotBlank(message = "经度为空")
	private String longitude;

	@NotBlank(message = "纬度为空")
	private String latitude;
	
	private String title;
	
	private String address;
	
	private String url;
	
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getActId() {
		return actId;
	}

	public void setActId(Integer actId) {
		this.actId = actId;
	}

	public Integer getActType() {
		return actType;
	}

	public void setActType(Integer actType) {
		this.actType = actType;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

}
