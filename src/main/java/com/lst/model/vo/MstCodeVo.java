/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstCodeVo   
 * 类描述： 编码视图类   
 * 创建人：zhangl  
 * 创建时间：2016年11月1日 下午12:14:40   
 * 修改人：zhangl   
 * 修改时间：2016年11月1日 下午12:14:40   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.model.vo;

import java.util.List;

import com.lst.model.MstCode;

/**
 * @ClassName: MstCodeVo
 * @Description: 编码视图类
 * @author zhangl
 * @date 2016年11月1日 下午12:14:40
 *
 */
public class MstCodeVo {
	
	private List<MstCode> mstCodes;
	
	private Integer parentId;

	public List<MstCode> getMstCodes() {
		return mstCodes;
	}

	public void setMstCodes(List<MstCode> mstCodes) {
		this.mstCodes = mstCodes;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	
}
