package com.lst.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class MstGymnasium extends BaseModel {
    private Integer id;

    @NotBlank(message = "场馆名称为空")
    @Length(min = 3, max= 20 ,message = "场馆名称长度为3-20")
    private String gymname;
    
    @NotNull(message = "场馆类型为空")
    private Integer gymtype;

   // @NotBlank(message = "联系人为空")
    private String gymcontact;

   // @NotBlank(message = "联系方式为空")
    //@Pattern(regexp = CommonEnum.REGEX_MOBILE , message = "手机号格式错误")
    private String gymphone;

    private String gymbill;

    @NotBlank(message = "内容为空")
    private String content;

    private Integer province;

    private Integer city;

    private Integer area;

    private String address;

    private String website;

    private String weixin;

   // @NotNull(message = "开始时间为空")
    private Date startdate;

  // @NotNull(message = "结束时间为空")
    private Date enddate;
    
    @NotBlank(message = "营业时间为空")
    private String openinghours;
    
	private String longitude;

	private String latitude;

    private Integer praisenum;

    private Integer browsenum;

    private Integer favoritenum;

    private Integer sharenum;

    private Integer gymstatus;

    private Boolean istop;

    private Boolean enabled;

    private Boolean isdeleted;

    private Integer reserved1;

    private Integer reserved2;

    private BigDecimal reserved3;

    private BigDecimal reserved4;

    private Date reserved5;

    private Date reserved6;

    private Boolean reserved7;

    private Boolean reserved8;

    private String reserved9;

    private String reserved10;

    private String reserved11;

    private String reserved12;

    private String remark;

    private Date createdate;

    private Integer createuser;

    private Date updatedate;

    private Integer updateuser;
    
    
    private String gymtypeName;
    
    //特色课程
    private List<GymnasiumDetail> speGds;
    
    //其他课程
    private List<GymnasiumDetail> otherGds;
    
    private List<String> gymBills;
    

    public List<String> getGymBills() {
		return gymBills;
	}

	public void setGymBills(List<String> gymBills) {
		this.gymBills = gymBills;
	}

	public Integer getGymtype() {
		return gymtype;
	}

	public void setGymtype(Integer gymtype) {
		this.gymtype = gymtype;
	}

	public String getGymtypeName() {
		return gymtypeName;
	}

	public void setGymtypeName(String gymtypeName) {
		this.gymtypeName = gymtypeName;
	}

	public String getOpeninghours() {
		return openinghours;
	}

	public void setOpeninghours(String openinghours) {
		this.openinghours = openinghours;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGymname() {
        return gymname;
    }

    public void setGymname(String gymname) {
        this.gymname = gymname == null ? null : gymname.trim();
    }

    public String getGymcontact() {
        return gymcontact;
    }

    public void setGymcontact(String gymcontact) {
        this.gymcontact = gymcontact == null ? null : gymcontact.trim();
    }

    public String getGymphone() {
        return gymphone;
    }

    public void setGymphone(String gymphone) {
        this.gymphone = gymphone == null ? null : gymphone.trim();
    }

    public String getGymbill() {
        return gymbill;
    }

    public void setGymbill(String gymbill) {
        this.gymbill = gymbill == null ? null : gymbill.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getProvince() {
        return province;
    }

    public void setProvince(Integer province) {
        this.province = province;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website == null ? null : website.trim();
    }

    public String getWeixin() {
        return weixin;
    }

    public void setWeixin(String weixin) {
        this.weixin = weixin == null ? null : weixin.trim();
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public Integer getPraisenum() {
        return praisenum;
    }

    public void setPraisenum(Integer praisenum) {
        this.praisenum = praisenum;
    }

    public Integer getBrowsenum() {
        return browsenum;
    }

    public void setBrowsenum(Integer browsenum) {
        this.browsenum = browsenum;
    }

    public Integer getFavoritenum() {
        return favoritenum;
    }

    public void setFavoritenum(Integer favoritenum) {
        this.favoritenum = favoritenum;
    }

    public Integer getSharenum() {
        return sharenum;
    }

    public void setSharenum(Integer sharenum) {
        this.sharenum = sharenum;
    }

    public Integer getGymstatus() {
        return gymstatus;
    }

    public void setGymstatus(Integer gymstatus) {
        this.gymstatus = gymstatus;
    }

    public Boolean getIstop() {
        return istop;
    }

    public void setIstop(Boolean istop) {
        this.istop = istop;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public Integer getReserved1() {
        return reserved1;
    }

    public void setReserved1(Integer reserved1) {
        this.reserved1 = reserved1;
    }

    public Integer getReserved2() {
        return reserved2;
    }

    public void setReserved2(Integer reserved2) {
        this.reserved2 = reserved2;
    }

    public BigDecimal getReserved3() {
        return reserved3;
    }

    public void setReserved3(BigDecimal reserved3) {
        this.reserved3 = reserved3;
    }

    public BigDecimal getReserved4() {
        return reserved4;
    }

    public void setReserved4(BigDecimal reserved4) {
        this.reserved4 = reserved4;
    }

    public Date getReserved5() {
        return reserved5;
    }

    public void setReserved5(Date reserved5) {
        this.reserved5 = reserved5;
    }

    public Date getReserved6() {
        return reserved6;
    }

    public void setReserved6(Date reserved6) {
        this.reserved6 = reserved6;
    }

    public Boolean getReserved7() {
        return reserved7;
    }

    public void setReserved7(Boolean reserved7) {
        this.reserved7 = reserved7;
    }

    public Boolean getReserved8() {
        return reserved8;
    }

    public void setReserved8(Boolean reserved8) {
        this.reserved8 = reserved8;
    }

    public String getReserved9() {
        return reserved9;
    }

    public void setReserved9(String reserved9) {
        this.reserved9 = reserved9 == null ? null : reserved9.trim();
    }

    public String getReserved10() {
        return reserved10;
    }

    public void setReserved10(String reserved10) {
        this.reserved10 = reserved10 == null ? null : reserved10.trim();
    }

    public String getReserved11() {
        return reserved11;
    }

    public void setReserved11(String reserved11) {
        this.reserved11 = reserved11 == null ? null : reserved11.trim();
    }

    public String getReserved12() {
        return reserved12;
    }

    public void setReserved12(String reserved12) {
        this.reserved12 = reserved12 == null ? null : reserved12.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Integer getCreateuser() {
        return createuser;
    }

    public void setCreateuser(Integer createuser) {
        this.createuser = createuser;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public Integer getUpdateuser() {
        return updateuser;
    }

    public void setUpdateuser(Integer updateuser) {
        this.updateuser = updateuser;
    }

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public List<GymnasiumDetail> getSpeGds() {
		return speGds;
	}

	public void setSpeGds(List<GymnasiumDetail> speGds) {
		this.speGds = speGds;
	}

	public List<GymnasiumDetail> getOtherGds() {
		return otherGds;
	}

	public void setOtherGds(List<GymnasiumDetail> otherGds) {
		this.otherGds = otherGds;
	}
	
	
    
}