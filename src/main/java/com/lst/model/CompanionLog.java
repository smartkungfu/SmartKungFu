package com.lst.model;

import java.util.Date;


public class CompanionLog {
	
    private Integer addId;

    private Integer sourceId;
	
    private Integer createUser;
	
    private Date createDate;
	
    private String companionLogName;
	
    private String companionLogPortrait;

	public Integer getAddId() {
		return addId;
	}

	public void setAddId(Integer addId) {
		this.addId = addId;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Integer createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCompanionLogName() {
		return companionLogName;
	}

	public void setCompanionLogName(String companionLogName) {
		this.companionLogName = companionLogName;
	}

	public String getCompanionLogPortrait() {
		return companionLogPortrait;
	}

	public void setCompanionLogPortrait(String companionLogPortrait) {
		this.companionLogPortrait = companionLogPortrait;
	}
	
}