/**   
 * 携带参数   
 * 项目名称：SmartKungFu   
 * 类名称：FolPara   
 * 类描述：   
 * 创建人：zhangl  
 * 创建时间：2016年10月10日 下午6:56:46   
 * 修改人：zhangl   
 * 修改时间：2016年10月10日 下午6:56:46   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.model;

/**
 * @ClassName: FolPara
 * @Description: 携带参数
 * @author zhangl
 * @date 2016年10月10日 下午6:56:46
 *
 */
public class FolPara {

	/**
	 * 基本信息
	 */
	private Integer page;
	private Integer rows;
	private Integer pageSize;
	private String minDate;
	private String maxDate;
	private Integer menuId;
	/**
	 * 系统用户信息
	 */
	private String usrAccount;
	private String mobileNo;
	private String nameCn;

	/**
	 * 角色信息
	 */

	private String rolName;

	/**
	 * 权限信息
	 */

	private Integer masterType;

	/**
	 * 编码信息
	 */

	private String parentId;

	private String codeName;

	private String type;
	/**
	 * 订单信息
	 */

	private String orderNo;

	/**
	 *有点功夫 
	 */
	private String titleFol;
	
	private String authorFol;
	
	private String gymName;
	
	private String typeFol;
	
	private String ariTypeFol;
	
	private Integer courseId;
	
	private Integer actionId;
	
	/**
	 *App管理 
	 */
	private String nickName; //用户昵称
	
	private String index_exp;
	
	private String index_che;
	
	public String getIndex_exp() {
		return index_exp;
	}

	public void setIndex_exp(String index_exp) {
		this.index_exp = index_exp;
	}

	public String getIndex_che() {
		return index_che;
	}

	public void setIndex_che(String index_che) {
		this.index_che = index_che;
	}

	public String getTypeFol() {
		return typeFol;
	}

	public void setTypeFol(String typeFol) {
		this.typeFol = typeFol;
	}

	public String getAriTypeFol() {
		return ariTypeFol;
	}

	public void setAriTypeFol(String ariTypeFol) {
		this.ariTypeFol = ariTypeFol;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getGymName() {
		return gymName;
	}

	public void setGymName(String gymName) {
		this.gymName = gymName;
	}

	public String getTitleFol() {
		return titleFol;
	}

	public void setTitleFol(String titleFol) {
		this.titleFol = titleFol;
	}

	public Integer getMasterType() {
		return masterType;
	}

	public void setMasterType(Integer masterType) {
		this.masterType = masterType;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getRows() {
		return rows;
	}
	public void setRows(Integer rows) {
		this.rows = rows;
	}
	public String getUsrAccount() {
		return usrAccount;
	}
	public void setUsrAccount(String usrAccount) {
		this.usrAccount = usrAccount;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getMinDate() {
		return minDate;
	}
	public void setMinDate(String minDate) {
		this.minDate = minDate;
	}
	public String getMaxDate() {
		return maxDate;
	}
	public void setMaxDate(String maxDate) {
		this.maxDate = maxDate;
	}

	public String getNameCn() {
		return nameCn;
	}
	public void setNameCn(String nameCn) {
		this.nameCn = nameCn;
	}
	public Integer getMenuId() {
		return menuId;
	}
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	public String getRolName() {
		return rolName;
	}
	public void setRolName(String rolName) {
		this.rolName = rolName;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getAuthorFol() {
		return authorFol;
	}

	public void setAuthorFol(String authorFol) {
		this.authorFol = authorFol;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public Integer getActionId() {
		return actionId;
	}

	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}
	
}
