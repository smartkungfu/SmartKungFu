/**   
 *    
 * 项目名称：MedicalTeam   
 * 类名称：DataTimerTask   
 * 类描述：   
 * 创建人：zhangl  
 * 创建时间：2016年9月13日 下午6:13:06   
 * 修改人：zhangl   
 * 修改时间：2016年9月13日 下午6:13:06   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.model;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.logging.Logger;

import javax.servlet.ServletContext;


import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.service.MstRollService;
import com.mysql.jdbc.StringUtils;

/**
 * @ClassName: DataTimerTask
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangl
 * @date 2016年9月13日 下午6:13:06
 *
 */
public class DataTimerTask extends TimerTask {

	private Logger logger = Logger.getLogger("log");


	//private ServletContext servletContext;
	
	private MstRollService mstRollService;


	/**
	 * <p>Title: </p>
	 * <p>Description: </p>
	 */
	public DataTimerTask(ServletContext servletContext,MstRollService mstRollService) {
		//this.servletContext = servletContext;
		this.mstRollService = mstRollService;
	}

	/* (非 Javadoc)
	 * <p>Title: run</p>
	 * <p>Description: </p>
	 * @see java.util.TimerTask#run()
	 */
	@Override
	public void run() {
		Date startDate = new Date();
		logger.info("run begin: " + startDate);
		
		Map<String, Object> decMap = new HashMap<String, Object>();
		decMap.put("enabled", true);
		decMap.put("isDeleted", false);
		
		PageBounds pageBounds = new PageBounds();
		
		List<MstRoll> mstRolls = mstRollService.queryByList(decMap, pageBounds);
		
		//校验优惠卷是否过期
		String ids = "";
		for(MstRoll mstRoll : mstRolls){
			if(startDate.after(mstRoll.getEnddate())){
				
				ids += mstRoll.getId().toString() + ",";
			}
		}
		
		Map<String, Object> updMap = new HashMap<String, Object>();
		
		if(!StringUtils.isNullOrEmpty(ids)){
			ids = ids.substring(0,ids.length() - 1);
			updMap.put("ids", ids);
			
			mstRollService.updList(updMap);
		}		
	}

}
