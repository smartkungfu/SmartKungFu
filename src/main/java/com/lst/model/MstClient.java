package com.lst.model;

import java.math.BigDecimal;
import java.util.Date;


public class MstClient {
    private Integer id;

    private String password;

    private String namecn;

    private String nickname;

    private String height;

    private String weight;

    private String sign;

    private Integer aimid;

    private Integer knowid;

    private Boolean gender;

    private Date birthday;

    private String email;

    private String mobileno;

    private String thirdno;
    
    private String thirdnoqq;
    
    private String thirdnowb;

    private Integer logintype;

    private Integer province;

    private Integer city;

    private Integer area;

    private Integer street;

    private String address;

    private String idcard;

    private String portrait;

    private Integer nowcourse;
    private Boolean enabled;

    private Boolean isdeleted;

    private Integer reserved1;

    private Integer reserved2;

    private BigDecimal reserved3;

    private BigDecimal reserved4;

    private Date reserved5;

    private Date reserved6;

    private Boolean reserved7;

    private Boolean reserved8;

    private String reserved9;

    private String reserved10;

    private String reserved11;

    private String reserved12;

    private String remark;

    private Date createdate;

    private Integer createuser;

    private Date updatedate;

    private Integer updateuser;

    // 新增字段
    private String cUName;

    private String provinceName;

    private String cityName;

    private String areaName;
    
    private Boolean discussStatus;//评论通知状态
    
    private Boolean thumbStatus;//点赞通知状态
    
    private Boolean systemStatus;//系统通知状态

    public Integer getNowcourse() {
	return nowcourse;
    }

    public void setNowcourse(Integer nowcourse) {
	this.nowcourse = nowcourse;
    }

    public String getProvinceName() {
	return provinceName;
    }

    public void setProvinceName(String provinceName) {
	this.provinceName = provinceName;
    }

    public String getCityName() {
	return cityName;
    }

    public void setCityName(String cityName) {
	this.cityName = cityName;
    }

    public String getAreaName() {
	return areaName;
    }

    public void setAreaName(String areaName) {
	this.areaName = areaName;
    }

    public String getcUName() {
	return cUName;
    }

    public void setcUName(String cUName) {
	this.cUName = cUName;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password == null ? null : password.trim();
    }

    public String getNamecn() {
	return namecn;
    }

    public void setNamecn(String namecn) {
	this.namecn = namecn == null ? null : namecn.trim();
    }

    public String getNickname() {
	return nickname;
    }

    public void setNickname(String nickname) {
	this.nickname = nickname == null ? null : nickname.trim();
    }

    public Boolean getGender() {
	return gender;
    }

    public void setGender(Boolean gender) {
	this.gender = gender;
    }

    public Date getBirthday() {
	return birthday;
    }

    public void setBirthday(Date birthday) {
	this.birthday = birthday;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email == null ? null : email.trim();
    }

    public String getMobileno() {
	return mobileno;
    }

    public void setMobileno(String mobileno) {
	this.mobileno = mobileno == null ? null : mobileno.trim();
    }

    public String getThirdno() {
	return thirdno;
    }

    public void setThirdno(String thirdno) {
	this.thirdno = thirdno == null ? null : thirdno.trim();
    }

    public Integer getLogintype() {
	return logintype;
    }

    public void setLogintype(Integer logintype) {
	this.logintype = logintype;
    }

    public Integer getProvince() {
	return province;
    }

    public void setProvince(Integer province) {
	this.province = province;
    }

    public Integer getCity() {
	return city;
    }

    public void setCity(Integer city) {
	this.city = city;
    }

    public Integer getArea() {
	return area;
    }

    public void setArea(Integer area) {
	this.area = area;
    }

    public Integer getStreet() {
	return street;
    }

    public void setStreet(Integer street) {
	this.street = street;
    }

    public String getAddress() {
	return address;
    }

    public void setAddress(String address) {
	this.address = address == null ? null : address.trim();
    }

    public String getIdcard() {
	return idcard;
    }

    public void setIdcard(String idcard) {
	this.idcard = idcard == null ? null : idcard.trim();
    }

    public String getPortrait() {
	return portrait;
    }

    public void setPortrait(String portrait) {
	this.portrait = portrait == null ? null : portrait.trim();
    }

    public Boolean getEnabled() {
	return enabled;
    }

    public void setEnabled(Boolean enabled) {
	this.enabled = enabled;
    }

    public Boolean getIsdeleted() {
	return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
	this.isdeleted = isdeleted;
    }

    public Integer getReserved1() {
	return reserved1;
    }

    public void setReserved1(Integer reserved1) {
	this.reserved1 = reserved1;
    }

    public Integer getReserved2() {
	return reserved2;
    }

    public void setReserved2(Integer reserved2) {
	this.reserved2 = reserved2;
    }

    public BigDecimal getReserved3() {
	return reserved3;
    }

    public void setReserved3(BigDecimal reserved3) {
	this.reserved3 = reserved3;
    }

    public BigDecimal getReserved4() {
	return reserved4;
    }

    public void setReserved4(BigDecimal reserved4) {
	this.reserved4 = reserved4;
    }

    public Date getReserved5() {
	return reserved5;
    }

    public void setReserved5(Date reserved5) {
	this.reserved5 = reserved5;
    }

    public Date getReserved6() {
	return reserved6;
    }

    public void setReserved6(Date reserved6) {
	this.reserved6 = reserved6;
    }

    public Boolean getReserved7() {
	return reserved7;
    }

    public void setReserved7(Boolean reserved7) {
	this.reserved7 = reserved7;
    }

    public Boolean getReserved8() {
	return reserved8;
    }

    public void setReserved8(Boolean reserved8) {
	this.reserved8 = reserved8;
    }

    public String getReserved9() {
	return reserved9;
    }

    public void setReserved9(String reserved9) {
	this.reserved9 = reserved9 == null ? null : reserved9.trim();
    }

    public String getReserved10() {
	return reserved10;
    }

    public void setReserved10(String reserved10) {
	this.reserved10 = reserved10 == null ? null : reserved10.trim();
    }

    public String getReserved11() {
	return reserved11;
    }

    public void setReserved11(String reserved11) {
	this.reserved11 = reserved11 == null ? null : reserved11.trim();
    }

    public String getReserved12() {
	return reserved12;
    }

    public void setReserved12(String reserved12) {
	this.reserved12 = reserved12 == null ? null : reserved12.trim();
    }

    public String getRemark() {
	return remark;
    }

    public void setRemark(String remark) {
	this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreatedate() {
	return createdate;
    }

    public void setCreatedate(Date createdate) {
	this.createdate = createdate;
    }

    public Integer getCreateuser() {
	return createuser;
    }

    public void setCreateuser(Integer createuser) {
	this.createuser = createuser;
    }

    public Date getUpdatedate() {
	return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
	this.updatedate = updatedate;
    }

    public Integer getUpdateuser() {
	return updateuser;
    }

    public void setUpdateuser(Integer updateuser) {
	this.updateuser = updateuser;
    }

    public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getSign() {
	return sign;
    }

    public void setSign(String sign) {
	this.sign = sign;
    }

    public Integer getAimid() {
	return aimid;
    }

    public void setAimid(Integer aimid) {
	this.aimid = aimid;
    }

    public Integer getKnowid() {
	return knowid;
    }

    public void setKnowid(Integer knowid) {
	this.knowid = knowid;
    }

	public Boolean getDiscussStatus() {
		return discussStatus;
	}

	public void setDiscussStatus(Boolean discussStatus) {
		this.discussStatus = discussStatus;
	}

	public Boolean getThumbStatus() {
		return thumbStatus;
	}

	public void setThumbStatus(Boolean thumbStatus) {
		this.thumbStatus = thumbStatus;
	}

	public Boolean getSystemStatus() {
		return systemStatus;
	}

	public void setSystemStatus(Boolean systemStatus) {
		this.systemStatus = systemStatus;
	}

	public String getThirdnoqq() {
		return thirdnoqq;
	}

	public void setThirdnoqq(String thirdnoqq) {
		this.thirdnoqq = thirdnoqq;
	}

	public String getThirdnowb() {
		return thirdnowb;
	}

	public void setThirdnowb(String thirdnowb) {
		this.thirdnowb = thirdnowb;
	}

}