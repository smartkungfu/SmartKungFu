package com.lst.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstCode;

public interface MstCodeService {
	int deleteByPrimaryKey(Integer id);

	int insert(MstCode record);

	int insertSelective(MstCode record);

	MstCode selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(MstCode record);

	int updateByPrimaryKey(MstCode record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 获取结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstCode>    返回类型
	 */
	List<MstCode> queryByList(Map<String, Object> map,PageBounds pageBounds);

	/**
	 * 
	 * @Title: queryListByParentId
	 * @Description: 通过PARENTID 获取结果集
	 * @param @param parentId
	 * @param @return    设定文件
	 * @return List<MstCode>    返回类型
	 */
	List<MstCode> queryListByParentId(Integer parentId);

	/**
	 * 
	 * @Title: queryListByIds
	 * @Description:通过ID集获取结果集
	 * @param @param ids
	 * @param @return    设定文件
	 * @return List<MstCode>    返回类型
	 */
	List<MstCode>queryListByIds(@Param(value = "ids") String ids);

	/***
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delList(Map<String, Object> map);

	/**
	 * 
	 * @Title: queryListByType
	 * @Description:通过类型获取结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstCode>    返回类型
	 */
	List<MstCode> queryListByType(Map<String, Object> map,PageBounds pageBounds);

	/**
	 * 
	 * @Title: saveWithList
	 * @Description:批量更新操作
	 * @param @param map
	 * @param @param list    设定文件
	 * @return void    返回类型
	 */
	void saveWithList(Map<String, Object> map,List<MstCode> list);
}