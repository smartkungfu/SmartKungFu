package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.SuggestInfo;

/**
 * @ClassName: SuggestInfoService
 * @Description: 我的建议
 * @author zmm
 * @date 2017年10月10日 
 *
 */
public interface SuggestInfoService {

	int insert(SuggestInfo record);

	int updateSuggest(SuggestInfo record);
	
	 List<SuggestInfo> queryByList(Map<String, Object> map , PageBounds pageBounds);
	 
	 SuggestInfo selectByKey(Integer id);
}
