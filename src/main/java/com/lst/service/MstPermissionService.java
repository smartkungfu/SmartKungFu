package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstPermission;
import com.lst.model.MstUser;

public interface MstPermissionService {
    int deleteByPrimaryKey(Integer id);

    int insert(MstPermission record);

    int insertSelective(MstPermission record);

    MstPermission selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MstPermission record);

    int updateByPrimaryKey(MstPermission record);
    
    List<MstPermission> queryByList(Map<String, Object> map, PageBounds pageBounds);

	/**
	 * 
	 * @Title: queryUserPermissionBy
	 * @Description: 根据map查询用户权限  菜单
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<MstPermission>    返回类型
	 */
	List<MstPermission> queryUserPermissionByMap(Map<String, Object>  map);
	
	/**
	 * 
	 * @Title: queryUserPermissionBy
	 * @Description: 根据map查询角色权限  菜单
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<MstPermission>    返回类型
	 */
	List<MstPermission> queryRolePermissionByMap(Map<String, Object>  map);
	
	/**
	 * 
	 * @Title: deleteByMap
	 * @Description: 批量删除用户权限
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void deleteByMap(Map<String, Object> map);

	/**
	 * 
	 * @Title: saveList
	 * @Description: 保存用户权限
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void saveList(List<MstPermission> list);
	
	/**
	 * 
	 * @Title: queryMenuPermission
	 * @Description:获取菜单，并选中用户用户拥有权限的菜单
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<Map<String,Object>>    返回类型
	 */
	List<Map<String, Object>> queryMenuPermission(Map<String, Object>  map);
	
	/**
	 * 
	 * @Title: queryMenuPermission
	 * @Description:获取按钮，并选中用户用户拥有权限的按钮
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<Map<String,Object>>    返回类型
	 */
	List<Map<String, Object>> queryBtnPermission(Map<String, Object> map);
	
	/**
	 * 
	 * @Title: savePermission
	 * @Description:保存用户权限
	 * @param @param masterType
	 * @param @param masterValue
	 * @param @param menuIds
	 * @param @param buttonIds
	 * @param @param currentUser    设定文件
	 * @return void    返回类型
	 */
	void savePermission(String masterType, String masterValue, String menuIds, String buttonIds,MstUser currentUser);
}