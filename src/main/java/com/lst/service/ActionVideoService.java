package com.lst.service;


import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ActionVideo;

/**
 * @ClassName: ActionVideoService
 * @Description: 课程动作接口类
 *
 */
public interface ActionVideoService {
	

	int insert(ActionVideo record);
	
	List<ActionVideo> queryByList(Map<String, Object> map , PageBounds pageBounds);
 
	ActionVideo selectByPrimaryKey(Integer id);
	
	int updateByKey(ActionVideo record);
	
	void delList(Map<String, Object> map);
}
