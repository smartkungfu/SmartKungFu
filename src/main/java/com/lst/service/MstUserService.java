package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstUser;

public interface MstUserService {
    int deleteByPrimaryKey(Integer id);

    int insert(MstUser record);

    int insertSelective(MstUser record);

    MstUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MstUser record);

    int updateByPrimaryKey(MstUser record);
    
	/**
	 * 
	 * @Title: queryByList
	 * @Description: 获取数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstUser>    返回类型
	 */
	List<MstUser> queryByList(Map<String, Object> map ,PageBounds pageBounds);
	
	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delList(Map<String, Object> map);
	
	/**
	 * 
	 * @Title: saveInfo
	 * @Description: 用户信息表单提交
	 * @param @param mstUser
	 * @param @param sysId
	 * @param @param rolids    设定文件
	 * @return void    返回类型
	 */
	void saveInfo(MstUser mstUser,Integer sysId,Integer [] rolids);
}