package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.Answer;

public interface AnswerService {

    List<Answer> queryByList(Map<String, Object> map , PageBounds pageBounds);
    
    void delList(Map<String, Object> map);
    
    Answer selectByPrimaryKey(Integer id);
    
    int countByMap(Map<String, Object> map);
}