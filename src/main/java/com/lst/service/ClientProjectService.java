/**   
 *    
 * 项目名称：SmartKungFuService   
 * 类名称：ClientProjectService   
 * 类描述：用户活动接口层   
 * 创建人：zhangl  
 * 创建时间：2016年9月28日 下午12:25:11   
 * 修改人：zhangl   
 * 修改时间：2016年9月28日 下午12:25:11   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientProject;

/**
 * @ClassName: ClientProjectService
 * @Description:用户活动接口层
 * @author zhangl
 * @date 2016年9月28日 下午12:25:11
 *
 */
public interface ClientProjectService {
	int deleteByPrimaryKey(Integer id);

	int insert(ClientProject record);

	int insertSelective(ClientProject record);

	ClientProject selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ClientProject record);

	int updateByPrimaryKey(ClientProject record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description:查询结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<ClientProject>    返回类型
	 */
	List<ClientProject> queryByList(Map<String, Object> map,PageBounds pageBounds);
}
