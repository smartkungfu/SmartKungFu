/**   
 *  课程套餐接口层  
 * 项目名称：SmartKungFu   
 * 类名称：CoursePackageService   
 * 类描述：   课程套餐接口层
 * 创建人：zhangl  
 * 创建时间：2016年11月1日 下午6:37:35   
 * 修改人：zhangl   
 * 修改时间：2016年11月1日 下午6:37:35   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.CoursePackage;

/**
 * @ClassName: CoursePackageService
 * @Description:
 * @author zhangl
 * @date 2016年11月1日 下午6:37:35
 *
 */
public interface CoursePackageService {
	int deleteByPrimaryKey(Integer id);

	int insert(CoursePackage record);

	int insertSelective(CoursePackage record);

	CoursePackage selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(CoursePackage record);

	int updateByPrimaryKey(CoursePackage record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 查询结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<CoursePackage>    返回类型
	 */
	List<CoursePackage> queryByList(Map<String, Object> map,PageBounds pageBounds);

	/**
	 * 
	 * @Title: delListByCourseId
	 * @Description: 通过课程ID批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delListByCourseId(Map<String, Object> map);

	/**
	 * 
	 * @Title: saveList
	 * @Description:批量插入
	 * @param @param list    设定文件
	 * @return void    返回类型
	 */
	void saveList(List<CoursePackage> list); 

	/**
	 * 
	 * @Title: saveWithList
	 * @Description: 批量更新
	 * @param @param map
	 * @param @param list    设定文件
	 * @return void    返回类型
	 */
	void saveWithList(Map<String, Object> map,List<CoursePackage> list);
}
