package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClassPicture;

public interface ClassPictureService {
	
	int insert(ClassPicture record);

    /**
   	 * 
   	 * @Title: queryByList
   	 * @Description: 数据集
   	 * @param @param map
   	 * @param @return   pageBounds
   	 * @return List<MstOrder>    返回类型
   	 * @throws
   	 */
    List<ClassPicture> queryByList(Map<String, Object> map , PageBounds pageBounds);
    
    ClassPicture selectByPrimaryKey(Integer id);
	
	int updateByKey(ClassPicture record);
	
	void delList(Map<String, Object> map);
}