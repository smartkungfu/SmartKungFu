/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstTransactionService   
 * 类描述：   交易接口类
 * 创建人：zhangl  
 * 创建时间：2016年12月28日 下午12:24:10   
 * 修改人：zhangl   
 * 修改时间：2016年12月28日 下午12:24:10   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstTransaction;

/**
 * @ClassName: MstTransactionService
 * @Description: 交易接口类
 * @author zhangl
 * @date 2016年12月28日 下午12:24:10
 *
 */
public interface MstTransactionService {

	int deleteByPrimaryKey(Integer id);

	int insert(MstTransaction record);

	int insertSelective(MstTransaction record);

	MstTransaction selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(MstTransaction record);

	int updateByPrimaryKey(MstTransaction record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 查询数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstTransaction>    返回类型
	 */
	List<MstTransaction> queryByList(Map<String, Object> map,PageBounds pageBounds);
}
