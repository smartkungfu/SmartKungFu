/**   
*    
* 项目名称：SmartKungFu   
* 类名称：ClientRollService   
* 类描述：   
* 创建人：Wdd   
* 创建时间：2016年11月2日 上午11:49:57   
* 修改人：Wdd  
* 修改时间：2016年11月2日 上午11:49:57   
* 修改备注：   
* @version    
*    
*/
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientRoll;

/**
 * @ClassName: ClientRollService
 * @Description: 用户优惠
 * @author Wdd
 * @date 2016年11月2日 上午11:49:57
 *
 */
public interface ClientRollService {

	int deleteByPrimaryKey(Integer id);

	int insert(ClientRoll record);

	int insertSelective(ClientRoll record);

	ClientRoll selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ClientRoll record);

	int updateByPrimaryKey(ClientRoll record);
	
	/**
	 * 
	* @Title: queryByList
	* @Description: 查询数据集
	* @param @param map
	* @param @param pageBounds
	* @param @return    设定文件
	* @return List<ClientRoll>    返回类型
	* @throws
	 */
	List<ClientRoll> queryByList(Map<String, Object> map,PageBounds pageBounds);
}
