package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.SportRecords;

public interface SportRecordsService {
	
	List<SportRecords> queryByMap(Map<String, Object> map,PageBounds pageBounds);
	
}