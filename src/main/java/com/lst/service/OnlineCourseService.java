package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.OnlineCourse;

public interface OnlineCourseService {
    void deleteByPrimaryKey(Integer id);

    void insert(OnlineCourse onlineCourse);

    void insertSelective(OnlineCourse onlineCourse);

    OnlineCourse selectByPrimaryKey(Integer id);
    
    OnlineCourse selectByPrimaryKey2(Integer id);

    void updateByPrimaryKeySelective(OnlineCourse onlineCourse);

    void updateByPrimaryKey(OnlineCourse onlineCourse);

    /**
     * 
     * @Title: queryByList
     * @Description:获取数据集
     * @param @param map
     * @param @param pageBounds
     * @param @return 设定文件
     * @return List<MstRole> 返回类型
     */
    List<OnlineCourse> queryByList(Map<String, Object> map,
	    PageBounds pageBounds);

    /**
     * 
     * @Title: delList
     * @Description: 批量删除
     * @param @param map 设定文件
     * @return void 返回类型
     */
    void delList(Map<String, Object> map);
}