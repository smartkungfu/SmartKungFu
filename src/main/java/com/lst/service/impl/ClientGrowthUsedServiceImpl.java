/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：ClientGrowthUsedServiceImpl   
 * 类描述：   
 * 创建人：Wdd   
 * 创建时间：2017年1月13日 下午8:49:43   
 * 修改人：Wdd  
 * 修改时间：2017年1月13日 下午8:49:43   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ClientGrowthUsedMapper;
import com.lst.model.ClientGrowthUsed;
import com.lst.service.ClientGrowthUsedService;

/**
 * @ClassName: ClientGrowthUsedServiceImpl
 * @Description: 用户等级优惠
 * @author Wdd
 * @date 2017年1月13日 下午8:49:43
 * 
 */
@Service("ClientGrowthUsedServiceImpl")
public class ClientGrowthUsedServiceImpl implements ClientGrowthUsedService {

	@Resource
	private ClientGrowthUsedMapper dao;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(ClientGrowthUsed record) {
		return dao.insert(record);
	}

	@Override
	public int insertSelective(ClientGrowthUsed record) {
		return dao.insertSelective(record);
	}

	@Override
	public ClientGrowthUsed selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(ClientGrowthUsed record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(ClientGrowthUsed record) {
		return dao.updateByPrimaryKey(record);
	}
	
	/**
	 * 
	* @Title: queryByList
	* @Description: 查询结果集
	* @param @param map
	* @param @param pageBounds
	* @param @return    设定文件
	* @return List<ClientNum>    返回类型
	* @throws
	 */
	@Override
	public List<ClientGrowthUsed> queryByList(Map<String, Object> map,PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

}
