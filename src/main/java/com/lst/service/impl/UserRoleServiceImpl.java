/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：UserRoleServiceImpl   
 * 类描述： 用户角色实现层  
 * 创建人：zhangl  
 * 创建时间：2016年10月8日 上午10:57:56   
 * 修改人：zhangl   
 * 修改时间：2016年10月8日 上午10:57:56   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.UserRoleMapper;
import com.lst.model.UserRole;
import com.lst.service.UserRoleService;

/**
 * @ClassName: UserRoleServiceImpl
 * @Description: 用户角色实现层
 * @author zhangl
 * @date 2016年10月8日 上午10:57:56
 *
 */
@Service("userRoleService")
public class UserRoleServiceImpl implements UserRoleService {
	
	@Autowired
	private UserRoleMapper dao;
	
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(UserRole record) {
		return dao.insert(record);
	}

	public int insertSelective(UserRole record) {
		return dao.insertSelective(record);
	}

	public UserRole selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(UserRole record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(UserRole record) {
		return dao.updateByPrimaryKey(record);
	}

	/**
	 * 
	 * @Title: queryByList
	 * @Description:查询结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<UserRole>    返回类型
	 */
	public List<UserRole> queryByList(Map<String, Object> map,PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}
	
	/**
	 * 
	 * @Title: saveList
	 * @Description:批量插入
	 * @param @param list    设定文件
	 * @return void    返回类型
	 */
	public void saveList(List<UserRole> list){
		dao.saveList(list);
	}
	
	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	public void delList(Map<String, Object> map){
		dao.delList(map);
	}
}
