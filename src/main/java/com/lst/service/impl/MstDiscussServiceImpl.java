/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstDiscussServiceImpl   
 * 类描述：   
 * 创建人：Wdd   
 * 创建时间：2016年10月26日 下午4:44:31   
 * 修改人：Wdd  
 * 修改时间：2016年10月26日 下午4:44:31   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.MstDiscussMapper;
import com.lst.model.MstDiscuss;
import com.lst.service.MstDiscussService;

/**
 * @ClassName: MstDiscussServiceImpl
 * @Description: 评论消息
 * @author Wdd
 * @date 2016年10月26日 下午4:44:31
 *
 */
@Service("mstDiscussServiceImpl")
public class MstDiscussServiceImpl implements MstDiscussService{

	@Autowired
	private MstDiscussMapper dao;

	@Override
	public List<MstDiscuss> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public void delList(Map<String, Object> map) {
		dao.delList(map);
	}

	@Override
	public Integer queryCountByMap(Map<String, Object> map) {
		
		return dao.queryCountByMap(map);
	}

	@Override
	public List<MstDiscuss> queryByList2(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList2(map, pageBounds);
	}


}
