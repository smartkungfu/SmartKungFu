package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ClassPictureMapper;
import com.lst.model.ClassPicture;
import com.lst.service.ClassPictureService;


/**
 * @ClassName: ClassPictureServiceImpl
 * @Description: 功夫分类实现类层
 * 
 */
@Service("ClassPictureService")
public class ClassPictureServiceImpl implements ClassPictureService {
	
	@Autowired
	private ClassPictureMapper dao;

	@Override
	public List<ClassPicture> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public int insert(ClassPicture record) {
		
		return dao.insert(record);
	}

	@Override
	public ClassPicture selectByPrimaryKey(Integer id) {
		
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByKey(ClassPicture record) {
		
		return dao.updateByKey(record);
	}

	@Override
	public void delList(Map<String, Object> map) {
		
		dao.delList(map);
	}

	


}
