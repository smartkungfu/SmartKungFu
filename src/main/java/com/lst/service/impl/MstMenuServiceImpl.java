/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstMenuServiceImpl   
 * 类描述：   
 * 创建人：zhangl  
 * 创建时间：2016年10月8日 上午9:54:27   
 * 修改人：zhangl   
 * 修改时间：2016年10月8日 上午9:54:27   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.MstMenuMapper;
import com.lst.model.MstMenu;
import com.lst.service.MstMenuService;

/**
 * @ClassName: MstMenuServiceImpl
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangl
 * @date 2016年10月8日 上午9:54:27
 *
 */
@Service("mstMenuService")
public class MstMenuServiceImpl implements MstMenuService {
	
	@Autowired
	private MstMenuMapper dao;
	
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(MstMenu record) {
		return dao.insert(record);
	}

	public int insertSelective(MstMenu record) {
		return dao.insertSelective(record);
	}

	public MstMenu selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(MstMenu record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(MstMenu record) {
		return dao.updateByPrimaryKey(record);
	}

	/**
	 * 
	 * @Title: queryListForUserRoots
	 * @Description: 查询用户独立权限菜单和用户角色权限菜单 
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstMenu>    返回类型
	 */
	public List<MstMenu> queryListForUserRoots(Map<String, Object> map,PageBounds pageBounds) {
		return dao.queryListForUserRoots(map, pageBounds);
	}
}
