/**   
*    
* 项目名称：SmartKungFuService   
* 类名称：SuggestInfoServiceImpl   
* 类描述：   我的建议实现类层
* 创建人：zhanghl   
* 创建时间：2016年11月28日 下午6:58:18   
* 修改人：zhanghl   
* 修改时间：2016年11月28日 下午6:58:18   
* 修改备注：   
* @version    
*    
*/
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.SuggestInfoMapper;
import com.lst.model.SuggestInfo;
import com.lst.service.SuggestInfoService;

/**
 * @ClassName: SuggestInfoServiceImpl
 * @Description: 我的建议实现类层
 * @author zhanghl
 * @date 2016年11月28日 下午6:58:18
 *
 */
@Service("suggestInfoService")
public class SuggestInfoServiceImpl implements SuggestInfoService {

	@Autowired
	private SuggestInfoMapper dao;

	@Override
	public int insert(SuggestInfo record) {
		
		return dao.insert(record);
	}

	@Override
	public List<SuggestInfo> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public SuggestInfo selectByKey(Integer id) {
		
		return dao.selectByKey(id);
	}

	@Override
	public int updateSuggest(SuggestInfo record) {
		
		return dao.updateSuggest(record);
	}

	
}
