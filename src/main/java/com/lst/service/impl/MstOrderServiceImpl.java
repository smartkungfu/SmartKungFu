/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstOrderServiceImpl   
 * 类描述： 订单实现类层   
 * 创建人：zhangl  
 * 创建时间：2016年10月17日 上午10:49:09   
 * 修改人：zhangl   
 * 修改时间：2016年10月17日 上午10:49:09   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.MstOrderMapper;
import com.lst.model.MstOrder;
import com.lst.model.vo.MstOrderVo;
import com.lst.service.MstOrderService;

/**
 * @ClassName: MstOrderServiceImpl
 * @Description: 订单实现类层
 * @author zhangl
 * @date 2016年10月17日 上午10:49:09
 *
 */
@Service("mstOrderService")
public class MstOrderServiceImpl implements MstOrderService {
	
	@Autowired
	private MstOrderMapper dao;
	
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(MstOrder record) {
		return dao.insert(record);
	}

	public int insertSelective(MstOrder record) {
		return dao.insertSelective(record);
	}

	public MstOrder selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(MstOrder record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(MstOrder record) {
		return dao.updateByPrimaryKey(record);
	}

	/***
	 * 
	 * @Title: queryByList
	 * @Description: 查询结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstOrderVo>    返回类型
	 */
	public List<MstOrderVo> queryByList(Map<String, Object> map ,PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

	/***
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	public void delList(Map<String, Object> map) {
		dao.delList(map);
	}
}
