/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：OnlineCourseTipsServiceImpl   
 * 类描述：OnlineCourseTipsService   
 * 创建人：zhangl  
 * 创建时间：2016年12月30日 上午9:49:43   
 * 修改人：zhangl   
 * 修改时间：2016年12月30日 上午9:49:43   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.OnlineCourseTipsMapper;
import com.lst.model.OnlineCourseTips;
import com.lst.service.OnlineCourseTipsService;

/**
 * @ClassName: OnlineCourseTipsServiceImpl
 * @Description: 在线课程提示语实现类
 * @author zhangl
 * @date 2016年12月30日 上午9:49:43
 *
 */
@Service("onlineCourseTipsService")
public class OnlineCourseTipsServiceImpl implements OnlineCourseTipsService {

	@Autowired
	private OnlineCourseTipsMapper dao;

	/* (非 Javadoc)
	 * <p>Title: deleteByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param id
	 * @return
	 * @see com.lst.service.OnlineCourseTipsService#deleteByPrimaryKey(java.lang.Integer)
	 */
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	/* (非 Javadoc)
	 * <p>Title: insert</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.OnlineCourseTipsService#insert(com.lst.model.OnlineCourseTips)
	 */
	@Override
	public int insert(OnlineCourseTips record) {
		return dao.insert(record);
	}

	/* (非 Javadoc)
	 * <p>Title: insertSelective</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.OnlineCourseTipsService#insertSelective(com.lst.model.OnlineCourseTips)
	 */
	@Override
	public int insertSelective(OnlineCourseTips record) {
		return dao.insertSelective(record);
	}

	/* (非 Javadoc)
	 * <p>Title: selectByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param id
	 * @return
	 * @see com.lst.service.OnlineCourseTipsService#selectByPrimaryKey(java.lang.Integer)
	 */
	@Override
	public OnlineCourseTips selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	/* (非 Javadoc)
	 * <p>Title: updateByPrimaryKeySelective</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.OnlineCourseTipsService#updateByPrimaryKeySelective(com.lst.model.OnlineCourseTips)
	 */
	@Override
	public int updateByPrimaryKeySelective(OnlineCourseTips record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	/* (非 Javadoc)
	 * <p>Title: updateByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.OnlineCourseTipsService#updateByPrimaryKey(com.lst.model.OnlineCourseTips)
	 */
	@Override
	public int updateByPrimaryKey(OnlineCourseTips record) {
		return dao.updateByPrimaryKey(record);
	}

	/* (非 Javadoc)
	 * <p>Title: delList</p>
	 * <p>Description: </p>
	 * @param map
	 * @see com.lst.service.OnlineCourseTipsService#delList(java.util.Map)
	 */
	@Override
	public void delList(Map<String, Object> map) {
		dao.delList(map);
	}

	/* (非 Javadoc)
	 * <p>Title: queryByList</p>
	 * <p>Description: </p>
	 * @param map
	 * @param pageBounds
	 * @return
	 * @see com.lst.service.OnlineCourseTipsService#queryByList(java.util.Map, com.github.miemiedev.mybatis.paginator.domain.PageBounds)
	 */
	@Override
	public List<OnlineCourseTips> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

}
