package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.CourseActionMapper;
import com.lst.model.CourseAction;
import com.lst.service.CourseActionService;

/**
 * @ClassName: CourseActionServiceImpl
 * @Description:课程关联动作实现类
 * @author zmm
 * @date 2017年9月27日
 *
 */
@Service("courseActionService")
public class CourseActionServiceImpl implements CourseActionService {

	@Autowired
	private CourseActionMapper dao;

	@Override
	public int insert(CourseAction record) {
		
		return dao.insert(record);
	}

	@Override
	public CourseAction selectByPrimaryKey(Integer id) {
		
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int update(CourseAction record) {
		
		return dao.update(record);
	}

	@Override
	public List<CourseAction> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public void delList(Map<String, Object> map) {
		
		dao.delList(map);
	}

	

	
}
