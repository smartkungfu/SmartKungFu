/**   
 *    
 * 项目名称：SmartKungFuService   
 * 类名称：WenwInfoServiceImpl   
 * 类描述：   
 * 创建人：zhangl  
 * 创建时间：2016年9月22日 下午5:34:25   
 * 修改人：zhangl   
 * 修改时间：2016年9月22日 下午5:34:25   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.WenwInfoMapper;
import com.lst.model.WenwInfo;
import com.lst.service.WenwInfoService;

/**
 * @ClassName: WenwInfoServiceImpl
 * @Description:文章实体类
 * @author zhangl
 * @date 2016年9月22日 下午5:34:25
 *
 */
@Service("wenwInfoService")
public class WenwInfoServiceImpl implements WenwInfoService {

	@Autowired
	private WenwInfoMapper dao;

	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(WenwInfo record) {
		return dao.insert(record);
	}

	public int insertSelective(WenwInfo record) {
		return dao.insertSelective(record);
	}

	public WenwInfo selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(WenwInfo record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(WenwInfo record) {
		return dao.updateByPrimaryKey(record);
	}

	/**
	 * 
	 * @Title: queryByList
	 * @Description:查询数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<WenwInfo>    返回类型
	 */
	public List<WenwInfo> queryByList(Map<String, Object> map , PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}
	
	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	public void delList(Map<String, Object> map){
		dao.delList(map);
	}
}
