/**   
 *    
 * 项目名称：SmartKungFuService   
 * 类名称：StarInfoServiceImpl   
 * 类描述：   达人实现类
 * 创建人：zhangl  
 * 创建时间：2016年9月22日 下午5:18:29   
 * 修改人：zhangl   
 * 修改时间：2016年9月22日 下午5:18:29   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.StarInfoMapper;
import com.lst.model.StarInfo;
import com.lst.service.StarInfoService;

/**
 * @ClassName: StarInfoServiceImpl
 * @Description: 达人实现类
 * @author zhangl
 * @date 2016年9月22日 下午5:18:29
 *
 */
@Service("starInfoService")
public class StarInfoServiceImpl implements StarInfoService{

	@Autowired
	private StarInfoMapper dao;

	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(StarInfo record) {
		return dao.insert(record);
	}

	public int insertSelective(StarInfo record) {
		return dao.insertSelective(record);
	}

	public StarInfo selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(StarInfo record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(StarInfo record) {
		return dao.updateByPrimaryKey(record);
	}

	/**
	 * 
	 * @Title: queryByList
	 * @Description:查询数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<StarInfo>    返回类型
	 */
	public List<StarInfo> queryByList(Map<String, Object> map , PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

	public void delList(Map<String, Object> map){
		dao.delList(map);
	}
}
