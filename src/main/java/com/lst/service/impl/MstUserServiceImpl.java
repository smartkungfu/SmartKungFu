/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstUserServiceImpl   
 * 类描述：   
 * 创建人：zhangl  
 * 创建时间：2016年9月24日 下午5:06:52   
 * 修改人：zhangl   
 * 修改时间：2016年9月24日 下午5:06:52   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.common.CommonEnum;
import com.lst.dao.MstUserMapper;
import com.lst.dao.UserRoleMapper;
import com.lst.model.MstUser;
import com.lst.model.UserRole;
import com.lst.service.MstUserService;
import com.lst.utils.DateUtil;

/**
 * @ClassName: MstUserServiceImpl
 * @Description: 
 * @author zhangl
 * @date 2016年9月24日 下午5:06:52
 *
 */

@Service("mstUserService")
public class MstUserServiceImpl implements MstUserService {

	@Autowired
	private MstUserMapper dao;

	@Autowired
	private UserRoleMapper urDao;

	private Logger logger = Logger.getLogger("log");

	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(MstUser record) {
		return dao.insert(record);
	}

	public int insertSelective(MstUser record) {
		return dao.insertSelective(record);
	}

	public MstUser selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(MstUser record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(MstUser record) {
		return dao.updateByPrimaryKey(record);
	}

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 获取数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstUser>    返回类型
	 */
	public List<MstUser> queryByList(Map<String, Object> map ,PageBounds pageBounds){
		return dao.queryByList(map, pageBounds);
	}

	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	public void delList(Map<String, Object> map){
		dao.delList(map);
	}

	/**
	 * 
	 * @Title: saveInfo
	 * @Description: 用户信息表单提交
	 * @param @param mstUser
	 * @param @param sysId
	 * @param @param rolids    设定文件
	 * @return void    返回类型
	 */
	@Transactional(propagation = Propagation.REQUIRED , rollbackFor = Exception.class)
	public void saveInfo(MstUser mstUser,Integer sysId,Integer [] rolids){
		Date startDate = new Date();
		logger.info("saveInfo begin: " + startDate);

		try {
			if(mstUser.getId() == null){

				dao.insert(mstUser);

				List<UserRole> newUrs = new ArrayList<UserRole>();
				for(Integer rolid : rolids){
					UserRole tempUr = new UserRole();
					tempUr.setCreatedate(startDate);
					tempUr.setCreateuser(sysId);
					tempUr.setEnabled(true);
					tempUr.setIsdeleted(false);
					tempUr.setRolid(rolid);
					tempUr.setUserid(mstUser.getId());

					newUrs.add(tempUr);
				}
				
				if(!newUrs.isEmpty()){
					urDao.saveList(newUrs);
				}
				
			} else {

				dao.updateByPrimaryKeySelective(mstUser);

				Map<String, Object> rolMap = new HashMap<String,Object>();
				rolMap.put("enabled", 1);
				rolMap.put("isDeleted", 0);
				rolMap.put("orderByClause", "r.id asc");
				rolMap.put("rolType", CommonEnum.SYSCODE_USERTYPE_WEB);
				rolMap.put("userId", mstUser.getId());
				
				boolean flag = false;//标记用户角色是否更新

				List<UserRole> urs = urDao.queryByList(rolMap, new PageBounds());

				for(Integer rolid : rolids){
					for(UserRole ur : urs){
						if(!rolid.equals(ur.getRolid())){
							flag = true;//存在用户角色被更新
							break;
						}
					}
				}
				
				if(flag){
					List<UserRole> newUrs = new ArrayList<UserRole>();
					for(Integer rolid : rolids){
						UserRole tempUr = new UserRole();
						tempUr.setCreatedate(startDate);
						tempUr.setCreateuser(sysId);
						tempUr.setEnabled(true);
						tempUr.setIsdeleted(false);
						tempUr.setRolid(rolid);
						tempUr.setUserid(mstUser.getId());

						newUrs.add(tempUr);
					}
					
					if(!newUrs.isEmpty()){
						
						Map<String, Object> delMap = new HashMap<>();
						delMap.put("userId", mstUser.getId());
						delMap.put("updateUser", sysId);
						delMap.put("userId", mstUser.getId());
						
						urDao.delList(delMap);
						
						urDao.saveList(newUrs);
					}
				}

				
			}
		} catch (Exception e) {
			logger.error("saveInfo error:", e);
		}

		logger.info("saveInfo end run(s): " + DateUtil.calLastedTime(startDate));
	}
}
