package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.AnswerMapper;
import com.lst.model.Answer;
import com.lst.service.AnswerService;


/**
 * @ClassName: AnswerServiceImpl
 * @Description: 回答添加实现类层
 * 
 */
@Service("AnswerService")
public class AnswerServiceImpl implements AnswerService {
	
	@Autowired
	private AnswerMapper dao;

	@Override
	public List<Answer> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public void delList(Map<String, Object> map) {
		
		dao.delList(map);
	}

	@Override
	public Answer selectByPrimaryKey(Integer id) {
		
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int countByMap(Map<String, Object> map) {
		
		return dao.countByMap(map);
	}


}
