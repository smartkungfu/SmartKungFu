package com.lst.service.impl;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ActionMusicMapper;
import com.lst.model.ActionMusic;
import com.lst.service.ActionMusicService;

/**
 * @ClassName: ActionMusicServiceImpl
 * @Description: 课程动作实体类
 *
 */
@Service("actionMusicService")
public class ActionMusicServiceImpl implements ActionMusicService{

	@Autowired
	private ActionMusicMapper dao;

	@Override
	public int insert(ActionMusic record) {
		
		return dao.insert(record);
	}

	@Override
	public List<ActionMusic> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public ActionMusic selectByPrimaryKey(Integer id) {
		
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByKey(ActionMusic record) {
		
		return dao.updateByKey(record);
	}

	@Override
	public void delList(Map<String, Object> map) {
		
		dao.delList(map);
	}

	

}
