/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：ResourcesServiceImpl   
 * 类描述： 资源实现类 
 * 创建人：zhangl  
 * 创建时间：2016年10月18日 下午2:32:29   
 * 修改人：zhangl   
 * 修改时间：2016年10月18日 下午2:32:29   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lst.dao.ResourcesMapper;
import com.lst.model.Resources;
import com.lst.service.ResourcesService;

/**
 * @ClassName: ResourcesServiceImpl
 * @Description:资源实现类
 * @author zhangl
 * @date 2016年10月18日 下午2:32:29
 *
 */
@Service("resourcesService")
public class ResourcesServiceImpl implements ResourcesService {
	
	@Autowired
	private ResourcesMapper dao;
	
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(Resources record) {
		return dao.insert(record);
	}

	public int insertSelective(Resources record) {
		return dao.insertSelective(record);
	}

	public Resources selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(Resources record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(Resources record) {
		return dao.updateByPrimaryKey(record);
	}

	/**
	 * 
	 * @Title: saveList
	 * @Description: 批量插入
	 * @param @param list    设定文件
	 * @return void    返回类型
	 */
	@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
	public void saveList(List<Resources> list) {
		dao.saveList(list);
	}
}
