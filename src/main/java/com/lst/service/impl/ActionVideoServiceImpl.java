package com.lst.service.impl;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ActionVideoMapper;
import com.lst.model.ActionVideo;
import com.lst.service.ActionVideoService;

/**
 * @ClassName: ActionVideoServiceImpl
 * @Description: 课程动作实体类
 *
 */
@Service("actionVideoService")
public class ActionVideoServiceImpl implements ActionVideoService{

	@Autowired
	private ActionVideoMapper dao;

	@Override
	public int insert(ActionVideo record) {
		
		return dao.insert(record);
	}

	@Override
	public List<ActionVideo> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public ActionVideo selectByPrimaryKey(Integer id) {
		
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByKey(ActionVideo record) {
		
		return dao.updateByKey(record);
	}

	@Override
	public void delList(Map<String, Object> map) {
		dao.delList(map);
		
	}

}
