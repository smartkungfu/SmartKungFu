/**   
*    
* 项目名称：SmartKungFu   
* 类名称：ClientGrowthServiceImpl   
* 类描述：   
* 创建人：Wdd   
* 创建时间：2016年10月26日 下午5:52:02   
* 修改人：Wdd  
* 修改时间：2016年10月26日 下午5:52:02   
* 修改备注：   
* @version    
*    
*/
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ClientGrowthMapper;
import com.lst.model.ClientGrowth;

/**
 * @ClassName: ClientGrowthServiceImpl
 * @Description: 用户成就
 * @author Wdd
 * @date 2016年10月26日 下午5:52:02
 *
 */
@Service("clientGrowthServiceImpl")
public class ClientGrowthServiceImpl implements com.lst.service.ClientGrowthService{
	
	@Autowired
	private ClientGrowthMapper dao;
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(ClientGrowth record) {
		return dao.insert(record);
	}

	@Override
	public int insertSelective(ClientGrowth record) {
		return dao.insertSelective(record);
	}

	@Override
	public ClientGrowth selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(ClientGrowth record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(ClientGrowth record) {
		return dao.updateByPrimaryKey(record);
	}

	@Override
	public List<ClientGrowth> queryByList(Map<String, Object> map,PageBounds pageBounds){
		return dao.queryByList(map, pageBounds);
	}
}
