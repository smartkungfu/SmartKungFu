package com.lst.service.impl;



import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ReportMapper;
import com.lst.model.Report;
import com.lst.service.ReportService;


/**
 * @ClassName: ReportServiceImpl
 * @Description: 举报实现类层
 * 
 */
@Service("ReportService")
public class ReportServiceImpl implements ReportService {
	
	@Autowired
	private ReportMapper dao;

	@Override
	public List<Report> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList(map, pageBounds);
	}


	@Override
	public Report selectByKey(Integer id) {
		
		return dao.selectByKey(id);
	}


	@Override
	public void updateReport(Report report) {
		
		dao.updateReport(report);
	}

	

}
