package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.service.MstCodeService;
import com.lst.dao.MstCodeMapper;
import com.lst.model.MstCode;

/**
 * MstCodeService
 **/
@Service("mstCodeService")
public class MstCodeServiceImpl implements MstCodeService {
	
	@Autowired
	private MstCodeMapper dao;

	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(MstCode record) {
		return dao.insert(record);
	}

    public int insertSelective(MstCode record) {
		return dao.insertSelective(record);
	}

    public MstCode selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

    public int updateByPrimaryKeySelective(MstCode record) {
		return dao.updateByPrimaryKeySelective(record);
	}

    public int updateByPrimaryKey(MstCode record) {
		return dao.updateByPrimaryKey(record);
	}
    
    /**
	 * 
	 * @Title: queryByList
	 * @Description: 获取结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstCode>    返回类型
	 */
	public List<MstCode> queryByList(Map<String, Object> map,PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);			
	}

	/**
	 * 
	 * @Title: queryListByParentId
	 * @Description: 通过PARENTID 获取结果集
	 * @param @param parentId
	 * @param @return    设定文件
	 * @return List<MstCode>    返回类型
	 */
	public List<MstCode> queryListByParentId(Integer parentId) {
		return dao.queryListByParentId(parentId);
	}

	/**
	 * 
	 * @Title: queryListByIds
	 * @Description:通过ID集获取结果集
	 * @param @param ids
	 * @param @return    设定文件
	 * @return List<MstCode>    返回类型
	 */
	public List<MstCode>queryListByIds(@Param(value = "ids") String ids) {
		return dao.queryListByIds(ids);
	}

	/***
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	public void delList(Map<String, Object> map) {
		dao.delList(map);
	}
	
	/**
	 * 
	 * @Title: queryListByType
	 * @Description:通过类型获取结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstCode>    返回类型
	 */
	public List<MstCode> queryListByType(Map<String, Object> map,PageBounds pageBounds){
		return dao.queryListByType(map, pageBounds);
	}
	
	/**
	 * 
	 * @Title: saveWithList
	 * @Description:批量更新操作
	 * @param @param map
	 * @param @param list    设定文件
	 * @return void    返回类型
	 */
	@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class) 
	public void saveWithList(Map<String, Object> map,List<MstCode> list){
		dao.delListByParentId(map);
		
		if(!list.isEmpty()){
			dao.saveList(list);
		}
	}

}
