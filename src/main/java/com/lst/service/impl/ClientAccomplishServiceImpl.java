/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：ClientAccomplishServiceImpl   
 * 类描述：   用户等级实现类层
 * 创建人：zhangl  
 * 创建时间：2016年12月24日 下午1:17:40   
 * 修改人：zhangl   
 * 修改时间：2016年12月24日 下午1:17:40   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.google.gson.annotations.Expose;
import com.lst.dao.ClientAccomplishMapper;
import com.lst.model.ClientAccomplish;
import com.lst.service.ClientAccomplishService;

/**
 * @ClassName: ClientAccomplishServiceImpl
 * @Description: 用户等级实现类层
 * @author zhangl
 * @date 2016年12月24日 下午1:17:40
 *
 */
@Service("clientAccomplishService")
public class ClientAccomplishServiceImpl implements ClientAccomplishService {

	@Expose
	private ClientAccomplishMapper dao;
	
	/* (非 Javadoc)
	 * <p>Title: deleteByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param id
	 * @return
	 * @see com.lst.service.ClientAccomplishService#deleteByPrimaryKey(java.lang.Integer)
	 */
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	/* (非 Javadoc)
	 * <p>Title: insert</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.ClientAccomplishService#insert(com.lst.model.ClientAccomplish)
	 */
	@Override
	public int insert(ClientAccomplish record) {
		return dao.insert(record);
	}

	/* (非 Javadoc)
	 * <p>Title: insertSelective</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.ClientAccomplishService#insertSelective(com.lst.model.ClientAccomplish)
	 */
	@Override
	public int insertSelective(ClientAccomplish record) {
		return dao.insertSelective(record);
	}

	/* (非 Javadoc)
	 * <p>Title: selectByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param id
	 * @return
	 * @see com.lst.service.ClientAccomplishService#selectByPrimaryKey(java.lang.Integer)
	 */
	@Override
	public ClientAccomplish selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	/* (非 Javadoc)
	 * <p>Title: updateByPrimaryKeySelective</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.ClientAccomplishService#updateByPrimaryKeySelective(com.lst.model.ClientAccomplish)
	 */
	@Override
	public int updateByPrimaryKeySelective(ClientAccomplish record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	/* (非 Javadoc)
	 * <p>Title: updateByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.ClientAccomplishService#updateByPrimaryKey(com.lst.model.ClientAccomplish)
	 */
	@Override
	public int updateByPrimaryKey(ClientAccomplish record) {
		return dao.updateByPrimaryKey(record);
	}

	/* (非 Javadoc)
	 * <p>Title: queryByList</p>
	 * <p>Description: </p>
	 * @param map
	 * @param pageBounds
	 * @return
	 * @see com.lst.service.ClientAccomplishService#queryByList(java.util.Map, com.github.miemiedev.mybatis.paginator.domain.PageBounds)
	 */
	@Override
	public List<ClientAccomplish> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

}
