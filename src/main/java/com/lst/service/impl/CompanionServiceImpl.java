package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.CompanionMapper;
import com.lst.model.Companion;
import com.lst.service.CompanionService;


/**
 * @ClassName: CompanionServiceImpl
 * @Description: 结伴活动添加实现类层
 * 
 */
@Service("CompanionService")
public class CompanionServiceImpl implements CompanionService {
	
	@Autowired
	private CompanionMapper dao;

	@Override
	public List<Companion> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public void delList(Map<String, Object> map) {
		dao.delList(map);
	}

	@Override
	public Companion selectByPrimaryKey(Integer id) {
	
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int countByMap(Map<String, Object> map) {
		
		return dao.countByMap(map);
	}

}
