/**   
 *    
 * 项目名称：SmartKungFuService   
 * 类名称：ClientNumServiceImpl   
 * 类描述：   
 * 创建人：zhangl  
 * 创建时间：2016年9月29日 下午4:20:32   
 * 修改人：zhangl   
 * 修改时间：2016年9月29日 下午4:20:32   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ClientNumMapper;
import com.lst.model.ClientNum;
import com.lst.service.ClientNumService;

/**
 * @ClassName: ClientNumServiceImpl
 * @Description: 用户操作量实现类层
 * @author zhangl
 * @date 2016年9月29日 下午4:20:32
 *
 */
@Service("clientNumService")
public class ClientNumServiceImpl implements ClientNumService {

	@Autowired
	private ClientNumMapper dao;

	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(ClientNum record) {
		return dao.insert(record);
	}

	public int insertSelective(ClientNum record) {
		return dao.insertSelective(record);
	}

	public ClientNum selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(ClientNum record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(ClientNum record) {
		return dao.updateByPrimaryKey(record);
	}

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 获取统计量
	 * @param @param pageBounds
	 * @param @param clientNum
	 * @param @return    设定文件
	 * @return List<ClientNum>    返回类型
	 */
	public List<ClientNum> queryByList(Map<String, Object> map,PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}
	

	/**
	 * 
	 * @Title: queryCountByMap
	 * @Description: 统计用户操作量
	 * @param @param map
	 * @param @return    设定文件
	 * @return Integer    返回类型
	 */
	public Long queryCountByMap(Map<String, Object> map){
		Long count = dao.queryCountByMap(map);
		if (count == null ) count =(long) 0;
		return count;
	}
}
