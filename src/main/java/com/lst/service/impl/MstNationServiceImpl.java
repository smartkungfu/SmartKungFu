/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstNationServiceImpl   
 * 类描述：  地区实现类 
 * 创建人：zhangl  
 * 创建时间：2016年10月19日 下午6:24:19   
 * 修改人：zhangl   
 * 修改时间：2016年10月19日 下午6:24:19   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import org.springframework.stereotype.Service;

import com.lst.model.MstNation;
import com.lst.service.MstNationService;

/**
 * @ClassName: MstNationServiceImpl
 * @Description: 地区实现类
 * @author zhangl
 * @date 2016年10月19日 下午6:24:19
 *
 */
@Service("mstNationService")
public class MstNationServiceImpl implements MstNationService {

	/* (非 Javadoc)
	 * <p>Title: deleteByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param id
	 * @return
	 * @see com.lst.service.MstNationService#deleteByPrimaryKey(java.lang.Integer)
	 */
	@Override
	public int deleteByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (非 Javadoc)
	 * <p>Title: insert</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.MstNationService#insert(com.lst.model.MstNation)
	 */
	@Override
	public int insert(MstNation record) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (非 Javadoc)
	 * <p>Title: insertSelective</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.MstNationService#insertSelective(com.lst.model.MstNation)
	 */
	@Override
	public int insertSelective(MstNation record) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (非 Javadoc)
	 * <p>Title: selectByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param id
	 * @return
	 * @see com.lst.service.MstNationService#selectByPrimaryKey(java.lang.Integer)
	 */
	@Override
	public MstNation selectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (非 Javadoc)
	 * <p>Title: updateByPrimaryKeySelective</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.MstNationService#updateByPrimaryKeySelective(com.lst.model.MstNation)
	 */
	@Override
	public int updateByPrimaryKeySelective(MstNation record) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (非 Javadoc)
	 * <p>Title: updateByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.MstNationService#updateByPrimaryKey(com.lst.model.MstNation)
	 */
	@Override
	public int updateByPrimaryKey(MstNation record) {
		// TODO Auto-generated method stub
		return 0;
	}

}
