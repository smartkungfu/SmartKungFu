/**   
*    
* 项目名称：SmartKungFu   
* 类名称：ClientOnlineCourseServiceImpl   
* 类描述：   
* 创建人：Wdd   
* 创建时间：2017年1月13日 下午9:38:46   
* 修改人：Wdd  
* 修改时间：2017年1月13日 下午9:38:46   
* 修改备注：   
* @version    
*    
*/
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ClientOnlineCourseMapper;
import com.lst.model.ClientOnlineCourse;
import com.lst.service.ClientOnlineCourseService;

/**
 * @ClassName: ClientOnlineCourseServiceImpl
 * @Description: 用户在线课程
 * @author Wdd
 * @date 2017年1月13日 下午9:38:46
 *
 */
@Service("ClientOnlineCourseService")
public class ClientOnlineCourseServiceImpl implements ClientOnlineCourseService {
	
	@Resource
	private ClientOnlineCourseMapper dao;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(ClientOnlineCourse record) {
		return dao.insert(record);
	}

	@Override
	public int insertSelective(ClientOnlineCourse record) {
		return dao.insertSelective(record);
	}

	@Override
	public ClientOnlineCourse selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(ClientOnlineCourse record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(ClientOnlineCourse record) {
		return dao.updateByPrimaryKey(record);
	}

	@Override
	public List<ClientOnlineCourse> queryByList(Map<String, Object> map, PageBounds pageBouns) {
		return dao.queryByList(map, pageBouns);
	}

}
