/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：VideoNoteServiceImpl   
 * 类描述：   视频节点实现类
 * 创建人：zhangl  
 * 创建时间：2017年1月11日 下午2:05:21   
 * 修改人：zhangl   
 * 修改时间：2017年1月11日 下午2:05:21   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.VideoNoteMapper;
import com.lst.model.VideoNote;
import com.lst.service.VideoNoteService;

/**
 * @ClassName: VideoNoteServiceImpl
 * @Description:视频节点实现类
 * @author zhangl
 * @date 2017年1月11日 下午2:05:21
 *
 */
@Service("videoNoteService")
public class VideoNoteServiceImpl implements VideoNoteService {

	@Autowired
	private VideoNoteMapper dao;

	/* (非 Javadoc)
	 * <p>Title: deleteByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param id
	 * @return
	 * @see com.lst.service.VideoNoteService#deleteByPrimaryKey(java.lang.Integer)
	 */
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	/* (非 Javadoc)
	 * <p>Title: insert</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.VideoNoteService#insert(com.lst.model.VideoNote)
	 */
	@Override
	public int insert(VideoNote record) {
		return dao.insert(record);
	}

	/* (非 Javadoc)
	 * <p>Title: insertSelective</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.VideoNoteService#insertSelective(com.lst.model.VideoNote)
	 */
	@Override
	public int insertSelective(VideoNote record) {
		return dao.insertSelective(record);
	}

	/* (非 Javadoc)
	 * <p>Title: selectByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param id
	 * @return
	 * @see com.lst.service.VideoNoteService#selectByPrimaryKey(java.lang.Integer)
	 */
	@Override
	public VideoNote selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	/* (非 Javadoc)
	 * <p>Title: updateByPrimaryKeySelective</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.VideoNoteService#updateByPrimaryKeySelective(com.lst.model.VideoNote)
	 */
	@Override
	public int updateByPrimaryKeySelective(VideoNote record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	/* (非 Javadoc)
	 * <p>Title: updateByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.VideoNoteService#updateByPrimaryKey(com.lst.model.VideoNote)
	 */
	@Override
	public int updateByPrimaryKey(VideoNote record) {
		return dao.updateByPrimaryKey(record);
	}

	/* (非 Javadoc)
	 * <p>Title: queryByList</p>
	 * <p>Description: </p>
	 * @param map
	 * @param pageBounds
	 * @return
	 * @see com.lst.service.VideoNoteService#queryByList(java.util.Map, com.github.miemiedev.mybatis.paginator.domain.PageBounds)
	 */
	@Override
	public List<VideoNote> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

	/* (非 Javadoc)
	 * <p>Title: delList</p>
	 * <p>Description: </p>
	 * @param map
	 * @see com.lst.service.VideoNoteService#delList(java.util.Map)
	 */
	@Override
	public void delList(Map<String, Object> map) {
		dao.delList(map);
	}
}
