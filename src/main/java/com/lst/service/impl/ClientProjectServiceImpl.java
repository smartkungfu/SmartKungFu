/**   
 *    
 * 项目名称：SmartKungFuService   
 * 类名称：ClientProjectServiceImpl   
 * 类描述：    用户活动接口实现类层
 * 创建人：zhangl  
 * 创建时间：2016年9月28日 下午12:28:04   
 * 修改人：zhangl   
 * 修改时间：2016年9月28日 下午12:28:04   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ClientProjectMapper;
import com.lst.model.ClientProject;
import com.lst.service.ClientProjectService;

/**
 * @ClassName: ClientProjectServiceImpl
 * @Description: 用户活动接口实现类层
 * @author zhangl
 * @date 2016年9月28日 下午12:28:04
 *
 */
@Service("clientProjectService")
public class ClientProjectServiceImpl implements ClientProjectService {
	
	@Autowired
	private ClientProjectMapper dao;
	
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(ClientProject record) {
		return dao.insert(record);
	}

	public int insertSelective(ClientProject record) {
		return dao.insertSelective(record);
	}

	public ClientProject selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(ClientProject record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(ClientProject record) {
		return dao.updateByPrimaryKey(record);
	}

	/**
	 * 
	 * @Title: queryByList
	 * @Description:查询结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<ClientProject>    返回类型
	 */
	public List<ClientProject> queryByList(Map<String, Object> map,PageBounds pageBounds){
		return dao.queryByList(map, pageBounds);
	}
}
