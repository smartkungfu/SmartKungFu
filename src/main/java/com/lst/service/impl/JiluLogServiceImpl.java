package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.JiluLogMapper;
import com.lst.model.JiluLog;
import com.lst.service.JiluLogService;


/**
 * @ClassName: JiluLogServiceImpl
 * @Description: 喜欢实现类层
 * 
 */
@Service("JiluLogService")
public class JiluLogServiceImpl implements JiluLogService {
	
	@Autowired
	private JiluLogMapper dao;


	@Override
	public List<JiluLog> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList(map, pageBounds);
	}


	@Override
	public void updateByKey(Map<String, Object> map) {
		dao.updateByKey(map);
		
	}




}
