/**   
*    
* 项目名称：SmartKungFu   
* 类名称：ClientCourseServiceImpl   
* 类描述：   
* 创建人：Wdd   
* 创建时间：2016年11月2日 下午5:19:45   
* 修改人：Wdd  
* 修改时间：2016年11月2日 下午5:19:45   
* 修改备注：   
* @version    
*    
*/
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ClientCourseMapper;
import com.lst.model.ClientCourse;
import com.lst.service.ClientCourseService;

/**
 * @ClassName: ClientCourseServiceImpl
 * @Description: 用户课程
 * @author Wdd
 * @date 2016年11月2日 下午5:19:45
 *
 */
@Service("clientCourseServiceImpl")
public class ClientCourseServiceImpl implements ClientCourseService {

	@Autowired
	private ClientCourseMapper dao;
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(ClientCourse record) {
		return dao.insert(record);
	}

	@Override
	public int insertSelective(ClientCourse record) {
		return dao.insertSelective(record);
	}

	@Override
	public ClientCourse selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(ClientCourse record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(ClientCourse record) {
		return dao.updateByPrimaryKey(record);
	}

	@Override
	public List<ClientCourse> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

}
