/**   
 *    
 * 项目名称：SmartKungFuService   
 * 类名称：MstGymnasiumServiceImpl   
 * 类描述：    场馆实现类
 * 创建人：zhangl  
 * 创建时间：2016年9月22日 下午4:58:06   
 * 修改人：zhangl   
 * 修改时间：2016年9月22日 下午4:58:06   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.MstGymnasiumMapper;
import com.lst.model.MstGymnasium;
import com.lst.service.MstGymnasiumService;

/**
 * @ClassName: MstGymnasiumServiceImpl
 * @Description: 场馆实现类
 * @author zhangl
 * @date 2016年9月22日 下午4:58:06
 *
 */
@Service("mstGymnasiumService")
public class MstGymnasiumServiceImpl implements MstGymnasiumService {
	
	@Autowired
	private MstGymnasiumMapper dao;
	
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}
	public int insert(MstGymnasium record) {
		return dao.insert(record);
	}

	public int insertSelective(MstGymnasium record) {
		return dao.insertSelective(record);
	}

	public MstGymnasium selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(MstGymnasium record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(MstGymnasium record) {
		return dao.updateByPrimaryKey(record);
	}

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 查询数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstGymnasium>    返回类型
	 */
	public List<MstGymnasium> queryByList(Map<String, Object> map , PageBounds pageBounds){
		return dao.queryByList(map, pageBounds);
	}
	
	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map
	 * @return void    返回类型
	 */
	@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
	public void delList(Map<String, Object> map){
		dao.delList(map);
	}

}
