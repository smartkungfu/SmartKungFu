/**   
 *    
 * 项目名称：SmartKungFuService   
 * 类名称：MstClientServiceImpl   
 * 类描述：   用户接口实现类
 * 创建人：zhangl  
 * 创建时间：2016年9月22日 上午10:46:09   
 * 修改人：zhangl   
 * 修改时间：2016年9月22日 上午10:46:09   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.common.CommonEnum;
import com.lst.dao.ClientAccomplishMapper;
import com.lst.dao.ClientGrowthMapper;
import com.lst.dao.MstClientMapper;
import com.lst.dao.MstCodeMapper;
import com.lst.model.ClientAccomplish;
import com.lst.model.ClientGrowth;
import com.lst.model.MstClient;
import com.lst.model.MstCode;
import com.lst.model.MstUser;
import com.lst.service.MstClientService;
import com.lst.utils.DateUtil;

/**
 * @ClassName: MstClientServiceImpl
 * @Description: 用户接口实现类
 * @author zhangl
 * @date 2016年9月22日 上午10:46:09
 *
 */
@Service("mstClientService")
public class MstClientServiceImpl implements MstClientService {

	@Autowired
	private MstClientMapper dao;
	
	@Autowired
	private ClientGrowthMapper clientGrowthMapper;
	
	@Autowired
	private ClientAccomplishMapper clientAccomplishMapper;
	
	@Autowired
	private MstCodeMapper mstCodeMapper;
	
	private Logger logger = Logger.getLogger("log");

	public int deleteByPrimaryKey(Integer id){
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(MstClient record){
		return dao.insert(record);
	}

	public int insertSelective(MstClient record){
		return dao.insertSelective(record);
	}

	public MstClient selectByPrimaryKey(Integer id){
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(MstClient record){
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(MstClient record){
		return dao.updateByPrimaryKey(record);
	}

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 查询数据集
	 * @param @param map
	 * @param @param pageBouonds
	 * @param @return    设定文件
	 * @return List<MstClient>    返回类型
	 */
	public List<MstClient> queryByList(Map<String, Object> map,PageBounds pageBouonds){
		return dao.queryByList(map, pageBouonds);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , rollbackFor = Exception.class)
	public void saveInfo(MstClient mstClient,MstUser sysUser){
		Date startDate = new Date();
		logger.info("saveInfo begin: " + startDate);

		try {
			if(mstClient.getId() == null){
				mstClient.setCreatedate(startDate);
				mstClient.setCreateuser(sysUser.getId());
				mstClient.setEnabled(true);
				mstClient.setIsdeleted(false);
				
				dao.insert(mstClient);
				
				// 添加功力值
			    ClientGrowth cg = new ClientGrowth();

			    cg.setClientid(mstClient.getId());
			    cg.setGrowthtype(CommonEnum.SYSTEM_GROWTH_RANGE_REGISTER);
			    MstCode code = mstCodeMapper.selectByPrimaryKey(CommonEnum.SYSTEM_GROWTH_RANGE_REGISTER);

			    cg.setGrowthvalue(Integer.parseInt(code.getCode()));
			    cg.setEnabled(true);
			    cg.setIsdeleted(false);
			    cg.setCreatedate(new Date());
			    cg.setCreateuser(mstClient.getId());

			    clientGrowthMapper.insert(cg);

			    // 插入用户成就表
			    ClientAccomplish ca = new ClientAccomplish();

			    ca.setClientid(mstClient.getId());
			    ca.setGrade(1);
			    ca.setGrowthvalue(new BigDecimal(code.getCode()));
			    ca.setEnabled(true);
			    ca.setIsdeleted(false);
			    ca.setCreatedate(new Date());
			    ca.setCreateuser(mstClient.getId());

			    clientAccomplishMapper.insert(ca);
			} else {
				mstClient.setUpdatedate(startDate);
				mstClient.setUpdateuser(sysUser.getId());

				dao.updateByPrimaryKeySelective(mstClient);
			}
		} catch (Exception e) {
			logger.error("saveInfo error:", e);
		}

		logger.info("saveInfo end run(s): " + DateUtil.calLastedTime(startDate));
	}

	@Override
	public void delList(Map<String, Object> map) {
		dao.delList(map);
	}

	@Override
	public int countByMap(Map<String, Object> map) {
		
		return dao.countByMap(map);
	}
}
