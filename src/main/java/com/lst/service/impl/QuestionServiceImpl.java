package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.QuestionMapper;
import com.lst.model.Question;
import com.lst.service.QuestionService;


/**
 * @ClassName: QuestionServiceImpl
 * @Description: 问题添加实现类层
 * 
 */
@Service("QuestionService")
public class QuestionServiceImpl implements QuestionService {
	
	@Autowired
	private QuestionMapper dao;

	@Override
	public List<Question> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public void delList(Map<String, Object> map) {
		
		dao.delList(map);
	}

	@Override
	public Question selectByPrimaryKey(Integer id) {
		
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int countByMap(Map<String, Object> map) {
		
		return dao.countByMap(map);
	}

	

}
