/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstButtonServiceImpl   
 * 类描述：按钮实现类层   
 * 创建人：zhangl  
 * 创建时间：2016年10月11日 下午6:01:25   
 * 修改人：zhangl   
 * 修改时间：2016年10月11日 下午6:01:25   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lst.dao.MstButtonMapper;
import com.lst.model.MstButton;
import com.lst.service.MstButtonService;

/**
 * @ClassName: MstButtonServiceImpl
 * @Description:按钮实现类层
 * @author zhangl
 * @date 2016年10月11日 下午6:01:25
 *
 */
@Service("mstButtonService")
public class MstButtonServiceImpl implements MstButtonService {
	
	@Autowired
	private MstButtonMapper dao;
	
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(MstButton record) {
		return dao.insert(record);
	}

	public int insertSelective(MstButton record) {
		return dao.insertSelective(record);
	}

	public MstButton selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(MstButton record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(MstButton record) {
		return dao.updateByPrimaryKey(record);
	}

	/**
	 * 
	 * @Title: countByMenuid
	 * @Description: 查询菜单下的按钮数量 
	 * @param @param menuId
	 * @param @return    设定文件
	 * @return Integer    返回类型
	 */
	public Integer countByMenuid(Integer menuId) {
		return dao.countByMenuid(menuId);
	}

	/**
	 * 
	 * @Title: queryListByMenuId
	 * @Description:通过菜单id查询按钮
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<MstButton>    返回类型
	 */
	public List<MstButton> queryListByMenuId(Map<String, Object> map) {
		return dao.queryListByMenuId(map);
	}

	/**
	 * 
	 * @Title: queryListForUserRoots
	 * @Description: 查询用户独立权限按钮和用户角色权限按钮
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<MstButton>    返回类型
	 */
	public List<MstButton> queryListForUserRoots(Map<String, Object> map) {
		return dao.queryListForUserRoots(map);
	}
}
