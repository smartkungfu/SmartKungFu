package com.lst.service.impl;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.SportRecordsMapper;
import com.lst.model.SportRecords;
import com.lst.service.SportRecordsService;


@Service("sportRecordsService")
public class SportRecordsServiceImpl implements SportRecordsService{

	@Autowired
	private SportRecordsMapper dao;

	

	@Override
	public List<SportRecords> queryByMap(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByMap(map, pageBounds);
	}



}
