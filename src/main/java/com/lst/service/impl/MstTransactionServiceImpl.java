/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstTransactionServiceImpl   
 * 类描述：   交易实现类
 * 创建人：zhangl  
 * 创建时间：2016年12月28日 下午12:26:32   
 * 修改人：zhangl   
 * 修改时间：2016年12月28日 下午12:26:32   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.MstTransactionMapper;
import com.lst.model.MstTransaction;
import com.lst.service.MstTransactionService;

/**
 * @ClassName: MstTransactionServiceImpl
 * @Description:交易实现类
 * @author zhangl
 * @date 2016年12月28日 下午12:26:32
 *
 */
@Service("mstTransactionService")
public class MstTransactionServiceImpl implements MstTransactionService {
	
	@Autowired
	private MstTransactionMapper dao;
	
	/* (非 Javadoc)
	 * <p>Title: deleteByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param id
	 * @return
	 * @see com.lst.service.MstTransactionService#deleteByPrimaryKey(java.lang.Integer)
	 */
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	/* (非 Javadoc)
	 * <p>Title: insert</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.MstTransactionService#insert(com.lst.model.MstTransaction)
	 */
	@Override
	public int insert(MstTransaction record) {
		return dao.insert(record);
	}

	/* (非 Javadoc)
	 * <p>Title: insertSelective</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.MstTransactionService#insertSelective(com.lst.model.MstTransaction)
	 */
	@Override
	public int insertSelective(MstTransaction record) {
		return dao.insertSelective(record);
	}

	/* (非 Javadoc)
	 * <p>Title: selectByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param id
	 * @return
	 * @see com.lst.service.MstTransactionService#selectByPrimaryKey(java.lang.Integer)
	 */
	@Override
	public MstTransaction selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	/* (非 Javadoc)
	 * <p>Title: updateByPrimaryKeySelective</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.MstTransactionService#updateByPrimaryKeySelective(com.lst.model.MstTransaction)
	 */
	@Override
	public int updateByPrimaryKeySelective(MstTransaction record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	/* (非 Javadoc)
	 * <p>Title: updateByPrimaryKey</p>
	 * <p>Description: </p>
	 * @param record
	 * @return
	 * @see com.lst.service.MstTransactionService#updateByPrimaryKey(com.lst.model.MstTransaction)
	 */
	@Override
	public int updateByPrimaryKey(MstTransaction record) {
		return dao.updateByPrimaryKey(record);
	}

	/* (非 Javadoc)
	 * <p>Title: queryByList</p>
	 * <p>Description: </p>
	 * @param map
	 * @param pageBounds
	 * @return
	 * @see com.lst.service.MstTransactionService#queryByList(java.util.Map, com.github.miemiedev.mybatis.paginator.domain.PageBounds)
	 */
	@Override
	public List<MstTransaction> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

}
