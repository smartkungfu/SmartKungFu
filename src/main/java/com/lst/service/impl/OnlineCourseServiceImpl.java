package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.OnlineCourseMapper;
import com.lst.model.OnlineCourse;
import com.lst.service.OnlineCourseService;

@Service("onlineCourseService")
public class OnlineCourseServiceImpl implements OnlineCourseService {

	@Autowired
	private OnlineCourseMapper dao;

	@Override
	public void deleteByPrimaryKey(Integer id) {
		dao.deleteByPrimaryKey(id);
	}

	@Override
	public void insert(OnlineCourse onlineCourse) {
		dao.insert(onlineCourse);
	}

	@Override
	public void insertSelective(OnlineCourse onlineCourse) {
		dao.insertSelective(onlineCourse);
	}

	@Override
	public OnlineCourse selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public void updateByPrimaryKeySelective(OnlineCourse onlineCourse) {
		dao.updateByPrimaryKeySelective(onlineCourse);
	}

	@Override
	public void updateByPrimaryKey(OnlineCourse onlineCourse) {
		dao.updateByPrimaryKey(onlineCourse);
	}

	@Override
	public List<OnlineCourse> queryByList(Map<String, Object> map,
			PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public void delList(Map<String, Object> map) {
		dao.delList(map);
	}

	@Override
	public OnlineCourse selectByPrimaryKey2(Integer id) {
		
		return dao.selectByPrimaryKey2(id);
	}

}
