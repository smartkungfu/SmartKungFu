/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：ProjectDetailServiceImpl   
 * 类描述:活动详情实现类
 * 创建人：zhangl  
 * 创建时间：2016年10月20日 下午6:41:44   
 * 修改人：zhangl   
 * 修改时间：2016年10月20日 下午6:41:44   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ProjectDetailMapper;
import com.lst.model.ProjectDetail;
import com.lst.service.ProjectDetailService;

/**
 * @ClassName: ProjectDetailServiceImpl
 * @Description:活动详情实现类
 * @author zhangl
 * @date 2016年10月20日 下午6:41:44
 *
 */
@Service("projectDetailService")
public class ProjectDetailServiceImpl implements ProjectDetailService {
	
	@Autowired
	private ProjectDetailMapper dao;
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(ProjectDetail record) {
		return dao.insert(record);
	}

	@Override
	public int insertSelective(ProjectDetail record) {
		return dao.insertSelective(record);
	}

	@Override
	public ProjectDetail selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(ProjectDetail record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(ProjectDetail record) {
		return dao.updateByPrimaryKey(record);
	}

	@Override
	public List<ProjectDetail> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public List<ProjectDetail> queryClientProjectList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryClientProjectList(map, pageBounds);
	}
}
