/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：MstRoleServiceImpl   
 * 类描述：  角色实现类层 
 * 创建人：zhangl  
 * 创建时间：2016年10月12日 上午10:50:33   
 * 修改人：zhangl   
 * 修改时间：2016年10月12日 上午10:50:33   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.MstRoleMapper;
import com.lst.model.MstRole;
import com.lst.service.MstRoleService;

/**
 * @ClassName: MstRoleServiceImpl
 * @Description:角色实现类层
 * @author zhangl
 * @date 2016年10月12日 上午10:50:33
 *
 */
@Service("mstRoleService")
public class MstRoleServiceImpl implements MstRoleService {
	
	@Autowired
	private MstRoleMapper dao;
	
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(MstRole record) {
		return dao.insert(record);
	}

	public int insertSelective(MstRole record) {
		return dao.insertSelective(record);
	}

	public MstRole selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(MstRole record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(MstRole record) {
		return dao.updateByPrimaryKey(record);
	}

	/**
	 * 
	 * @Title: queryByList
	 * @Description:获取数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstRole>    返回类型
	 */
	public List<MstRole> queryByList(Map<String, Object> map,PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
	public void delList(Map<String, Object> map) {
		dao.delList(map);
	}
}
