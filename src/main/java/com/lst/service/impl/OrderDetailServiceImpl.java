/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：OrderDetailServiceImpl   
 * 类描述：  订单详情实现类  
 * 创建人：zhangl  
 * 创建时间：2016年12月5日 上午10:47:59   
 * 修改人：zhangl   
 * 修改时间：2016年12月5日 上午10:47:59   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.OrderDetailMapper;
import com.lst.model.OrderDetail;
import com.lst.service.OrderDetailService;

/**
 * @ClassName: OrderDetailServiceImpl
 * @Description: 订单详情实现类
 * @author zhangl
 * @date 2016年12月5日 上午10:47:59
 * 
 */
@Service("orderDetailServiceImpl")
public class OrderDetailServiceImpl implements OrderDetailService {

    @Autowired
    private OrderDetailMapper dao;

    /*
     * (非 Javadoc) <p>Title: deleteByPrimaryKey</p> <p>Description: </p>
     * 
     * @param id
     * 
     * @return
     * 
     * @see
     * com.lst.service.OrderDetailService#deleteByPrimaryKey(java.lang.Integer)
     */
    @Override
    public int deleteByPrimaryKey(Integer id) {
	return dao.deleteByPrimaryKey(id);
    }

    /*
     * (非 Javadoc) <p>Title: insert</p> <p>Description: </p>
     * 
     * @param record
     * 
     * @return
     * 
     * @see com.lst.service.OrderDetailService#insert(com.lst.model.OrderDetail)
     */
    @Override
    public int insert(OrderDetail record) {
	return dao.insert(record);
    }

    /*
     * (非 Javadoc) <p>Title: insertSelective</p> <p>Description: </p>
     * 
     * @param record
     * 
     * @return
     * 
     * @see
     * com.lst.service.OrderDetailService#insertSelective(com.lst.model.OrderDetail
     * )
     */
    @Override
    public int insertSelective(OrderDetail record) {
	return dao.insertSelective(record);
    }

    /*
     * (非 Javadoc) <p>Title: selectByPrimaryKey</p> <p>Description: </p>
     * 
     * @param id
     * 
     * @return
     * 
     * @see
     * com.lst.service.OrderDetailService#selectByPrimaryKey(java.lang.Integer)
     */
    @Override
    public OrderDetail selectByPrimaryKey(Integer id) {
	return dao.selectByPrimaryKey(id);
    }

    /*
     * (非 Javadoc) <p>Title: updateByPrimaryKeySelective</p> <p>Description:
     * </p>
     * 
     * @param record
     * 
     * @return
     * 
     * @see
     * com.lst.service.OrderDetailService#updateByPrimaryKeySelective(com.lst
     * .model.OrderDetail)
     */
    @Override
    public int updateByPrimaryKeySelective(OrderDetail record) {
	return dao.updateByPrimaryKeySelective(record);
    }

    /*
     * (非 Javadoc) <p>Title: updateByPrimaryKey</p> <p>Description: </p>
     * 
     * @param record
     * 
     * @return
     * 
     * @see com.lst.service.OrderDetailService#updateByPrimaryKey(com.lst.model.
     * OrderDetail)
     */
    @Override
    public int updateByPrimaryKey(OrderDetail record) {
	return dao.updateByPrimaryKey(record);
    }

    /*
     * (非 Javadoc) <p>Title: queryByList</p> <p>Description: </p>
     * 
     * @param map
     * 
     * @param pageBounds
     * 
     * @return
     * 
     * @see com.lst.service.OrderDetailService#queryByList(java.util.Map,
     * com.github.miemiedev.mybatis.paginator.domain.PageBounds)
     */
    @Override
    public List<OrderDetail> queryByList(Map<String, Object> map,
	    PageBounds pageBounds) {
	return dao.queryByList(map, pageBounds);
    }

    @Override
    @Transactional
    public void updateStatus(List<OrderDetail> ods, Integer userid) {

	for (OrderDetail od : ods) {
	    OrderDetail _od = new OrderDetail();
	    _od.setId(od.getId());
	    if (od.getOrderstatus() == 234) {
		_od.setOrderstatus(210);
	    } else if (od.getOrderstatus() == 210) {
		_od.setOrderstatus(209);
	    }
	    _od.setUpdatedate(new Date());
	    _od.setUpdateuser(userid);

	    dao.updateByPrimaryKeySelective(_od);

	}

    }

}
