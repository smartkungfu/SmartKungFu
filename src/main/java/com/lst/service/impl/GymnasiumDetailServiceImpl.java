/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：GymnasiumDetailServiceImpl   
 * 类描述： 场馆详情实现类   
 * 创建人：zhangl  
 * 创建时间：2016年10月21日 下午12:32:08   
 * 修改人：zhangl   
 * 修改时间：2016年10月21日 下午12:32:08   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.GymnasiumDetailMapper;
import com.lst.model.GymnasiumDetail;
import com.lst.service.GymnasiumDetailService;

/**
 * @ClassName: GymnasiumDetailServiceImpl
 * @Description: 场馆详情实现类
 * @author zhangl
 * @date 2016年10月21日 下午12:32:08
 *
 */
@Service("gymnasiumDetailService")
public class GymnasiumDetailServiceImpl implements GymnasiumDetailService {
	
	@Autowired
	private GymnasiumDetailMapper dao;
	
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(GymnasiumDetail record) {
		return dao.insert(record);
	}

	@Override
	public int insertSelective(GymnasiumDetail record) {
		return dao.insertSelective(record);
	}

	@Override
	public GymnasiumDetail selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(GymnasiumDetail record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(GymnasiumDetail record) {
		return dao.updateByPrimaryKey(record);
	}

	@Override
	public List<GymnasiumDetail> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public void delList(Map<String, Object> map) {
		dao.delList(map);
	}

	@Override
	public List<GymnasiumDetail> queryListByMap(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryListByMap(map, pageBounds);
	}

}
