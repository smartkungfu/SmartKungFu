/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：ClientRefundServiceImpl   
 * 类描述：   退款申请实现类
 * 创建人：zhangl  
 * 创建时间：2016年12月5日 下午5:15:10   
 * 修改人：zhangl   
 * 修改时间：2016年12月5日 下午5:15:10   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ClientRefundMapper;
import com.lst.model.ClientRefund;
import com.lst.service.ClientRefundService;

/**
 * @ClassName: ClientRefundServiceImpl
 * @Description:退款申请实现类
 * @author zhangl
 * @date 2016年12月5日 下午5:15:10
 *
 */
@Service("clientRefundServiceImpl")
public class ClientRefundServiceImpl implements ClientRefundService {

	@Autowired
	private ClientRefundMapper dao;

	/* (非 Javadoc)
	* <p>Title: deleteByPrimaryKey</p>
	* <p>Description: </p>
	* @param id
	* @return
	* @see com.lst.service.ClientRefundService#deleteByPrimaryKey(java.lang.Integer)
	*/
	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	/* (非 Javadoc)
	* <p>Title: insert</p>
	* <p>Description: </p>
	* @param record
	* @return
	* @see com.lst.service.ClientRefundService#insert(com.lst.model.ClientRefund)
	*/
	@Override
	public int insert(ClientRefund record) {
		return dao.insert(record);
	}

	/* (非 Javadoc)
	* <p>Title: insertSelective</p>
	* <p>Description: </p>
	* @param record
	* @return
	* @see com.lst.service.ClientRefundService#insertSelective(com.lst.model.ClientRefund)
	*/
	@Override
	public int insertSelective(ClientRefund record) {
		return dao.insertSelective(record);
	}

	/* (非 Javadoc)
	* <p>Title: selectByPrimaryKey</p>
	* <p>Description: </p>
	* @param id
	* @return
	* @see com.lst.service.ClientRefundService#selectByPrimaryKey(java.lang.Integer)
	*/
	@Override
	public ClientRefund selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	/* (非 Javadoc)
	* <p>Title: updateByPrimaryKeySelective</p>
	* <p>Description: </p>
	* @param record
	* @return
	* @see com.lst.service.ClientRefundService#updateByPrimaryKeySelective(com.lst.model.ClientRefund)
	*/
	@Override
	public int updateByPrimaryKeySelective(ClientRefund record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	/* (非 Javadoc)
	* <p>Title: updateByPrimaryKey</p>
	* <p>Description: </p>
	* @param record
	* @return
	* @see com.lst.service.ClientRefundService#updateByPrimaryKey(com.lst.model.ClientRefund)
	*/
	@Override
	public int updateByPrimaryKey(ClientRefund record) {
		return dao.updateByPrimaryKey(record);
	}

	/* (非 Javadoc)
	* <p>Title: queryByList</p>
	* <p>Description: </p>
	* @param map
	* @param pageBouns
	* @return
	* @see com.lst.service.ClientRefundService#queryByList(java.util.Map, com.github.miemiedev.mybatis.paginator.domain.PageBounds)
	*/
	@Override
	public List<ClientRefund> queryByList(Map<String, Object> map, PageBounds pageBouns) {
		return dao.queryByList(map, pageBouns);
	}
	
	
}
