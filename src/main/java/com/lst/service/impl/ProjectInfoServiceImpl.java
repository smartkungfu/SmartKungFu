/**   
 *    
 * 项目名称：SmartKungFuService   
 * 类名称：ProjectInfoServiceImpl   
 * 类描述：活动实体类   
 * 创建人：zhangl  
 * 创建时间：2016年9月22日 下午5:38:38   
 * 修改人：zhangl   
 * 修改时间：2016年9月22日 下午5:38:38   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ProjectInfoMapper;
import com.lst.model.ProjectInfo;
import com.lst.service.ProjectInfoService;

/**
 * @ClassName: ProjectInfoServiceImpl
 * @Description: 活动实体类
 * @author zhangl
 * @date 2016年9月22日 下午5:38:38
 *
 */
@Service("projectInfoService")
public class ProjectInfoServiceImpl implements ProjectInfoService {

	@Autowired
	private ProjectInfoMapper dao;

	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(ProjectInfo record) {
		return dao.insert(record);
	}

	public int insertSelective(ProjectInfo record) {
		return dao.insertSelective(record);
	}

	public ProjectInfo selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(ProjectInfo record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(ProjectInfo record) {
		return dao.updateByPrimaryKey(record);
	}

	/**
	 * 
	 * @Title: queryByList
	 * @Description:查询数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<ProjectInfo>    返回类型
	 */
	public List<ProjectInfo> queryByList(Map<String, Object> map , PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}
	
	
	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
	public void delList(Map<String, Object> map){
		dao.delList(map);
	}
}	
