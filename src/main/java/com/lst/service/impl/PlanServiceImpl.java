package com.lst.service.impl;



import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.PlanMapper;
import com.lst.model.Plan;
import com.lst.service.PlanService;


/**
 * @ClassName: PlanServiceImpl
 * @Description: 制定计划实现类层
 * 
 */
@Service("PlanService")
public class PlanServiceImpl implements PlanService {
	
	@Autowired
	private PlanMapper dao;

	@Override
	public List<Plan> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList(map, pageBounds);
	}

}
