package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.VideoMusicMapper;
import com.lst.model.VideoMusic;
import com.lst.service.VideoMusicService;

/**
 * @ClassName: VideoMusicServiceImpl
 * @Description:动作配音实现类
 * @author zmm
 * @date 2017年9月27日
 *
 */
@Service("videoMusicService")
public class VideoMusicServiceImpl implements VideoMusicService {

	@Autowired
	private VideoMusicMapper dao;

	@Override
	public int insert(VideoMusic record) {
		
		return dao.insert(record);
	}

	@Override
	public List<VideoMusic> queryByList(Map<String, Object> map, PageBounds pageBounds) {
	
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public VideoMusic selectByPrimaryKey(Integer id) {
		
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int update(VideoMusic record) {
		
		return dao.update(record);
	}

	@Override
	public void delList(Map<String, Object> map) {
		dao.delList(map);
		
	}

	
}
