package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.DynamicMapper;
import com.lst.model.Dynamic;
import com.lst.service.DynamicService;


/**
 * @ClassName: DynamicServiceImpl
 * @Description: 动态发布实现类层
 * @date 2016年9月27日 下午7:40:36
 * 
 */
@Service("DynamicService")
public class DynamicServiceImpl implements DynamicService {
	
	@Autowired
	private DynamicMapper dao;

	@Override
	public List<Dynamic> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public void delList(Map<String, Object> map) {
		
		dao.delList(map);
	}

	@Override
	public Dynamic selectByPrimaryKey(Integer id) {
		
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int countByMap(Map<String, Object> map) {
		
		return dao.countByMap(map);
	}

	

}
