package com.lst.service.impl;



import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.PlanCourseMapper;
import com.lst.model.PlanCourse;
import com.lst.service.PlanCourseService;


/**
 * @ClassName: PlanCourseServiceImpl
 * @Description: 制定计划实现类层
 * 
 */
@Service("PlanCourseService")
public class PlanCourseServiceImpl implements PlanCourseService {
	
	@Autowired
	private PlanCourseMapper dao;

	@Override
	public List<PlanCourse> queryList(Map<String, Object> map,PageBounds pageBounds) {
		
		return dao.queryList(map,pageBounds);
	}
}
