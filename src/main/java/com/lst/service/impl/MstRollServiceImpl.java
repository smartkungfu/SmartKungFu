/**   
*    
* 项目名称：SmartKungFu   
* 类名称：MstRollServiceImpl   
* 类描述：   
* 创建人：Wdd   
* 创建时间：2016年12月2日 上午10:49:31   
* 修改人：Wdd  
* 修改时间：2016年12月2日 上午10:49:31   
* 修改备注：   
* @version    
*    
*/
package com.lst.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.MstRollMapper;
import com.lst.model.MstRoll;
import com.lst.model.MstUser;
import com.lst.service.MstRollService;
import com.lst.utils.DateUtil;

/**
 * @ClassName: MstRollServiceImpl
 * @Description: 优惠券
 * @author Wdd
 * @date 2016年12月2日 上午10:49:31
 *
 */
@Service("mstRollService")
public class MstRollServiceImpl implements MstRollService {
	
	private Logger logger = Logger.getLogger("log");
	
	@Resource
	private MstRollMapper dao;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(MstRoll record) {
		return dao.insert(record);
	}

	@Override
	public int insertSelective(MstRoll record) {
		return dao.insertSelective(record);
	}

	@Override
	public MstRoll selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(MstRoll record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(MstRoll record) {
		return dao.updateByPrimaryKey(record);
	}

	@Override
	public List<MstRoll> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public void delList(Map<String, Object> map) {
		dao.delList(map);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , rollbackFor = Exception.class)
	public void saveInfo(MstRoll mstRoll, MstUser sysUser) {
		Date startDate = new Date();
		logger.info("saveInfo begin: " + startDate);
		
		try {
			if(mstRoll.getAritype().equals(127)){
				mstRoll.setValue(mstRoll.getMan() +";"+ mstRoll.getJian());
				mstRoll.setContent("满" + mstRoll.getMan() + "减" + mstRoll.getJian());
			}else if(mstRoll.getAritype().equals(128)){
				mstRoll.setValue(mstRoll.getDazhe());
				mstRoll.setContent(mstRoll.getDazhe() + "折");
			}else if(mstRoll.getAritype().equals(129)){
				mstRoll.setValue(mstRoll.getZhijian());
				mstRoll.setContent("直减" + mstRoll.getZhijian());
			}
			
			if(mstRoll.getId() == null){
				mstRoll.setCreatedate(startDate);
				mstRoll.setCreateuser(sysUser.getId());
				mstRoll.setEnabled(true);
				mstRoll.setIsdeleted(false);

				dao.insert(mstRoll);
			} else {
				mstRoll.setUpdatedate(startDate);
				mstRoll.setUpdateuser(sysUser.getId());

				dao.updateByPrimaryKeySelective(mstRoll);
			}
		} catch (Exception e) {
			logger.error("saveInfo error:", e);
		}

		logger.info("saveInfo end run(s): " + DateUtil.calLastedTime(startDate));
		

		
	}
	
	/**
	 * 
	 * @Title: updList
	 * @Description: 更新过期状态
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	public void updList(Map<String, Object> map){
		dao.updList(map);
	}
	
}
