/**   
*    
* 项目名称：SmartKungFu   
* 类名称：MstDiscussServiecImpl   
* 类描述：   
* 创建人：Wdd   
* 创建时间：2016年12月13日 下午9:54:41   
* 修改人：Wdd  
* 修改时间：2016年12月13日 下午9:54:41   
* 修改备注：   
* @version    
*    
*/
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.MstGradeMapper;
import com.lst.model.MstGrade;
import com.lst.service.MstGradeService;

/**
 * @ClassName: MstDiscussServiecImpl
 * @Description: 功夫等级
 * @author Wdd
 * @date 2016年12月13日 下午9:54:41
 *
 */
@Service("mstGradeServiceImpl")
public class MstGradeServiceImpl implements MstGradeService {
	
	@Autowired
	private MstGradeMapper dao;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}
	
	@Override
	public int insert(MstGrade record) {
		return dao.insert(record);
	}

	@Override
	public int insertSelective(MstGrade record) {
		return dao.insertSelective(record);
	}

	@Override
	public MstGrade selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(MstGrade record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(MstGrade record) {
		return dao.updateByPrimaryKey(record);
	}

	@Override
	public List<MstGrade> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

}
