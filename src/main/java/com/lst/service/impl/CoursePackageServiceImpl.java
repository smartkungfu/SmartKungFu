/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：CoursePackageServiceImpl   
 * 类描述：  课程套餐实现类 
 * 创建人：zhangl  
 * 创建时间：2016年11月1日 下午6:43:18   
 * 修改人：zhangl   
 * 修改时间：2016年11月1日 下午6:43:18   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.CoursePackageMapper;
import com.lst.model.CoursePackage;
import com.lst.service.CoursePackageService;

/**
 * @ClassName: CoursePackageServiceImpl
 * @Description: 课程套餐实现类
 * @author zhangl
 * @date 2016年11月1日 下午6:43:18
 *
 */
@Service("coursePackageService")
public class CoursePackageServiceImpl implements CoursePackageService {
	
	@Autowired
	private CoursePackageMapper dao;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(CoursePackage record) {
		return dao.insert(record);
	}
	
	@Override
	public int insertSelective(CoursePackage record) {
		return dao.insertSelective(record);
	}

	@Override
	public CoursePackage selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}
	
	@Override
	public int updateByPrimaryKeySelective(CoursePackage record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(CoursePackage record) {
		return dao.updateByPrimaryKey(record);
	}

	@Override
	public List<CoursePackage> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public void delListByCourseId(Map<String, Object> map) {
		dao.delListByCourseId(map);
	}

	@Override
	public void saveList(List<CoursePackage> list) {
		dao.saveList(list);
	}
	

	/**
	 * 
	 * @Title: saveWithList
	 * @Description: 批量更新
	 * @param @param map
	 * @param @param list    设定文件
	 * @return void    返回类型
	 */
	@Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
	public void saveWithList(Map<String, Object> map,List<CoursePackage> list){
		
		dao.delListByCourseId(map);
		
		if(!list.isEmpty()) dao.saveList(list);
	}
}
