/**   
*    
* 项目名称：SmartKungFu   
* 类名称：MstPermissionServiceImpl   
* 类描述：   
* 创建人：zhangl  
* 创建时间：2016年10月12日 下午4:54:37   
* 修改人：zhangl   
* 修改时间：2016年10月12日 下午4:54:37   
* 修改备注：   
* @version    
*    
*/
package com.lst.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.common.CommonEnum;
import com.lst.dao.MstButtonMapper;
import com.lst.dao.MstMenuMapper;
import com.lst.dao.MstPermissionMapper;
import com.lst.model.MstButton;
import com.lst.model.MstMenu;
import com.lst.model.MstPermission;
import com.lst.model.MstUser;
import com.lst.service.MstPermissionService;
import com.lst.utils.DateUtil;

/**
 * @ClassName: MstPermissionServiceImpl
 * @Description: 权限实现类层
 * @author zhangl
 * @date 2016年10月12日 下午4:54:37
 *
 */
@Service("mstPermissionService")
public class MstPermissionServiceImpl implements MstPermissionService {
	
	Logger logger = Logger.getLogger("log");
	
	@Autowired
	private MstPermissionMapper dao;
	
	@Autowired
	private MstMenuMapper menuDao;
	
	@Autowired
	private MstButtonMapper btnDao;
	
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	public int insert(MstPermission record) {
		return dao.insert(record);
	}

    public int insertSelective(MstPermission record) {
		return dao.insertSelective(record);
	}

    public MstPermission selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
		
	}

    public int updateByPrimaryKeySelective(MstPermission record) {
		return dao.updateByPrimaryKeySelective(record);
	}

    public int updateByPrimaryKey(MstPermission record) {
		return dao.updateByPrimaryKey(record);
	}
    
    public List<MstPermission> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

	/**
	 * 
	 * @Title: queryUserPermissionBy
	 * @Description: 根据map查询用户权限  菜单
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<MstPermission>    返回类型
	 */
    public List<MstPermission> queryUserPermissionByMap(Map<String, Object>  map) {
		return dao.queryUserPermissionByMap(map);
	}
	
	/**
	 * 
	 * @Title: queryUserPermissionBy
	 * @Description: 根据map查询角色权限  菜单
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<MstPermission>    返回类型
	 */
	public List<MstPermission> queryRolePermissionByMap(Map<String, Object>  map) {
		return dao.queryRolePermissionByMap(map);
	}
	
	/**
	 * 
	 * @Title: deleteByMap
	 * @Description: 批量删除用户权限
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	public void deleteByMap(Map<String, Object> map) {
		dao.deleteByMap(map);
	}

	/**
	 * 
	 * @Title: saveList
	 * @Description: 保存用户权限
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	public void saveList(List<MstPermission> list) {
		dao.saveList(list);
	}
	
	/**
	 * @Title: queryMenuPermission
	 * @Description: 查询角色或用户的菜单权限
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<Map<String,Object>>    返回类型
	 * @throws
	 */
	public List<Map<String, Object>> queryMenuPermission(Map<String, Object> map) {
		Date startDate = new Date();
		logger.info("queryMenuPermission begin : " + startDate);

		List<MstPermission> permissionList = null;
		String mastertype = map.get("mastertype").toString();

		if (mastertype.equals(CommonEnum.SYSCODE_ROOTBELONG_2.toString())) {
			permissionList = dao.queryUserPermissionByMap(map);
		} else if (mastertype.equals(CommonEnum.SYSCODE_ROOTBELONG_3.toString())) {
			permissionList = dao.queryRolePermissionByMap(map);
		}

		List<MstMenu> menuList = menuDao.queryListByParentId(map);

		Integer actionValue = null;
		Boolean isClick = false;

		Map<String, Object> menuMap = null;

		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();

		for (MstMenu menu : menuList) {
			isClick = false;

			for (MstPermission permission : permissionList) {
				actionValue = permission.getActionvalue();

				if (actionValue.equals(menu.getId())) {
					isClick = true;
				}
			}

			menuMap = new HashMap<String, Object>();

			menuMap.put("checked", isClick);
			menuMap.put("id", menu.getId());
			menuMap.put("parentid", menu.getParentid());
			menuMap.put("text", menu.getName());
			menuMap.put("url", "");
			menuMap.put("iconCls", menu.getIcon());
			menuMap.put("treeType", "MENU");

			int count = menuDao.countByParentid(menu.getId());

			if (count > 0) {
				menuMap.put("state", "closed");
			} else {
				//无子菜单,还需要判断是否有按键
				if (btnDao.countByMenuid(menu.getId()) > 0) {
					menuMap.put("state", "closed");
				} else {
					menuMap.put("state", "");
				}
			}

			mapList.add(menuMap);
		}

		logger.info("queryMenuPermission run end(s) : " + DateUtil.calLastedTime(startDate));

		return mapList;
	}
	
	/**
	 * @Title: queryBtnPermission
	 * @Description: 查询用户或角色的btn权限
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<Map<String,Object>>    返回类型
	 * @throws
	 */
	public List<Map<String, Object>> queryBtnPermission(Map<String, Object> map) {
		Date startDate = new Date();
		logger.info("queryBtnPermission begin : " + startDate);

		List<MstPermission> permissionList = null;
		String mastertype = map.get("mastertype").toString();

		if (mastertype.equals(CommonEnum.SYSCODE_ROOTBELONG_3.toString())) {
			permissionList = dao.queryUserPermissionByMap(map);
		} else if (mastertype.equals(CommonEnum.SYSCODE_ROOTBELONG_2.toString())) {
			permissionList = dao.queryRolePermissionByMap(map);
		}

		List<MstButton> btnList = btnDao.queryListByMenuId(map);

		Integer actionValue = null;
		Boolean isClick = false;

		Map<String, Object> btnMap = null;

		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();

		for (MstButton btn : btnList) {
			isClick = false;

			for (MstPermission permission : permissionList) {
				actionValue = permission.getActionvalue();

				if (actionValue.equals(btn.getId())) {
					isClick = true;
				}
			}

			btnMap = new HashMap<String, Object>();

			btnMap.put("checked", isClick);
			btnMap.put("id", btn.getId());
			btnMap.put("parentid", btn.getMenuid());
			btnMap.put("text", btn.getBtnname());
			btnMap.put("url", "");
			btnMap.put("iconCls", btn.getBtnicon());
			btnMap.put("treeType", "BUTTON");
			btnMap.put("state", "");	

			mapList.add(btnMap);
		}

		logger.info("queryBtnPermission run end(s) : " + DateUtil.calLastedTime(startDate));

		return mapList;
	}
	
	/**
	 * @Title: savePermission
	 * @Description: 保存权限信息
	 * @param @param masterType
	 * @param @param masterValue
	 * @param @param menuIds
	 * @param @param buttonIds
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @throws
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void savePermission(String masterType, String masterValue, String menuIds, String buttonIds,MstUser currentUser) {
		Date startDate = new Date();
		logger.info("savePermission begin : " + startDate);

		//删除原权限
		Map<String, Object> delMap = new HashMap<String, Object>();
		delMap.put("masterType", Long.valueOf(masterType));
		delMap.put("masterValue", Long.valueOf(masterValue));
		delMap.put("updatedate", startDate);
		delMap.put("updateuser", currentUser.getId());

		dao.deleteByMap(delMap);

		String[] menuId = menuIds.split(",");
		String[] btnId = buttonIds.split(",");

		List<MstPermission> saveList = new ArrayList<MstPermission>();
		MstPermission permission = null;

		//整理菜单权限
		for (String menu : menuId) {
			permission = new MstPermission();

			permission.setMastertype(Integer.valueOf(masterType));
			permission.setMastervalue(Integer.valueOf(masterValue));
			permission.setActiontype(CommonEnum.SYSCODE_ROOTDISTRI_5);
			permission.setActionvalue(Integer.valueOf(menu));
			permission.setEnabled(true);
			permission.setIsdeleted(false);
			permission.setCreatedate(startDate);
			permission.setCreateuser(currentUser.getId());

			saveList.add(permission);
		}

		//整理按键权限
		for (String btn : btnId) {
			if(!StringUtils.isBlank(btn)){
				permission = new MstPermission();

				permission.setMastertype(Integer.valueOf(masterType));
				permission.setMastervalue(Integer.valueOf(masterValue));
				permission.setActiontype(CommonEnum.SYSCODE_ROOTDISTRI_6);
				permission.setActionvalue(Integer.valueOf(btn));
				permission.setEnabled(true);
				permission.setIsdeleted(false);
				permission.setCreatedate(startDate);
				permission.setCreateuser(currentUser.getId());

				saveList.add(permission);
			}
		}

		//如果新权限不为空,添加新的权限
		if(saveList != null) {
			dao.saveList(saveList);
		}

		logger.info("savePermission run end(s) : " + DateUtil.calLastedTime(startDate));
	}
}
