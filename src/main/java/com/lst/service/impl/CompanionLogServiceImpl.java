package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.CompanionLogMapper;
import com.lst.model.CompanionLog;
import com.lst.service.CompanionLogService;


/**
 * @ClassName: CompanionLogServiceImpl
 * @Description: 嘿哈活动报名实现类层
 * 
 */
@Service("CompanionLogService")
public class CompanionLogServiceImpl implements CompanionLogService {
	
	 @Autowired
	 private CompanionLogMapper dao;

	@Override
	public int insert(CompanionLog record) {
		
		return dao.insert(record);
	}

	@Override
	public List<CompanionLog> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		
		return dao.queryByList(map, pageBounds);
	}

	@Override
	public int countBySourceId(Integer sourceId) {
		
		return dao.countBySourceId(sourceId);
	}

	@Override
	public CompanionLog queryByMap(Map<String, Object> map) {
		
		return dao.queryByMap(map);
	}

	

	

}
