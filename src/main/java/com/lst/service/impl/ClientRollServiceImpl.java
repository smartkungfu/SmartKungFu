/**   
*    
* 项目名称：SmartKungFu   
* 类名称：ClientRollServiceImpl   
* 类描述：   
* 创建人：Wdd   
* 创建时间：2016年11月2日 上午11:51:58   
* 修改人：Wdd  
* 修改时间：2016年11月2日 上午11:51:58   
* 修改备注：   
* @version    
*    
*/
package com.lst.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.dao.ClientRollMapper;
import com.lst.model.ClientRoll;
import com.lst.service.ClientRollService;

/**
 * @ClassName: ClientRollServiceImpl
 * @Description: 用户优惠
 * @author Wdd
 * @date 2016年11月2日 上午11:51:58
 *
 */
@Service("clientRollServiceImpl")
public class ClientRollServiceImpl implements ClientRollService{
	
	@Autowired
	private ClientRollMapper dao;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return dao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(ClientRoll record) {
		return dao.insert(record);
	}

	@Override
	public int insertSelective(ClientRoll record) {
		return dao.insertSelective(record);
	}

	@Override
	public ClientRoll selectByPrimaryKey(Integer id) {
		return dao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(ClientRoll record) {
		return dao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(ClientRoll record) {
		return dao.updateByPrimaryKey(record);
	}

	@Override
	public List<ClientRoll> queryByList(Map<String, Object> map, PageBounds pageBounds) {
		return dao.queryByList(map, pageBounds);
	}

}
