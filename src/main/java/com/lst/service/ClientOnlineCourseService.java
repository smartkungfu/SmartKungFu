/**   
*    
* 项目名称：SmartKungFu   
* 类名称：ClientOnlineCourseService   
* 类描述：   
* 创建人：Wdd   
* 创建时间：2017年1月13日 下午9:37:22   
* 修改人：Wdd  
* 修改时间：2017年1月13日 下午9:37:22   
* 修改备注：   
* @version    
*    
*/
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientOnlineCourse;

/**
 * @ClassName: ClientOnlineCourseService
 * @Description: 用户在线课程
 * @author Wdd
 * @date 2017年1月13日 下午9:37:22
 *
 */
public interface ClientOnlineCourseService {
	
	int deleteByPrimaryKey(Integer id);

    int insert(ClientOnlineCourse record);

    int insertSelective(ClientOnlineCourse record);

    ClientOnlineCourse selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClientOnlineCourse record);

    int updateByPrimaryKey(ClientOnlineCourse record);
    
    List<ClientOnlineCourse> queryByList(Map<String, Object> map, PageBounds pageBouns);
}
