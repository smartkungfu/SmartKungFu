package com.lst.service;

import com.lst.model.MstSetting;

public interface MstSettingService {
    int deleteByPrimaryKey(Integer id);

    int insert(MstSetting record);

    int insertSelective(MstSetting record);

    MstSetting selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MstSetting record);

    int updateByPrimaryKeyWithBLOBs(MstSetting record);

    int updateByPrimaryKey(MstSetting record);
}