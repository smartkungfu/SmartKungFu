/**   
 *    
 * 项目名称：SmartKungFuService   
 * 类名称：ClientNumService   
 * 类描述： 用户操作量接口层  
 * 创建人：zhangl  
 * 创建时间：2016年9月29日 下午4:19:24   
 * 修改人：zhangl   
 * 修改时间：2016年9月29日 下午4:19:24   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientNum;

/**
 * @ClassName: ClientNumService
 * @Description: 用户操作量接口层
 * @author zhangl
 * @date 2016年9月29日 下午4:19:24
 *
 */
public interface ClientNumService {
	
	int deleteByPrimaryKey(Integer id);

	int insert(ClientNum record);

	int insertSelective(ClientNum record);

	ClientNum selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ClientNum record);

	int updateByPrimaryKey(ClientNum record);
	
	/**
	 * 
	 * @Title: queryByList
	 * @Description: 获取统计量
	 * @param @param pageBounds
	 * @param @param clientNum
	 * @param @return    设定文件
	 * @return List<ClientNum>    返回类型
	 */
	List<ClientNum> queryByList(Map<String, Object> map,PageBounds pageBounds);
	

	/**
	 * 
	 * @Title: queryCountByMap
	 * @Description: 统计用户操作量
	 * @param @param map
	 * @param @return    设定文件
	 * @return Integer    返回类型
	 */
	Long queryCountByMap(Map<String, Object> map);
}
