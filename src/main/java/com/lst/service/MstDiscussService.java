/**   
 *    
 * 项目名称：SmartKungFuService   
 * 类名称：MstDiscussService   
 * 类描述：   评论接口
 * 创建人：zhangl  
 * 创建时间：2016年10月10日 上午10:58:14   
 * 修改人：zhangl   
 * 修改时间：2016年10月10日 上午10:58:14   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstDiscuss;

/**
 * @ClassName: MstDiscussService
 * @Description: 评论接口
 * @author zhangl
 * @date 2016年10月10日 上午10:58:14
 *
 */
public interface MstDiscussService {
    List<MstDiscuss> queryByList(Map<String, Object> map, PageBounds pageBounds);
    
    void delList(Map<String, Object> map);
    
    Integer queryCountByMap(Map<String, Object> map);
    
    List<MstDiscuss> queryByList2(Map<String, Object> map, PageBounds pageBounds);
}
