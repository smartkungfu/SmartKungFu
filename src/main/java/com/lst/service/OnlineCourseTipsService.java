/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：OnlineCourseTipsService   
 * 类描述：在线课程提示语   
 * 创建人：zhangl  
 * 创建时间：2016年12月30日 上午9:46:28   
 * 修改人：zhangl   
 * 修改时间：2016年12月30日 上午9:46:28   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.OnlineCourseTips;

/**
 * @ClassName: OnlineCourseTipsService
 * @Description:在线课程提示语
 * @author zhangl
 * @date 2016年12月30日 上午9:46:28
 *
 */
public interface OnlineCourseTipsService {
	int deleteByPrimaryKey(Integer id);

	int insert(OnlineCourseTips record);

	int insertSelective(OnlineCourseTips record);

	OnlineCourseTips selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(OnlineCourseTips record);

	int updateByPrimaryKey(OnlineCourseTips record);

	void delList(Map<String, Object> map);

	List<OnlineCourseTips> queryByList(Map<String, Object> map, PageBounds pageBounds);
}
