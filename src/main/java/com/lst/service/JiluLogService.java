package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.JiluLog;

public interface JiluLogService {
    
	void updateByKey(Map<String, Object> map);
    
    /**
   	 * 
   	 * @Title: queryByList
   	 * @Description: 数据集
   	 * @param @param map
   	 * @param @return   pageBounds
   	 * @return List<MstOrder>    返回类型
   	 * @throws
   	 */
    List<JiluLog> queryByList(Map<String, Object> map , PageBounds pageBounds);
}