/**   
*    
* 项目名称：SmartKungFu   
* 类名称：MstDiscussServiec   
* 类描述：   
* 创建人：Wdd   
* 创建时间：2016年12月13日 下午9:54:27   
* 修改人：Wdd  
* 修改时间：2016年12月13日 下午9:54:27   
* 修改备注：   
* @version    
*    
*/
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstGrade;

/**
 * @ClassName: MstDiscussServiec
 * @Description: 功夫等级
 * @author Wdd
 * @date 2016年12月13日 下午9:54:27
 *
 */
public interface MstGradeService {
	int deleteByPrimaryKey(Integer id);

	int insert(MstGrade record);

	int insertSelective(MstGrade record);

	MstGrade selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(MstGrade record);

	int updateByPrimaryKey(MstGrade record);
	
	/**
	 * 
	* @Title: queryByList
	* @Description: 查询数据集
	* @param @param map
	* @param @param pageBounds
	* @param @return    设定文件
	* @return List<MstDiscuss>    返回类型
	* @throws
	 */
	List<MstGrade> queryByList(Map<String, Object> map,PageBounds pageBounds);

}
