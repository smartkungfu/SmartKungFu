/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：OrderDetailService   
 * 类描述：订单详情接
 * 创建人：zhangl  
 * 创建时间：2016年12月5日 上午10:46:38   
 * 修改人：zhangl   
 * 修改时间：2016年12月5日 上午10:46:38   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.OrderDetail;

/**
 * @ClassName: OrderDetailService
 * @Description:订单详情接口
 * @author zhangl
 * @date 2016年12月5日 上午10:46:38
 * 
 */
public interface OrderDetailService {
    int deleteByPrimaryKey(Integer id);

    int insert(OrderDetail record);

    int insertSelective(OrderDetail record);

    OrderDetail selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OrderDetail record);

    int updateByPrimaryKey(OrderDetail record);

    /**
     * 
     * @Title: queryByList
     * @Description:获取数据集
     * @param @param map
     * @param @param pageBounds
     * @param @return 设定文件
     * @return List<OrderDetail> 返回类型
     */
    List<OrderDetail> queryByList(Map<String, Object> map, PageBounds pageBounds);

    void updateStatus(List<OrderDetail> ods, Integer userid);
}
