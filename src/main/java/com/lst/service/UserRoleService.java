package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.UserRole;

public interface UserRoleService {
    int deleteByPrimaryKey(Integer id);

    int insert(UserRole record);

    int insertSelective(UserRole record);

    UserRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserRole record);

    int updateByPrimaryKey(UserRole record);
    
    /**
	 * 
	 * @Title: queryByList
	 * @Description:查询结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<UserRole>    返回类型
	 */
	List<UserRole> queryByList(Map<String, Object> map,PageBounds pageBounds);
	
	/**
	 * 
	 * @Title: saveList
	 * @Description:批量插入
	 * @param @param list    设定文件
	 * @return void    返回类型
	 */
	void saveList(List<UserRole> list);
	
	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delList(Map<String, Object> map);
}