package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientGrowth;

public interface ClientGrowthService {
    int deleteByPrimaryKey(Integer id);

    int insert(ClientGrowth record);

    int insertSelective(ClientGrowth record);

    ClientGrowth selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClientGrowth record);

    int updateByPrimaryKey(ClientGrowth record);
    
    /**
     * 
    * @Title: queryByList
    * @Description: 查询数据集
    * @param @param map
    * @param @param pageBounds
    * @param @return    设定文件
    * @return List<ClientGrowth>    返回类型
    * @throws
     */
    List<ClientGrowth> queryByList(Map<String, Object> map,PageBounds pageBounds);
}