package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.Question;

public interface QuestionService {

    Question selectByPrimaryKey(Integer id);
	
	List<Question> queryByList(Map<String, Object> map , PageBounds pageBounds);
	
	void delList(Map<String, Object> map);
	
	int countByMap(Map<String, Object> map);
}