package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.CourseAction;

/**
 * @ClassName: CourseActionService
 * @Description: 课程关联动作接口类
 * @author zmm
 * @date 2017年9月27日 
 *
 */
public interface CourseActionService {

int insert(CourseAction record);
	
	CourseAction selectByPrimaryKey(Integer id);
	
	int update(CourseAction record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 查询结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<VideoNote>    返回类型
	 */
	List<CourseAction> queryByList(Map<String, Object> map,PageBounds pageBounds);

	/**
	 * 
	 * @Title: delList
	 * @Description:批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delList(Map<String, Object> map);
}
