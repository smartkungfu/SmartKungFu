package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.Companion;

public interface CompanionService {

	List<Companion> queryByList(Map<String, Object> map , PageBounds pageBounds);
	
	void delList(Map<String, Object> map);
	
	Companion selectByPrimaryKey(Integer id);
	
	int countByMap(Map<String, Object> map);
}