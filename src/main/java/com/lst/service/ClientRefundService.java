/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：ClientRefundService   
 * 类描述：退款请求接口
 * 创建人：zhangl  
 * 创建时间：2016年12月5日 下午5:06:03   
 * 修改人：zhangl   
 * 修改时间：2016年12月5日 下午5:06:03   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientRefund;

/**
 * @ClassName: ClientRefundService
 * @Description:退款请求接口
 * @author zhangl
 * @date 2016年12月5日 下午5:06:03
 *
 */
public interface ClientRefundService {
	int deleteByPrimaryKey(Integer id);

	int insert(ClientRefund record);

	int insertSelective(ClientRefund record);

	ClientRefund selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ClientRefund record);

	int updateByPrimaryKey(ClientRefund record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 获取结果集
	 * @param @param map
	 * @param @param pageBouns
	 * @param @return    设定文件
	 * @return List<ClientRefund>    返回类型
	 */
	List<ClientRefund> queryByList(Map<String, Object> map,PageBounds pageBouns);
}
