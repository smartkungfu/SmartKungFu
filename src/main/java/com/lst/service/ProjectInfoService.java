package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ProjectInfo;

public interface ProjectInfoService {
	int deleteByPrimaryKey(Integer id);

	int insert(ProjectInfo record);

	int insertSelective(ProjectInfo record);

	ProjectInfo selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ProjectInfo record);

	int updateByPrimaryKey(ProjectInfo record);


	/**
	 * 
	 * @Title: queryByList
	 * @Description:查询数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<ProjectInfo>    返回类型
	 */
	List<ProjectInfo> queryByList(Map<String, Object> map , PageBounds pageBounds);
	
	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delList(Map<String, Object> map);
}