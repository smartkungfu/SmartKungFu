package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstOrder;
import com.lst.model.vo.MstOrderVo;

public interface MstOrderService {
    int deleteByPrimaryKey(Integer id);

    int insert(MstOrder record);

    int insertSelective(MstOrder record);

    MstOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MstOrder record);

    int updateByPrimaryKey(MstOrder record);
    
    /***
	 * 
	 * @Title: queryByList
	 * @Description: 查询结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstOrderVo>    返回类型
	 */
	List<MstOrderVo> queryByList(Map<String, Object> map ,PageBounds pageBounds);

	/***
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delList(Map<String, Object> map);
}