package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstGymnasium;

public interface MstGymnasiumService {
	int deleteByPrimaryKey(Integer id);

	int insert(MstGymnasium record);

	int insertSelective(MstGymnasium record);

	MstGymnasium selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(MstGymnasium record);

	int updateByPrimaryKey(MstGymnasium record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 查询数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstGymnasium>    返回类型
	 */
	List<MstGymnasium> queryByList(Map<String, Object> map , PageBounds pageBounds);
	
	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map
	 * @return void    返回类型
	 */
	void delList(Map<String, Object> map);
}