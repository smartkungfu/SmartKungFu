package com.lst.service;



import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.Report;

public interface ReportService {

	List<Report> queryByList(Map<String, Object> map , PageBounds pageBounds);
	
	Report selectByKey(Integer id);
	
	void updateReport(Report report);
}