/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：ClientAccomplishService   
 * 类描述：用户等级接口类   
 * 创建人：zhangl  
 * 创建时间：2016年12月24日 下午1:15:51   
 * 修改人：zhangl   
 * 修改时间：2016年12月24日 下午1:15:51   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientAccomplish;

/**
 * @ClassName: ClientAccomplishService
 * @Description:用户等级接口类
 * @author zhangl
 * @date 2016年12月24日 下午1:15:51
 *
 */
public interface ClientAccomplishService {
	
	int deleteByPrimaryKey(Integer id);

	int insert(ClientAccomplish record);

	int insertSelective(ClientAccomplish record);

	ClientAccomplish selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ClientAccomplish record);

	int updateByPrimaryKey(ClientAccomplish record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 获取数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<ClientAccomplish>    返回类型
	 */
	List<ClientAccomplish> queryByList(Map<String, Object> map , PageBounds pageBounds);
}
