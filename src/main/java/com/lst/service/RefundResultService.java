/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：RefundResultService   
 * 类描述：退款处理接口   
 * 创建人：zhangl  
 * 创建时间：2016年12月6日 上午11:01:48   
 * 修改人：zhangl   
 * 修改时间：2016年12月6日 上午11:01:48   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service;

import java.math.BigDecimal;

import com.lst.model.ClientRefund;
import com.lst.model.MstUser;
import com.lst.model.RefundResult;

/**
 * @ClassName: RefundResultService
 * @Description:退款处理接口
 * @author zhangl
 * @date 2016年12月6日 上午11:01:48
 *
 */
public interface RefundResultService {
	int deleteByPrimaryKey(Integer id);

	int insert(RefundResult record);

	int insertSelective(RefundResult record);

	RefundResult selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(RefundResult record);

	int updateByPrimaryKey(RefundResult record);

	/**
	 * 
	 * @Title: dealRefund
	 * @Description: 退款处理
	 * @param @param cr
	 * @param @param isTicket
	 * @param @param isGrade
	 * @param @param isGradeValue
	 * @param @param refPrice
	 * @param @param sysUser
	 * @param @return    设定文件
	 * @return boolean    返回类型
	 */
	boolean dealRefund(ClientRefund cr,boolean isTicket,boolean isGrade,boolean isGradeValue,BigDecimal refPrice,MstUser sysUser);
}
