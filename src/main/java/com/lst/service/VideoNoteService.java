/**   
 *    
 * 项目名称：SmartKungFu   
 * 类名称：VideoNoteService   
 * 类描述：   视频节点接口类
 * 创建人：zhangl  
 * 创建时间：2017年1月11日 下午2:04:06   
 * 修改人：zhangl   
 * 修改时间：2017年1月11日 下午2:04:06   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.VideoNote;

/**
 * @ClassName: VideoNoteService
 * @Description: 视频节点接口类
 * @author zhangl
 * @date 2017年1月11日 下午2:04:06
 *
 */
public interface VideoNoteService {
	int deleteByPrimaryKey(Integer id);

	int insert(VideoNote record);

	int insertSelective(VideoNote record);

	VideoNote selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(VideoNote record);

	int updateByPrimaryKey(VideoNote record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 查询结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<VideoNote>    返回类型
	 */
	List<VideoNote> queryByList(Map<String, Object> map,PageBounds pageBounds);

	/**
	 * 
	 * @Title: delList
	 * @Description:批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delList(Map<String, Object> map);
}
