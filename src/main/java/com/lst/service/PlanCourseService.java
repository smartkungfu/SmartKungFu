package com.lst.service;



import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.PlanCourse;

public interface PlanCourseService {

	List<PlanCourse> queryList(Map<String, Object> map,PageBounds pageBounds);
}