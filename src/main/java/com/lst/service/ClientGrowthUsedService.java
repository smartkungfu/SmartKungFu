/**   
*    
* 项目名称：SmartKungFu   
* 类名称：ClientGrowthUsedService   
* 类描述：   
* 创建人：Wdd   
* 创建时间：2017年1月13日 下午8:49:08   
* 修改人：Wdd  
* 修改时间：2017年1月13日 下午8:49:08   
* 修改备注：   
* @version    
*    
*/
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientGrowthUsed;

/**
 * @ClassName: ClientGrowthUsedService
 * @Description: 用户等级优惠
 * @author Wdd
 * @date 2017年1月13日 下午8:49:08
 *
 */
public interface ClientGrowthUsedService {

	int deleteByPrimaryKey(Integer id);

    int insert(ClientGrowthUsed record);

    int insertSelective(ClientGrowthUsed record);

    ClientGrowthUsed selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClientGrowthUsed record);

    int updateByPrimaryKey(ClientGrowthUsed record);
    
    List<ClientGrowthUsed> queryByList(Map<String, Object> map, PageBounds pageBouns);
}
