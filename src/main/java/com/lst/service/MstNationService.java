package com.lst.service;

import com.lst.model.MstNation;

public interface MstNationService {
    int deleteByPrimaryKey(Integer id);

    int insert(MstNation record);

    int insertSelective(MstNation record);

    MstNation selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MstNation record);

    int updateByPrimaryKey(MstNation record);
}