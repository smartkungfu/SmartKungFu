/**   
*    
* 项目名称：SmartKungFu   
* 类名称：MstRollService   
* 类描述：   
* 创建人：Wdd   
* 创建时间：2016年12月2日 上午10:46:55   
* 修改人：Wdd  
* 修改时间：2016年12月2日 上午10:46:55   
* 修改备注：   
* @version    
*    
*/
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstRoll;
import com.lst.model.MstUser;

/**
 * @ClassName: MstRollService
 * @Description: 优惠券
 * @author Wdd
 * @date 2016年12月2日 上午10:46:55
 *
 */
public interface MstRollService {
    int deleteByPrimaryKey(Integer id);

    int insert(MstRoll record);

    int insertSelective(MstRoll record);

    MstRoll selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MstRoll record);

    int updateByPrimaryKey(MstRoll record);
    
    List<MstRoll> queryByList(Map<String, Object> map,PageBounds pageBounds);
    
    void delList(Map<String, Object> map);
    
    void saveInfo(MstRoll mstRoll,MstUser sysUser);
    
	/**
	 * 
	 * @Title: updList
	 * @Description: 更新过期状态
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void updList(Map<String, Object> map);
}
