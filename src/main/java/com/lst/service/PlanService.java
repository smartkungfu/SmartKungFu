package com.lst.service;



import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.Plan;

public interface PlanService {

	List<Plan> queryByList(Map<String,Object> map,PageBounds pageBounds);
}