package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientCourse;

public interface ClientCourseService {
    int deleteByPrimaryKey(Integer id);

    int insert(ClientCourse record);

    int insertSelective(ClientCourse record);

    ClientCourse selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClientCourse record);

    int updateByPrimaryKey(ClientCourse record);
    
    /**
     * 
    * @Title: queryByList
    * @Description: 查询数据集
    * @param @param map
    * @param @param pageBounds
    * @param @return    设定文件
    * @return List<ClientCourse>    返回类型
    * @throws
     */
    List<ClientCourse> queryByList(Map<String, Object> map,PageBounds pageBounds);
}