package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.GymnasiumDetail;

public interface GymnasiumDetailService {
    int deleteByPrimaryKey(Integer id);

    int insert(GymnasiumDetail record);

    int insertSelective(GymnasiumDetail record);

    GymnasiumDetail selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GymnasiumDetail record);

    int updateByPrimaryKey(GymnasiumDetail record);
    
	/**
	 * 
	 * @Title: queryByList
	 * @Description:查询结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<GymnasiumDetail>    返回类型
	 */
	List<GymnasiumDetail> queryByList(Map<String, Object> map,PageBounds pageBounds);
	
	List<GymnasiumDetail> queryListByMap(Map<String, Object> map,PageBounds pageBounds);

	/**
	 * 
	 * @Title: delList
	 * @Description:批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delList(Map<String, Object> map);
}