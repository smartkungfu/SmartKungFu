package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.StarInfo;

public interface StarInfoService {
	int deleteByPrimaryKey(Integer id);

	int insert(StarInfo record);

	int insertSelective(StarInfo record);

	StarInfo selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(StarInfo record);

	int updateByPrimaryKey(StarInfo record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description:查询数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<StarInfo>    返回类型
	 */
	List<StarInfo> queryByList(Map<String, Object> map , PageBounds pageBounds);
	
	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delList(Map<String, Object> map);
}