package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstMessage;
import com.lst.model.MstUser;

public interface MstMessageService {
    int deleteByPrimaryKey(Integer id);

    int insert(MstMessage record);

    int insertSelective(MstMessage record);

    MstMessage selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MstMessage record);

    int updateByPrimaryKey(MstMessage record);
    
    /**
     * 
    * @Title: queryByList
    * @Description: 查询数据集
    * @param @param map
    * @param @param pageBounds
    * @param @return    设定文件
    * @return List<MstMessage>    返回类型
    * @throws
     */
    List<MstMessage> queryByList(Map<String, Object> map,PageBounds pageBounds);
    
    public void pushMessage(String grade, String content,String reserved10, MstUser sysUser);
    
    public void saveInfo(String content, MstUser sysUser,String reserved10);
}