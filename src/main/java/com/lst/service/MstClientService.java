/**   
 *    
 * 项目名称：SmartKungFuService   
 * 类名称：MstClientService   
 * 类描述：用户接口   
 * 创建人：zhangl  
 * 创建时间：2016年9月22日 上午10:41:44   
 * 修改人：zhangl   
 * 修改时间：2016年9月22日 上午10:41:44   
 * 修改备注：   
 * @version    
 *    
 */
package com.lst.service;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstClient;
import com.lst.model.MstUser;

/**
 * @ClassName: MstClientService
 * @Description:用户接口
 * @author zhangl
 * @date 2016年9月22日 上午10:41:44
 *
 */
public interface MstClientService {
	int deleteByPrimaryKey(Integer id);

	int insert(MstClient record);

	int insertSelective(MstClient record);

	MstClient selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(MstClient record);

	int updateByPrimaryKey(MstClient record);
	
	int countByMap(Map<String, Object> map);
	/**
	 * 
	 * @Title: queryByList
	 * @Description: 查询数据集
	 * @param @param map
	 * @param @param pageBouonds
	 * @param @return    设定文件
	 * @return List<MstClient>    返回类型
	 */
	List<MstClient> queryByList(Map<String, Object> map,PageBounds pageBouonds);
	
	/**
	 * 
	* @Title: saveInfo
	* @Description: 添加用户
	* @param @param mstClient
	* @param @param sysUser    设定文件
	* @return void    返回类型
	* @throws
	 */
	void saveInfo(MstClient mstClient,MstUser sysUser);
	
	/**
	 * 
	* @Title: delList
	* @Description: 批量删除用户
	* @param @param map    设定文件
	* @return void    返回类型
	* @throws
	 */
	void delList(Map<String, Object> map);
}
