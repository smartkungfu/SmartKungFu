/**   
*    
* 项目名称：BBM_Manage   
* 类名称：ConfigProp   
* 类描述：   
* 创建人：Zhanglin   
* 创建时间：2015年10月16日 下午7:15:38   
* 修改人：Zhanglin    
* 修改时间：2015年10月16日 下午7:15:38   
* 修改备注：   
* @version    
*    
*/
package com.lst.utils;

import java.io.InputStream;
import java.util.Properties;

/**
 * @ClassName: ConfigProp
 * @Description: 读取配置文件值
 * @author Zhanglin
 * @date 2015年10月16日 下午7:15:38
 *
 */
public class ConfigProp {
	private static String sms_url;

	private static String sms_user;

	private static String sms_pwd;

	private static String img_file_path;
	
	private static String video_file_path;

	private static String excel_file_path;

	private static String img_file_path_linux;
	
	private static String video_file_path_linux;

	private static String img_file_path_http;
	
	private static String temp_file_path;
	
	private static String appkey;
	
	private static String masterSecret;
	
	private static String medicalservice_url_sms;
	
	
	private static String SERVICE_IP;
	private static String SMS_URL;
	private static String REGISTER_URL;
	private static String SERVICE_PORT;
	
	private static String MYTEST_URL;
	
	private static String resources_path;
	
	private static String env;

	static {
		try {
			Properties props = new Properties();
			// 加载
			ClassLoader cl = ConfigProp.class.getClassLoader();
			// 读取
			InputStream is = cl.getResourceAsStream("kungfu.properties");

			// 加载信息
			props.load(is);
			
			sms_url = props.getProperty("sms_url");
			sms_user = props.getProperty("sms_user");
			sms_pwd = props.getProperty("sms_pwd");
			img_file_path = props.getProperty("img_file_path");
			video_file_path = props.getProperty("video_file_path");
			excel_file_path = props.getProperty("excel_file_path");
			img_file_path_linux = props.getProperty("img_file_path_linux");
			img_file_path_http = props.getProperty("img_file_path_http");
			temp_file_path = props.getProperty("temp_file_path");
			video_file_path_linux = props.getProperty("video_file_path_linux");
			appkey = props.getProperty("appkey");
			masterSecret = props.getProperty("masterSecret");
			
			medicalservice_url_sms = props.getProperty("medicalservice_url_sms");
			
			SERVICE_IP = props.getProperty("SERVICE_IP");
			SMS_URL = props.getProperty("SMS_URL");
			REGISTER_URL = props.getProperty("REGISTER_URL");
			SERVICE_PORT = props.getProperty("SERVICE_PORT");
			
			MYTEST_URL = props.getProperty("MYTEST_URL");
			
			resources_path = props.getProperty("resources_path");
			
			env = props.getProperty("env");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void setMasterSecret(String masterSecret) {
		ConfigProp.masterSecret = masterSecret;
	}

	public static String getMasterSecret() {
		return masterSecret;
	}
	
	public static void setAppkey(String appkey) {
		ConfigProp.appkey = appkey;
	}

	public static String getAppkey() {
		return appkey;
	}

	public static String getSms_url() {
		return sms_url;
	}
	
	public static void setSms_url(String sms_url) {
		ConfigProp.sms_url = sms_url;
	}
	
	public static String getVideo_file_path_linux() {
		return video_file_path_linux;
	}
	
	public static void setVideo_file_path_linux(String video_file_path_linux) {
		ConfigProp.video_file_path_linux = video_file_path_linux;
	}
	
	public static String getTemp_file_path() {
		return temp_file_path;
	}

	public static void setTemp_file_path(String temp_file_path) {
		ConfigProp.temp_file_path = temp_file_path;
	}

	public static String getExcel_file_path() {
		return excel_file_path;
	}
	
	public static String getImg_file_path() {
		return img_file_path;
	}
	
	public static String getVideo_file_path() {
		return video_file_path;
	}

	public static String getSms_pwd() {
		return sms_pwd;
	}

	public static String getSms_user() {
		return sms_user;
	}

	public static void setExcel_file_path(String excel_file_path) {
		ConfigProp.excel_file_path = excel_file_path;
	}
	
	public static void setImg_file_path(String img_file_path) {
		ConfigProp.img_file_path = img_file_path;
	}
	
	public static void setideo_file_path(String video_file_path) {
		ConfigProp.video_file_path = video_file_path;
	}

	public static void setSms_pwd(String sms_pwd) {
		ConfigProp.sms_pwd = sms_pwd;
	}

	public static void setSms_user(String sms_user) {
		ConfigProp.sms_user = sms_user;
	}

	public static String getImg_file_path_linux() {
		return img_file_path_linux;
	}

	public static void setImg_file_path_linux(String img_file_path_linux) {
		ConfigProp.img_file_path_linux = img_file_path_linux;
	}

	public static String getImg_file_path_http() {
		return img_file_path_http;
	}

	public static void setImg_file_path_http(String img_file_path_http) {
		ConfigProp.img_file_path_http = img_file_path_http;
	}

	public static String getMedicalservice_url_sms() {
		return medicalservice_url_sms;
	}

	public static void setMedicalservice_url_sms(String medicalservice_url_sms) {
		ConfigProp.medicalservice_url_sms = medicalservice_url_sms;
	}

	public static void setVideo_file_path(String video_file_path) {
		ConfigProp.video_file_path = video_file_path;
	}
	
	/**
	 * @return the sERVICE_IP
	 */
	public static String getSERVICE_IP() {
		return SERVICE_IP;
	}

	/**
	 * @param sERVICE_IP the sERVICE_IP to set
	 */
	public static void setSERVICE_IP(String sERVICE_IP) {
		SERVICE_IP = sERVICE_IP;
	}

	/**
	 * @return the sMS_URL
	 */
	public static String getSMS_URL() {
		return SMS_URL;
	}

	/**
	 * @param sMS_URL the sMS_URL to set
	 */
	public static void setSMS_URL(String sMS_URL) {
		SMS_URL = sMS_URL;
	}

	/**
	 * @return the rEGISTER_URL
	 */
	public static String getREGISTER_URL() {
		return REGISTER_URL;
	}

	/**
	 * @param rEGISTER_URL the rEGISTER_URL to set
	 */
	public static void setREGISTER_URL(String rEGISTER_URL) {
		REGISTER_URL = rEGISTER_URL;
	}

	/**
	 * @return the sERVICE_PORT
	 */
	public static String getSERVICE_PORT() {
		return SERVICE_PORT;
	}

	/**
	 * @param sERVICE_PORT the sERVICE_PORT to set
	 */
	public static void setSERVICE_PORT(String sERVICE_PORT) {
		SERVICE_PORT = sERVICE_PORT;
	}

	/**
	 * @return the mYTEST_URL
	 */
	public static String getMYTEST_URL() {
		return MYTEST_URL;
	}

	/**
	 * @param mYTEST_URL the mYTEST_URL to set
	 */
	public static void setMYTEST_URL(String mYTEST_URL) {
		MYTEST_URL = mYTEST_URL;
	}

	/**
	 * @return the resources_path
	 */
	public static String getResources_path() {
		return resources_path;
	}

	/**
	 * @param resources_path the resources_path to set
	 */
	public static void setResources_path(String resources_path) {
		ConfigProp.resources_path = resources_path;
	}

	/**
	 * @return the env
	 */
	public static String getEnv() {
		return env;
	}

	/**
	 * @param env the env to set
	 */
	public static void setEnv(String env) {
		ConfigProp.env = env;
	}
	
}
