package com.lst.dao;

import com.lst.model.ClientViewCourse;

public interface ClientViewCourseMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClientViewCourse record);

    int insertSelective(ClientViewCourse record);

    ClientViewCourse selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClientViewCourse record);

    int updateByPrimaryKey(ClientViewCourse record);
}