package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstClient;

public interface MstClientMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(MstClient record);

	int insertSelective(MstClient record);

	MstClient selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(MstClient record);

	int updateByPrimaryKey(MstClient record);
	
	int countByMap(Map<String, Object> map);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 查询数据集
	 * @param @param map
	 * @param @param pageBouonds
	 * @param @return    设定文件
	 * @return List<MstClient>    返回类型
	 */
	List<MstClient> queryByList(Map<String, Object> map,PageBounds pageBouonds);
	
	List<MstClient> queryByList2(Map<String, Object> map);
	
	void delList(Map<String, Object> map);
}