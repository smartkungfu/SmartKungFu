package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientAccomplish;

public interface ClientAccomplishMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(ClientAccomplish record);

	int insertSelective(ClientAccomplish record);

	ClientAccomplish selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ClientAccomplish record);

	int updateByPrimaryKey(ClientAccomplish record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 获取数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<ClientAccomplish>    返回类型
	 */
	List<ClientAccomplish> queryByList(Map<String, Object> map , PageBounds pageBounds);

	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delList(Map<String, Object> map);
	
    List<ClientAccomplish> queryByUser(Integer clientId);
}