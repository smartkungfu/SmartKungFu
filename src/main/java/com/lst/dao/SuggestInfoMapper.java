package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.SuggestInfo;

public interface SuggestInfoMapper {
  
    int insert(SuggestInfo record);

    int updateSuggest(SuggestInfo record);
    
    List<SuggestInfo> queryByList(Map<String, Object> map , PageBounds pageBounds);
    
    SuggestInfo selectByKey(Integer id);
}