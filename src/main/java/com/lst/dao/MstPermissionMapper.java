package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstPermission;

public interface MstPermissionMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(MstPermission record);

	int insertSelective(MstPermission record);

	MstPermission selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(MstPermission record);

	int updateByPrimaryKey(MstPermission record);

	List<MstPermission> queryByList(Map<String, Object> map, PageBounds pageBounds);

	/**
	 * 
	 * @Title: queryUserPermissionBy
	 * @Description: 根据map查询用户权限  菜单
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<MstPermission>    返回类型
	 */
	List<MstPermission> queryUserPermissionByMap(Map<String, Object>  map);

	/**
	 * 
	 * @Title: queryUserPermissionBy
	 * @Description: 根据map查询角色权限  菜单
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<MstPermission>    返回类型
	 */
	List<MstPermission> queryRolePermissionByMap(Map<String, Object>  map);

	/**
	 * 
	 * @Title: deleteByMap
	 * @Description: 批量删除用户权限
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void deleteByMap(Map<String, Object> map);

	/**
	 * 
	 * @Title: saveList
	 * @Description: 保存用户权限
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void saveList(List<MstPermission> list);
}