package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientNum;

public interface ClientNumMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(ClientNum record);

	int insertSelective(ClientNum record);

	ClientNum selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ClientNum record);

	int updateByPrimaryKey(ClientNum record);

	List<ClientNum> queryByList(Map<String, Object> map,PageBounds pageBounds);

	/**
	 * 
	 * @Title: queryCountByMap
	 * @Description: 统计用户操作量
	 * @param @param map
	 * @param @return    设定文件
	 * @return Integer    返回类型
	 */
	Long queryCountByMap(Map<String, Object> map);

}