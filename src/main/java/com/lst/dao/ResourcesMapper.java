package com.lst.dao;

import java.util.List;

import com.lst.model.Resources;

public interface ResourcesMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(Resources record);

	int insertSelective(Resources record);

	Resources selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(Resources record);

	int updateByPrimaryKey(Resources record);

	/**
	 * 
	 * @Title: saveList
	 * @Description: 批量插入
	 * @param @param list    设定文件
	 * @return void    返回类型
	 */
	void saveList(List<Resources> list);
}