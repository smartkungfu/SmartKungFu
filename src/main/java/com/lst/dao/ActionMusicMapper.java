package com.lst.dao;


import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ActionMusic;

public interface ActionMusicMapper {

	int insert(ActionMusic record);

	List<ActionMusic> queryByList(Map<String, Object> map , PageBounds pageBounds);
	
	ActionMusic selectByPrimaryKey(Integer id);
	
	int updateByKey(ActionMusic record);
	
	void delList(Map<String, Object> map);
}