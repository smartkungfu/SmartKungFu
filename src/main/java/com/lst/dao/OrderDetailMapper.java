package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.OrderDetail;

public interface OrderDetailMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(OrderDetail record);

	int insertSelective(OrderDetail record);

	OrderDetail selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(OrderDetail record);

	int updateByPrimaryKey(OrderDetail record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description:获取数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<OrderDetail>    返回类型
	 */
	List<OrderDetail> queryByList(Map<String, Object> map ,PageBounds pageBounds);
}