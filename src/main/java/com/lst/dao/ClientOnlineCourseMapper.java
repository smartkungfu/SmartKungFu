package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientOnlineCourse;

public interface ClientOnlineCourseMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClientOnlineCourse record);

    int insertSelective(ClientOnlineCourse record);

    ClientOnlineCourse selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClientOnlineCourse record);

    int updateByPrimaryKey(ClientOnlineCourse record);
    
    List<ClientOnlineCourse> queryByList(Map<String, Object> map, PageBounds pageBouns);
}