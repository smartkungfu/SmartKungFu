package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstRole;

public interface MstRoleMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(MstRole record);

	int insertSelective(MstRole record);

	MstRole selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(MstRole record);

	int updateByPrimaryKey(MstRole record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description:获取数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstRole>    返回类型
	 */
	List<MstRole> queryByList(Map<String, Object> map,PageBounds pageBounds);

	/**
	 * 
	 * @Title: delList
	 * @Description: 批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delList(Map<String, Object> map);
}