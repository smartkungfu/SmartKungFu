package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.OnlineCourse;

public interface OnlineCourseMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OnlineCourse record);

    int insertSelective(OnlineCourse record);

    OnlineCourse selectByPrimaryKey(Integer id);
    
    OnlineCourse selectByPrimaryKey2(Integer id);

    int updateByPrimaryKeySelective(OnlineCourse record);

    int updateByPrimaryKey(OnlineCourse record);

    List<OnlineCourse> queryByList(Map<String, Object> map, PageBounds pageBounds);
    
    void delList(Map<String, Object> map); 
}