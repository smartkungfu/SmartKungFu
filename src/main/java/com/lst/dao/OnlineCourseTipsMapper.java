package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.OnlineCourseTips;

public interface OnlineCourseTipsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OnlineCourseTips record);

    int insertSelective(OnlineCourseTips record);

    OnlineCourseTips selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OnlineCourseTips record);

    int updateByPrimaryKey(OnlineCourseTips record);
    
    void delList(Map<String, Object> map);
    
    List<OnlineCourseTips> queryByList(Map<String, Object> map, PageBounds pageBounds);
}