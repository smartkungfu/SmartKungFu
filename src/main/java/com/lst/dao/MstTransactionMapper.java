package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstTransaction;

public interface MstTransactionMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(MstTransaction record);

	int insertSelective(MstTransaction record);

	MstTransaction selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(MstTransaction record);

	int updateByPrimaryKey(MstTransaction record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 查询数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstTransaction>    返回类型
	 */
	List<MstTransaction> queryByList(Map<String, Object> map,PageBounds pageBounds);
}