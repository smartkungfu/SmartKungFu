package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstDiscuss;

public interface MstDiscussMapper {
    
    List<MstDiscuss> queryByList(Map<String, Object> map, PageBounds pageBounds);
    
    void delList(Map<String, Object> map);
    
    Integer queryCountByMap(Map<String, Object> map);
    
    List<MstDiscuss> queryByList2(Map<String, Object> map, PageBounds pageBounds);
}