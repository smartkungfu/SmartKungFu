package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstMessage;

public interface MstMessageMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MstMessage record);

    int insertSelective(MstMessage record);

    MstMessage selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MstMessage record);

    int updateByPrimaryKey(MstMessage record);
    
    List<MstMessage> queryByList(Map<String, Object> map, PageBounds pageBounds);
}