package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientRoll;

public interface ClientRollMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClientRoll record);

    int insertSelective(ClientRoll record);

    ClientRoll selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClientRoll record);

    int updateByPrimaryKey(ClientRoll record);
    
    List<ClientRoll> queryByList(Map<String, Object> map, PageBounds pageBounds);
}