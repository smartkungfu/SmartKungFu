package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstGrade;

public interface MstGradeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MstGrade record);

    int insertSelective(MstGrade record);

    MstGrade selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MstGrade record);

    int updateByPrimaryKey(MstGrade record);
    
    List<MstGrade> queryByList(Map<String, Object> map, PageBounds pageBounds);
}