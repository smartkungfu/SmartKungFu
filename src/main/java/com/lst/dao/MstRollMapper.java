package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstRoll;

public interface MstRollMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(MstRoll record);

	int insertSelective(MstRoll record);

	MstRoll selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(MstRoll record);

	int updateByPrimaryKey(MstRoll record);

	List<MstRoll> queryByList(Map<String, Object> map,PageBounds pageBounds);

	void delList(Map<String, Object> map);

	/**
	 * 
	 * @Title: updList
	 * @Description: 更新过期状态
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void updList(Map<String, Object> map);
}