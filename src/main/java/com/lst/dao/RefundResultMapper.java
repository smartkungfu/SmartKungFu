package com.lst.dao;

import com.lst.model.RefundResult;

public interface RefundResultMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RefundResult record);

    int insertSelective(RefundResult record);

    RefundResult selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RefundResult record);

    int updateByPrimaryKey(RefundResult record);
}