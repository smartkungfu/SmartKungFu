package com.lst.dao;

import com.lst.model.ClientAnswerPoint;

public interface ClientAnswerPointMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClientAnswerPoint record);

    int insertSelective(ClientAnswerPoint record);

    ClientAnswerPoint selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClientAnswerPoint record);

    int updateByPrimaryKey(ClientAnswerPoint record);
}