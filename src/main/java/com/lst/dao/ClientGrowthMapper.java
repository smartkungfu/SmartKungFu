package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientGrowth;

public interface ClientGrowthMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClientGrowth record);

    int insertSelective(ClientGrowth record);

    ClientGrowth selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClientGrowth record);

    int updateByPrimaryKey(ClientGrowth record);
    
    List<ClientGrowth> queryByList(Map<String, Object> map,PageBounds pageBounds);
}