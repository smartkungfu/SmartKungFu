package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ProjectDetail;

public interface ProjectDetailMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(ProjectDetail record);

	int insertSelective(ProjectDetail record);

	ProjectDetail selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ProjectDetail record);

	int updateByPrimaryKey(ProjectDetail record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 获取结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<ProjectDetail>    返回类型
	 */
	List<ProjectDetail> queryByList(Map<String, Object> map	,PageBounds	pageBounds);
	
	List<ProjectDetail> queryClientProjectList(Map<String, Object> map	,PageBounds	pageBounds);

}