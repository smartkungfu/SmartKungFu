package com.lst.dao;

import com.lst.model.MstSchedule;

public interface MstScheduleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MstSchedule record);

    int insertSelective(MstSchedule record);

    MstSchedule selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MstSchedule record);

    int updateByPrimaryKey(MstSchedule record);
}