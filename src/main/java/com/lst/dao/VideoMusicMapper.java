package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.VideoMusic;

public interface VideoMusicMapper {

	int insert(VideoMusic record);
	
	VideoMusic selectByPrimaryKey(Integer id);
	
	int update(VideoMusic record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 查询结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<VideoNote>    返回类型
	 */
	List<VideoMusic> queryByList(Map<String, Object> map,PageBounds pageBounds);

	/**
	 * 
	 * @Title: delList
	 * @Description:批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delList(Map<String, Object> map);
}