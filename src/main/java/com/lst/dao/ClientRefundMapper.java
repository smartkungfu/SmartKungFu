package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientRefund;

public interface ClientRefundMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(ClientRefund record);

	int insertSelective(ClientRefund record);

	ClientRefund selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ClientRefund record);

	int updateByPrimaryKey(ClientRefund record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 获取结果集
	 * @param @param map
	 * @param @param pageBouns
	 * @param @return    设定文件
	 * @return List<ClientRefund>    返回类型
	 */
	List<ClientRefund> queryByList(Map<String, Object> map,PageBounds pageBouns);
}