package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.Plan;

public interface PlanMapper {
	
	List<Plan> queryByList(Map<String,Object> map,PageBounds pageBounds);
}