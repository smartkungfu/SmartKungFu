package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.Dynamic;

public interface DynamicMapper {
	 
	/**
	 * 
	 * @Title: queryByList
	 * @Description:查询数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<WenwInfo>    返回类型
	 */
	List<Dynamic> queryByList(Map<String, Object> map , PageBounds pageBounds);

	void delList(Map<String, Object> map);
	
	Dynamic selectByPrimaryKey(Integer id);
	
	int countByMap(Map<String, Object> map);
}