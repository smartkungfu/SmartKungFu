package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientProject;

public interface ClientProjectMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(ClientProject record);

	int insertSelective(ClientProject record);

	ClientProject selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ClientProject record);

	int updateByPrimaryKey(ClientProject record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description:查询结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<ClientProject>    返回类型
	 */
	List<ClientProject> queryByList(Map<String, Object> map,PageBounds pageBounds);
}