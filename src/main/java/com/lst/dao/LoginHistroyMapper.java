package com.lst.dao;

import com.lst.model.LoginHistroy;

public interface LoginHistroyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LoginHistroy record);

    int insertSelective(LoginHistroy record);

    LoginHistroy selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LoginHistroy record);

    int updateByPrimaryKey(LoginHistroy record);
}