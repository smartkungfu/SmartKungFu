package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.Question;

public interface QuestionMapper {

	List<Question> queryByList(Map<String, Object> map , PageBounds pageBounds);
	
	void delList(Map<String, Object> map);
	
	Question selectByPrimaryKey(Integer id);
	
	int countByMap(Map<String, Object> map);
}