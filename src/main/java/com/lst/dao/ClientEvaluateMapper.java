package com.lst.dao;

import com.lst.model.ClientEvaluate;

public interface ClientEvaluateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClientEvaluate record);

    int insertSelective(ClientEvaluate record);

    ClientEvaluate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClientEvaluate record);

    int updateByPrimaryKey(ClientEvaluate record);
}