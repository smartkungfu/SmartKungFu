package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.CoursePackage;

public interface CoursePackageMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(CoursePackage record);

	int insertSelective(CoursePackage record);

	CoursePackage selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(CoursePackage record);

	int updateByPrimaryKey(CoursePackage record);

	/**
	 * 
	 * @Title: queryByList
	 * @Description: 查询结果集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<CoursePackage>    返回类型
	 */
	List<CoursePackage> queryByList(Map<String, Object> map,PageBounds pageBounds);

	/**
	 * 
	 * @Title: delListByCourseId
	 * @Description: 通过课程ID批量删除
	 * @param @param map    设定文件
	 * @return void    返回类型
	 */
	void delListByCourseId(Map<String, Object> map);

	/**
	 * 
	 * @Title: saveList
	 * @Description:批量插入
	 * @param @param list    设定文件
	 * @return void    返回类型
	 */
	void saveList(List<CoursePackage> list); 
}