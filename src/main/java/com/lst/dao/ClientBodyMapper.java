package com.lst.dao;

import com.lst.model.ClientBody;

public interface ClientBodyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClientBody record);

    int insertSelective(ClientBody record);

    ClientBody selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClientBody record);

    int updateByPrimaryKey(ClientBody record);
}