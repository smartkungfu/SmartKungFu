package com.lst.dao;


import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ActionVideo;

public interface ActionVideoMapper {

	int insert(ActionVideo record);

	List<ActionVideo> queryByList(Map<String, Object> map , PageBounds pageBounds);
	
	ActionVideo selectByPrimaryKey(Integer id);
	
	int updateByKey(ActionVideo record);
	
	void delList(Map<String, Object> map);
}