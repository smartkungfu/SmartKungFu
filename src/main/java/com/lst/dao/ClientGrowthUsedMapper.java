package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClientGrowthUsed;

public interface ClientGrowthUsedMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClientGrowthUsed record);

    int insertSelective(ClientGrowthUsed record);

    ClientGrowthUsed selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClientGrowthUsed record);

    int updateByPrimaryKey(ClientGrowthUsed record);
    
    List<ClientGrowthUsed> queryByList(Map<String, Object> map,PageBounds pageBounds);
}