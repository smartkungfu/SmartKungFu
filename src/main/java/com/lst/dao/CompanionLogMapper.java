package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.CompanionLog;


public interface CompanionLogMapper {

	int insert(CompanionLog record);
	
	int countBySourceId(Integer sourceId);
	
	CompanionLog queryByMap(Map<String, Object> map);
	/**
	 * 
	 * @Title: queryByList
	 * @Description:查询数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<WenwInfo>    返回类型
	 */
	List<CompanionLog> queryByList(Map<String, Object> map , PageBounds pageBounds);
}