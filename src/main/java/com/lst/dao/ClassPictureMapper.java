package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.ClassPicture;

public interface ClassPictureMapper {
	
	int insert(ClassPicture record);
	
	/**
	 * 
	 * @Title: queryByList
	 * @Description:查询数据集
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<WenwInfo>    返回类型
	 */
	List<ClassPicture> queryByList(Map<String, Object> map , PageBounds pageBounds);
	
	ClassPicture selectByPrimaryKey(Integer id);
	
	int updateByKey(ClassPicture record);
	
	void delList(Map<String, Object> map);
}