package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.MstMenu;

public interface MstMenuMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(MstMenu record);

	int insertSelective(MstMenu record);

	MstMenu selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(MstMenu record);

	int updateByPrimaryKey(MstMenu record);

	/**
	 * 
	 * @Title: queryListForUserRoots
	 * @Description: 查询用户独立权限菜单和用户角色权限菜单 
	 * @param @param map
	 * @param @param pageBounds
	 * @param @return    设定文件
	 * @return List<MstMenu>    返回类型
	 */
	List<MstMenu> queryListForUserRoots(Map<String, Object> map,PageBounds pageBounds);

	/**
	 * 
	 * @Title: queryListByParentId
	 * @Description: 查询上级菜单
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<MstMenu>    返回类型
	 */
	List<MstMenu> queryListByParentId(Map<String, Object> map);
	
	/**
	 * 
	 * @Title: countByParentid
	 * @Description: 查询父节点下有几个菜单 
	 * @param @param parentId
	 * @param @return    设定文件
	 * @return Integer    返回类型
	 */
	Integer countByParentid(Integer parentId);
}