package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.lst.model.Report;

public interface ReportMapper {
	
	Report selectByKey(Integer id);
	
	List<Report> queryByList(Map<String, Object> map , PageBounds pageBounds);
	
	void updateReport(Report report);
}