package com.lst.dao;

import java.util.List;
import java.util.Map;

import com.lst.model.MstButton;

public interface MstButtonMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MstButton record);

    int insertSelective(MstButton record);

    MstButton selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MstButton record);

    int updateByPrimaryKey(MstButton record);
    
    /**
	 * 
	 * @Title: countByMenuid
	 * @Description: 查询菜单下的按钮数量 
	 * @param @param menuId
	 * @param @return    设定文件
	 * @return Integer    返回类型
	 */
	Integer countByMenuid(Integer menuId);

	/**
	 * 
	 * @Title: queryListByMenuId
	 * @Description:通过菜单id查询按钮
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<MstButton>    返回类型
	 */
	List<MstButton> queryListByMenuId(Map<String, Object> map);

	/**
	 * 
	 * @Title: queryListForUserRoots
	 * @Description: 查询用户独立权限按钮和用户角色权限按钮
	 * @param @param map
	 * @param @return    设定文件
	 * @return List<MstButton>    返回类型
	 */
	List<MstButton> queryListForUserRoots(Map<String, Object> map);
}