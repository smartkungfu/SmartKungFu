package com.lst.dao;

import com.lst.model.MstNation;

public interface MstNationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MstNation record);

    int insertSelective(MstNation record);

    MstNation selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MstNation record);

    int updateByPrimaryKey(MstNation record);
}